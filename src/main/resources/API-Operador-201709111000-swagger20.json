{
  "swagger" : "2.0",
  "info" : {
    "description" : "Servicios ofrecidos por el actor operador  IP/REV",
    "version" : "1.0.0",
    "title" : "Operador IP/REV API",
    "contact" : { }
  },
  "host" : "endpoint-operador",
  "schemes" : [ "https" ],
  "consumes" : [ "application/json" ],
  "produces" : [ "application/json" ],
  "security" : [ {
    "OpenID Connect" : [ ]
  } ],
  "paths" : {
    "/v1/listasUsuarios" : {
      "put" : {
        "summary" : "Notificar nueva lista de usuarios.",
        "description" : "Permite al intermediador notificar de la existencia de una nueva lista.",
        "consumes" : [ ],
        "parameters" : [ {
          "name" : "body",
          "in" : "body",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/ListaUsuario"
          }
        } ],
        "responses" : {
          "200" : {
            "description" : "Status 200"
          }
        }
      }
    },
    "/v1/pasos/{codigoPaso}/solicitarPruebas" : {
      "put" : {
        "summary" : "Solicitar prueba del paso de vehículo.",
        "description" : "Esta servicio es consumido por el intermediador para solicitar al operador las pruebas de un paso de vehículo.",
        "parameters" : [ ],
        "responses" : {
          "200" : {
            "description" : "Status 200",
            "schema" : {
              "$ref" : "#/definitions/Ticket"
            }
          }
        }
      },
      "parameters" : [ {
        "name" : "codigoPaso",
        "in" : "path",
        "required" : true,
        "type" : "string"
      } ]
    },
    "/v1/confirmacionesPasos" : {
      "put" : {
        "summary" : "Confirmar procesamiento de paso.",
        "description" : "Este servicio es consumido por el Intermediador para confirmar un procesamiento de un paso de vehículo.",
        "consumes" : [ ],
        "parameters" : [ {
          "name" : "body",
          "in" : "body",
          "required" : true,
          "schema" : {
            "type" : "array",
            "items" : {
              "$ref" : "#/definitions/ConfirmacionProcesamientoPaso"
            }
          }
        } ],
        "responses" : {
          "200" : {
            "description" : "Status 200"
          }
        }
      }
    },
    "/v1/confirmacionesAjustes" : {
      "put" : {
        "summary" : "Confirmación de procesamiento de ajuste(s).",
        "description" : "Este servicio es utilizado por el INT IP/REV para confirmar el procesamiento de transacción de ajuste(s).",
        "consumes" : [ ],
        "parameters" : [ {
          "name" : "body",
          "in" : "body",
          "required" : true,
          "schema" : {
            "type" : "array",
            "items" : {
              "$ref" : "#/definitions/ConfirmacionProcesamientoAjuste"
            }
          }
        } ],
        "responses" : {
          "200" : {
            "description" : "Recepción de la confirmación."
          },
          "404" : {
            "description" : "El código de ajuste no fue encontrado."
          }
        }
      }
    },
    "/v1/saldos" : {
      "put" : {
        "summary" : "Notificar el saldo de un usuario",
        "description" : "Este servicio permite al INT IP/REV actualizar los saldos en los OP IP/REV de los usuarios con modalidad prepago simple o prepago con cargo recurrente. ",
        "consumes" : [ ],
        "parameters" : [ {
          "name" : "body",
          "in" : "body",
          "required" : true,
          "schema" : {
            "type" : "array",
            "items" : {
              "$ref" : "#/definitions/Saldo"
            }
          }
        } ],
        "responses" : {
          "200" : {
            "description" : "Recepción de la confirmación."
          }
        }
      }
    }
  },
  "securityDefinitions" : {
    "OpenID Connect" : {
      "type" : "apiKey",
      "name" : "Authorization",
      "in" : "header"
    }
  },
  "definitions" : {
    "ListaUsuario" : {
      "type" : "object",
      "required" : [ "codigoIntermediador", "codigoLista", "codigoListaAnterior", "fechaEmision", "numeroRegistros", "tipoLista" ],
      "properties" : {
        "codigoIntermediador" : {
          "$ref" : "#/definitions/CodigoIntermediador"
        },
        "codigoLista" : {
          "$ref" : "#/definitions/CodigoListaUsuario"
        },
        "tipoLista" : {
          "type" : "string",
          "description" : "Identifica si la lista es T=total, P=parcial o N=negra.",
          "enum" : [ "T", "P", "N" ]
        },
        "fechaEmision" : {
          "type" : "string",
          "format" : "date-time",
          "description" : "Fecha y hora de la generación de la lista por parte del INT IP/REV."
        },
        "codigoListaAnterior" : {
          "$ref" : "#/definitions/CodigoListaUsuario"
        },
        "numeroRegistros" : {
          "type" : "integer",
          "description" : "Cantidad de registros incluidas en la lista.",
          "minimum" : 1
        }
      },
      "description" : "Representa una lista de usuarios de un INT IP/REV."
    },
    "CodigoListaUsuario" : {
      "type" : "string",
      "description" : "La lista de usuario es un recurso identificado por el patrón: CODIGOINTERMEDIADOR_YYYYMMDD_NNNNN[X]. YYYYMMDD corresponde al día de generación de la lista, NNNNN es un consecutivo para el día, y el caracter X se refiere a T=total, N=listas negras y P=parciales (tipo de lista).\n\nEjemplo: 10012_20170801_00001P",
      "example" : "10012_20170511_00001T"
    },
    "CodigoIntermediador" : {
      "type" : "integer",
      "description" : "Representa el código de intermediador INT IP/REV asignado por el Ministerio de Transporte.",
      "example" : 10019,
      "minimum" : 10000,
      "maximum" : 99999
    },
    "Usuario" : {
      "type" : "object",
      "required" : [ "categoria", "codigoIntermediador", "epc", "estado", "estadoSaldo", "modalidad", "placa", "saldo", "saldoBajo", "tid", "version" ],
      "properties" : {
        "codigoIntermediador" : {
          "$ref" : "#/definitions/CodigoIntermediador"
        },
        "placa" : {
          "type" : "string",
          "description" : "Placa del vehículo.",
          "minLength" : 6,
          "maxLength" : 10,
          "pattern" : "^[a-zA-Z0-9\\-]+$"
        },
        "tid" : {
          "type" : "string",
          "description" : "Código tid del tag. Las longitudes válidas son de 24 y 32 caracteres (96 bits), pero debe ser ampliable a futuro para la próxima generación de 128 bits. Caracteres hexadecimales\nEjemplo:\nUn tag de 24 caracteres hexadecimales:\nE2003412012BC1FFEEE2EC14\n\nUn tag que no tenga las longitudes aquí especificadas, no será válido.",
          "minLength" : 24,
          "maxLength" : 32,
          "pattern" : "^[\\S]+$"
        },
        "epc" : {
          "type" : "string",
          "description" : "Código del EPC asignado al tag según los rangos habilitados.",
          "minLength" : 24,
          "maxLength" : 24,
          "pattern" : "^[0-9]+$"
        },
        "categoria" : {
          "type" : "string",
          "description" : "Código de categoría. Según tabla de categorías:\n\nA\t(Automóvil, camioneta o campero),\nB1\t(Microbús (sin doble ruedas)),\nB2\t(Bus o buseta de 2 ejes),\nC1\t(Camión liviano de 2 ejes),\nC2\t(Camión pesado de 2 ejes),\nC3\t(Camión de 3 ejes),\nC4\t(Camión de 4 ejes),\nC5\t(Camión de 5 ejes),\nC6\t(Camión de 6 ejes o más).",
          "enum" : [ "A", "B1", "B2", "C1", "C2", "C3", "C4", "C5", "C6" ],
          "minLength" : 1,
          "maxLength" : 2
        },
        "estado" : {
          "type" : "integer",
          "description" : "Estado del vehículo:\n1 – Matriculado (ACTIVO),\n2 – Suspendido (INACTIVO),\n3 – Cancelado (el usuario ha sido des-matriculado del sistema).",
          "enum" : [ 1, 2, 3 ]
        },
        "estadoSaldo" : {
          "type" : "integer",
          "description" : "Este campo se utiliza para indicar el estado del saldo de un usuario.\n\n1 – Con saldo. Siempre deja pasar al vehículo,\n2 – Sin saldo. Nunca deja pasar al vehículo,\n3 – Use valor del saldo. Deja pasar al vehículo según el saldo y el valor del peaje,\n4 – Vehículo exento ley 787 ministerio (para uso exclusivo del ministerio de transporte).",
          "enum" : [ 1, 2, 3, 4 ]
        },
        "saldo" : {
          "type" : "number",
          "description" : "Saldo actual para el vehículo."
        },
        "saldoBajo" : {
          "type" : "boolean",
          "description" : "Determina si el usuario tiene o no saldo escaso. true = saldo escaso, false en caso contrario.\n\nEl saldo escaso es una convención entre el INT IP/REV y su usuario y equivale a un umbral de valor económico."
        },
        "numeroCliente" : {
          "type" : "string",
          "description" : "Número de contrato o referencia del usuario en el intermediador para consultas o disputas.",
          "maxLength" : 100,
          "pattern" : "^[\\S]+$"
        },
        "modalidad" : {
          "type" : "string",
          "description" : "Permite definir la modalidad del usuario.",
          "enum" : [ "PrepagoSimple", "PrepagoCargoRecurrente", "PagoInmediato", "Pospago" ]
        },
        "version" : {
          "type" : "string",
          "format" : "date-time",
          "description" : "Versión del registro, siempre debe ser creciente con respecto a la versión anterior de la información del usuario.\n\nEl código de versión debe ser expresado como una fecha de actualización del registro por parte del INT IP/REV.\n\nEn cualquier caso, el INT IP/REV debe garantizar que sea único y creciente."
        }
      },
      "description" : "Representa información de un registro de datos para una lista."
    },
    "NotificacionErrorProcesamientoListas" : {
      "type" : "object",
      "required" : [ "codigoLista", "codigoOperador", "errores" ],
      "properties" : {
        "codigoLista" : {
          "$ref" : "#/definitions/CodigoListaUsuario"
        },
        "codigoOperador" : {
          "$ref" : "#/definitions/CodigoOperador"
        },
        "errores" : {
          "$ref" : "#/definitions/ErroresProcesamientoLista"
        }
      },
      "description" : "Representa la información de errores en el procesamiento de una lista de usuario. Esta notificación es realizada por el operador al intermediador."
    },
    "ErroresProcesamientoLista" : {
      "type" : "object",
      "required" : [ "cantidadAprobados", "cantidadRechazados", "cantidadRegistros", "codigoLista", "errores" ],
      "properties" : {
        "codigoLista" : {
          "$ref" : "#/definitions/CodigoListaUsuario"
        },
        "cantidadRegistros" : {
          "type" : "integer",
          "description" : "Cantidad de registros de la lista.",
          "minimum" : 0
        },
        "cantidadAprobados" : {
          "type" : "integer",
          "description" : "Cantidad de registros aprobados.",
          "minimum" : 0
        },
        "cantidadRechazados" : {
          "type" : "integer",
          "description" : "Cantidad de registros rechazados.",
          "minimum" : 0
        },
        "errores" : {
          "type" : "array",
          "description" : "Lista con detalle de usuarios rechazados.",
          "items" : {
            "$ref" : "#/definitions/ErrorUsuario"
          }
        }
      },
      "description" : "Contiene los totales de los registros procesados y la lista de errores en el procesamiento de una lista de usuario. Cuando el detalle de la lista no se pudo deserializar (json inválido), se genera un ErrorUsuario indicando esta situación con los siguientes datos:\n\nplaca = \"AAA000\"\ntid = \"AAAAAAAAAAAAAAAAAAAAAAAA\""
    },
    "ErrorUsuario" : {
      "type" : "object",
      "required" : [ "codigoRespuesta", "placa", "tid", "version" ],
      "properties" : {
        "placa" : {
          "type" : "string",
          "description" : "Identifica la placa del vehículo.",
          "minLength" : 6,
          "maxLength" : 10,
          "pattern" : "^[a-zA-Z0-9\\-]+$"
        },
        "tid" : {
          "type" : "string",
          "description" : "Código tid del tag.",
          "minLength" : 24,
          "maxLength" : 32,
          "pattern" : "^[\\S]+$"
        },
        "version" : {
          "type" : "string",
          "format" : "date-time",
          "description" : "Versión del registro reportado en el mensaje original."
        },
        "codigoRespuesta" : {
          "type" : "integer",
          "description" : "1001 – Valor inválido en atributos\n1002 – Atributo faltante\n1003 – Registro multi INT-IP/REV.",
          "enum" : [ 1001, 1002, 1003 ]
        },
        "descripcion" : {
          "type" : "string",
          "description" : "Descripción de fallo.",
          "maxLength" : 200
        }
      },
      "description" : "Representa la información de error al procesar un usuario de una lista de usuarios. Este error es específico a solamente un registro."
    },
    "CodigoOperador" : {
      "type" : "integer",
      "description" : "Representa el código de operador OP IP/REV asignado por el Ministerio de Transporte.",
      "example" : 10009,
      "minimum" : 10000,
      "maximum" : 99999
    },
    "Paso" : {
      "type" : "object",
      "required" : [ "cantidadDobleRuedas", "cantidadEjes", "carril", "categoriaCobrada", "categoriaDAC", "codigoCliente", "codigoListaUsuario", "codigoOperador", "codigoPaso", "epc", "estacion", "existeDescrepancia", "existeDiscrepanciaPlaca", "fechaActualizacionUsuario", "fechaHora", "fechaRecaudo", "placa", "sentido", "tid", "tipoLectura", "valor" ],
      "properties" : {
        "codigoPaso" : {
          "$ref" : "#/definitions/CodigoPaso"
        },
        "codigoOperador" : {
          "$ref" : "#/definitions/CodigoOperador"
        },
        "fechaHora" : {
          "type" : "string",
          "format" : "date-time",
          "description" : "Fecha y hora de la transacción."
        },
        "fechaRecaudo" : {
          "type" : "string",
          "format" : "date-time",
          "description" : "Fecha de recaudo de acuerdo al operador."
        },
        "codigoCliente" : {
          "type" : "string",
          "description" : "Identificador del cliente. Se sugiere que no empiece por ceros.",
          "minLength" : 1,
          "maxLength" : 100,
          "pattern" : "^[\\S]+$"
        },
        "placa" : {
          "type" : "string",
          "description" : "Placa del vehículo.",
          "minLength" : 6,
          "maxLength" : 10,
          "pattern" : "^[a-zA-Z0-9\\-]+$"
        },
        "tid" : {
          "type" : "string",
          "description" : "Código tid del tag.",
          "minLength" : 24,
          "maxLength" : 32,
          "pattern" : "^[\\S]+$"
        },
        "epc" : {
          "type" : "string",
          "description" : "Código del EPC asignado al tag según los rangos habilitados.",
          "minLength" : 24,
          "maxLength" : 24,
          "pattern" : "^[0-9]+$"
        },        
        "estacion" : {
          "type" : "string",
          "description" : "Código de la estación de peaje",
          "minLength" : 1,
          "maxLength" : 3,
          "pattern" : "^[\\S]+$"
        },
        "carril" : {
          "type" : "string",
          "description" : "Código de carril de tránsito.",
          "minLength" : 1,
          "maxLength" : 10,
          "pattern" : "^[\\S]+$"
        },
        "valor" : {
          "type" : "number",
          "description" : "Tarifa aplicada a la transacción. Deben ser mayores o igual a 0 (cero para el caso de vehículos exentos).",
          "minimum" : 0
        },
        "cantidadEjes" : {
          "type" : "integer",
          "description" : "Ejes totales detectados.",
          "minimum" : 1
        },
        "cantidadDobleRuedas" : {
          "type" : "integer",
          "description" : "Ejes doble ruedas detectados.",
          "minimum" : 0
        },
        "categoriaDAC" : {
          "type" : "integer",
          "description" : "Categoría DAC según Ministerio de Transporte."
        },
        "categoriaCobrada" : {
          "type" : "string",
          "description" : "Código de la categoría según la tabla de categorías de la estación de peaje.",
          "minLength" : 1,
          "maxLength" : 10
        },
        "tipoLectura" : {
          "type" : "integer",
          "description" : "Forma de registro vehículo: 1 – Antena,\n2 – Manual por placa,\n3 – Manual por tag,\n4 – Manual por auditoría,\n5 – Placa OCR.",
          "enum" : [ 1, 2, 3, 4, 5 ],
          "minimum" : 1,
          "maximum" : 5
        },
        "sentido" : {
          "type" : "string",
          "description" : "Código del sentido que transita el vehículo. Su estructura debe ser:\n“3 caracteres origen”-“3 caracteres destino” en mayúscula para mantener el estándar.",
          "minLength" : 7,
          "maxLength" : 7,
          "pattern" : "[A-Z]{3}\\-[A-Z]{3}",
          "example" : "BOG-MED"
        },
        "placaOCR" : {
          "type" : "string",
          "description" : "Placa identificada por el sensor OCR",
          "minLength" : 6,
          "maxLength" : 10,
          "pattern" : "^[a-zA-Z0-9\\-]+$"
        },
        "existeDescrepancia" : {
          "type" : "boolean",
          "description" : "Indica si existe diferencia entre la categoría registrada y la detectada \ntrue – Si hay discrepancia,\nfalse – No hay discrepancia."
        },
        "fechaActualizacionUsuario" : {
          "type" : "string",
          "format" : "date-time",
          "description" : "Fecha en el que se actualizó el saldo. Incluir milisegundos."
        },
        "codigoListaUsuario" : {
          "$ref" : "#/definitions/CodigoListaUsuario"
        },
        "existeDiscrepanciaPlaca" : {
          "type" : "boolean",
          "description" : "Indica si existe diferencia entre la placa registrada y la detectada.\ntrue – Si hay discrepancia\nfalse – No hay discrepancia"
        }
      },
      "description" : "Representa la información de un paso de vehículo. El paso es generado por un Operador OP IP/REV."
    },
    "CodigoPaso" : {
      "type" : "string",
      "description" : "Representa el código único de transacción para el paso de un vehículo. El código está conformado por OPERADOR_CODIGOUNICOTRANSACCION. Aquí el operador representa el código del operador asignado por el Ministerio de Transporte y el código único de transacción generado por el operador.",
      "example" : "10034_555456564564646525548563258"
    },
    "ConfirmacionProcesamientoPaso" : {
      "type" : "object",
      "required" : [ "codigoIntermediador", "codigoPaso", "codigoRespuesta", "tokenConfirmacion" ],
      "properties" : {
        "codigoPaso" : {
          "$ref" : "#/definitions/CodigoPaso"
        },
        "codigoIntermediador" : {
          "$ref" : "#/definitions/CodigoIntermediador"
        },
        "tokenConfirmacion" : {
          "$ref" : "#/definitions/UID"
        },
        "codigoRespuesta" : {
          "type" : "integer",
          "description" : "1 – Aceptada,\n1001 – Valor inválido, en atributos,\n1002 – Atributo faltante,\n2001 – Vehículo no encontrado.",
          "enum" : [ 1, 1001, 1002, 2001 ]
        },
        "descripcion" : {
          "type" : "string",
          "description" : "Descripción de fallos.",
          "maxLength" : 200
        }
      },
      "description" : "Representa la información de una confirmación por parte del Intermediador al Operador del procesamiento de un paso de vehículo. Esta confirmación de procesamiento de un paso puede reportar el resultado como exitoso o fallido."
    },
    "Ajuste" : {
      "type" : "object",
      "required" : [ "codigoAjuste", "codigoOperador", "codigoPasoReferencia", "fechaHora", "fechaRecaudo", "motivoAjuste", "tipoOperacion", "valor", "valorAjuste" ],
      "properties" : {
        "codigoAjuste" : {
          "$ref" : "#/definitions/CodigoAjuste"
        },
        "codigoOperador" : {
          "$ref" : "#/definitions/CodigoOperador"
        },
        "fechaHora" : {
          "type" : "string",
          "format" : "date-time",
          "description" : "Fecha y hora del ajuste."
        },
        "fechaRecaudo" : {
          "type" : "string",
          "format" : "date",
          "description" : "Fecha de recaudo de según el OP IP/REV."
        },
        "codigoPasoReferencia" : {
          "$ref" : "#/definitions/CodigoPaso"
        },
        "motivoAjuste" : {
          "type" : "integer",
          "description" : "Razón por la que se realiza el ajuste:\n\n1 – Cambio de categoría,\n2 – Cancelación,\n3 - Cambio de Tarifa.",
          "enum" : [ 1, 2, 3 ]
        },
        "valorAjuste" : {
          "type" : "number",
          "description" : "Valor absoluto del ajuste.",
          "minimum" : 0
        },
        "valor" : {
          "type" : "number",
          "description" : "Nuevo valor de la transacción tras haber sido ajustada. Debe ser mayor o igual a 0 (cero para el caso de vehículos exentos).",
          "minimum" : 0
        },
        "categoria" : {
          "type" : "string",
          "description" : "En caso de cambio de categoría, se debe enviar la nueva categoría."
        },
        "tipoOperacion" : {
          "type" : "integer",
          "description" : "Indica si el ajuste realizado corresponde a un débito o un crédito.\n5 - Crédito,\n6 - Débito.",
          "enum" : [ 5, 6 ]
        }
      },
      "description" : "Representa la información de un ajuste reportado por un Operador OP IP/REV."
    },
    "CodigoAjuste" : {
      "type" : "string",
      "description" : "Representa el código único de transacción para el ajuste. El código está conformado por OPERADOR_CODIGOUNICOTRANSACCION. Aquí el operador representa el código del operador asignado por el Ministerio de Transporte y el código único de transacción generado por el operador.",
      "example" : "10034_555456564564646525548563258"
    },
    "ConfirmacionProcesamientoAjuste" : {
      "type" : "object",
      "required" : [ "codigoAjuste", "codigoIntermediador", "codigoRespuesta", "tokenConfirmacion" ],
      "properties" : {
        "codigoAjuste" : {
          "$ref" : "#/definitions/CodigoAjuste"
        },
        "codigoIntermediador" : {
          "$ref" : "#/definitions/CodigoIntermediador"
        },
        "tokenConfirmacion" : {
          "$ref" : "#/definitions/UID"
        },
        "codigoRespuesta" : {
          "type" : "integer",
          "description" : "1 – Aceptada,\n1001 – Valor inválido, en atributos,\n1002 – Atributo faltante,\n2001 – Vehículo no encontrado.",
          "enum" : [ 1, 1001, 1002, 2001 ]
        },
        "descripcion" : {
          "type" : "string",
          "description" : "Descripción de fallos.",
          "maxLength" : 200
        }
      },
      "description" : "Representa la información de una confirmación por parte del Intermediador al Operador del procesamiento de un ajuste. Esta confirmación de procesamiento de un ajuste puede reportar el resultado como exitoso o fallido."
    },
    "NegacionPaso" : {
      "type" : "object",
      "required" : [ "carril", "codigoLista", "codigoNegacion", "codigoOperador", "diaContable", "epc", "estacion", "fechaHora", "placa", "razon", "sentido", "tid", "tipoLectura", "version" ],
      "properties" : {
        "codigoNegacion" : {
          "$ref" : "#/definitions/CodigoNegacion"
        },
        "codigoOperador" : {
          "$ref" : "#/definitions/CodigoOperador"
        },
        "fechaHora" : {
          "type" : "string",
          "format" : "date-time",
          "description" : "Fecha y hora de la transacción."
        },
        "diaContable" : {
          "type" : "string",
          "format" : "date",
          "description" : "Fecha del día contable de la transacción en formato ISO 8601."
        },
        "placa" : {
          "type" : "string",
          "description" : "Placa del vehículo.",
          "minLength" : 6,
          "maxLength" : 10,
          "pattern" : "^[a-zA-Z0-9\\-]+$"
        },
        "tid" : {
          "type" : "string",
          "description" : "Código tid del tag.",
          "minLength" : 24,
          "maxLength" : 32,
          "pattern" : "^[\\S]+$"
        },
        "epc" : {
          "type" : "string",
          "description" : "Código del EPC asignado al tag según los rangos habilitados.",
          "minLength" : 24,
          "maxLength" : 24,
          "pattern" : "^[0-9]+$"
        },        
        "estacion" : {
          "type" : "string",
          "description" : "Código de la estación de peaje.",
          "maxLength" : 3,
          "pattern" : "^[\\S]+$"
        },
        "carril" : {
          "type" : "string",
          "description" : "Código de carril del tránsito.",
          "maxLength" : 10,
          "pattern" : "^[\\S]+$"
        },
        "tipoLectura" : {
          "type" : "integer",
          "description" : "Forma de registro del vehículo:\n1 – Antena,\n2 – Manual por placa,\n3 – Manual por tag,\n4 – Manual por auditoría,\n5 – Placa OCR.",
          "enum" : [ 1, 2, 3, 4, 5 ]
        },
        "sentido" : {
          "type" : "string",
          "description" : "Código del sentido que transita el vehículo. Su estructura debe ser:\n“3 caracteres origen”-“3 caracteres destino” en mayúscula para mantener el estándar.",
          "minLength" : 7,
          "maxLength" : 7,
          "pattern" : "[A-Z]{3}\\-[A-Z]{3}",
          "example" : "BOG-MED"
        },
        "version" : {
          "type" : "string",
          "format" : "date-time",
          "description" : "Código de la versión del registro del usuario con la que fue autorizada la transacción."
        },
        "codigoLista" : {
          "$ref" : "#/definitions/CodigoListaUsuario"
        },
        "razon" : {
          "type" : "string",
          "description" : "Razón por la que el paso no es autorizado.",
          "maxLength" : 1000
        }
      },
      "description" : "Representa la información de una negación de autorización de paso de vehículo reportado por un Operador OP IP/REV."
    },
    "CodigoNegacion" : {
      "type" : "string",
      "description" : "Representa un código de transacción único para una negación de paso. El código está conformado por OPERADOR_CODIGOUNICOTRANSACCION. Aquí el operador representa el código del operador asignado por el Ministerio de Transporte y el código único de transacción generado por el operador.",
      "example" : "10034_555456564564646525548563258"
    },
    "Ticket" : {
      "type" : "object",
      "required" : [ "codigo", "fecha", "referencia" ],
      "properties" : {
        "codigo" : {
          "type" : "string",
          "description" : "El código único del Ticket. Está conformado por OPERADOR/INTERMEDIADOR_CODIGOTICKET. Aquí el operador representa el código del operador/intermediario asignado por el Ministerio de Transporte y el código único de ticket relacionado con la transacción.",
          "minLength" : 10,
          "maxLength" : 50,
          "example" : "10012_552223"
        },
        "fecha" : {
          "type" : "string",
          "format" : "date-time",
          "description" : "Fecha y hora de generación del ticket."
        },
        "referencia" : {
          "type" : "string",
          "description" : "Define la referencia a la solicitud. Para el caso de solicitudes de pruebas de paso éste poseerá el codigoPaso.",
          "maxLength" : 100
        }
      },
      "description" : "Define un identificador que servirá de mecanismo de seguimiento para una solicitud."
    },
    "PruebasPaso" : {
      "type" : "object",
      "required" : [ "codigoPaso", "codigoRespuesta", "pruebas", "ticket" ],
      "properties" : {
        "ticket" : {
          "$ref" : "#/definitions/Ticket"
        },
        "codigoPaso" : {
          "$ref" : "#/definitions/CodigoPaso"
        },
        "codigoRespuesta" : {
          "type" : "integer",
          "description" : "1 – Ok,\n2001 – Transacción no encontrada.",
          "enum" : [ 1, 2001 ]
        },
        "pruebas" : {
          "type" : "array",
          "description" : "Lista de fotografias. Cada elemento del arreglo corresponde a un string como representación en Base64 del contenido del archivo.",
          "items" : {
            "type" : "string",
            "format" : "binary"
          }
        }
      },
      "description" : "Representa las pruebas de paso de un vehículo."
    },
    "Saldo" : {
      "type" : "object",
      "required" : [ "codigoIntermediador", "epc", "placa", "saldo", "saldoBajo", "tid", "version" ],
      "properties" : {
        "codigoIntermediador" : {
          "$ref" : "#/definitions/CodigoIntermediador"
        },
        "placa" : {
          "type" : "string",
          "description" : "Placa del vehículo.",
          "minLength" : 6,
          "maxLength" : 10,
          "pattern" : "^[a-zA-Z0-9\\-]+$"
        },
        "tid" : {
          "type" : "string",
          "description" : "Código tid del tag. Las longitudes válidas son de 24 y 32 caracteres (96 bits), pero debe ser ampliable a futuro para la próxima generación de 128 bits. Caracteres hexadecimales\nEjemplo:\nUn tag de 24 caracteres hexadecimales:\nE2003412012BC1FFEEE2EC14\n\nUn tag que no tenga las longitudes aquí especificadas, no será válido.",
          "minLength" : 24,
          "maxLength" : 32,
          "pattern" : "^[\\S]+$"
        },
        "epc" : {
          "type" : "string",
          "description" : "Código del EPC asignado al tag según los rangos habilitados",
          "minLength" : 24,
          "maxLength" : 24,
          "pattern" : "^[0-9]+$"
        },
        "saldo" : {
          "type" : "number",
          "description" : "Saldo asociado a la placa."
        },
        "saldoBajo" : {
          "type" : "boolean",
          "description" : "Determina si el usuario tiene o no saldo escaso. true = saldo escaso, false en caso contrario.\n\nEl saldo escaso es una convención entre el INT IP/REV y su usuario y equivale a un umbral de valor económico."
        },
        "version" : {
          "type" : "string",
          "format" : "date-time",
          "description" : "Versión del registro, siempre debe ser creciente con respecto a la versión anterior de la información del usuario.\n\nEl código de versión debe ser expresado como una fecha de actualización del registro por parte del INT IP/REV.\n\nEn cualquier caso, el INT IP/REV debe garantizar que sea único y creciente."
        }
      },
      "description" : "Representa la información mínima de un saldo de un usuario. "
    },
    "UID" : {
      "type" : "string",
      "description" : "Código único que representa una confirmación de transacción sobre un elemento específico. El código está conformado por {OPERADOR/INTERMEDIADOR}CODIGOUNICO. Aqui el operador/intermediador representa el código asignado por el Ministerio de Transporte y el código único de identificación generado.",
      "example" : "10034_555456564564646525548563258"
    },
    "ErrorResponse" : {
      "type" : "object",
      "required" : [ "codigo" ],
      "properties" : {
        "codigo" : {
          "type" : "integer",
          "description" : "Codigo error HTTP"
        },
        "error" : {
          "type" : "string",
          "description" : "De no ser un código estándar de la especificación, debe utilizarse para complementar el error."
        }
      }
    }
  }
}