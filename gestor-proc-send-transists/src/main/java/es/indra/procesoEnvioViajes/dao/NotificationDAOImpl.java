package es.indra.procesoEnvioViajes.dao;

import java.math.BigDecimal;
import java.time.temporal.ChronoUnit;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.indra.procesoEnvioViajes.models.SigtDscNotifType;
import es.indra.procesoEnvioViajes.models.SigtOpToIntNotif;

@Repository
public class NotificationDAOImpl implements NotificationDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public NotificationDAOImpl () {
		
	}

	@Transactional
	public Long save(SigtOpToIntNotif notification) {
		Session currentSession = sessionFactory.getCurrentSession();
				String sql = "INSERT INTO sigt_op_to_int_notif (NOTIF_TYPE, COD_AGENCY, NUM_RECORDS, CREATION_DATE, MODIFICATION_USER, APPLICATION, MODIFICATION_DATE, MODIFICATION, OPTIMIST_LOCK) VALUES "
						+ "(:notifType, :codAgency, :numRecord, :creationDate, :modificationUser, null, null, :modification, :optimistLock); SELECT SCOPE_IDENTITY() AS IDENTIFIER";
				
				NativeQuery<?> query = currentSession.createNativeQuery(sql);
				query.setParameter("notifType", notification.getNotifType().getId());
				query.setParameter("codAgency", notification.getCodAgency().getId());
				query.setParameter("numRecord", notification.getNumRecords());
				query.setParameter("creationDate", notification.getCreationDate().toInstant().truncatedTo(ChronoUnit.MILLIS));
				query.setParameter("modificationUser", notification.getModificationUser());
				query.setParameter("modification", notification.getModification().intValue());
				query.setParameter("optimistLock", notification.getOptimistLock().intValue());
				
				Long id = ((BigDecimal) query.getSingleResult()).longValue();
				notification.setIdentifier(id);
		return id;
	}
	
	public SigtDscNotifType findNotifTypeById(Integer id) {
		Session currentSession = sessionFactory.getCurrentSession();
		return currentSession.get(SigtDscNotifType.class, id);
	}
}
