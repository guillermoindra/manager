package es.indra.procesoEnvioViajes.dao;

import java.util.List;

import es.indra.procesoEnvioViajes.models.SigtOpTrxPend;

public interface TransitDAO {
	
	/**
	 * Recupera todos los transits relacionados con una agencia
	 * y cuyo status sea el indicado
	 */
	public List<SigtOpTrxPend> findByCodAgencyAndStatus (Integer codAgency, Integer status);
	
	/**
	 * Recupera todos los transits cuyo status sea el indicado
	 */
	public List<SigtOpTrxPend> findByStatus (Integer status);
	
	/**
	 * This method updates all transits from a list to specified values. In addition, it increments the numSend value of each transit.
	 * This operation is done with a single SQL query
	 * 
	 * @param transits The list of transit of which take ID
	 * @param status The status to update transits
	 * @param idNotif The idNotif to update transits
	 */
	public void batchUpdateStatusAndIdNotifAndNumSend (List<SigtOpTrxPend> transits, Integer status, Long idNotif);
	
	
	/**
	 * This method updates all transits from a list to specified values. In addition, it increments the numSend value of each transit.
	 * This operation is done with a single SQL query
	 * 
	 * @param transits The list of transit of which take ID
	 * @param status The status to update transits
	 * @param idNotifSigt The idNotifSigt to update transits
	 */
	public void batchUpdateStatusAndIdNotifSigtAndNumSend (List<SigtOpTrxPend> transits, Integer status, Long idNotifSigt);
	
	/**
	 * Guarda transaccionalmente todos los transits de la lista
	 */
	public void saveAll (List<SigtOpTrxPend> transits);
	
	/**
	 * Guarda o actualiza un viaje
	 */
	public void save (SigtOpTrxPend transit);
}
