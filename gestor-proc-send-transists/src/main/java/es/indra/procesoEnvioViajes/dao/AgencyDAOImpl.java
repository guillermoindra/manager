package es.indra.procesoEnvioViajes.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.indra.procesoEnvioViajes.models.SigtDscCodAgency;

@Repository
public class AgencyDAOImpl implements AgencyDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	public AgencyDAOImpl () {
		
	}
	
	@Transactional
	public SigtDscCodAgency findById(Integer id) {
		Session currentSession = sessionFactory.getCurrentSession();
		return currentSession.get(SigtDscCodAgency.class, id);
	}

	@Transactional
	public List<SigtDscCodAgency> findAll() {
		Session currentSession = sessionFactory.getCurrentSession();
		return currentSession.createQuery("FROM Agency "
				+ "ORDER BY IDENTIFIER ASC", SigtDscCodAgency.class).getResultList();
	}
	
	public List<SigtDscCodAgency> findAllExceptOne(Integer id) {
		Session currentSession = sessionFactory.getCurrentSession();
		String sql = "SELECT * "				
				+ "FROM SIGT_DSC_COD_AGENCY "
				+ "WHERE ID != :id"; // TODO: Definir limits y nombre de tabla en archivo properties externo
		
		NativeQuery<SigtDscCodAgency> query = currentSession.createNativeQuery(sql, SigtDscCodAgency.class);
		
		query.setParameter("id", id);
		
		return query.getResultList();
	}
	
}
