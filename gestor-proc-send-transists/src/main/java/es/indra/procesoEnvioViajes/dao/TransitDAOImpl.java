package es.indra.procesoEnvioViajes.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.indra.procesoEnvioViajes.models.SigtOpTrxPend;

@Repository
public class TransitDAOImpl implements TransitDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Value("${maxTransitsToSendPerIteration}")
	private int maxTransits;
	
	public TransitDAOImpl () {
		
	}
	
	@Transactional
	public List<SigtOpTrxPend> findByCodAgencyAndStatus(Integer codAgency, Integer status) {
		Session currentSession = sessionFactory.getCurrentSession();
		
		String sql = "SELECT TOP (:maxTransits) * "				
				+ "FROM sigt_op_trx_pend "
				+ "WHERE STATUS=:status AND COD_AGENCY=:codAgency"; // TODO: Definir limits y nombre de tabla en archivo properties externo
				
		NativeQuery<SigtOpTrxPend> query = currentSession.createNativeQuery(sql, SigtOpTrxPend.class);
		query.setParameter("maxTransits", maxTransits);
		query.setParameter("status", status);
		query.setParameter("codAgency", codAgency);
		
		List<SigtOpTrxPend> transits = query.getResultList();
		
		return transits;
	}

	@Transactional
	public List<SigtOpTrxPend> findByStatus(Integer status) {
		Session currentSession = sessionFactory.getCurrentSession();
		
		String sql = "SELECT TOP (:maxTransits) * "
				+ "FROM sigt_op_trx_pend "
				+ "WHERE STATUS=:status"; // TODO: Definir limits y nombre de tabla en archivo properties externo
				
		NativeQuery<SigtOpTrxPend> query = currentSession.createNativeQuery(sql, SigtOpTrxPend.class);
		query.setParameter("maxTransits", maxTransits);
		query.setParameter("status", status);
		
		List<SigtOpTrxPend> transits = query.getResultList();
		
		return transits;
	}
	
	@Transactional
	public void batchUpdateNumSend(List<SigtOpTrxPend> transits) {
		Session currentSession = sessionFactory.getCurrentSession();
		
		String sql = "UPDATE sigt_op_trx_pend "
				+ "SET NUM_SEND=NUM_SEND+1 "
				+ "WHERE IDENTIFIER IN ("; 
		
		// Lista de identificadores comma-sep
		StringBuilder sb = new StringBuilder(sql);
		boolean first = true;
		for (SigtOpTrxPend t : transits) {
			if (first)
				first = false;
			else
				sb.append(",");
			sb.append(t.getIdentifier());
		}
		sb.append(")");
		
		NativeQuery<?> query = currentSession.createNativeQuery(sb.toString());
		
		query.executeUpdate();
	}
	
	@Transactional
	public void batchUpdateStatusAndIdNotifAndNumSend(List<SigtOpTrxPend> transits, Integer status, Long idNotif) {
		Session currentSession = sessionFactory.getCurrentSession();
		
		String sql = "UPDATE sigt_op_trx_pend "
				+ "SET STATUS=:status, "
				+ "ID_NOTIF=:idNotif, NUM_SEND=NUM_SEND+1 "
				+ "WHERE IDENTIFIER IN ";
				
		// Lista de identificadores comma-sep
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		boolean first = true;
		for (SigtOpTrxPend t : transits) {
			if (first)
				first = false;
			else
				sb.append(",");
			sb.append(t.getIdentifier());
		}
		sb.append(")");
		
		sql = sql + sb.toString();
		
		NativeQuery<?> query = currentSession.createNativeQuery(sql);
		query.setParameter("status", status);
		query.setParameter("idNotif", idNotif);
		
		query.executeUpdate();
	}
	
	@Transactional
	public void batchUpdateStatusAndIdNotifSigtAndNumSend(List<SigtOpTrxPend> transits, Integer status, Long idNotifSigt) {
		Session currentSession = sessionFactory.getCurrentSession();
		
		String sql = "UPDATE sigt_op_trx_pend "
				+ "SET STATUS=:status, "
				+ "ID_NOTIF_SIGT=:idNotifSigt, NUM_SEND=NUM_SEND+1 "
				+ "WHERE IDENTIFIER IN ";
				
		// Lista de identificadores comma-sep
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		boolean first = true;
		for (SigtOpTrxPend t : transits) {
			if (first)
				first = false;
			else
				sb.append(",");
			sb.append(t.getIdentifier());
		}
		sb.append(")");
		
		sql = sql + sb.toString();
		
		NativeQuery<?> query = currentSession.createNativeQuery(sql);
		query.setParameter("status", status);
		query.setParameter("idNotifSigt", idNotifSigt);
		
		query.executeUpdate();
	}
	
	@Transactional
	public void saveAll(List<SigtOpTrxPend> transits) {
		Session currentSession = sessionFactory.getCurrentSession();
		for (SigtOpTrxPend t : transits) {
			currentSession.saveOrUpdate(t);
		}
	}

	@Transactional
	public void save(SigtOpTrxPend transits) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.saveOrUpdate(transits);
	}

}
