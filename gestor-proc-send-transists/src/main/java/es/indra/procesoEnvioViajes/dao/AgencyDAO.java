package es.indra.procesoEnvioViajes.dao;

import java.util.List;

import es.indra.procesoEnvioViajes.models.SigtDscCodAgency;

public interface AgencyDAO {
	
	/**
	 * Method that search Agency in DB 
	 * @param id
	 * @return the agency which match provided id or null if not exists
	 */
	public SigtDscCodAgency findById(Integer id);
	
	/**
	 * Method that takes all data from Agencies table
	 * @return a list with all Agencies, empty list if there are no agencies
	 */
	public List<SigtDscCodAgency> findAll();
	
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public List<SigtDscCodAgency> findAllExceptOne(Integer id);
	
}
