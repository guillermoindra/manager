package es.indra.procesoEnvioViajes.services;

import java.util.List;

import es.indra.procesoEnvioViajes.models.SigtDscCodAgency;
import es.indra.procesoEnvioViajes.models.SigtDscNotifType;
import es.indra.procesoEnvioViajes.models.SigtOpToIntNotif;
import es.indra.procesoEnvioViajes.models.SigtOpTrxPend;

public interface MainService {
	
	public List<SigtDscCodAgency> getAllAgencies ();
	
	public SigtDscCodAgency getAgency (Integer id);
	
	public List<SigtDscCodAgency> getAllAgenciesExceptOne (Integer id);
	
	/**
	 *  Busca viajes por codAgency y status
	 * 
	 * @param codAgency
	 * @param status 
	 * @return a list of transits which match those parameters
	 */
	public List<SigtOpTrxPend> getTransitsByCodAgencyAndStatus (Integer codAgency, Integer status);

	/**
	 *  Busca viajes por status
	 * 
	 * @param status 
	 * @return a list of transits which match that parameter
	 */
	public List<SigtOpTrxPend> getTransitsByStatus (Integer status);
	
	/**
	 *  Crea una notificaci�n
	 * 
	 * @param notification 
	 * @return el identificador de la notificaci�n insertada (autogenerado por la BD)
	 */
	public Long createNotification(SigtOpToIntNotif notification);
	
	public SigtDscNotifType getNotifTypeById(Integer id);
	
	/**
	 * Ejecuta una �nica consulta SQL que actualiza el status e idNotif e incrementa numSend
	 * @param lista de viajes de los cuales se coger� �nicamente el ID
	 * @param status
	 * @param idNotif
	 */
	public void batchUpdateTransitsStatusAndIdNotifAndNumSend(List<SigtOpTrxPend> transits, Integer status, Long idNotif);
	
	/**
	 * Ejecuta una �nica consulta SQL que actualiza el status e idNotifSigt e incrementa numSend
	 * @param lista de viajes de los cuales se coger� �nicamente el ID
	 * @param status
	 * @param idNotifSigt
	 */
	public void batchUpdateTransitsStatusAndIdNotifSigtAndNumSend(List<SigtOpTrxPend> transits, Integer status, Long idNotifSigt);
	
	
	public void batchUpdateTransitsNumSend(List<SigtOpTrxPend> transits);
	
	
}
