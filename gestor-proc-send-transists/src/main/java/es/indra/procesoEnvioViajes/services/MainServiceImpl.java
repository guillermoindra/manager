package es.indra.procesoEnvioViajes.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.indra.procesoEnvioViajes.dao.AgencyDAOImpl;
import es.indra.procesoEnvioViajes.dao.NotificationDAOImpl;
import es.indra.procesoEnvioViajes.dao.TransitDAOImpl;
import es.indra.procesoEnvioViajes.models.SigtDscCodAgency;
import es.indra.procesoEnvioViajes.models.SigtDscNotifType;
import es.indra.procesoEnvioViajes.models.SigtOpToIntNotif;
import es.indra.procesoEnvioViajes.models.SigtOpTrxPend;

@Service
public class MainServiceImpl implements MainService {
	
	//FIXME Las variable deben ser las interfaz, no el Objeto que lo implementa.
	@Autowired
	TransitDAOImpl transitDAO;
	
	@Autowired
	AgencyDAOImpl agencyDAO;
	
	@Autowired	
	NotificationDAOImpl notificationDAO;
	
	public MainServiceImpl () {
		
	}
	
	public List<SigtDscCodAgency> getAllAgencies () {
		return agencyDAO.findAll();
	}
	
	
	public SigtDscCodAgency getAgency (Integer id) {
		return agencyDAO.findById(id);
	}
	
	public List<SigtDscCodAgency> getAllAgenciesExceptOne (Integer id) {
		return agencyDAO.findAllExceptOne(id);
	}
	
	public List<SigtOpTrxPend> getTransitsByCodAgencyAndStatus(Integer codAgency, Integer status) {
		return transitDAO.findByCodAgencyAndStatus(codAgency, status);
	}
	
	
	public List<SigtOpTrxPend> getTransitsByStatus(Integer status) {
		return transitDAO.findByStatus(status);
	}
	

	public Long createNotification(SigtOpToIntNotif notification) {
		return notificationDAO.save(notification);
	}
	
	public SigtDscNotifType getNotifTypeById(Integer id) {
		return notificationDAO.findNotifTypeById(id);
	}

	public void batchUpdateTransitsStatusAndIdNotifAndNumSend(List<SigtOpTrxPend> transits, Integer status, Long idNotif) {
		transitDAO.batchUpdateStatusAndIdNotifAndNumSend(transits, status, idNotif);
	}

	
	public void batchUpdateTransitsStatusAndIdNotifSigtAndNumSend(List<SigtOpTrxPend> transits, Integer status,
			Long idNotifSigt) {
		transitDAO.batchUpdateStatusAndIdNotifSigtAndNumSend(transits, status, idNotifSigt);		
	}
	
	
	public void batchUpdateTransitsNumSend(List<SigtOpTrxPend> transits) {
		transitDAO.batchUpdateNumSend(transits);
	}
	
}
