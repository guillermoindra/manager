package es.indra.procesoEnvioViajes;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import es.indra.procesoEnvioViajes.config.SpringConfig;
import es.indra.procesoEnvioViajes.travelSendJob.TravelSendJobLauncher;

@ComponentScan(basePackages = { "es.indra.operator.client.api" })
public class App {
	static ApplicationContext context;
	// private static String[] springConfig = {"spring/batch/jobs/job-config.xml" };

	public static void main(String[] args) {
		// context = new ClassPathXmlApplicationContext(springConfig);
		context = new AnnotationConfigApplicationContext(SpringConfig.class);
		TravelSendJobLauncher jobLauncher = (TravelSendJobLauncher) context.getBean("travelSendJobLauncher");

		String mode = context.getEnvironment().getProperty("mode");

		while (true) { // condici�n de finalizaci�n del proceso
			if (args.length > 0) { // hay par�metros
				if (args[0].equalsIgnoreCase("SIGT"))
					jobLauncher.run("SIGT");
				else if (args[0].equalsIgnoreCase(("AGENC")))
					jobLauncher.run("AGENC");
				else
					jobLauncher.run("AGENC");

			} else { // en caso de no haber par�metros, tomar el modo del archivo properties
				if (mode.equalsIgnoreCase("SIGT"))
					jobLauncher.run("SIGT");
				else if (mode.equalsIgnoreCase(("AGENC")))
					jobLauncher.run("AGENC");
				else
					jobLauncher.run("AGENC");
			}
		}
	}
}
