package es.indra.procesoEnvioViajes.travelSendJob;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import es.indra.operator.client.ApiException;
import es.indra.operator.client.api.DefaultApi;
import es.indra.operator.client.api.utils.GenericConstant;
import es.indra.operator.client.model.Paso;
import es.indra.procesoEnvioViajes.models.SigtDscCodAgency;
import es.indra.procesoEnvioViajes.models.SigtDscNotifType;
import es.indra.procesoEnvioViajes.models.SigtOpToIntNotif;
import es.indra.procesoEnvioViajes.models.SigtOpTrxPend;
import es.indra.procesoEnvioViajes.services.MainServiceImpl;

public class AgencyPendingTravelsSlaveStep implements Tasklet {
	
	private SigtDscCodAgency agency;
	
	@Autowired
	private DefaultApi api;
	
	@Autowired
	MainServiceImpl mainService;
	
	@Value("${agencyRefreshingTime}")
	private String refreshingTime;
	
	private boolean isFirstExecution=true;
	private Long endTime;
	private static final int NOTIF_TYPE=1;
	private static final int MIN_LENGTH_ID_TRANSIT = 27;
	
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		int status;
		int new_status;
		List<SigtOpTrxPend> transits;
		boolean isSigt=false;
		
		// Extraer los parámetros necesarios del Step execution context
		this.agency = (SigtDscCodAgency) chunkContext.getStepContext().getStepExecutionContext().get("agency");
		
		if (agency.getId()==0) {
			isSigt=true;
			status=2;
			new_status=3;
			// consultar travels con status=2
			transits = mainService.getTransitsByStatus(status);
			
		} else {
			status=1;
			new_status=2;
			// consultar travels con status=1 y agency code determinado anteriormente
			transits = mainService.getTransitsByCodAgencyAndStatus(agency.getId(), status);
		
		}
		
		if (!transits.isEmpty()) {
			// registrar la notificaci�n en la tabla de notificaciones con notif_type=1 y
			// num_record=numero de viajes le�dos
			SigtOpToIntNotif notification = createNotification(agency, transits.size());
			// la notificaci�n se registrar� en el caso de que todo haya ido bien
			
			List<Paso> pasos = parse(transits);
			
			try {
				api.v1PasosPost(pasos,agency.getWsUrl());
				
				Long notificationId = mainService.createNotification(notification); // registro de la notificaci�n
				
				// Si todo ha ido bien, guardar viaje con STATUS=2, ID_NOTIF_SIGT=id de la
				// notificaci�n creada al principio, y NUMSEND++
				if (isSigt) mainService.batchUpdateTransitsStatusAndIdNotifSigtAndNumSend(transits, new_status, notificationId);
				else mainService.batchUpdateTransitsStatusAndIdNotifAndNumSend(transits, new_status, notificationId);
				
			} catch (ApiException e) {
				e.printStackTrace();
				// No conex o error: rollback y registro en las tablas de error AUD_BATCH.
				// Mantener Travel con STATUS=1 e incrementar NUMSEND
				// TODO: registro en tablas AUD_BATCH
				mainService.batchUpdateTransitsNumSend(transits);
			}
				
		}

		return continueExecution() ? RepeatStatus.CONTINUABLE : RepeatStatus.FINISHED; // RepeatStatus.CONTINUABLE vuelve a ejecutar �ste m�todo
	}
	
	/**
	 * 
	 * @param transits
	 * @return
	 */
	private List<Paso> parse(List<SigtOpTrxPend> transits) {
		List<Paso> pasoList = new ArrayList<Paso>();

		for (SigtOpTrxPend transit : transits) {

			// Create Paso Object, and sets data
			Paso paso = new Paso();
			String idStr = transit.getIdentifier().toString();
			while (idStr.length() < MIN_LENGTH_ID_TRANSIT){
				idStr = "0"+idStr;				
			}
			paso.setCodigoPaso(GenericConstant.OPERATOR_CODE + "_" + idStr);
			paso.setCodigoOperador(Integer.parseInt(transit.getCodAgency().getExternalCode()));
			paso.setFechaHora(new DateTime(transit.getTrxDate()));
			paso.setFechaRecaudo(new DateTime(transit.getTrxDate()));
			paso.setCodigoCliente(transit.getClientId());
			paso.setPlaca(transit.getPlate());
			paso.setTid(transit.getTid());
			paso.setEpc(transit.getEpc());
			paso.setEstacion(transit.getStationId().getIdPlace().toString());
			paso.setCarril(transit.getLaneId().getIdPlace().toString());
			paso.setValor(transit.getAmount());
			paso.setCantidadEjes(transit.getNumAxles());
			paso.setCantidadDobleRuedas(transit.getNumDoubleWheel());

			paso.setCategoriaDAC((transit.getDetectedClass() != null)
					? Integer.parseInt(transit.getDetectedClass().getExternalCode()) : null);

			paso.setCategoriaCobrada((transit.getChargedClass() != null)
					? transit.getChargedClass().getExternalCode() : null);

			paso.setTipoLectura((transit.getReadType() != null)
					? Integer.parseInt(transit.getReadType().getExternalCode()) : null);

			paso.setSentido(transit.getDirection());
			paso.setPlacaOCR(transit.getPlateOcr());
			paso.setExisteDescrepancia(transit.getIsDiscrepancy());

			paso.setFechaActualizacionUsuario(
					(transit.getListDate() != null) ? new DateTime(transit.getListDate()) : null);

			paso.setCodigoListaUsuario(transit.getIdList());
			paso.setExisteDiscrepanciaPlaca(transit.getIsPlateDiscrepancy());

			pasoList.add(paso);

		}
		
		return pasoList;
	}

	private SigtOpToIntNotif createNotification(SigtDscCodAgency agency, Integer numRecords) {
		SigtOpToIntNotif n = new SigtOpToIntNotif();
		SigtDscNotifType notifType = mainService.getNotifTypeById(NOTIF_TYPE);
		
		n.setNotifType(notifType);
		n.setCodAgency(agency);
		n.setNumRecords(numRecords);
		n.setCreationDate(new Date(System.currentTimeMillis()));
		n.setModificationUser("PROC_VIAJES");
		n.setModification(new Byte("1"));
		n.setOptimistLock(new Byte("1"));
		
		return n;
	}

	private boolean continueExecution () {
		boolean result;
		if (isFirstExecution) {
			endTime = System.currentTimeMillis() + Long.parseLong(refreshingTime)*1000;
			result = true;
			isFirstExecution=false;
		}
		else {
			if (System.currentTimeMillis()<endTime) {
				result = true;
			} else {
				result = false;
				isFirstExecution=true;
			}
		}
		return result;
	}

}
