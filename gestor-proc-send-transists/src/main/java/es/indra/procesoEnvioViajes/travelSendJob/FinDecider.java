package es.indra.procesoEnvioViajes.travelSendJob;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;
import org.springframework.batch.core.job.flow.JobExecutionDecider;

public class FinDecider implements JobExecutionDecider {
	
	public FlowExecutionStatus decide(JobExecution jobExecution, StepExecution stepExecution) {
		// Here you can establish the end condition for the spring batch Job
		return new FlowExecutionStatus("CONTINUE");

	}

}
