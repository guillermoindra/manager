package es.indra.procesoEnvioViajes.travelSendJob;

import java.util.HashMap;
import java.util.Map;

import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;

import es.indra.procesoEnvioViajes.models.AgencyCollection;
import es.indra.procesoEnvioViajes.models.SigtDscCodAgency;

public class AgencyPendingTravelsPartitioner implements Partitioner {
	private AgencyCollection agencies;
	
	public Map<String, ExecutionContext> partition(int gridSize) {
		Map<String, ExecutionContext> partitionMap = new HashMap<String, ExecutionContext>();

		// sea crea un nombre y un contexto por cada agencia
		// cada elemento del map ser� despu�s un thread
		for (Long i = new Long(0); i < agencies.getSize(); i++) {
			ExecutionContext context = new ExecutionContext();

			SigtDscCodAgency agency = agencies.getValue(i);
			// DefaultApi api = new DefaultApi(agency.getWsUrl());

			context.put("agency", agency);
			// context.put("api", api);

			partitionMap.put(agency.getId().toString(), context);
		}
		return partitionMap;
	}

	public AgencyCollection getAgencies() {
		return agencies;
	}

	public void setAgencies(AgencyCollection agencies) {
		this.agencies = agencies;
	}

}
