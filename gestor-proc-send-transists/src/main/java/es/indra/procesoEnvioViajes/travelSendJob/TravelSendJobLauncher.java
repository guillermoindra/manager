package es.indra.procesoEnvioViajes.travelSendJob;

import java.util.UUID;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;

public class TravelSendJobLauncher {
	@Autowired
	private JobLauncher jobLauncher;
	
	@Autowired
	private Job job;
	
	private JobExecution execution;
		
	public void run(String mode) {
		try {
			JobParametersBuilder jobBuilder = new JobParametersBuilder();
			jobBuilder.addString("UUID", UUID.randomUUID().toString(), true);
			jobBuilder.addString("mode", mode);
			JobParameters jobParameters = jobBuilder.toJobParameters();
			
			Long startTime = System.nanoTime();
			
			execution = jobLauncher.run(job, jobParameters);
			
			Long endTime = System.nanoTime();
			
			System.out.println("Exit Status : " + execution.getStatus());
			System.out.println("Elapsed time: " + (endTime - startTime) / 1000000 + "ms");
			
		} catch (Exception exceptionObj) {
			exceptionObj.printStackTrace();
		}
	}
}
