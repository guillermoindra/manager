package es.indra.procesoEnvioViajes.travelSendJob;

import java.util.List;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import es.indra.procesoEnvioViajes.exceptions.ProcSendTransitsException;
import es.indra.procesoEnvioViajes.models.AgencyCollection;
import es.indra.procesoEnvioViajes.models.SigtDscCodAgency;
import es.indra.procesoEnvioViajes.services.MainServiceImpl;


public class AgenciesReaderTaskletStep implements Tasklet {
	
	private AgencyCollection agencies;
	
	@Autowired
	MainServiceImpl mainService;
	
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
		try {
			agencies.reset();
			
			String mode = (String) chunkContext.getStepContext().getJobParameters().get("mode");
			
			if (mode.equalsIgnoreCase("SIGT")) { // mode SIGT
				SigtDscCodAgency a = mainService.getAgency(0);
				if (a == null) throw new ProcSendTransitsException("Agency 0 not found");
				else agencies.addAgency(a);
				
			} else { // mode AGENC
				List<SigtDscCodAgency> ags = mainService.getAllAgenciesExceptOne(0);
				if (ags.isEmpty()) throw new ProcSendTransitsException("No agencies found");
				else {
					for (SigtDscCodAgency a : ags) {
						agencies.addAgency(a);
					}
				}
			}
			
			chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext().put("gridSize", agencies.getSize());
			System.out.println("Threads: " + agencies.getSize());
			
		} catch (ProcSendTransitsException e) {
	    	e.printStackTrace();
	    	Thread.sleep(5000);
	    	return RepeatStatus.CONTINUABLE;
	    }
		
		return RepeatStatus.FINISHED;
	}

	public AgencyCollection getAgencies() {
		return agencies;
	}

	public void setAgencies(AgencyCollection agencies) {
		this.agencies = agencies;
	}
}
