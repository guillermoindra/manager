package es.indra.procesoEnvioViajes.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SIGT_DSC_VEHICLE_CLASS")
public class SigtDscVehicleClass {

	@Id
	@Column(name="ID_CATEGORY", nullable=false)
	private Integer idCategory = null ;
	
	@Column(name="DESCRIPTION", nullable=false)
	private String description = null;
	
	@Column(name="EXTERNAL_CODE", nullable=false)
	private String externalCode = null ;	
	
	
	public SigtDscVehicleClass() {
		super();
	}
	
	public SigtDscVehicleClass(Integer idCategory) {
		super();
		this.idCategory = idCategory;
	}

	public Integer getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(Integer idCategory) {
		this.idCategory = idCategory;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getExternalCode() {
		return externalCode;
	}

	public void setExternalCode(String externalCode) {
		this.externalCode = externalCode;
	}

}
