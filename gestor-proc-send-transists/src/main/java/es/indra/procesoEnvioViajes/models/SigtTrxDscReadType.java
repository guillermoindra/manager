package es.indra.procesoEnvioViajes.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SIGT_TRX_DSC_READ_TYPE")
public class SigtTrxDscReadType {
	
	@Id
	@Column(name="ID", nullable=false)
	private Integer id = null ;
	
	@Column(name="DESCRIPTION", nullable=false)
	private String description = null ;
	
	@Column(name="EXTERNAL_CODE", nullable=false)
	private String externalCode = null ;

	
	public SigtTrxDscReadType() {
		super();
	}

	public SigtTrxDscReadType(Integer id) {
		super();
		this.id = id;
	}

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getExternalCode() {
		return externalCode;
	}

	public void setExternalCode(String externalCode) {
		this.externalCode = externalCode;
	}
	
}
