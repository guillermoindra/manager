package es.indra.procesoEnvioViajes.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="SIGT_OP_TO_INT_NOTIF")
public class SigtOpToIntNotif {
		
	@Id
	@Column(name="IDENTIFIER", nullable=false)
	private Long identifier = null;
	
	@ManyToOne
	@JoinColumn(name="NOTIF_TYPE", referencedColumnName="ID")
	private SigtDscNotifType notifType = null;
	
	@ManyToOne
	@JoinColumn(name="COD_AGENCY", referencedColumnName="ID")
	private SigtDscCodAgency codAgency = null;
	
	@Column(name="NUM_RECORDS", nullable=false)
	private Integer numRecords = null;
	
	@Column(name="CREATION_DATE", nullable=false)
	private Date creationDate = null ;

	@Column(name="MODIFICATION_USER", nullable=false)
	private String modificationUser = null ;
	
	@Column(name="APPLICATION", nullable=true)
	private Integer application = null ;
	
	@Column(name="MODIFICATION_DATE", nullable=true)
	private Date modificationDate = null ;

	@Column(name="MODIFICATION", nullable=false)
	private Byte modification = null ;

	@Column(name="OPTIMIST_LOCK", nullable=false)
	private Byte optimistLock = null ;
	
	
	public SigtOpToIntNotif() {
		super();
	}
	
	public SigtOpToIntNotif(Long identifier) {
		super();
		this.identifier=identifier;
	}
	
	
	public Long getIdentifier() {
		return identifier;
	}
	public void setIdentifier(Long identifier) {
		this.identifier = identifier;
	}
	public SigtDscNotifType getNotifType() {
		return notifType;
	}
	public void setNotifType(SigtDscNotifType notifType) {
		this.notifType = notifType;
	}
	public SigtDscCodAgency getCodAgency() {
		return codAgency;
	}
	public void setCodAgency(SigtDscCodAgency codAgency) {
		this.codAgency = codAgency;
	}
	public Integer getNumRecords() {
		return numRecords;
	}
	public void setNumRecords(Integer numRecords) {
		this.numRecords = numRecords;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getModificationUser() {
		return modificationUser;
	}
	public void setModificationUser(String modificationUser) {
		this.modificationUser = modificationUser;
	}
	public Integer getApplication() {
		return application;
	}
	public void setApplication(Integer application) {
		this.application = application;
	}
	public Date getModificationDate() {
		return modificationDate;
	}
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}
	public Byte getModification() {
		return modification;
	}
	public void setModification(Byte modification) {
		this.modification = modification;
	}
	public Byte getOptimistLock() {
		return optimistLock;
	}
	public void setOptimistLock(Byte optimistLock) {
		this.optimistLock = optimistLock;
	}	
}
