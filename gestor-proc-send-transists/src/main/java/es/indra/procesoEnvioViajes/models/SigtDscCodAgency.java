package es.indra.procesoEnvioViajes.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SIGT_DSC_COD_AGENCY")
public class SigtDscCodAgency {
	
	@Id
	@Column(name="ID", nullable=false)
	private Integer id = null ;
	
	@Column(name="DESCRIPTION", nullable=false)
	private String description = null ;
	
	@Column(name="WS_URL", nullable=false)
	private String wsUrl = null ;
	
	@Column(name="EXTERNAL_CODE", nullable=false)
	private String externalCode = null ;
	
	public SigtDscCodAgency() {
		super();
	}
	
	public SigtDscCodAgency(Integer id) {
		super();
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getExternalCode() {
		return externalCode;
	}

	public void setExternalCode(String externalCode) {
		this.externalCode = externalCode;
	}

	public String getWsUrl() {
		return wsUrl;
	}

	public void setWsUrl(String wsUrl) {
		this.wsUrl = wsUrl;
	}

}

	/*@Id
	@Column(name="IDENTIFIER")
	private Integer identifier;
	
	@Column(name="DESCRIPTION", nullable=false)
	private String description;
	@Column(name="WS_URL", nullable=false)
	private String ws_url;
	@Column(name="EXTERNAL_CODE", nullable=false)
	private String external_code;
	
	public Agency () {
		
	}
	
	public Agency(Integer identifier, String description, String ws_url, String external_code) {
		this.identifier = identifier;
		this.description = description;
		this.ws_url = ws_url;
		this.external_code = external_code;
	}
	
	public Integer getIdentifier() {
		return identifier;
	}
	public void setIdentifier(Integer identifier) {
		this.identifier = identifier;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getWs_url() {
		return ws_url;
	}
	public void setWs_url(String ws_url) {
		this.ws_url = ws_url;
	}
	public String getExternal_code() {
		return external_code;
	}
	public void setExternal_code(String external_code) {
		this.external_code = external_code;
	}

	@Override
	public String toString() {
		return "Agency [Identifier=" + identifier + ", description=" + description + ", ws_url=" + ws_url
				+ ", external_code=" + external_code + "]";
	}
}*/
