package es.indra.procesoEnvioViajes.models;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="SIGT_OP_TRX_PEND")
public class SigtOpTrxPend {
	
	@Id
	@Column(name="IDENTIFIER", nullable=false)
	private Long identifier=null;
	
	@Column(name="UUID", nullable=false)
	private String uuid = null;
	
	@ManyToOne
	@JoinColumn(name="COD_AGENCY", referencedColumnName="ID")
	private SigtDscCodAgency codAgency = null;
	
	@ManyToOne
	@JoinColumn(name="LANE_ID", referencedColumnName="ID_PLACE")
	private SigtDscPlaces laneId = null;
	
	@Column(name="LANE_EXTERNAL_ID", nullable=false)
	private String laneExternalId = null ;
	
	@ManyToOne
	@JoinColumn(name="STATION_ID", referencedColumnName="ID_PLACE") 
	private SigtDscPlaces stationId = null ;
	
	@ManyToOne
	@JoinColumn(name="DETECTED_CLASS", referencedColumnName="ID_CATEGORY")
	private SigtDscVehicleClass detectedClass = null ;
	
	@ManyToOne
	@JoinColumn(name="CHARGED_CLASS", referencedColumnName="ID_CATEGORY")
	private SigtDscVehicleClass chargedClass = null ; 
	
	@ManyToOne
	@JoinColumn(name="ID_NOTIF", referencedColumnName="IDENTIFIER", nullable=true)
	private SigtOpToIntNotif idNotif = null ;
	
	@ManyToOne
	@JoinColumn(name="ID_NOTIF_SIGT", referencedColumnName="IDENTIFIER", nullable=true)
	private SigtOpToIntNotif idNotifSigt = null ;
	
	@Column(name="NUM_SEND", nullable=false)
	private Integer numSend = null ;

	@ManyToOne
	@JoinColumn(name="STATUS", referencedColumnName="ID_STATUS")
	private SigtOpTrxDscStatus status = null ;
		
	@ManyToOne
	@JoinColumn(name="READ_TYPE", referencedColumnName="ID", nullable=false) 
	private SigtTrxDscReadType readType = null ;
	
	@Column(name="TRX_DATE", nullable=false)
	private Date trxDate = null ; 

	@Column(name="TRX_ID", nullable=false)
	private Long trxId = null;

	@Column(name="CLIENT_ID", nullable=false)
	private String  clientId = null ;
		
	@Column(name="PLATE", nullable=false)
	private String plate = null;

	@Column(name="TID", nullable=false)
	private String tid = null;

	@Column(name="EPC", nullable=false)
	private String epc = null;

	@Column(name="AMOUNT", nullable=false)
	private BigDecimal amount = null ;

	@Column(name="NUM_AXLES", nullable=false)
	private Integer numAxles = null ;

	@Column(name="NUM_DOUBLE_WHEEL", nullable=false)
	private Integer numDoubleWheel = null ; 

	@Column(name="DIRECTION", nullable=false)
	private String direction  = null ; 

	@Column(name="PLATE_OCR", nullable=false)
	private String plateOcr = null ;

	@Column(name="IS_DISCREPANCY", nullable=false)
	private Boolean isDiscrepancy = null ;

	@Column(name="LIST_DATE", nullable=false)
	private Date listDate = null ;

	@Column(name="ID_LIST", nullable=false)
	private String idList = null ;

	@Column(name="IS_PLATE_DISCREPANCY", nullable=false)
	private Boolean isPlateDiscrepancy = null ;

	@Column(name="CREATION_DATE", nullable=false)
	private Date creationDate = null ;

	@Column(name="MODIFICATION_USER", nullable=false)
	private String modificationUser = null ;
	
	@Column(name="APPLICATION", nullable=true)
	private Integer application = null ;
	
	@Column(name="MODIFICATION_DATE", nullable=true)
	private Date modificationDate = null ;

	@Column(name="MODIFICATION", nullable=false)
	private Byte modification = null ;

	@Column(name="OPTIMIST_LOCK", nullable=false)
	private Byte optimistLock = null ;
	
	
	public SigtOpTrxPend() {
		super();
	}
	
	public SigtOpTrxPend(Long identifier) {
		super();
		this.identifier=identifier;
	}
	
	
	public Long getIdentifier() {
		return identifier;
	}
	public void setIdentifier(Long identifier) {
		this.identifier = identifier;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public SigtDscCodAgency getCodAgency() {
		return codAgency;
	}
	public void setCodAgency(SigtDscCodAgency codAgency) {
		this.codAgency = codAgency;
	}
	public SigtDscPlaces getLaneId() {
		return laneId;
	}
	public void setLaneId(SigtDscPlaces laneId) {
		this.laneId = laneId;
	}
	public String getLaneExternalId() {
		return laneExternalId;
	}
	public void setLaneExternalId(String laneExternalId) {
		this.laneExternalId = laneExternalId;
	}
	public SigtDscPlaces getStationId() {
		return stationId;
	}
	public void setStationId(SigtDscPlaces stationId) {
		this.stationId = stationId;
	}
	public SigtDscVehicleClass getDetectedClass() {
		return detectedClass;
	}
	public void setDetectedClass(SigtDscVehicleClass detectedClass) {
		this.detectedClass = detectedClass;
	}
	public SigtDscVehicleClass getChargedClass() {
		return chargedClass;
	}
	public void setChargedClass(SigtDscVehicleClass chargedClass) {
		this.chargedClass = chargedClass;
	}
	public SigtOpToIntNotif getIdNotif() {
		return idNotif;
	}
	public void setIdNotif(SigtOpToIntNotif idNotif) {
		this.idNotif = idNotif;
	}
	public SigtOpToIntNotif getIdNotifSigt() {
		return idNotifSigt;
	}
	public void setIdNotifSigt(SigtOpToIntNotif idNotifSigt) {
		this.idNotifSigt = idNotifSigt;
	}
	public Integer getNumSend() {
		return numSend;
	}
	public void setNumSend(Integer numSend) {
		this.numSend = numSend;
	}
	public SigtOpTrxDscStatus getStatus() {
		return status;
	}
	public void setStatus(SigtOpTrxDscStatus status) {
		this.status = status;
	}
	public SigtTrxDscReadType getReadType() {
		return readType;
	}
	public void setReadType(SigtTrxDscReadType readType) {
		this.readType = readType;
	}
	public Date getTrxDate() {
		return trxDate;
	}
	public void setTrxDate(Date trxDate) {
		this.trxDate = trxDate;
	}
	public Long getTrxId() {
		return trxId;
	}
	public void setTrxId(Long trxId) {
		this.trxId = trxId;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getPlate() {
		return plate;
	}
	public void setPlate(String plate) {
		this.plate = plate;
	}
	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	public String getEpc() {
		return epc;
	}
	public void setEpc(String epc) {
		this.epc = epc;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public Integer getNumAxles() {
		return numAxles;
	}
	public void setNumAxles(Integer numAxles) {
		this.numAxles = numAxles;
	}
	public Integer getNumDoubleWheel() {
		return numDoubleWheel;
	}
	public void setNumDoubleWheel(Integer numDoubleWheel) {
		this.numDoubleWheel = numDoubleWheel;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public String getPlateOcr() {
		return plateOcr;
	}
	public void setPlateOcr(String plateOcr) {
		this.plateOcr = plateOcr;
	}
	public Boolean getIsDiscrepancy() {
		return isDiscrepancy;
	}
	public void setIsDiscrepancy(Boolean isDiscrepancy) {
		this.isDiscrepancy = isDiscrepancy;
	}
	public Date getListDate() {
		return listDate;
	}
	public void setListDate(Date listDate) {
		this.listDate = listDate;
	}
	public String getIdList() {
		return idList;
	}
	public void setIdList(String idList) {
		this.idList = idList;
	}
	public Boolean getIsPlateDiscrepancy() {
		return isPlateDiscrepancy;
	}
	public void setIsPlateDiscrepancy(Boolean isPlateDiscrepancy) {
		this.isPlateDiscrepancy = isPlateDiscrepancy;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getModificationUser() {
		return modificationUser;
	}
	public void setModificationUser(String modificationUser) {
		this.modificationUser = modificationUser;
	}
	public Integer getApplication() {
		return application;
	}
	public void setApplication(Integer application) {
		this.application = application;
	}
	public Date getModificationDate() {
		return modificationDate;
	}
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}
	public Byte getModification() {
		return modification;
	}
	public void setModification(Byte modification) {
		this.modification = modification;
	}
	public Byte getOptimistLock() {
		return optimistLock;
	}
	public void setOptimistLock(Byte optimistLock) {
		this.optimistLock = optimistLock;
	}
}
