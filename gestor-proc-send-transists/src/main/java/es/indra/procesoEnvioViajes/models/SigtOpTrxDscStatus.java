package es.indra.procesoEnvioViajes.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SIGT_OP_TRX_DSC_STATUS")
public class SigtOpTrxDscStatus {
	
	@Id
	@Column(name="ID_STATUS", nullable=false)
	private Integer idStatus = null ;
	
	@Column(name="DESCRIPTION", nullable=false)
	private String description = null ;	
	
	
	public SigtOpTrxDscStatus() {
		super();
	}
	
	public SigtOpTrxDscStatus(Integer idStatus) {
		super();
		this.idStatus = idStatus;
	}

	
	public Integer getIdStatus() {
		return idStatus;
	}

	public void setIdStatus(Integer idStatus) {
		this.idStatus = idStatus;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}