package es.indra.procesoEnvioViajes.models;

import java.util.concurrent.ConcurrentHashMap;

public class AgencyCollection {
	private volatile ConcurrentHashMap<Long, SigtDscCodAgency> agencies;
	private volatile Long count;
	
	public AgencyCollection() {
		this.agencies = new ConcurrentHashMap<Long, SigtDscCodAgency>();
		this.count=new Long(0);		
	}
	
	public synchronized void reset () {
		this.agencies = new ConcurrentHashMap<Long, SigtDscCodAgency>();
		this.count=new Long(0);
	}
	
	public synchronized SigtDscCodAgency getValue (Long key) {
		return agencies.get(key);
	}

	public synchronized void addAgency (SigtDscCodAgency agency) {
		agencies.put(this.count, agency);
		count++;
	}
	
	public synchronized ConcurrentHashMap<Long, SigtDscCodAgency> getAgencies() {
		return agencies;
	}

	public synchronized void setAgencies(ConcurrentHashMap<Long, SigtDscCodAgency> agencies) {
		this.agencies = agencies;
	}
	
	public synchronized int getSize () {
		return agencies.size();
	}
}
