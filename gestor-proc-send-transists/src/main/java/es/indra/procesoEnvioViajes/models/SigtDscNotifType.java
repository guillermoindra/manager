package es.indra.procesoEnvioViajes.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SIGT_DSC_NOTIF_TYPE")
public class SigtDscNotifType {
	
	@Id
	@Column(name="ID", nullable=false)
	private Integer id = null;	
	
	@Column(name="DESCRIPTION", nullable=false)
	private String description = null ;
	
	
	public SigtDscNotifType () {
		super();
	}
	
	public SigtDscNotifType(Integer id) {
		super();
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
