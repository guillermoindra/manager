package es.indra.procesoEnvioViajes.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SIGT_DSC_PLACES")
public class SigtDscPlaces {
	
	@Id
	@Column(name="ID_PLACE", nullable=false)
	private Integer idPlace = null ;
	
	@Column(name="EXTERNAL_CODE", nullable=false)
	private String externalCode = null ;	
	
	@Column(name="DIRECTION", nullable=false)
	private String direction=null;
	
	public SigtDscPlaces() {
		super();
	}
	
	public SigtDscPlaces(Integer idPlace) {
		super();
		this.idPlace = idPlace;
	}

	public Integer getIdPlace() {
		return idPlace;
	}

	public void setIdPlace(Integer idPlace) {
		this.idPlace = idPlace;
	}

	public String getExternalCode() {
		return externalCode;
	}

	public void setExternalCode(String externalCode) {
		this.externalCode = externalCode;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}
	

}