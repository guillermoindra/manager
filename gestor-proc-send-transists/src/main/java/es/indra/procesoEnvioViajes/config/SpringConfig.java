package es.indra.procesoEnvioViajes.config;

import java.net.MalformedURLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;

import es.indra.procesoEnvioViajes.dao.AgencyDAOImpl;
import es.indra.procesoEnvioViajes.dao.NotificationDAOImpl;
import es.indra.procesoEnvioViajes.dao.TransitDAOImpl;
import es.indra.procesoEnvioViajes.services.MainServiceImpl;

@Configuration
@EnableBatchProcessing
@PropertySource("classpath:app.properties")
@ImportResource("classpath:spring/batch/jobs/travelSendJob-config.xml")
public class SpringConfig {

	@Value("org/springframework/batch/core/schema-drop-sqlserver.sql")
	private Resource dropReopsitoryTables;

	@Value("org/springframework/batch/core/schema-sqlserver.sql")
	private Resource dataRepositorySchema;
	
	@Autowired
	Environment env;
	
	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getProperty("dataSource.driverClassName"));
		dataSource.setUrl(env.getProperty("dataSource.url"));
		dataSource.setUsername(env.getProperty("dataSource.username"));
		dataSource.setPassword(env.getProperty("dataSource.password"));
		return dataSource;
	}
	
	@Bean
	public DataSourceInitializer dataSourceInitializer (DataSource dataSource) throws MalformedURLException {
		ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator();
		databasePopulator.addScript(dropReopsitoryTables);
		databasePopulator.addScript(dataRepositorySchema);
		databasePopulator.setIgnoreFailedDrops(true);
		
		DataSourceInitializer initializer = new DataSourceInitializer();
		initializer.setDataSource(dataSource);
		initializer.setDatabasePopulator(databasePopulator);
		
		initializer.setEnabled(Boolean.parseBoolean(env.getProperty("createBatchMetaTables", "false")));
		
		return initializer;
	}
	
	@Bean
    public JobRepository getJobRepository() throws Exception {
        JobRepositoryFactoryBean factory = new JobRepositoryFactoryBean();
        factory.setDataSource(dataSource());
        factory.setTransactionManager(transactionManager());
        factory.afterPropertiesSet();
        return (JobRepository) factory.getObject();
    }
 
	@Bean
    public JobLauncher getJobLauncher() throws Exception {
        SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
        jobLauncher.setJobRepository(getJobRepository());
        jobLauncher.afterPropertiesSet();
        
        return jobLauncher;
    }
    
    @Bean
    public SessionFactory sessionFactory () {
    	LocalSessionFactoryBuilder builder= new LocalSessionFactoryBuilder(dataSource());
    	builder.scanPackages("es.indra.procesoEnvioViajes.models");
    	Properties hibernateProperties = buildHibernateProperties();
    	builder.addProperties(hibernateProperties);
    	
    	return builder.buildSessionFactory();
    }
    
    private Properties buildHibernateProperties() {
        Properties props = new Properties();

        // Set the default-schema for Hibernate to use.
//        props.setProperty("hibernate.default_schema", env.getProperty("hibernate.default_schema"));

        props.setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
        props.setProperty("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));

        // Sets the DEBUG Level of the hibernate: SQL generation layer.
        props.setProperty("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
        return props;
    }
    
    @Bean
    public HibernateTransactionManager transactionManager() {
        return new HibernateTransactionManager(sessionFactory());
    }
    
    @Bean 
    public MainServiceImpl mainService(){
       return new MainServiceImpl();
    }
    
    @Bean 
    public AgencyDAOImpl agencyDAOImpl(){
       return new AgencyDAOImpl();
    }
    
    @Bean 
    public NotificationDAOImpl notificationDAOImpl(){
       return new NotificationDAOImpl();
    }
    
    @Bean 
    public TransitDAOImpl transitDAOImpl(){
       return new TransitDAOImpl();
    }
    
}