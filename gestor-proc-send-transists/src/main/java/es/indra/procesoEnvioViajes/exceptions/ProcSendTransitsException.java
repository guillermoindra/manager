package es.indra.procesoEnvioViajes.exceptions;

public class ProcSendTransitsException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public ProcSendTransitsException () {
		super();
	}
	
	public ProcSendTransitsException (String message) {
		super(message);
	}
	
	public ProcSendTransitsException (String message, Throwable cause) {
		super(message, cause);
	}

}
