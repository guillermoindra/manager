package es.indra.common.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class AuditModel implements Serializable{
	
	private static final long serialVersionUID = 200808837064848262L;

	private String uuid;
	private String url;
	private String wsInput;
	private String wsOutput;
	private String operation;
	private Date dateReception;
	private Date dateSend;
	private long duration;
	private int status;
	private String host;
	
	private Timestamp timeReception;
	
	
	/**
	 * Constructor.
	 */
	public AuditModel() {
		this.dateReception = new Date();
	}
	
	
	/**
	 * Constructor.
	 * @param uuid
	 * @param url
	 * @param wsInput
	 * @param wsOuput
	 * @param operation
	 * @param dateReception
	 * @param dateSend
	 * @param duration
	 * @param errorCode
	 * @param host
	 */
	public AuditModel(String uuid, String url, String wsInput, String wsOutput, String operation, Date dateReception,
			Date dateSend, long duration, int status, String host) {
		super();
		this.uuid = uuid;
		this.url = url;
		this.wsInput = wsInput;
		this.wsOutput = wsOutput;
		this.operation = operation;
		this.dateReception = dateReception;
		this.dateSend = dateSend;
		this.duration = duration;
		this.status = status;
		this.host = host;
	}


	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	/**
	 * @return the uuid
	 */
	public String getUuid() {
		return uuid;
	}


	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}


	/**
	 * @return the wsInput
	 */
	public String getWsInput() {
		return wsInput;
	}


	/**
	 * @return the wsOutput
	 */
	public String getWsOutput() {
		return wsOutput;
	}


	/**
	 * @return the operation
	 */
	public String getOperation() {
		return operation;
	}


	/**
	 * @return the dateReception
	 */
	public Date getDateReception() {
		return dateReception;
	}


	/**
	 * @return the dateSend
	 */
	public Date getDateSend() {
		return dateSend;
	}


	/**
	 * @return the duration
	 */
	public long getDuration() {
		return duration;
	}


	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}


	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}


	/**
	 * @return the timeReception
	 */
	public Timestamp getTimeReception() {
		return timeReception;
	}


	/**
	 * @param uuid the uuid to set
	 */
	public AuditModel setUuid(String uuid) {
		this.uuid = uuid;
		return this;
	}


	/**
	 * @param url the url to set
	 */
	public AuditModel setUrl(String url) {
		this.url = url;
		return this;
	}


	/**
	 * @param wsInput the wsInput to set
	 */
	public AuditModel setWsInput(String wsInput) {
		this.wsInput = wsInput;
		return this;
	}


	/**
	 * @param wsOutput the wsOutput to set
	 */
	public AuditModel setWsOutput(String wsOutput) {
		this.wsOutput = wsOutput;
		return this;
	}


	/**
	 * @param operation the operation to set
	 */
	public AuditModel setOperation(String operation) {
		this.operation = operation;
		return this;
	}


	/**
	 * @param dateReception the dateReception to set
	 */
	public AuditModel setDateReception(Date dateReception) {
		this.dateReception = dateReception;
		return this;
	}


	/**
	 * @param dateSend the dateSend to set
	 */
	public AuditModel setDateSend(Date dateSend) {
		this.dateSend = dateSend;
		return this;
	}


	/**
	 * @param duration the duration to set
	 */
	public AuditModel setDuration(long duration) {
		this.duration = duration;
		return this;
	}


	/**
	 * @param status the status to set
	 */
	public AuditModel setStatus(int status) {
		this.status = status;
		return this;
	}


	/**
	 * @param host the host to set
	 */
	public AuditModel setHost(String host) {
		this.host = host;
		return this;
	}


	/**
	 * @param timeReception the timeReception to set
	 */
	public AuditModel setTimeReception(Timestamp timeReception) {
		this.timeReception = timeReception;
		return this;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AuditModel \n" + (uuid != null ? "uuid: " + uuid + "\n" : "")
				+ (url != null ? "url: " + url + "\n" : "") + (wsInput != null ? "wsInput: " + wsInput + "\n" : "")
				+ (wsOutput != null ? "wsOuput: " + wsOutput + "\n" : "")
				+ (operation != null ? "operation: " + operation + "\n" : "")
				+ (dateReception != null ? "dateReception: " + dateReception + "\n" : "")
				+ (dateSend != null ? "dateSend: " + dateSend + "\n" : "") + "duration: " + duration + "\nstatus: "
				+ status + "\n" + (host != null ? "host: " + host + "\n" : "")
				+ (timeReception != null ? "timeReception: " + timeReception : "");
	}
	
	

	
}
