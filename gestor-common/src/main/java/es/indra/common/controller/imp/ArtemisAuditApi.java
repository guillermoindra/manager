package es.indra.common.controller.imp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import es.indra.common.controller.AuditApi;
import es.indra.common.model.AuditModel;

@Service
@Profile("!rabbitmq")
public class ArtemisAuditApi implements AuditApi{
	
	@Autowired
	private Environment env;
	
	Logger LOGGER = LoggerFactory.getLogger(ArtemisAuditApi.class);

	private final JmsTemplate jmsTemplate;
	
	//@Value( "${artemis.queue}" )
	//static String artemisQueue;
	
	public ArtemisAuditApi(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

	public void sendAudit(AuditModel model) {
		try {
			System.out.println("Artemis");
			LOGGER.info("Receive the message: {} ", model.toString());
			jmsTemplate.convertAndSend(env.getProperty("artemis.queue"), model);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	
}
