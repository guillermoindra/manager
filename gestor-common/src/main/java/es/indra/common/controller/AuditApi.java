package es.indra.common.controller;

import es.indra.common.model.AuditModel;

public interface AuditApi {
	public void sendAudit(AuditModel model);
}
