package es.indra.operator.client.api.services;

import java.util.Date;
import java.util.List;

import es.indra.operator.client.api.bdModel.GenericFilterDTO;
import es.indra.operator.client.api.bdModel.SigtOpLstNotif;

/**
 * Interface for Lst Service.
 * 
 * @author acoboj
 */
public interface LstService {
	
	
	void saveSigtOpLstNotif(SigtOpLstNotif sigtOpLstNotif) throws Exception;
	
	/**
	 * Find SigtOpLstNotif list.
	 * 
	 * @param filter - SigtOpLstNotif filter
	 * @return list of SigtOpLstNotif for process
	 * @throws Exception - the exception
	 */
	List<SigtOpLstNotif> findSigtOpLstNotif(GenericFilterDTO filter) throws Exception;
	
	/**
	 * Update the SigtOpLstNotif object in database.
	 * 
	 * @param sigtOpLstNotif - the object dto arg
	 * @throws Exception - the exception
	 */
	void update(SigtOpLstNotif sigtOpLstNotif) throws Exception;
	
	/**
	 * Read ws of detail list and save in database.
	 * 
	 * @param sigtOpLstNotif - the object dto arg
	 * @param currentDate - Current date
	 * @return auditMessage - Audit message
	 * @throws Exception - the exception
	 */
	String readAndSaveLstDetails(SigtOpLstNotif sigtOpLstNotif, Date currentDate) throws Exception;
	
	/**
	 * Read detail list error from database and send by ws.
	 * 
	 * @param sigtOpLstNotif - the object dto arg	 
	 * @param currentDate - Current date
	 * @return auditMessage - Audit message
	 * @throws Exception - the exception
	 */
	String notificationLstDetailsError(SigtOpLstNotif sigtOpLstNotif, Date currentDate)throws Exception;
	
	/**
	 * Find from database SigtOpLstNotif with details error.
	 * 
	 * @param filter - the object dto arg
	 * @return list of SigtOpLstNotif for process
	 * @throws Exception - the exception
	 */
	List<SigtOpLstNotif> findSigtOpLstNotifWithErrors(GenericFilterDTO filter)throws Exception;
}
