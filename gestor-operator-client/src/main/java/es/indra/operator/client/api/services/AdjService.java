package es.indra.operator.client.api.services;

import java.util.Date;
import java.util.List;

import es.indra.operator.client.api.bdModel.GenericFilterDTO;
import es.indra.operator.client.api.bdModel.SigtOpTrxAdjPend;

/**
 * Interface for Transactions Service.
 * 
 * @author rharo
 */
public interface AdjService {
	
	/**
	 * Find adjustment list pending for send.
	 * 
	 * @param adjPendFilterArg - adjustment filter
	 * @return list of adjustment for send
	 * @throws Exception - the exception
	 */
	List<SigtOpTrxAdjPend> findAdjPend(GenericFilterDTO adjPendFilterArg) throws Exception;;

	/**
	 * Send pending adjustment to intermediador.
	 * 
	 * @param sigtOpTrxAdjPendList - list of adjustment for send 
	 * @param currentDate - Current date
	 * @return auditMessage - Audit message
	 * @throws Exception - the exception
	 */
	String sendAdjustments(List<SigtOpTrxAdjPend> sigtOpTrxAdjPendList, Date currentDate) throws Exception;
	
}