package es.indra.operator.client.api.services;

import es.indra.operator.client.api.bdModel.Audit;

/**
 * Interface for Base Service.
 * 
 * @author acoboj
 */
public interface BaseService {
	
	/**
	 * Register auditory operations.
	 * 
	 * @param audit - Audit object with auditory information
	 * 
	 * @throws Exception - the exception
	 */
	void registerAudit(Audit audit) throws Exception;
	
	/**
	 * Get descriptor information by id.
	 * 
	 * @param id - Id filter
	 * @param c - Class of returned object
	 * @return object - Returned object
	 * @throws Exception - the exception
	 */
	Object getDescriptorsById(Integer id, Class<?> c) throws Exception;
	
	
	/**
	 * Get descriptor information by external code.
	 * 
	 * @param externalCode - External code filter
	 * @param c - Class of returned object
	 * @return object - Returned object
	 * @throws Exception - the exception
	 */
	Object getDescriptorsByExternalCode(String externalCode, Class<?> c) throws Exception;
}
