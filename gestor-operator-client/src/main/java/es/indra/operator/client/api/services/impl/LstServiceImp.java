package es.indra.operator.client.api.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.joda.time.DateTime;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import es.indra.operator.client.api.DefaultApi;
import es.indra.operator.client.api.bdModel.GenericFilterDTO;
import es.indra.operator.client.api.bdModel.SigtCodeError;
import es.indra.operator.client.api.bdModel.SigtDscBalanceStatus;
import es.indra.operator.client.api.bdModel.SigtDscCodAgency;
import es.indra.operator.client.api.bdModel.SigtDscLstModality;
import es.indra.operator.client.api.bdModel.SigtDscTagStatus;
import es.indra.operator.client.api.bdModel.SigtDscVehicleClass;
import es.indra.operator.client.api.bdModel.SigtOpDscLstNotifStatus;
import es.indra.operator.client.api.bdModel.SigtOpLstDtlPend;
import es.indra.operator.client.api.bdModel.SigtOpLstDtlPro;
import es.indra.operator.client.api.bdModel.SigtOpLstNotif;
import es.indra.operator.client.api.services.LstService;
import es.indra.operator.client.api.utils.GenericConstant;
import es.indra.operator.client.logger.LogController;
import es.indra.operator.client.model.ErrorUsuario;
import es.indra.operator.client.model.ErroresProcesamientoLista;
import es.indra.operator.client.model.NotificacionErrorProcesamientoListas;
import es.indra.operator.client.model.Usuario;



/**
 * This class is the implementation of the LstService interface.
 * 
 * @author acoboj
 */
@Transactional
public class LstServiceImp extends BaseServiceImp implements LstService {

	private final static DefaultApi api = new DefaultApi();

	

	@Override
	public void saveSigtOpLstNotif(SigtOpLstNotif sigtOpLstNotif) throws Exception {
		//lstDao.save(sigtOpLstNotif);
	}

	@Override
	public List<SigtOpLstNotif> findSigtOpLstNotif(GenericFilterDTO filter) throws Exception {
		return null; //lstDao.findSigtOpLstNotif(filter);
	}
	
	@Override
	public List<SigtOpLstNotif> findSigtOpLstNotifWithErrors(GenericFilterDTO filter) throws Exception {
		return null; //lstDao.findSigtOpLstNotifWithErrors(filter);
	}

	@Override
	public void update(SigtOpLstNotif sigtOpLstNotif) throws Exception {
		//lstDao.update(sigtOpLstNotif);
	}

	@Override
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public String readAndSaveLstDetails(SigtOpLstNotif sigtOpLstNotif, Date currentDate) throws Exception {

		String auditMessage = new String();
		auditMessage += sigtOpLstNotif.getIdList() + "\n";
		String codigoListaUsuario = sigtOpLstNotif.getIdList();
		LogController.getLog().debug("Main.v1ListasUsuariosCodigoListaUsuarioDetallesGetTest.readAndSaveLstDetails: Getting user list detail...");

		//FIXME Modificar ip por parametro recibido
		List<Usuario> response = api.v1ListasUsuariosCodigoListaUsuarioDetallesGet(codigoListaUsuario,"localhost");
		
		LogController.getLog().debug("Main.v1ListasUsuariosCodigoListaUsuarioDetallesGetTest.readAndSaveLstDetails: User list detail received.");

//		List<Usuario> response = new ArrayList<Usuario>(); //= api.v1ListasUsuariosCodigoListaUsuarioDetallesGet(codigoListaUsuario);
//		Usuario testUser = new Usuario ();
//		testUser.setCodigoIntermediador(10000);
//		testUser.setPlaca("1234ABC");
//		testUser.setTid("TID1");
//		testUser.setEpc("EPC1");		
//		testUser.setCategoria(Usuario.CategoriaEnum.A);
//		testUser.setEstado(1);
//		testUser.setEstadoSaldo(1);
//		testUser.setSaldo(new BigDecimal(1500));
//		testUser.setSaldoBajo(true);
//		testUser.setNumeroCliente("cliente1");
//		testUser.setModalidad(Usuario.ModalidadEnum.POSPAGO);
//		testUser.setVersion(new DateTime(currentDate));
//		response.add(testUser);
		
		if(response != null && !response.isEmpty()){
			for (Usuario usuario : response) {	
				SigtDscTagStatus sigtDscTagStatus = (SigtDscTagStatus) getDescriptorsByExternalCode(
						usuario.getEstado().toString(), SigtDscTagStatus.class);
				SigtDscCodAgency sigtDscCodAgency = (SigtDscCodAgency) getDescriptorsByExternalCode(
						usuario.getCodigoIntermediador().toString(), SigtDscCodAgency.class);
				SigtDscLstModality sigtDscLstModality = (SigtDscLstModality) getDescriptorsByExternalCode(
						usuario.getModalidad().toString(), SigtDscLstModality.class);
				SigtDscBalanceStatus sigtDscBalanceStatus = (SigtDscBalanceStatus) getDescriptorsByExternalCode(
						usuario.getEstadoSaldo().toString(), SigtDscBalanceStatus.class);
				SigtDscVehicleClass sigtDscLstVehicleClass = (SigtDscVehicleClass) getDescriptorsByExternalCode(
						usuario.getCategoria().toString(), SigtDscVehicleClass.class);
	
				SigtOpLstDtlPend sigtOpLstDtlPend = new SigtOpLstDtlPend(null, sigtDscTagStatus, sigtDscCodAgency,
						sigtDscLstModality, sigtDscBalanceStatus, sigtOpLstNotif, sigtDscLstVehicleClass,
						usuario.getPlaca(), usuario.getTid(), usuario.getEpc(), usuario.getSaldo(), usuario.getSaldoBajo(),
						usuario.getNumeroCliente(), usuario.getVersion().toDate(), currentDate,
						GenericConstant.MODIFICATION_USER, GenericConstant.APPLICATION_ID, currentDate, (byte) 0, (byte) 1);
				//lstDao.save(sigtOpLstDtlPend);
				auditMessage += usuario + "\n";	
			}

			SigtOpDscLstNotifStatus sigtOpDscLstNotifStatus = new SigtOpDscLstNotifStatus(
					GenericConstant.SigtOpDscLstNotifStatus_SOLICITADA, "");
			sigtOpLstNotif.setSigtOpDscLstNotifStatus(sigtOpDscLstNotifStatus);
			sigtOpLstNotif.setModificationUser(GenericConstant.MODIFICATION_USER);
			sigtOpLstNotif.setApplication(GenericConstant.APPLICATION_ID);
			sigtOpLstNotif.setModificationDate(currentDate);
			sigtOpLstNotif.setModification((byte) 1);
			sigtOpLstNotif.setOptimistLock((byte) (sigtOpLstNotif.getOptimistLock() + 1));
			update(sigtOpLstNotif);
		}
		return auditMessage;
	}

	@Override
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public String notificationLstDetailsError(SigtOpLstNotif sigtOpLstNotif, Date currentDate) throws Exception {
		String auditMessage = new String();
		
		Set<SigtOpLstDtlPro> sigtOpLstDtlPros = sigtOpLstNotif.getSigtOpLstDtlPros();
			
		String codigoListaUsuario = sigtOpLstNotif.getIdList();
		NotificacionErrorProcesamientoListas body = new NotificacionErrorProcesamientoListas();
		body.setCodigoLista(sigtOpLstNotif.getIdList());
		body.setCodigoOperador(GenericConstant.OPERATOR_CODE);
		ErroresProcesamientoLista erroresProcesamientoList = new ErroresProcesamientoLista();
		erroresProcesamientoList.setCantidadAprobados(sigtOpLstNotif.getNumAccepted());
		erroresProcesamientoList.setCantidadRechazados(sigtOpLstNotif.getNumRejected());
		erroresProcesamientoList.setCantidadRegistros(sigtOpLstNotif.getNumRecord());
		erroresProcesamientoList.setCodigoLista(sigtOpLstNotif.getIdList());
		List<ErrorUsuario> errores = new ArrayList<ErrorUsuario>();
		erroresProcesamientoList.setErrores(errores);
		body.setErrores(erroresProcesamientoList);

		for (SigtOpLstDtlPro sigtOpLstDtlPro : sigtOpLstDtlPros) {
			ErrorUsuario errorUsuario = new ErrorUsuario();
			
			SigtCodeError sigtCodeError = (SigtCodeError) super.getDescriptorsById(
					sigtOpLstDtlPro.getSigtOpLstDtlErr().getSigtCodeError().getId(), SigtCodeError.class);
			
			errorUsuario.setCodigoRespuesta(new Integer(sigtCodeError.getExternalCode()));
			errorUsuario.setDescripcion(sigtCodeError.getDescription());
			errorUsuario.setPlaca(sigtOpLstDtlPro.getPlate());
			errorUsuario.setTid(sigtOpLstDtlPro.getTid());
			errorUsuario.setVersion(new DateTime(sigtOpLstDtlPro.getVersionDate()));
			
			errores.add(errorUsuario);
		}		
				
		SigtOpDscLstNotifStatus sigtOpDscLstNotifStatus = new SigtOpDscLstNotifStatus(
				GenericConstant.SigtOpDscLstNotifStatus_CERRADA, "");
		SigtOpLstNotif sigtOpLstNotifToUpdate = null; //(SigtOpLstNotif) lstDao.findByPK(sigtOpLstNotif.getIdentifier(), SigtOpLstNotif.class);
		sigtOpLstNotifToUpdate.setSigtOpDscLstNotifStatus(sigtOpDscLstNotifStatus);
		sigtOpLstNotifToUpdate.setModificationUser(GenericConstant.MODIFICATION_USER);
		sigtOpLstNotifToUpdate.setApplication(GenericConstant.APPLICATION_ID);
		sigtOpLstNotifToUpdate.setModificationDate(currentDate);
		sigtOpLstNotifToUpdate.setModification((byte) 1);
		sigtOpLstNotifToUpdate.setOptimistLock((byte) (sigtOpLstNotif.getOptimistLock() + 1));
		update(sigtOpLstNotifToUpdate);
		
		//FIXME Modificar ip por parametro recibido
		api.v1ListasUsuariosCodigoListaUsuarioNotificarErroresProcesamientoPut(codigoListaUsuario, body,"localhost");
		auditMessage += body.toString();
		
		return auditMessage;
	}

	
}
