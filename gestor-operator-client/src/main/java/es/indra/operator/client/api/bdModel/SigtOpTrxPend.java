package es.indra.operator.client.api.bdModel;

import java.math.BigDecimal;
import java.util.Date;

/**
 * SigtOpTrxPend class, implements the object for transactions pending to send.
 * 
 */
public class SigtOpTrxPend implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private String identifier;
	private SigtDscVehicleClass sigtDscVehicleClassByChargedClass;
	private SigtOpToIntNotif sigtOpToIntNotif;
	private SigtDscPlaces sigtDscPlacesByLaneId;
	private SigtOpTrxDscStatus sigtOpTrxDscStatus;
	private SigtDscVehicleClass sigtDscVehicleClassByDetectedClass;
	private SigtDscCodAgency sigtDscCodAgency;
	private SigtDscPlaces sigtDscPlacesByStationId;
	private SigtTrxDscReadType sigtTrxDscReadType;
	private Long trxId;
	private Date trxDate;
	private Date trxCollectionDate;
	private String clientId;
	private String plate;
	private String tid;
	private String epc;
	private BigDecimal amount;
	private Integer numAxles;
	private Integer numDoubleWheel;
	private String direction;
	private String plateOcr;
	private Boolean isDiscrepancy;
	private Date listDate;
	private String idList;
	private Boolean isPlateDiscrepancy;
	private Date creationDate;
	private String modificationUser;
	private Integer application;
	private Date modificationDate;
	private Byte modification;
	private Byte optimistLock;

	public SigtOpTrxPend() {
	}

	public SigtOpTrxPend(String identifier, SigtOpTrxDscStatus sigtOpTrxDscStatus, SigtDscCodAgency sigtDscCodAgency,
			SigtDscPlaces sigtDscPlacesByStationId, Long trxId, Date trxDate, Date trxCollectionDate, String tid,
			String epc, BigDecimal amount, Integer numAxles, Integer numDoubleWheel, Boolean isDiscrepancy,
			Boolean isPlateDiscrepancy, Date creationDate, Integer application, Date modificationDate, Byte modification,
			Byte optimistLock) {
		this.identifier = identifier;
		this.sigtOpTrxDscStatus = sigtOpTrxDscStatus;
		this.sigtDscCodAgency = sigtDscCodAgency;
		this.sigtDscPlacesByStationId = sigtDscPlacesByStationId;
		this.trxId = trxId;
		this.trxDate = trxDate;
		this.trxCollectionDate = trxCollectionDate;
		this.tid = tid;
		this.epc = epc;
		this.amount = amount;
		this.numAxles = numAxles;
		this.numDoubleWheel = numDoubleWheel;
		this.isDiscrepancy = isDiscrepancy;
		this.isPlateDiscrepancy = isPlateDiscrepancy;
		this.creationDate = creationDate;
		this.application = application;
		this.modificationDate = modificationDate;
		this.modification = modification;
		this.optimistLock = optimistLock;
	}

	public SigtOpTrxPend(String identifier, SigtDscVehicleClass sigtDscVehicleClassByChargedClass,
			SigtOpToIntNotif sigtOpToIntNotif, SigtDscPlaces sigtDscPlacesByLaneId,
			SigtOpTrxDscStatus sigtOpTrxDscStatus, SigtDscVehicleClass sigtDscVehicleClassByDetectedClass,
			SigtDscCodAgency sigtDscCodAgency, SigtDscPlaces sigtDscPlacesByStationId,
			SigtTrxDscReadType sigtTrxDscReadType, Long trxId, Date trxDate, Date trxCollectionDate, String clientId,
			String plate, String tid, String epc, BigDecimal amount, Integer numAxles, Integer numDoubleWheel, String direction,
			String plateOcr, Boolean isDiscrepancy, Date listDate, String idList, Boolean isPlateDiscrepancy,
			Date creationDate, String modificationUser, Integer application, Date modificationDate, Byte modification,
			Byte optimistLock) {
		this.identifier = identifier;
		this.sigtDscVehicleClassByChargedClass = sigtDscVehicleClassByChargedClass;
		this.sigtOpToIntNotif = sigtOpToIntNotif;
		this.sigtDscPlacesByLaneId = sigtDscPlacesByLaneId;
		this.sigtOpTrxDscStatus = sigtOpTrxDscStatus;
		this.sigtDscVehicleClassByDetectedClass = sigtDscVehicleClassByDetectedClass;
		this.sigtDscCodAgency = sigtDscCodAgency;
		this.sigtDscPlacesByStationId = sigtDscPlacesByStationId;
		this.sigtTrxDscReadType = sigtTrxDscReadType;
		this.trxId = trxId;
		this.trxDate = trxDate;
		this.trxCollectionDate = trxCollectionDate;
		this.clientId = clientId;
		this.plate = plate;
		this.tid = tid;
		this.epc = epc;
		this.amount = amount;
		this.numAxles = numAxles;
		this.numDoubleWheel = numDoubleWheel;
		this.direction = direction;
		this.plateOcr = plateOcr;
		this.isDiscrepancy = isDiscrepancy;
		this.listDate = listDate;
		this.idList = idList;
		this.isPlateDiscrepancy = isPlateDiscrepancy;
		this.creationDate = creationDate;
		this.modificationUser = modificationUser;
		this.application = application;
		this.modificationDate = modificationDate;
		this.modification = modification;
		this.optimistLock = optimistLock;
	}

	public String getIdentifier() {
		return this.identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public SigtDscVehicleClass getSigtDscVehicleClassByChargedClass() {
		return this.sigtDscVehicleClassByChargedClass;
	}

	public void setSigtDscVehicleClassByChargedClass(SigtDscVehicleClass sigtDscVehicleClassByChargedClass) {
		this.sigtDscVehicleClassByChargedClass = sigtDscVehicleClassByChargedClass;
	}

	public SigtOpToIntNotif getSigtOpToIntNotif() {
		return this.sigtOpToIntNotif;
	}

	public void setSigtOpToIntNotif(SigtOpToIntNotif sigtOpToIntNotif) {
		this.sigtOpToIntNotif = sigtOpToIntNotif;
	}

	public SigtDscPlaces getSigtDscPlacesByLaneId() {
		return this.sigtDscPlacesByLaneId;
	}

	public void setSigtDscPlacesByLaneId(SigtDscPlaces sigtDscPlacesByLaneId) {
		this.sigtDscPlacesByLaneId = sigtDscPlacesByLaneId;
	}

	public SigtOpTrxDscStatus getSigtOpTrxDscStatus() {
		return this.sigtOpTrxDscStatus;
	}

	public void setSigtOpTrxDscStatus(SigtOpTrxDscStatus sigtOpTrxDscStatus) {
		this.sigtOpTrxDscStatus = sigtOpTrxDscStatus;
	}

	public SigtDscVehicleClass getSigtDscVehicleClassByDetectedClass() {
		return this.sigtDscVehicleClassByDetectedClass;
	}

	public void setSigtDscVehicleClassByDetectedClass(SigtDscVehicleClass sigtDscVehicleClassByDetectedClass) {
		this.sigtDscVehicleClassByDetectedClass = sigtDscVehicleClassByDetectedClass;
	}

	public SigtDscCodAgency getSigtDscCodAgency() {
		return this.sigtDscCodAgency;
	}

	public void setSigtDscCodAgency(SigtDscCodAgency sigtDscCodAgency) {
		this.sigtDscCodAgency = sigtDscCodAgency;
	}

	public SigtDscPlaces getSigtDscPlacesByStationId() {
		return this.sigtDscPlacesByStationId;
	}

	public void setSigtDscPlacesByStationId(SigtDscPlaces sigtDscPlacesByStationId) {
		this.sigtDscPlacesByStationId = sigtDscPlacesByStationId;
	}

	public SigtTrxDscReadType getSigtTrxDscReadType() {
		return this.sigtTrxDscReadType;
	}

	public void setSigtTrxDscReadType(SigtTrxDscReadType sigtTrxDscReadType) {
		this.sigtTrxDscReadType = sigtTrxDscReadType;
	}

	public Long getTrxId() {
		return this.trxId;
	}

	public void setTrxId(Long trxId) {
		this.trxId = trxId;
	}

	public Date getTrxDate() {
		return this.trxDate;
	}

	public void setTrxDate(Date trxDate) {
		this.trxDate = trxDate;
	}

	public Date getTrxCollectionDate() {
		return this.trxCollectionDate;
	}

	public void setTrxCollectionDate(Date trxCollectionDate) {
		this.trxCollectionDate = trxCollectionDate;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getPlate() {
		return this.plate;
	}

	public void setPlate(String plate) {
		this.plate = plate;
	}

	public String getTid() {
		return this.tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getEpc() {
		return this.epc;
	}

	public void setEpc(String epc) {
		this.epc = epc;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getNumAxles() {
		return this.numAxles;
	}

	public void setNumAxles(Integer numAxles) {
		this.numAxles = numAxles;
	}

	public Integer getNumDoubleWheel() {
		return this.numDoubleWheel;
	}

	public void setNumDoubleWheel(Integer numDoubleWheel) {
		this.numDoubleWheel = numDoubleWheel;
	}

	public String getDirection() {
		return this.direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getPlateOcr() {
		return this.plateOcr;
	}

	public void setPlateOcr(String plateOcr) {
		this.plateOcr = plateOcr;
	}

	public Boolean isIsDiscrepancy() {
		return this.isDiscrepancy;
	}

	public void setIsDiscrepancy(Boolean isDiscrepancy) {
		this.isDiscrepancy = isDiscrepancy;
	}

	public Date getListDate() {
		return this.listDate;
	}

	public void setListDate(Date listDate) {
		this.listDate = listDate;
	}

	public String getIdList() {
		return this.idList;
	}

	public void setIdList(String idList) {
		this.idList = idList;
	}

	public Boolean isIsPlateDiscrepancy() {
		return this.isPlateDiscrepancy;
	}

	public void setIsPlateDiscrepancy(Boolean isPlateDiscrepancy) {
		this.isPlateDiscrepancy = isPlateDiscrepancy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getModificationUser() {
		return this.modificationUser;
	}

	public void setModificationUser(String modificationUser) {
		this.modificationUser = modificationUser;
	}

	public Integer getApplication() {
		return this.application;
	}

	public void setApplication(Integer application) {
		this.application = application;
	}

	public Date getModificationDate() {
		return this.modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Byte getModification() {
		return this.modification;
	}

	public void setModification(Byte modification) {
		this.modification = modification;
	}

	public Byte getOptimistLock() {
		return this.optimistLock;
	}

	public void setOptimistLock(Byte optimistLock) {
		this.optimistLock = optimistLock;
	}

}
