package es.indra.operator.client.api.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import es.indra.operator.client.api.DefaultApi;
import es.indra.operator.client.api.bdModel.GenericFilterDTO;
import es.indra.operator.client.api.bdModel.SigtDscCodAgency;
import es.indra.operator.client.api.bdModel.SigtDscNotifType;
import es.indra.operator.client.api.bdModel.SigtOpToIntNotif;
import es.indra.operator.client.api.bdModel.SigtOpTrxAdjDscStatus;
import es.indra.operator.client.api.bdModel.SigtOpTrxAdjPend;
import es.indra.operator.client.api.services.AdjService;
import es.indra.operator.client.api.utils.GenericConstant;
import es.indra.operator.client.model.Ajuste;

/**
 * This class is the implementation of the TRXService interface.
 * 
 * @author rharo
 */
@Transactional
public class AdjServiceImp extends BaseServiceImp implements AdjService {

	private static final int MIN_LENGTH_ID_TRX_ADJUSTMENT = 27;
	
	private final static DefaultApi api = new DefaultApi();

	/////

	@Override
	public List<SigtOpTrxAdjPend> findAdjPend(GenericFilterDTO adjPendFilterArg) throws Exception {
		return null; //adjDao.findAdjPend(adjPendFilterArg);
	}

	@Override
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public String sendAdjustments(List<SigtOpTrxAdjPend> sigtOpTrxAdjPendList, Date currentDate) throws Exception {

		String auditMessage = new String();

		// notification data
		SigtOpToIntNotif sigtOpToIntNotif = createIntNotif(sigtOpTrxAdjPendList, currentDate);
		List<Ajuste> body = new ArrayList<Ajuste>();

		for (SigtOpTrxAdjPend sigtOpTrxAdjPend : sigtOpTrxAdjPendList) {
			String idStr = sigtOpTrxAdjPend.getIdentifier();
			while (idStr.length() < MIN_LENGTH_ID_TRX_ADJUSTMENT){
				idStr = "0"+idStr;				
			}
			// Create Ajuste Object, and sets data
			Ajuste ajuste = new Ajuste();
			ajuste.setCodigoAjuste(GenericConstant.OPERATOR_CODE + "_" + idStr);
			ajuste.setCodigoOperador(Integer.parseInt(sigtOpTrxAdjPend.getSigtDscCodAgency().getExternalCode()));
			ajuste.setFechaHora(new DateTime(sigtOpTrxAdjPend.getTrxDate()));
			ajuste.setFechaRecaudo(new DateTime(sigtOpTrxAdjPend.getTrxCollectionDate()));
			ajuste.setCodigoPasoReferencia(GenericConstant.OPERATOR_CODE + "_" + idStr);
			ajuste.setMotivoAjuste(Integer.parseInt(sigtOpTrxAdjPend.getSigtTrxAdjDscReason().getExternalCode()));
			ajuste.setValorAjuste(sigtOpTrxAdjPend.getAdjustmentAmount());
			ajuste.setValor(sigtOpTrxAdjPend.getAmount());
			ajuste.setCategoria((sigtOpTrxAdjPend.getSigtDscVehicleClass() != null)
					? sigtOpTrxAdjPend.getSigtDscVehicleClass().getExternalCode() : null);
			ajuste.setTipoOperacion(Integer.parseInt(sigtOpTrxAdjPend.getSigtTrxAdjDscOptType().getExternalCode()));

			body.add(ajuste);

		}

		// Update transactions with status 2, and idNotif
		updateSigtOpTrxAdjPend(sigtOpTrxAdjPendList,sigtOpToIntNotif,currentDate);

		//FIXME Modificar ip por parametro recibido
		api.v1AjustesPost(body,"localhost");
		
		auditMessage += body.toString();

		return auditMessage;

	}

	/**
	 * Update  SigtOpTrxPend objects in database.
	 * 
	 * @param sigtOpTrxAdjPendList - SigtOpTrxAdjPend list
	 * @param sigtOpToIntNotifArg - notification object
	 * @param currentDateArg - current date
	 * @throws Exception - the exception
	 */
	private void updateSigtOpTrxAdjPend(List<SigtOpTrxAdjPend> sigtOpTrxAdjPendList, SigtOpToIntNotif sigtOpToIntNotifArg, Date currentDateArg) throws Exception {
		
		SigtOpTrxAdjDscStatus sigtOpTrxAdjDscStatus = (SigtOpTrxAdjDscStatus) super.getDescriptorsById(
				GenericConstant.SigtOpTrxAdjDscStatus_ENVIADO, SigtOpTrxAdjDscStatus.class);
		
		for(SigtOpTrxAdjPend sigtOpTrxAdjPend : sigtOpTrxAdjPendList){
			sigtOpTrxAdjPend.setSigtOpTrxAdjDscStatus(sigtOpTrxAdjDscStatus);
			sigtOpTrxAdjPend.setModificationUser(GenericConstant.MODIFICATION_USER); 
			sigtOpTrxAdjPend.setApplication(GenericConstant.APPLICATION_ID); 
			sigtOpTrxAdjPend.setModificationDate(currentDateArg);    
			sigtOpTrxAdjPend.setModification((byte) 1); 
			sigtOpTrxAdjPend.setOptimistLock((byte) (sigtOpTrxAdjPend.getOptimistLock() + 1)); 
			sigtOpTrxAdjPend.setSigtOpToIntNotif(sigtOpToIntNotifArg);
			//adjDao.update(sigtOpTrxAdjPend);	
		}
	}

	/**
	 * Method that create a notification.
	 * 
	 * @param sigtOpTrxAdjPendList - list status adjustments
	 * @param currentDateArg - current date
	 * @return SigtOpToIntNotif object
	 */
	private SigtOpToIntNotif createIntNotif(List<SigtOpTrxAdjPend> sigtOpTrxAdjPendList, Date currentDateArg)
			throws Exception {

		SigtDscNotifType sigtDscNotifType = (SigtDscNotifType) super.getDescriptorsById(
				GenericConstant.SigtDscNotifType_NOTIFICACION_AJUSTES, SigtDscNotifType.class);
		
		SigtDscCodAgency sigtDscCodAgency = (SigtDscCodAgency) super.getDescriptorsByExternalCode(
				GenericConstant.OPERATOR_CODE.toString(), SigtDscCodAgency.class);

		SigtOpToIntNotif sigtOpToIntNotif = new SigtOpToIntNotif();
		sigtOpToIntNotif.setSigtDscNotifType(sigtDscNotifType);
		sigtOpToIntNotif.setSigtDscCodAgency(sigtDscCodAgency);
		sigtOpToIntNotif.setNumRecords(sigtOpTrxAdjPendList.size());
		sigtOpToIntNotif.setApplication(GenericConstant.APPLICATION_ID);
		sigtOpToIntNotif.setModificationUser(GenericConstant.MODIFICATION_USER);
		sigtOpToIntNotif.setCreationDate(currentDateArg);
		sigtOpToIntNotif.setModificationDate(currentDateArg);
		sigtOpToIntNotif.setModification((byte) 0);
		sigtOpToIntNotif.setOptimistLock((byte) 0);

		Long idenNotif = null; //(Long) adjDao.save(sigtOpToIntNotif);
		sigtOpToIntNotif.setIdentifier(idenNotif);

		return sigtOpToIntNotif;
	}

	
}