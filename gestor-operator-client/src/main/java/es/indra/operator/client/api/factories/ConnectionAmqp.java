package es.indra.operator.client.api.factories;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.artemis.jms.client.ActiveMQConnectionFactory;
import org.springframework.stereotype.Component;

@Component
public class ConnectionAmqp {
	
	private Session session;
	private MessageProducer producer;
	private Destination tempDest;
	
	public ConnectionAmqp() {
		createConnectionFactory();
	}
	
	public Session getSession() {
		return session;
	}

	public MessageProducer getProducer() {
		return producer;
	}

	public Destination getTempDest() {
		return tempDest;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public void setProducer(MessageProducer producer) {
		this.producer = producer;
	}

	public void setTempDest(Destination tempDest) {
		this.tempDest = tempDest;
	}

	private void createConnectionFactory() {
		if (session == null) {
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://192.168.210.130:61616",
					"admin", "admin");

			int ackMode;
			String clientQueueName;
			boolean transacted = false;

			clientQueueName = "audit-queue";
			ackMode = Session.AUTO_ACKNOWLEDGE;

			Connection connection;
			try {
				connection = connectionFactory.createConnection();
				connection.start();
				session = connection.createSession(transacted, ackMode);
				Destination adminQueue = session.createQueue(clientQueueName);

				// Setup a message producer to send message to the queue the server is consuming
				// from
				producer = session.createProducer(adminQueue);
				producer.setDeliveryMode(DeliveryMode.PERSISTENT);

				// Create a temporary queue that this client will listen for responses on then
				// create a consumer
				// that consumes message from this temporary queue...for a real application a
				// client should reuse
				// the same temp queue for each message to the server...one temp queue per
				// client
				tempDest = session.createTemporaryQueue();
				MessageConsumer responseConsumer = session.createConsumer(tempDest);

				// This class will handle the messages to the temp queue as well
				responseConsumer.setMessageListener(new MessageListener() {

					public void onMessage(Message message) {
						String messageText = null;
						try {
							if (message instanceof TextMessage) {
								TextMessage textMessage = (TextMessage) message;
								messageText = textMessage.getText();
								System.out.println("messageText = " + messageText);
							} else if (message instanceof ObjectMessage) {
								ObjectMessage textMessage = (ObjectMessage) message;
								messageText = (String) textMessage.getObject();
								System.out.println("messageObject = " + messageText);
							}
						} catch (JMSException e) {
							// Handle the exception appropriately
						}
					}
				});

			} catch (JMSException e) {
				connectionFactory.close();
				// Handle the exception appropriately
				e.printStackTrace();
			}
		}

	}
}
