package es.indra.operator.client.main;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.indra.operator.client.api.DefaultApi;
import es.indra.operator.client.api.bdModel.Audit;
import es.indra.operator.client.api.bdModel.GenericFilterDTO;
import es.indra.operator.client.api.bdModel.SigtOpLstNotif;
import es.indra.operator.client.api.bdModel.SigtOpTrxAdjPend;
import es.indra.operator.client.api.bdModel.SigtOpTrxNegPassPend;
import es.indra.operator.client.api.bdModel.SigtOpTrxPend;
import es.indra.operator.client.api.bdModel.SigtOpTrxTestNotif;
import es.indra.operator.client.api.services.AdjService;
import es.indra.operator.client.api.services.BaseService;
import es.indra.operator.client.api.services.LstService;
import es.indra.operator.client.api.services.TrxService;
import es.indra.operator.client.api.utils.GenericConstant;
import es.indra.operator.client.logger.LogController;
import es.indra.operator.client.model.ListaUsuario;
import es.indra.operator.client.model.Paso;

public class Main {

	private static DefaultApi api = null;
	static BaseService baseService;
	static LstService lstService;
	static TrxService trxService;
	static AdjService adjService;

	public static void main(String[] args) throws Exception {

		/*baseService = (BaseService) Factory.getBean("baseService");
		lstService = (LstService) Factory.getBean("lstService");
		trxService = (TrxService) Factory.getBean("trxService");
		adjService = (AdjService) Factory.getBean("adjService");*/
		api = new DefaultApi("localhost");


		MainWindows mainWindows = new MainWindows();
		mainWindows.setVisible(true);

	}

	/**
	 * Consultar los detalles (usuarios) de la lista solicitada.
	 * 
	 * @throws Exception
	 *             exception
	 */
	public static void v1ListasUsuariosCodigoListaUsuarioDetallesGetTest() throws Exception {
		Thread thread = new Thread() {
			public void run() {
				while (true) {
					String auditMessage = new String();	
					Date currentDate = new Date();

					try {
						GenericFilterDTO filter = new GenericFilterDTO();
						filter.setStatus(GenericConstant.SigtOpDscLstNotifStatus_PENDIENTE_SOLICITAR);
						List<SigtOpLstNotif> findSigtOpLstNotif = lstService.findSigtOpLstNotif(filter);
						if (findSigtOpLstNotif != null) {
							for (SigtOpLstNotif sigtOpLstNotif : findSigtOpLstNotif) {
								// Transactional operation
								auditMessage += lstService.readAndSaveLstDetails(sigtOpLstNotif, currentDate);								
							}
							
						}else{
							Thread.sleep(GenericConstant.SLEEP_TIME);
						}
					} catch (Exception e) {
						LogController.getLog()
								.error("Main.v1ListasUsuariosCodigoListaUsuarioDetallesGetTest: General error occurred during execution!. "
										+ e.getMessage());
						auditMessage += "General error occurred during execution!. " + e.getMessage();
						
					}finally {
						Audit audit = new Audit(currentDate, "v1ListasUsuariosCodigoListaUsuarioDetallesGetTest", auditMessage);
						try {
							baseService.registerAudit(audit);
						} catch (Exception e) {
							LogController.getLog().info("RegisterAudit: Error saving audit.");

						}						
					}	
				}
			};
		};
		thread.start();
	}

	/**
	 * Notificar errores de procesamiento de una lista de usuario.
	 *
	 * Permite que el operador informe al intermediador del procesamiento de una
	 * lista de usuario. Esta operación solo se llama si el operador presenta
	 * errores en el procesamiento.
	 * 
	 * @throws Exception
	 *             exception
	 */
	public static void v1ListasUsuariosCodigoListaUsuarioNotificarErroresProcesamientoPutTest() throws Exception {
		Thread thread = new Thread() {
			public void run() {					
				while (true) {
					String auditMessage = new String();	
					Date currentDate = new Date();
					
					try {
						GenericFilterDTO filter = new GenericFilterDTO();
						filter.setStatus(GenericConstant.SigtOpDscLstNotifStatus_PENDIENTE_INFORMAR_RECHAZOS);
						List<SigtOpLstNotif> findSigtOpLstNotif = lstService.findSigtOpLstNotifWithErrors(filter);
						if (findSigtOpLstNotif != null) {
							for (SigtOpLstNotif sigtOpLstNotif : findSigtOpLstNotif) {
								// Transactional operation
								auditMessage += lstService.notificationLstDetailsError(sigtOpLstNotif, currentDate);
							}
						}else{
							Thread.sleep(GenericConstant.SLEEP_TIME);
						}
					} catch (Exception e) {
						LogController.getLog().error("Main.v1ListasUsuariosCodigoListaUsuarioNotificarErroresProcesamientoPutTest: General error occurred during execution!. "
										+ e.getMessage());
						auditMessage += "General error occurred during execution!. " + e.getMessage();
						
					}
					finally {
						Audit audit = new Audit(currentDate, "v1ListasUsuariosCodigoListaUsuarioNotificarErroresProcesamientoPutTest", auditMessage);
						try {
							baseService.registerAudit(audit);
						} catch (Exception e) {
							LogController.getLog().info("RegisterAudit: Error saving audit.");

						}						
					}	
				}				
			};
		};
		thread.start();
	}

	/**
	 * Registrar ajuste(s) de paso.
	 *
	 * Este servicio es consumido por el operador para informar al intermediador
	 * de un ajuste(s) en un paso. Una vez se procese, el resultado deberá ser
	 * confirmado como exitoso o fallido al operador.
	 * 
	 * @throws Exception
	 *             - the exception
	 */
	public static void v1AjustesPostTest() throws Exception {

		Thread thread = new Thread() {
			public void run() {

				while (true) {
					String auditMessage = new String();	
					Date currentDate = new Date();
					
					try {
						GenericFilterDTO adjPendFilter = new GenericFilterDTO();
						adjPendFilter.setStatus(GenericConstant.SigtOpTrxAdjDscStatus_PENDIENTE_ENVIO);
						adjPendFilter.setMaxResult(GenericConstant.SigtOpTrxAdjPend_MAX_RESULT);
						adjPendFilter.setColumnOrder(GenericConstant.SigtOpTrxAdjPend_COLUMN_ORDER);
						adjPendFilter.setOrderType(GenericConstant.SigtOpTrxAdjPend_ORDER_TYPE);
						// Gets adjustments pending for send
						List<SigtOpTrxAdjPend> findAdjPend = adjService.findAdjPend(adjPendFilter);

						if (findAdjPend != null && !findAdjPend.isEmpty()) {

							// Send the adjustments and save the notify(Transactional operation).
							auditMessage += adjService.sendAdjustments(findAdjPend, currentDate);

						} else {
							Thread.sleep(GenericConstant.SLEEP_TIME);
						}

					} catch (Exception e) {
						LogController.getLog().error("Main.v1AjustesPostTest: General error occurred during execution!. " + e.getMessage());
						auditMessage += "General error occurred during execution!. " + e.getMessage();						
					}
					finally {
						Audit audit = new Audit(currentDate, "v1AjustesPostTest", auditMessage);
						try {
							baseService.registerAudit(audit);
						} catch (Exception e) {
							LogController.getLog().info("RegisterAudit: Error saving audit.");

						}						
					}	
				}
			};
		};
		thread.start();
	}

	/**
	 * Registrar negación de paso.
	 *
	 * Este servicio es consumido por el operador para informar al intermediador
	 * de una negación de paso.
	 * 
	 * @throws Exception
	 *             - the exception
	 */
	public static void v1NegacionesPasosPostTest() throws Exception {

		Thread thread = new Thread() {
			public void run() {
				while (true) {
					String auditMessage = new String();	
					Date currentDate = new Date();
					
					try {
						GenericFilterDTO trxNegPendFilter = new GenericFilterDTO();
						trxNegPendFilter.setStatus(GenericConstant.SigtOpTrxNegPassDscStatus_PENDIENTE_ENVIO);
						trxNegPendFilter.setMaxResult(GenericConstant.SigtOpTrxNegPassPend_MAX_RESULT);
						trxNegPendFilter.setColumnOrder(GenericConstant.SigtOpTrxNegPassPend_COLUMN_ORDER);
						trxNegPendFilter.setOrderType(GenericConstant.SigtOpTrxNegPassPend_ORDER_TYPE);
						// Gets negations transactions pending for send
						List<SigtOpTrxNegPassPend> findTrxNegPend = trxService.findTrxNegPend(trxNegPendFilter);

						if (findTrxNegPend != null && !findTrxNegPend.isEmpty()) {

							// Send the adjustments and save the notify(Transactional operation).
							auditMessage += trxService.sendNegTransits(findTrxNegPend, currentDate);

						} else {
							Thread.sleep(GenericConstant.SLEEP_TIME);
						}

					} catch (Exception e) {
						LogController.getLog().error("Main.v1NegacionesPasosPostTest: General error occurred during execution!. "
										+ e.getMessage());
						auditMessage += "General error occurred during execution!. " + e.getMessage();						
					}
					finally {
						Audit audit = new Audit(currentDate, "v1NegacionesPasosPostTest", auditMessage);
						try {
							baseService.registerAudit(audit);
						} catch (Exception e) {
							LogController.getLog().info("RegisterAudit: Error saving audit.");

						}						
					}	
				}
			};
		};
		thread.start();
	}

	/**
	 * Entregar pruebas para un paso de vehículo.
	 *
	 * Este servicio es consumido por el operador para entregar las pruebas de
	 * un paso de vehículo. Se inicia cuando el intermediario solicita pruebas
	 * al operador sobre un paso específico.
	 * 
	 * 
	 * @throws Exception
	 *             exception
	 */
	public static void v1PasosCodigoPasoPruebasPutTest() throws Exception {
		Thread thread = new Thread() {
			public void run() {
				while (true) {
					String auditMessage = new String();	
					Date currentDate = new Date();
					
					try {
						GenericFilterDTO filter = new GenericFilterDTO();
						filter.setStatus(GenericConstant.SigtOpDscTrxTestNotifStatus_PENDIENTE_ENVIAR);
						List<SigtOpTrxTestNotif> findSigtOpTrxTestNotif = trxService.findSigtOpTrxTestNotif(filter);
						if (findSigtOpTrxTestNotif != null) {
							for (SigtOpTrxTestNotif sigtOpTrxTestNotif : findSigtOpTrxTestNotif) {
								// Transactional operation
								auditMessage += trxService.sendSigtOpTrxTestResponse(sigtOpTrxTestNotif, currentDate);
							}
						}else{
							Thread.sleep(GenericConstant.SLEEP_TIME);
						}
					} catch (Exception e) {
						LogController.getLog().error("Main.v1PasosCodigoPasoPruebasPutTest: General error occurred during execution!. "
										+ e.getMessage());
						auditMessage += "General error occurred during execution!. " + e.getMessage();						
					}		
					finally {
						Audit audit = new Audit(currentDate, "v1PasosCodigoPasoPruebasPutTest", auditMessage);
						try {
							baseService.registerAudit(audit);
						} catch (Exception e) {
							LogController.getLog().info("RegisterAudit: Error saving audit.");

						}						
					}									
				}

			};
		};
		thread.start();
	}

	/**
	 * Registrar paso(s) de vehículo.
	 *
	 * Este servicio es consumido por el operador para informar al intermediador
	 * de un paso(s) de vehículo. Una vez se procese, el resultado deberá ser
	 * confirmado por exitoso o fallido al operador.
	 * 
	 * @throws Exception
	 *             - the exception
	 */
	public static void v1PasosPostTest() throws Exception {

		Thread thread = new Thread() {
			public void run() {

				while (true) {
					String auditMessage = new String();	
					Date currentDate = new Date();
					
					try {
						GenericFilterDTO trxPendFilter = new GenericFilterDTO();
						trxPendFilter.setStatus(GenericConstant.SigtOpTrxDscStatus_PENDIENTE_ENVIO);
						trxPendFilter.setMaxResult(GenericConstant.SigtOpTrxPend_MAX_RESULT);
						trxPendFilter.setColumnOrder(GenericConstant.SigtOpTrxPend_COLUMN_ORDER);
						trxPendFilter.setOrderType(GenericConstant.SigtOpTrxPend_ORDER_TYPE);
						// Gets transits pending for send
						List<SigtOpTrxPend> findTrxPend = null; //trxService.findTrxPend(trxPendFilter);
						
						List<Paso> body = new ArrayList<Paso>();
						Paso paso = new Paso();
						paso.setCodigoPaso("sdflkjsdlkfjklsdfjlkjsdlfjlsdkfj");
						body.add(paso);
						Paso paso2 = new Paso();
						paso2.setCodigoPaso("fffffffffffffffff");
						body.add(paso2);
						try {

							//FIXME Modificar ip por parametro recibido
							api.v1PasosPost(body,"localhost");
						} catch (Exception e) {
							// TODO: handle exception
						}
						
						// La api deberia de devolver el uuid para que sea registrado.

						System.out.println("dasfsdfsdfsdfsd: " + GenericConstant.SLEEP_TIME);
						
						if (findTrxPend != null && !findTrxPend.isEmpty()) {

							// Send the transits and save the notify(Transactional operation).
							//auditMessage += trxService.sendTransits(findTrxPend, currentDate);

						} else {
							System.out.println("Entra SLEEP_TIME : " + GenericConstant.SLEEP_TIME);
							Thread.sleep(GenericConstant.SLEEP_TIME);
						}

					} catch (Exception e) {
						LogController.getLog().error("Main.v1PasosPostTest: General error occurred during execution!. " + e.getMessage());
						auditMessage += "General error occurred during execution!. " + e.getMessage();
						e.printStackTrace();
						break;
					}	
					finally {
						Audit audit = new Audit(currentDate, "v1PasosPostTest", auditMessage);
						try {
							baseService.registerAudit(audit);
						} catch (Exception e) {
							LogController.getLog().info("RegisterAudit: Error saving audit.");

						}						
					}	
				}
			};
		};
		thread.start();
	}

	/**
	 * Consultar lista de usuarios por diferentes parámetros
	 * 
	 * 
	 * @param dateFilter
	 *            filter
	 * 
	 * @throws Exception
	 *             exception
	 */
	public static void v1ListasUsuariosGetTest(Date dateFilter) throws Exception {
		Date currentDate = new Date();
		String auditMessage = new String();

		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		String dateFilterStr = df.format(dateFilter);

		// All Users list for a specified day.

		//FIXME Modificar ip por parametro recibido
		List<ListaUsuario> response = api.v1ListasUsuariosGet(dateFilterStr,"localhost");

		if (response != null && !response.isEmpty()) {
			for (ListaUsuario listaUsuario : response) {
				auditMessage += listaUsuario.toString() + "\n";
			}
		}

		Audit audit = new Audit(currentDate, "v1ListasUsuariosGetTest", auditMessage);
		baseService.registerAudit(audit);
	}

	/**
	 * Consultar la información general de una lista de usuario.
	 *
	 * Información básica de una lista de usuario. Esto no incluye los registros
	 * particulares de usuarios de la lista.
	 * 
	 * @param codeFilter
	 *            filter
	 * 
	 * @throws Exception
	 *             exception
	 */
	public static void v1ListasUsuariosCodigoListaUsuarioGetTest(String codeFilter) throws Exception {
		Date currentDate = new Date();
		String auditMessage = new String();

		String codigoListaUsuario = codeFilter;

		//FIXME Modificar ip por parametro recibido
		ListaUsuario response = api.v1ListasUsuariosCodigoListaUsuarioGet(codigoListaUsuario,"localhost");

		if (response != null) {
			auditMessage += response.toString() + "\n";
		}

		Audit audit = new Audit(currentDate, "v1ListasUsuariosCodigoListaUsuarioGetTest", auditMessage);
		baseService.registerAudit(audit);
	}

	/**
	 * Consultar las listas generadas a posteriori.
	 *
	 * @param codeFilter
	 *            filter
	 * 
	 * @throws Exception
	 *             exception
	 */
	public static void v1ListasUsuariosCodigoListaUsuarioListasPosterioresGetTest(String codeFilter) throws Exception {
		Date currentDate = new Date();
		String auditMessage = new String();

		String codigoListaUsuario = codeFilter;
		
		//FIXME Modificar ip por parametro recibido
		List<ListaUsuario> response = api.v1ListasUsuariosCodigoListaUsuarioListasPosterioresGet(codigoListaUsuario,"localhost");

		if (response != null && !response.isEmpty()) {
			for (ListaUsuario listaUsuario : response) {
				auditMessage += listaUsuario.toString() + "\n";
			}
		}

		Audit audit = new Audit(currentDate, "v1ListasUsuariosCodigoListaUsuarioListasPosterioresGetTest",
				auditMessage);
		baseService.registerAudit(audit);
	}
}
