package es.indra.operator.client.api.bdModel;

import java.util.Date;

public class Audit implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private String identifier;
	private String message;
	private Date creationDate;


	

	public Audit(Date creationDate, String identifier, String message) {
		super();
		this.identifier = identifier;
		this.message = message;
		this.creationDate = creationDate;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	
}