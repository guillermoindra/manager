package es.indra.operator.client.api.services;

import java.util.Date;
import java.util.List;

import es.indra.operator.client.api.bdModel.GenericFilterDTO;
import es.indra.operator.client.api.bdModel.SigtOpTrxNegPassPend;
import es.indra.operator.client.api.bdModel.SigtOpTrxPend;
import es.indra.operator.client.api.bdModel.SigtOpTrxTestNotif;

/**
 * Interface for Transactions Service.
 * 
 * @author rharo
 */
public interface TrxService {

	/**
	 * Find transactions list pending for send.
	 * 
	 * @param trxPendFilterArg - transaction filter
	 * @return list of transactions for send
	 * @throws Exception - the exception
	 */
	List<SigtOpTrxPend> findTrxPend(GenericFilterDTO trxPendFilterArg) throws Exception;

	/**
	 * Send pending transactions to intermediador.
	 * 
	 * @param findSigtOpTrxPendArg - list of transactions for send
	 * @param currentDate - Current date
	 * @return auditMessage - Audit message
	 * @throws Exception - the exception
	 */
	String sendTransits(List<SigtOpTrxPend> findSigtOpTrxPendArg, Date currentDate) throws Exception;
	
	/**
	 * Find the list of transit negations pending to send.
	 * 
	 * @param trxNegPendFilterArg - transaction filter
	 * @return list of transit negations for send
	 * @throws Exception - the exception
	 */
	List<SigtOpTrxNegPassPend> findTrxNegPend(GenericFilterDTO trxNegPendFilterArg) throws Exception;

	/**
	 * Send negation transactions to intermediador.
	 * 
	 * @param listSigtOpTrxNegPassPendArg - list of transactions for send
	 * @param currentDate - Current date
	 * @return auditMessage - Audit message
	 * @throws Exception - the exception
	 */
	String sendNegTransits(List<SigtOpTrxNegPassPend> listSigtOpTrxNegPassPendArg, Date currentDate) throws Exception;
	
	/**
	 * Find SigtOpTrxTestNotif list pending for send.
	 * 
	 * @param filter - SigtOpTrxTestNotif filter
	 * @return list of SigtOpTrxTestNotif for send
	 * @throws Exception - the exception
	 */
	List<SigtOpTrxTestNotif> findSigtOpTrxTestNotif(GenericFilterDTO filter) throws Exception;
	
	/**
	 * Send pending SigtOpTrxTestResponse to intermediador.
	 * 
	 * @param sigtOpTrxTestNotif - notification of response to send
	 * @param currentDate - Current date
	 * @return auditMessage - Audit message
	 * @throws Exception - the exception
	 */
	String sendSigtOpTrxTestResponse(SigtOpTrxTestNotif sigtOpTrxTestNotif, Date currentDate) throws Exception;
	
}