package es.indra.operator.client.api.bdModel;

import java.io.Serializable;

/**
 * Generic filter DTO.
 * 
 * @author rharo
 */
public class GenericFilterDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The max result. */
	private Integer maxResult;

	/** The status of records. */
	private Integer status;

	/** The column order. */
	private String columnOrder;

	/** The order type. */
	private String orderType;
	
	/** The identifier. */
	private Long identifier;
	
	/** The identifier. */
	private Integer codAgency;

	/**
	 * Constructor.
	 */
	public GenericFilterDTO() {

	}

	/**
	 * Gets the serialversionuid.
	 * 
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * Return maxResult attribute.
	 *
	 * @return maxResult - Attribute returned
	 */
	public Integer getMaxResult() {
		return maxResult;
	}

	/**
	 * Set attribute maxResult.
	 *
	 * @param maxResultArg
	 *            - Set value
	 */
	public void setMaxResult(Integer maxResultArg) {
		maxResult = maxResultArg;
	}

	/**
	 * Return status attribute.
	 *
	 * @return status - Attribute returned
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Set attribute status.
	 *
	 * @param statusArg
	 *            - Set value
	 */
	public void setStatus(Integer statusArg) {
		status = statusArg;
	}

	/**
	 * Return columnOrder attribute.
	 *
	 * @return columnOrder - Attribute returned
	 */
	public String getColumnOrder() {
		return columnOrder;
	}

	/**
	 * Set attribute columnOrder.
	 *
	 * @param columnOrderArg
	 *            - Set value
	 */
	public void setColumnOrder(String columnOrderArg) {
		columnOrder = columnOrderArg;
	}

	/**
	 * Return orderType attribute.
	 *
	 * @return orderType - Attribute returned
	 */
	public String getOrderType() {
		return orderType;
	}

	/**
	 * Set attribute orderType.
	 *
	 * @param orderTypeArg
	 *            - Set value
	 */
	public void setOrderType(String orderTypeArg) {
		orderType = orderTypeArg;
	}

	/**
	 * Return identifier attribute.
	 *
	 * @return identifier - Attribute returned
	*/
	public Long getIdentifier() {
		return identifier;
	}

	/**
	 * Set attribute identifier.
	 *
	 * @param  identifierArg - Set value
	 */
	public void setIdentifier(Long identifierArg) {
		identifier = identifierArg;
	}

	/**
	 * Return codAgency attribute.
	 *
	 * @return codAgency - Attribute returned
	*/
	public Integer getCodAgency() {
		return codAgency;
	}

	/**
	 * Set attribute codAgency.
	 *
	 * @param  codAgencyArg - Set value
	 */
	public void setCodAgency(Integer codAgencyArg) {
		codAgency = codAgencyArg;
	}

}
