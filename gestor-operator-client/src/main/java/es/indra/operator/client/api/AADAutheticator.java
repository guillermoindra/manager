package es.indra.operator.client.api;

import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.naming.ServiceUnavailableException;

import com.microsoft.aad.adal4j.AuthenticationContext;
import com.microsoft.aad.adal4j.AuthenticationResult;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.jwk.source.RemoteJWKSet;
import com.nimbusds.jose.proc.JWSKeySelector;
import com.nimbusds.jose.proc.JWSVerificationKeySelector;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.proc.BadJWTException;
import com.nimbusds.jwt.proc.ConfigurableJWTProcessor;
import com.nimbusds.jwt.proc.DefaultJWTProcessor;

import es.indra.operator.client.api.utils.GenericConstant;

public class AADAutheticator {
    

    public static AuthenticationResult getAccessTokenFromUserCredentials() throws Exception {

		System.setProperty("http.proxyHost", GenericConstant.PROXY_HOST);
		System.setProperty("http.proxyPort", GenericConstant.PROXY_PORT);
		System.setProperty("http.proxyUser", GenericConstant.PROXY_USER);
		System.setProperty("http.proxyPassword", GenericConstant.PROXY_PASSWORD);
		System.setProperty("https.proxyHost", GenericConstant.PROXY_HOST);
		System.setProperty("https.proxyPort", GenericConstant.PROXY_PORT);
		System.setProperty("https.proxyUser", GenericConstant.PROXY_USER);
		System.setProperty("https.proxyPassword", GenericConstant.PROXY_PASSWORD); 	
    	
        AuthenticationContext context = null;
        AuthenticationResult result = null;
        ExecutorService service = null;
        try {
            service = Executors.newFixedThreadPool(1);
            context = new AuthenticationContext(GenericConstant.AUTHORITY, false, service);
            Future<AuthenticationResult> future = context.acquireToken(
                    "https://graph.windows.net", GenericConstant.CLIENT_ID, GenericConstant.USER_NAME, GenericConstant.PASSWORD,
                    null);
            result = future.get();
        } finally {
            service.shutdown();
        }

        if (result == null) {
            throw new ServiceUnavailableException(
                    "authentication result was null");
        }
        return result;
    }
    
	@SuppressWarnings({"rawtypes", "unchecked"})
    public static boolean validateToken(String token) throws Exception {

		System.setProperty("http.proxyHost", GenericConstant.PROXY_HOST);
		System.setProperty("http.proxyPort", GenericConstant.PROXY_PORT);
		System.setProperty("http.proxyUser", GenericConstant.PROXY_USER);
		System.setProperty("http.proxyPassword", GenericConstant.PROXY_PASSWORD);
		System.setProperty("https.proxyHost", GenericConstant.PROXY_HOST);
		System.setProperty("https.proxyPort", GenericConstant.PROXY_PORT);
		System.setProperty("https.proxyUser", GenericConstant.PROXY_USER);
		System.setProperty("https.proxyPassword", GenericConstant.PROXY_PASSWORD);
		
    	// Set up a JWT processor to parse the tokens and then check their signature
    	// and validity time window (bounded by the "iat", "nbf" and "exp" claims)
		ConfigurableJWTProcessor jwtProcessor = new DefaultJWTProcessor();

    	// The public RSA keys to validate the signatures will be sourced from the
    	// OAuth 2.0 server's JWK set, published at a well-known URL. The RemoteJWKSet
    	// object caches the retrieved keys to speed up subsequent look-ups and can
    	// also gracefully handle key-rollover
		JWKSource keySource = new RemoteJWKSet(new URL("https://login.microsoftonline.com/" + GenericConstant.TENANT + "/discovery/v2.0/keys"));
    	
    	// The expected JWS algorithm of the access tokens (agreed out-of-band)
    	JWSAlgorithm expectedJWSAlg = JWSAlgorithm.RS256;

    	// Configure the JWT processor with a key selector to feed matching public
    	// RSA keys sourced from the JWK set URL
		JWSKeySelector keySelector = new JWSVerificationKeySelector(expectedJWSAlg, keySource);
    	jwtProcessor.setJWSKeySelector(keySelector);

    	// Process the token
    	try {
        	JWTClaimsSet claimsSet = jwtProcessor.process(token, null);
        	
        	// Print out the token claims set
        	System.out.println(claimsSet.toJSONObject());  
        	
        	return true;
    	}
    	catch (BadJWTException ex) {
    		ex.printStackTrace();	
    		return false;
    	}
    }
}