package es.indra.operator.client.api.services.impl;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import es.indra.operator.client.api.bdModel.Audit;
import es.indra.operator.client.api.services.BaseService;
import es.indra.operator.client.logger.LogController;


/**
 * This class is the implementation of the BaseService interface.
 * 
 * @author acoboj
 */
@Transactional
public class BaseServiceImp implements BaseService {

	
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void registerAudit(Audit audit) throws Exception {
		//auditDao.registerAudit(audit);
		LogController.getLog().info("BaseServiceImp.registerAudit: Saving the audit object");
	}

	@Override
	public Object getDescriptorsById(Integer id, Class<?> c)  throws Exception {
		return null; //descriptorsDao.getDescriptorsById(id, c);
	}

	@Override
	public Object getDescriptorsByExternalCode(String externalCode, Class<?> c)  throws Exception {
		return null; //descriptorsDao.getDescriptorsByExternalCode(externalCode, c);
	}
	
}
