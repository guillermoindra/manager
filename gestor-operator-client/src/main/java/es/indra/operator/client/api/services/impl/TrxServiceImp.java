package es.indra.operator.client.api.services.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import es.indra.operator.client.api.DefaultApi;
import es.indra.operator.client.api.bdModel.GenericFilterDTO;
import es.indra.operator.client.api.bdModel.SigtDscCodAgency;
import es.indra.operator.client.api.bdModel.SigtDscNotifType;
import es.indra.operator.client.api.bdModel.SigtDscTrxTestResponseCode;
import es.indra.operator.client.api.bdModel.SigtOpDscTrxTestNotifStatus;
import es.indra.operator.client.api.bdModel.SigtOpToIntNotif;
import es.indra.operator.client.api.bdModel.SigtOpTrxDscStatus;
import es.indra.operator.client.api.bdModel.SigtOpTrxNegPassDscStatus;
import es.indra.operator.client.api.bdModel.SigtOpTrxNegPassPend;
import es.indra.operator.client.api.bdModel.SigtOpTrxPend;
import es.indra.operator.client.api.bdModel.SigtOpTrxTestNotif;
import es.indra.operator.client.api.bdModel.SigtOpTrxTestResponse;
import es.indra.operator.client.api.bdModel.SigtOpTrxTestResponseImage;
import es.indra.operator.client.api.services.TrxService;
import es.indra.operator.client.api.utils.GenericConstant;
import es.indra.operator.client.logger.LogController;
import es.indra.operator.client.model.NegacionPaso;
import es.indra.operator.client.model.Paso;
import es.indra.operator.client.model.PruebasPaso;
import es.indra.operator.client.model.Ticket;

/**
 * This class is the implementation of the TRXService interface.
 * 
 * @author rharo
 */
@Transactional
public class TrxServiceImp extends BaseServiceImp implements TrxService {

	private static final int MIN_LENGTH_ID_TRANSIT = 27;

	private final static DefaultApi api = new DefaultApi();
	
	@Override
	public List<SigtOpTrxTestNotif> findSigtOpTrxTestNotif(GenericFilterDTO filter) throws Exception {
		return null; //trxDao.findSigtOpTrxTestNotif(filter);

	}

	@Override
	public List<SigtOpTrxPend> findTrxPend(GenericFilterDTO trxPendFilter) throws Exception {
		return null; //trxDao.findTrxPend(trxPendFilter);
	}

	@Override
	public List<SigtOpTrxNegPassPend> findTrxNegPend(GenericFilterDTO trxNegPendFilterArg) throws Exception {
		return null; //trxDao.findTrxNegPend(trxNegPendFilterArg);
	}

	@Override
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public String sendSigtOpTrxTestResponse(SigtOpTrxTestNotif sigtOpTrxTestNotif, Date currentDate) throws Exception {
		String auditMessage = new String();

		String idTransit = sigtOpTrxTestNotif.getIdTransit();
		String codigoPaso = GenericConstant.OPERATOR_CODE + "_" + idTransit;

		LogController.getLog().debug("TrxServiceImp.sendSigtOpTrxTestResponse: Código de paso del cual enviamos prueba: "+codigoPaso);
		
		PruebasPaso body = new PruebasPaso();

		Boolean transitFounded = false;
//		GenericFilterDTO trxPendFilterArg = new GenericFilterDTO();
//		trxPendFilterArg.setIdentifier(idTransit);
//		trxPendFilterArg.setStatus(GenericConstant.SigtOpTrxDscStatus_ENVIADO);
//		List<SigtOpTrxPend> findTrxPend = trxDao.findTrxPend(trxPendFilterArg);
//		if (findTrxPend != null && !findTrxPend.isEmpty()) {
//			transitFounded = true;
//		}
		transitFounded = true;

		if (transitFounded) { // Transit founded
			SigtDscCodAgency sigtDscCodAgency = (SigtDscCodAgency) getDescriptorsByExternalCode(
					GenericConstant.OPERATOR_CODE.toString(), SigtDscCodAgency.class);
			SigtDscTrxTestResponseCode sigtDscTrxTestResponseCodePending = (SigtDscTrxTestResponseCode) getDescriptorsById(
					GenericConstant.SigtDscTrxTestResponseCode_TICKET_PENDIENTE_TRATAMIENTO,SigtDscTrxTestResponseCode.class);
			SigtOpTrxTestResponse sigtOpTrxTestResponseFilter = new SigtOpTrxTestResponse();
			sigtOpTrxTestResponseFilter.setSigtDscCodAgency(sigtDscCodAgency);
			sigtOpTrxTestResponseFilter.setIdTransit(idTransit);
			sigtOpTrxTestResponseFilter.setSigtDscTrxTestResponseCode(sigtDscTrxTestResponseCodePending);
			ArrayList<SigtOpTrxTestResponse> sigtOpTrxTestResponseFinded = null; //(ArrayList<SigtOpTrxTestResponse>) trxDao.findSigtOpTrxTestResponse(sigtOpTrxTestResponseFilter);
			SigtOpTrxTestResponse sigtOpTrxTestResponseLastDate = sigtOpTrxTestResponseFinded.get(0);
			for (SigtOpTrxTestResponse d : sigtOpTrxTestResponseFinded) {
				if(d.getCreationDate().after(sigtOpTrxTestResponseLastDate.getCreationDate())){
					sigtOpTrxTestResponseLastDate = d;
				}
			}		
			
			String transitImagesPath = GenericConstant.TRANSIT_IMAGES_PATH;
			String transitImagesExtension = GenericConstant.TRANSIT_IMAGES_EXT;

			File dirTransitImages = new File(transitImagesPath + File.separator + idTransit);
			File[] allFiles = dirTransitImages.listFiles();
			ArrayList<File> jpgFiles = new ArrayList<File>();
			if (allFiles != null && allFiles.length > 0) {
				for (File f : allFiles) {
					if (f.getName().contains(transitImagesExtension)) {
						jpgFiles.add(f);
					}
				}
			}

			SigtDscTrxTestResponseCode sigtDscTrxTestResponseCode = null;

			if (jpgFiles != null && !jpgFiles.isEmpty()) {
				sigtDscTrxTestResponseCode = (SigtDscTrxTestResponseCode) getDescriptorsById(
						GenericConstant.SigtDscTrxTestResponseCode_PRUEBAS_DISPONIBLES,
						SigtDscTrxTestResponseCode.class);
			} else {
				sigtDscTrxTestResponseCode = (SigtDscTrxTestResponseCode) getDescriptorsById(
						GenericConstant.SigtDscTrxTestResponseCode_PRUEBAS_NO_DISPONIBLES,
						SigtDscTrxTestResponseCode.class);
			}

			sigtOpTrxTestResponseLastDate.setSigtDscTrxTestResponseCode(sigtDscTrxTestResponseCode);
			sigtOpTrxTestResponseLastDate.setModification((byte)1);
			sigtOpTrxTestResponseLastDate.setModificationDate(currentDate);
			sigtOpTrxTestResponseLastDate.setModificationUser(GenericConstant.MODIFICATION_USER);
			sigtOpTrxTestResponseLastDate.setOptimistLock((byte)(sigtOpTrxTestResponseLastDate.getOptimistLock()+1));
			//trxDao.update(sigtOpTrxTestResponseLastDate);

			String idStr = sigtOpTrxTestResponseLastDate.getIdentifier().toString();
			while (idStr.length() < MIN_LENGTH_ID_TRANSIT){
				idStr = "0"+idStr;				
			}
			
			body.setCodigoPaso(codigoPaso);
			body.setCodigoRespuesta(new Integer(sigtDscTrxTestResponseCode.getExternalCode()));
			Ticket ticket = new Ticket();
			ticket.setCodigo(GenericConstant.OPERATOR_CODE + "_" + idStr);
			ticket.setFecha(new DateTime(currentDate));
			ticket.setReferencia(codigoPaso);
			body.setTicket(ticket);

			if (jpgFiles != null && !jpgFiles.isEmpty()) {
				List<String> pruebas = new ArrayList<String>();
				for (File file : jpgFiles) {					
					String encodeImg = encoder(file.getAbsolutePath());

					SigtOpTrxTestResponseImage sigtOpTrxTestResponseImage = new SigtOpTrxTestResponseImage(null,
							sigtOpTrxTestResponseLastDate, file.getAbsolutePath(), encodeImg);
					//trxDao.save(sigtOpTrxTestResponseImage);
					pruebas.add(encodeImg);
				}
				body.setPruebas(pruebas);
			}
		} else { // Transit not found

			SigtDscCodAgency sigtDscCodAgency = (SigtDscCodAgency) getDescriptorsByExternalCode(
					GenericConstant.OPERATOR_CODE.toString(), SigtDscCodAgency.class);
			SigtDscTrxTestResponseCode sigtDscTrxTestResponseCode = (SigtDscTrxTestResponseCode) getDescriptorsById(
					GenericConstant.SigtDscTrxTestResponseCode_TRANSITO_NO_ENCONTRADO,
					SigtDscTrxTestResponseCode.class);

			SigtOpTrxTestResponse sigtOpTrxTestResponse = new SigtOpTrxTestResponse(null, sigtDscTrxTestResponseCode,
					sigtDscCodAgency, idTransit, null, currentDate, GenericConstant.MODIFICATION_USER,
					GenericConstant.APPLICATION_ID, currentDate, (byte) 0, (byte) 0);
			Long sigtOpTrxTestResponseId = null; //(Long) trxDao.save(sigtOpTrxTestResponse);
			
			String idStr = sigtOpTrxTestResponseId.toString();
			while (idStr.length() < MIN_LENGTH_ID_TRANSIT){
				idStr = "0"+idStr;				
			}
			body.setCodigoPaso(codigoPaso);
			body.setCodigoRespuesta(new Integer(sigtDscTrxTestResponseCode.getExternalCode()));
			body.setPruebas(new ArrayList<String>());
			Ticket ticket = new Ticket();
			ticket.setCodigo(GenericConstant.OPERATOR_CODE + "_" + idStr);
			ticket.setFecha(new DateTime(currentDate));
			ticket.setReferencia(codigoPaso);
			body.setTicket(ticket);
		}

		SigtOpDscTrxTestNotifStatus sigtOpDscTrxTestNotifStatus = new SigtOpDscTrxTestNotifStatus(
				GenericConstant.SigtOpDscTrxTestNotifStatus_ENVIADO, "");
		sigtOpTrxTestNotif.setSigtOpDscTrxTestNotifStatus(sigtOpDscTrxTestNotifStatus);
		sigtOpTrxTestNotif.setModificationUser(GenericConstant.MODIFICATION_USER);
		sigtOpTrxTestNotif.setApplication(GenericConstant.APPLICATION_ID);
		sigtOpTrxTestNotif.setModificationDate(currentDate);
		sigtOpTrxTestNotif.setModification((byte) 1);
		sigtOpTrxTestNotif.setOptimistLock((byte) (sigtOpTrxTestNotif.getOptimistLock() + 1));
		//trxDao.update(sigtOpTrxTestNotif);

		LogController.getLog().debug("TrxServiceImp.sendSigtOpTrxTestResponse: Enviamos prueba: \n"+body.toString());
		
		//FIXME Modificar ip por parametro recibido
		 api.v1PasosCodigoPasoPruebasPut(codigoPaso, body,"localhost");
		 auditMessage += body.toString();

		return auditMessage;
	}

	public String encoder(String imagePath) throws IOException {
		String base64Image = "";
		File file = new File(imagePath);
		FileInputStream imageInFile = new FileInputStream(file);
		// Reading a Image file from file system
		byte imageData[] = new byte[(int) file.length()];
		imageInFile.read(imageData);
		base64Image = Base64.getEncoder().encodeToString(imageData);
		imageInFile.close();
		return base64Image;
	}

	public void decoder(String base64Image, String pathFile) throws IOException {
			FileOutputStream imageOutFile = new FileOutputStream(pathFile);
			// Converting a Base64 String into Image byte array
			byte[] imageByteArray = Base64.getDecoder().decode(base64Image);
			imageOutFile.write(imageByteArray);
			imageOutFile.close();
	}

	@Override
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public String sendTransits(List<SigtOpTrxPend> listSigtOpTrxPendArg, Date currentDate) throws Exception {

		String auditMessage = new String();

		// notification data
		SigtDscNotifType sigtDscNotifType = (SigtDscNotifType) super.getDescriptorsById(
				GenericConstant.SigtDscNotifType_NOTIFICACION_VIAJES, SigtDscNotifType.class);
		SigtOpToIntNotif sigtOpToIntNotif = createIntNotif(sigtDscNotifType, listSigtOpTrxPendArg.size(),
				currentDate);
		List<Paso> body = new ArrayList<Paso>();

		for (SigtOpTrxPend sigtOpTrxPend : listSigtOpTrxPendArg) {

			// Create Paso Object, and sets data
			Paso paso = new Paso();
			String idStr = sigtOpTrxPend.getIdentifier();
			while (idStr.length() < MIN_LENGTH_ID_TRANSIT){
				idStr = "0"+idStr;				
			}
			paso.setCodigoPaso(GenericConstant.OPERATOR_CODE + "_" + idStr);
			paso.setCodigoOperador(Integer.parseInt(sigtOpTrxPend.getSigtDscCodAgency().getExternalCode()));
			paso.setFechaHora(new DateTime(sigtOpTrxPend.getTrxDate()));
			paso.setFechaRecaudo(new DateTime(sigtOpTrxPend.getTrxCollectionDate()));
			paso.setCodigoCliente(sigtOpTrxPend.getClientId());
			paso.setPlaca(sigtOpTrxPend.getPlate());
			paso.setTid(sigtOpTrxPend.getTid());
			paso.setEpc(sigtOpTrxPend.getEpc());
			paso.setEstacion(sigtOpTrxPend.getSigtDscPlacesByStationId().getExternalDescription());
			paso.setCarril(sigtOpTrxPend.getSigtDscPlacesByLaneId().getExternalDescription());
			paso.setValor(sigtOpTrxPend.getAmount());
			paso.setCantidadEjes(sigtOpTrxPend.getNumAxles());
			paso.setCantidadDobleRuedas(sigtOpTrxPend.getNumDoubleWheel());

			paso.setCategoriaDAC((sigtOpTrxPend.getSigtDscVehicleClassByDetectedClass() != null)
					? sigtOpTrxPend.getSigtDscVehicleClassByDetectedClass().getId() : null);

			paso.setCategoriaCobrada((sigtOpTrxPend.getSigtDscVehicleClassByChargedClass() != null)
					? sigtOpTrxPend.getSigtDscVehicleClassByChargedClass().getExternalCode() : null);

			paso.setTipoLectura((sigtOpTrxPend.getSigtTrxDscReadType() != null)
					? Integer.parseInt(sigtOpTrxPend.getSigtTrxDscReadType().getExternalCode()) : null);

			paso.setSentido(sigtOpTrxPend.getDirection());
			paso.setPlacaOCR(sigtOpTrxPend.getPlateOcr());
			paso.setExisteDescrepancia(sigtOpTrxPend.isIsDiscrepancy());

			paso.setFechaActualizacionUsuario(
					(sigtOpTrxPend.getListDate() != null) ? new DateTime(sigtOpTrxPend.getListDate()) : null);

			paso.setCodigoListaUsuario(sigtOpTrxPend.getIdList());
			paso.setExisteDiscrepanciaPlaca(sigtOpTrxPend.isIsPlateDiscrepancy());

			body.add(paso);

		}

		// Update transactions with status 2, and idNotif
		updateSigtOpTrxPend(listSigtOpTrxPendArg, sigtOpToIntNotif, currentDate);

		auditMessage += body.toString();
		
		//FIXME Modificar ip por parametro recibido
		api.v1PasosPost(body,"localhost");


		return auditMessage;

	}

	@Override
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public String sendNegTransits(List<SigtOpTrxNegPassPend> listSigtOpTrxNegPassPendArg, Date currentDate) throws Exception {

		String auditMessage = new String();

		// notification data
		SigtDscNotifType sigtDscNotifType = (SigtDscNotifType) super.getDescriptorsById(
				GenericConstant.SigtDscNotifType_NOTIFICACION_NEGACION_VIAJES, SigtDscNotifType.class);
		SigtOpToIntNotif sigtOpToIntNotif = createIntNotif(sigtDscNotifType, listSigtOpTrxNegPassPendArg.size(),
				currentDate);
		List<NegacionPaso> body = new ArrayList<NegacionPaso>();

		for (SigtOpTrxNegPassPend sigtOpTrxNegPassPend : listSigtOpTrxNegPassPendArg) {

			// Create negacionPaso Object, and sets data
			NegacionPaso negacionPaso = new NegacionPaso();
			String idStr = sigtOpTrxNegPassPend.getIdentifier();
			while (idStr.length() < MIN_LENGTH_ID_TRANSIT){
				idStr = "0"+idStr;				
			}
			
			negacionPaso.setCodigoNegacion(GenericConstant.OPERATOR_CODE + "_" + idStr);
			negacionPaso.setCodigoOperador(
					Integer.parseInt(sigtOpTrxNegPassPend.getSigtDscCodAgency().getExternalCode()));
			negacionPaso.setFechaHora(new DateTime(sigtOpTrxNegPassPend.getTrxDate()));
			negacionPaso.setDiaContable(new LocalDate(sigtOpTrxNegPassPend.getTrxCollectionDate()));
			negacionPaso.setPlaca((sigtOpTrxNegPassPend.getPlate() != null) ? sigtOpTrxNegPassPend.getPlate() : null);
			negacionPaso.setTid(sigtOpTrxNegPassPend.getTid());
			negacionPaso.setEpc(sigtOpTrxNegPassPend.getEpc());
			negacionPaso.setEstacion(sigtOpTrxNegPassPend.getSigtDscPlacesByStationId().getExternalDescription());
			negacionPaso.setCarril(sigtOpTrxNegPassPend.getSigtDscPlacesByLaneId().getExternalDescription());
			negacionPaso.setTipoLectura((sigtOpTrxNegPassPend.getSigtTrxDscReadType() != null)
					? Integer.parseInt(sigtOpTrxNegPassPend.getSigtTrxDscReadType().getExternalCode()) : null);
			negacionPaso.setSentido(sigtOpTrxNegPassPend.getDirection());
			negacionPaso.setVersion((sigtOpTrxNegPassPend.getListDate() != null) ? new DateTime(sigtOpTrxNegPassPend.getListDate()) : null);
			negacionPaso.setCodigoLista(sigtOpTrxNegPassPend.getIdList());
			negacionPaso.setRazon(sigtOpTrxNegPassPend.getSigtTrxNegPassDscReason().getExternalCode());
			
			body.add(negacionPaso);

		}

		// Update negation transactions with status 2, and idNotif
		updateSigtOpTrxNegPend(listSigtOpTrxNegPassPendArg, sigtOpToIntNotif, currentDate);

		//FIXME Modificar ip por parametro recibido
		api.v1NegacionesPasosPost(body,"localhost");

		auditMessage += body.toString();

		return auditMessage;
	}

	/**
	 * Method that create a notification.
	 * 
	 * @param sigtDscNotifTypeArg
	 *            - notification type
	 * @param listSizeArg
	 *            - list Size
	 * @param currentDateArg
	 *            - current date
	 * @return SigtOpToIntNotif object
	 */
	private SigtOpToIntNotif createIntNotif(SigtDscNotifType sigtDscNotifTypeArg, int listSizeArg,
			Date currentDateArg) throws Exception {

		SigtDscCodAgency sigtDscCodAgency = (SigtDscCodAgency) super.getDescriptorsByExternalCode(
				GenericConstant.OPERATOR_CODE.toString(), SigtDscCodAgency.class);

		SigtOpToIntNotif sigtOpToIntNotif = new SigtOpToIntNotif();
		sigtOpToIntNotif.setSigtDscNotifType(sigtDscNotifTypeArg);
		sigtOpToIntNotif.setSigtDscCodAgency(sigtDscCodAgency);
		sigtOpToIntNotif.setNumRecords(listSizeArg);
		sigtOpToIntNotif.setApplication(GenericConstant.APPLICATION_ID);
		sigtOpToIntNotif.setModificationUser(GenericConstant.MODIFICATION_USER);
		sigtOpToIntNotif.setCreationDate(currentDateArg);
		sigtOpToIntNotif.setModificationDate(currentDateArg);
		sigtOpToIntNotif.setModification((byte) 0);
		sigtOpToIntNotif.setOptimistLock((byte) 0);

		Long idenNotif = (Long) null; //trxDao.save(sigtOpToIntNotif);
		sigtOpToIntNotif.setIdentifier(idenNotif);

		return sigtOpToIntNotif;
	}

	/**
	 * Update SigtOpTrxPend objects in database.
	 * 
	 * @param listSigtOpTrxPendArg
	 *            - SigtOpTrxPend list
	 * @param sigtOpToIntNotifArg
	 *            - notification object
	 * @param currentDateArg
	 *            - current date
	 * @throws Exception
	 *             - the exception
	 */
	private void updateSigtOpTrxNegPend(List<SigtOpTrxNegPassPend> listSigtOpTrxNegPassPendArg,
			SigtOpToIntNotif sigtOpToIntNotifArg, Date currentDateArg) throws Exception {

		SigtOpTrxNegPassDscStatus sigtOpTrxNegPassDscStatus = (SigtOpTrxNegPassDscStatus) super.getDescriptorsById(
				GenericConstant.SigtOpTrxNegPassDscStatus_ENVIADO, SigtOpTrxNegPassDscStatus.class);

		for (SigtOpTrxNegPassPend sigtOpTrxNegPassPend : listSigtOpTrxNegPassPendArg) {
			sigtOpTrxNegPassPend.setSigtOpTrxNegPassDscStatus(sigtOpTrxNegPassDscStatus);
			sigtOpTrxNegPassPend.setModificationUser(GenericConstant.MODIFICATION_USER);
			sigtOpTrxNegPassPend.setApplication(GenericConstant.APPLICATION_ID);
			sigtOpTrxNegPassPend.setModificationDate(currentDateArg);
			sigtOpTrxNegPassPend.setModification((byte) 1);
			sigtOpTrxNegPassPend.setOptimistLock((byte) (sigtOpTrxNegPassPend.getOptimistLock() + 1));
			sigtOpTrxNegPassPend.setSigtOpToIntNotif(sigtOpToIntNotifArg);
			//trxDao.update(sigtOpTrxNegPassPend);
		}
	}

	/**
	 * Update SigtOpTrxPend objects in database.
	 * 
	 * @param listSigtOpTrxPendArg
	 *            - SigtOpTrxPend list
	 * @param sigtOpToIntNotifArg
	 *            - notification object
	 * @param currentDateArg
	 *            - current date
	 * @throws Exception
	 *             - the exception
	 */
	private void updateSigtOpTrxPend(List<SigtOpTrxPend> listSigtOpTrxPendArg, SigtOpToIntNotif sigtOpToIntNotifArg,
			Date currentDateArg) throws Exception {

		SigtOpTrxDscStatus sigtOpTrxDscStatus = (SigtOpTrxDscStatus) super.getDescriptorsById(
				GenericConstant.SigtOpTrxDscStatus_ENVIADO, SigtOpTrxDscStatus.class);

		for (SigtOpTrxPend sigtOpTrxPend : listSigtOpTrxPendArg) {
			sigtOpTrxPend.setSigtOpTrxDscStatus(sigtOpTrxDscStatus);
			sigtOpTrxPend.setModificationUser(GenericConstant.MODIFICATION_USER);
			sigtOpTrxPend.setApplication(GenericConstant.APPLICATION_ID);
			sigtOpTrxPend.setModificationDate(currentDateArg);
			sigtOpTrxPend.setModification((byte) 1);
			sigtOpTrxPend.setOptimistLock((byte) (sigtOpTrxPend.getOptimistLock() + 1));
			sigtOpTrxPend.setSigtOpToIntNotif(sigtOpToIntNotifArg);
			//trxDao.update(sigtOpTrxPend);
		}
	}

	
}