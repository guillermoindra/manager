package es.indra.operator.client.main;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.toedter.calendar.JDateChooser;

public class MainWindows extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	public MainWindows() throws HeadlessException {
		super();
		this.configure();
		this.initialize();
	}

	private void configure() {
		this.setTitle("Test launcher");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(935, 380);
		this.setLocationRelativeTo(null);
		this.setLayout(new FlowLayout(FlowLayout.TRAILING));
		this.setResizable(false);
	}

	private void initialize() {

		JPanel container = new JPanel();
		container.setLayout(new GridLayout(10, 1));

		JPanel panelOp1 = getPanelOp1();
		JPanel panelOp2 = getPanelOp2();
		JPanel panelOp3 = getPanelOp3();
		JPanel panelOp4 = getPanelOp4();
		JPanel panelOp5 = getPanelOp5();
		JPanel panelOp6 = getPanelOp6();
		JPanel panelOp7 = getPanelOp7();
		JPanel panelOp8 = getPanelOp8();
		JPanel panelOp9 = getPanelOp9();

		container.add(panelOp1);
		container.add(panelOp2);
		container.add(panelOp3);
		container.add(panelOp4);
		container.add(panelOp5);
		container.add(panelOp6);
		container.add(panelOp7);
		container.add(panelOp8);
		container.add(panelOp9);

		this.add(container, BorderLayout.CENTER);

	}
	
	private JPanel getPanelOp9() {

		JPanel panelOp = new JPanel();
		panelOp.setLayout(new FlowLayout(FlowLayout.TRAILING));

		JLabel labelOp = new JLabel("Operation 9:  v1PasosCodigoPasoPruebasPutTest: ");		
		
		JLabel labelOpRunning = new JLabel(" Running...");
		labelOpRunning.setEnabled(false);

		
		JButton buttonOp = new JButton();
		buttonOp.setText("Run");
		buttonOp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {					
					Main.v1PasosCodigoPasoPruebasPutTest();
					buttonOp.setEnabled(false);
					labelOpRunning.setEnabled(true);
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});				
		panelOp.add(labelOp);
		panelOp.add(buttonOp);
		panelOp.add(labelOpRunning);
		return panelOp;
	}
	
	private JPanel getPanelOp8() {
		JPanel panelOp = new JPanel();
		panelOp.setLayout(new FlowLayout(FlowLayout.TRAILING));

		JLabel labelOp = new JLabel("Operation 8:  v1NegacionesPasosPostTest: ");

		JLabel labelOpRunning = new JLabel(" Running...");
		labelOpRunning.setEnabled(false);

		JButton buttonOp = new JButton();
		buttonOp.setText("Run");
		buttonOp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonOp.setEnabled(false);
				try {
					Main.v1NegacionesPasosPostTest();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				labelOpRunning.setEnabled(true);
			}
		});
		panelOp.add(labelOp);
		panelOp.add(buttonOp);
		panelOp.add(labelOpRunning);
		return panelOp;
	}	
	
	private JPanel getPanelOp7() {
		JPanel panelOp = new JPanel();
		panelOp.setLayout(new FlowLayout(FlowLayout.TRAILING));

		JLabel labelOp = new JLabel("Operation 7:  v1PasosPostTest: ");

		JLabel labelOpRunning = new JLabel(" Running...");
		labelOpRunning.setEnabled(false);

		JButton buttonOp = new JButton();
		buttonOp.setText("Run");
		buttonOp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonOp.setEnabled(false);
				try {
					Main.v1PasosPostTest();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				labelOpRunning.setEnabled(true);
			}
		});
		panelOp.add(labelOp);
		panelOp.add(buttonOp);
		panelOp.add(labelOpRunning);
		return panelOp;
	}	
	
	private JPanel getPanelOp6() {
		JPanel panelOp = new JPanel();
		panelOp.setLayout(new FlowLayout(FlowLayout.TRAILING));

		JLabel labelOp = new JLabel("Operation 6:  v1AjustesPostTest: ");

		JLabel labelOpRunning = new JLabel(" Running...");
		labelOpRunning.setEnabled(false);

		JButton buttonOp = new JButton();
		buttonOp.setText("Run");
		buttonOp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonOp.setEnabled(false);
				try {
					Main.v1AjustesPostTest();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				labelOpRunning.setEnabled(true);
			}
		});
		panelOp.add(labelOp);
		panelOp.add(buttonOp);
		panelOp.add(labelOpRunning);
		return panelOp;
	}
	
	private JPanel getPanelOp5() {

		JPanel panelOp = new JPanel();
		panelOp.setLayout(new FlowLayout(FlowLayout.TRAILING));

		JLabel labelOp = new JLabel("Operation 5:  v1ListasUsuariosCodigoListaUsuarioListasPosterioresGetTest: ");		
		
		JLabel labelOpRunning = new JLabel(" Running...");
		labelOpRunning.setEnabled(false);

		JTextField filter = new JTextField(30);
		
		JButton buttonOp = new JButton();
		buttonOp.setText("Run");
		buttonOp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if(filter != null && filter.getText() != null && !filter.getText().isEmpty()){
						String filterStr = filter.getText();
						Main.v1ListasUsuariosCodigoListaUsuarioListasPosterioresGetTest(filterStr);
						buttonOp.setEnabled(false);
						labelOpRunning.setEnabled(true);
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});				
		panelOp.add(labelOp);
		panelOp.add(filter);
		panelOp.add(buttonOp);
		panelOp.add(labelOpRunning);
		return panelOp;
	}
	
	private JPanel getPanelOp4() {

		JPanel panelOp = new JPanel();
		panelOp.setLayout(new FlowLayout(FlowLayout.TRAILING));

		JLabel labelOp = new JLabel("Operation 4:  v1ListasUsuariosCodigoListaUsuarioGetTest: ");		
		
		JLabel labelOpRunning = new JLabel(" Running...");
		labelOpRunning.setEnabled(false);

		JTextField filter = new JTextField(30);
		
		JButton buttonOp = new JButton();
		buttonOp.setText("Run");
		buttonOp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if(filter != null && filter.getText() != null && !filter.getText().isEmpty()){
						String filterStr = filter.getText();
						Main.v1ListasUsuariosCodigoListaUsuarioGetTest(filterStr);
						buttonOp.setEnabled(false);
						labelOpRunning.setEnabled(true);
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});				
		panelOp.add(labelOp);
		panelOp.add(filter);
		panelOp.add(buttonOp);
		panelOp.add(labelOpRunning);
		return panelOp;
	}
	
	private JPanel getPanelOp3() {

		JPanel panelOp = new JPanel();
		panelOp.setLayout(new FlowLayout(FlowLayout.TRAILING));

		JLabel labelOp = new JLabel("Operation 3:  v1ListasUsuariosGetTest: ");		
		
		JLabel labelOpRunning = new JLabel(" Running...");
		labelOpRunning.setEnabled(false);

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		JDateChooser jdDateFilter = new JDateChooser();
		
		JButton buttonOp = new JButton();
		buttonOp.setText("Run");
		buttonOp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if(jdDateFilter != null && jdDateFilter.getDate() != null){
						Date dateFilter = jdDateFilter.getDate();
						Main.v1ListasUsuariosGetTest(dateFilter);
						buttonOp.setEnabled(false);
						labelOpRunning.setEnabled(true);
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});				
		panelOp.add(labelOp);
		panelOp.add(jdDateFilter);
		panelOp.add(buttonOp);
		panelOp.add(labelOpRunning);
		return panelOp;
	}

	private JPanel getPanelOp2() {

		JPanel panelOp = new JPanel();
		panelOp.setLayout(new FlowLayout(FlowLayout.TRAILING));

		JLabel labelOp = new JLabel("Operation 2:  v1ListasUsuariosCodigoListaUsuarioNotificarErroresProcesamientoPutTest: ");		
		
		JLabel labelOpRunning = new JLabel(" Running...");
		labelOpRunning.setEnabled(false);

		JButton buttonOp = new JButton();
		buttonOp.setText("Run");
		buttonOp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonOp.setEnabled(false);
				try {
					Main.v1ListasUsuariosCodigoListaUsuarioNotificarErroresProcesamientoPutTest();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				labelOpRunning.setEnabled(true);
			}
		});
		panelOp.add(labelOp);
		panelOp.add(buttonOp);
		panelOp.add(labelOpRunning);
		return panelOp;
	}

	private JPanel getPanelOp1() {
		JPanel panelOp = new JPanel();
		panelOp.setLayout(new FlowLayout(FlowLayout.TRAILING));

		JLabel labelOp = new JLabel("Operation 1:  v1ListasUsuariosCodigoListaUsuarioDetallesGetTest: ");

		JLabel labelOpRunning = new JLabel(" Running...");
		labelOpRunning.setEnabled(false);

		JButton buttonOp = new JButton();
		buttonOp.setText("Run");
		buttonOp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonOp.setEnabled(false);
				try {
					Main.v1ListasUsuariosCodigoListaUsuarioDetallesGetTest();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				labelOpRunning.setEnabled(true);
			}
		});
		panelOp.add(labelOp);
		panelOp.add(buttonOp);
		panelOp.add(labelOpRunning);
		return panelOp;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//
	}
}
