package es.indra.procesoEnvioViajes.dao;

import java.util.List;

import es.indra.procesoEnvioViajes.models.Travel;

public interface TravelDAO {
	
	/**
	 * Recupera todos los travels relacionados con una agencia
	 * y cuyo status sea el indicado
	 */
	public List<Travel> findByCodAgencyAndStatus (Integer codAgency, Integer status);
	
	/**
	 * Recupera todos los travels cuyo status sea el indicado
	 */
	public List<Travel> findByStatus (Integer status);
	
	/**
	 * This method updates all travels from a list to specified values. In addition, it increments the numSend value of each travel.
	 * This operation is done with a single SQL query
	 * 
	 * @param travels The list of Travel of which take ID
	 * @param status The status to update travels
	 * @param idNotif The idNotif to update travels
	 */
	public void batchUpdateStatusAndIdNotifAndNumSend (List<Travel> travels, Integer status, Long idNotif);
	
	
	/**
	 * This method updates all travels from a list to specified values. In addition, it increments the numSend value of each travel.
	 * This operation is done with a single SQL query
	 * 
	 * @param travels The list of Travel of which take ID
	 * @param status The status to update travels
	 * @param idNotifSigt The idNotifSigt to update travels
	 */
	public void batchUpdateStatusAndIdNotifSigtAndNumSend (List<Travel> travels, Integer status, Long idNotifSigt);
	
	/**
	 * Guarda transaccionalmente todos los travels de la lista
	 */
	public void saveAll (List<Travel> travels);
	
	/**
	 * Guarda o actualiza un viaje
	 */
	public void save (Travel travel);
}
