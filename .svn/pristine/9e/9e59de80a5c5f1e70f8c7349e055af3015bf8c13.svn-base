package es.indra.server.operador.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import es.indra.server.operador.bdModel.GenericFilterDTO;
import es.indra.server.operador.bdModel.SigtDscCodAgency;
import es.indra.server.operador.bdModel.SigtDscNotifType;
import es.indra.server.operador.bdModel.SigtOpToIntNotif;
import es.indra.server.operador.bdModel.SigtOpTrxAdjPend;
import es.indra.server.operador.bdModel.SigtOpTrxAdjPro;
import es.indra.server.operador.daos.AdjDao;
import es.indra.server.operador.log.LogController;
import es.indra.server.operador.model.ConfirmacionProcesamientoAjuste;
import es.indra.server.operador.services.AdjService;
import es.indra.server.operador.utils.GenericConstant;


/**
 * This class is the implementation of the AdjService interface.
 * 
 * @author rharo
 */
@Service
@Transactional
public class AdjServiceImp extends BaseServiceImp implements AdjService {

	/** Adjustment DAO */
	private AdjDao adjDao;

	@Override
	public SigtOpTrxAdjPend findSigtOpTrxAdjPend(GenericFilterDTO genericFilterDTOArg) throws Exception {
		return adjDao.findSigtOpTrxAdjPend(genericFilterDTOArg);
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public String updateConfirmedAdj(List<ConfirmacionProcesamientoAjuste> listStatusAdjArg, Date currentDateArg) throws Exception {

		String auditMessage = new String();

		if(!listStatusAdjArg.isEmpty()){
			
		// notification data
		SigtOpToIntNotif sigtOpToIntNotif = createIntNotif(listStatusAdjArg.size(),currentDateArg);

		//process each transaction
		for (ConfirmacionProcesamientoAjuste confirmacionProcesamientoAjuste : listStatusAdjArg) {

			LogController.getLog().debug(confirmacionProcesamientoAjuste.toString()+"\n");
			auditMessage += confirmacionProcesamientoAjuste.toString() + "\n";
			
			String dataTransit[] = confirmacionProcesamientoAjuste.getCodigoAjuste().split("_");

			Integer operatorCode = Integer.parseInt(dataTransit[0].toString().trim());
			Long identifier =  Long.parseLong(dataTransit[1].toString().trim()); 
			
			
			//find SigtOpTrxPend by filter
			GenericFilterDTO genericFilterDTO = new GenericFilterDTO();
			genericFilterDTO.setIdentifier(identifier);
			genericFilterDTO.setStatus(GenericConstant.SigtOpTrxAdjDscStatus_ENVIADO);
			SigtOpTrxAdjPend sigtOpTrxAdjPend = (SigtOpTrxAdjPend) findSigtOpTrxAdjPend(genericFilterDTO);
			
			if(sigtOpTrxAdjPend != null && operatorCode.equals(GenericConstant.OPERATOR_CODE)){
				
				SigtOpTrxAdjPro  sigtOpTrxAdjPro =  new SigtOpTrxAdjPro(
						
						sigtOpTrxAdjPend.getIdentifier(),
						sigtOpTrxAdjPend.getSigtTrxAdjDscOptType(),
						sigtOpTrxAdjPend.getSigtDscVehicleClass(),
						sigtOpTrxAdjPend.getSigtOpToIntNotif(),
						sigtOpTrxAdjPend.getSigtTrxAdjDscReason(),
						sigtOpToIntNotif,
						sigtOpTrxAdjPend.getSigtDscCodAgency(),
						confirmacionProcesamientoAjuste.getCodigoRespuesta(),
						sigtOpTrxAdjPend.getTrxId(),
						sigtOpTrxAdjPend.getTrxDate(),
						sigtOpTrxAdjPend.getTrxCollectionDate(),
						sigtOpTrxAdjPend.getTrxAdjustedId(),
						sigtOpTrxAdjPend.getAdjustmentAmount(),
						sigtOpTrxAdjPend.getAmount(),
						confirmacionProcesamientoAjuste.getTokenConfirmacion(),
						confirmacionProcesamientoAjuste.getDescripcion(),
						currentDateArg,
						GenericConstant.MODIFICATION_USER,
						GenericConstant.APPLICATION_ID,
						null,
						(byte) 0, 
						(byte) 0);
				
				//Save the new SigtOpTrxAdjPro Object, and delete the SigtOpTrxAdjPend object by PK
				adjDao.save(sigtOpTrxAdjPro);
				//adjDao.deleteByPK(SigtOpTrxAdjPend.class,identifier); //Descomentar en produción

			}else{
				
				LogController.getLog()
				.warn("V1ApiServiceImpl.v1ConfirmacionesAjustesPut: Adjustment not sent :  "
						+ confirmacionProcesamientoAjuste.getCodigoAjuste());
			}

		}
		
		}else{
			LogController.getLog()
			.warn("V1ApiServiceImpl.v1ConfirmacionesAjustesPut: received list empty ");
			
		}
		
		return auditMessage;
	}

	/**
	 * Method that create a notification.
	 * 
	 *@param listSize - list size
	 * @param currentDateArg - current date
	 * @return  SigtOpToIntNotif object
	 */
	private SigtOpToIntNotif createIntNotif(int listSize,Date currentDateArg) throws Exception {
		
		SigtDscCodAgency sigtDscCodAgency = (SigtDscCodAgency) super.getDescriptorsByExternalCode(
				GenericConstant.OPERATOR_CODE.toString(), SigtDscCodAgency.class);
		
		SigtDscNotifType sigtDscNotifType = (SigtDscNotifType)
				super.getDescriptorsById(GenericConstant.SigtDscNotifType_CONFIRMACION_AJUSTES,
				SigtDscNotifType.class);
		
		SigtOpToIntNotif sigtOpToIntNotif = new SigtOpToIntNotif();
		sigtOpToIntNotif.setSigtDscNotifType(sigtDscNotifType);
		sigtOpToIntNotif.setSigtDscCodAgency(sigtDscCodAgency);
		sigtOpToIntNotif.setNumRecords(listSize);
		sigtOpToIntNotif.setApplication(GenericConstant.APPLICATION_ID);
		sigtOpToIntNotif.setModificationUser(GenericConstant.MODIFICATION_USER);
		sigtOpToIntNotif.setCreationDate(currentDateArg);
		sigtOpToIntNotif.setModificationDate(currentDateArg);
		sigtOpToIntNotif.setModification((byte) 0);
		sigtOpToIntNotif.setOptimistLock((byte) 0);
		
		Long  idenNotif = (Long) adjDao.save(sigtOpToIntNotif);
		sigtOpToIntNotif.setIdentifier(idenNotif);

		return sigtOpToIntNotif;
	}
	
	/**
	 * Return adjDao attribute.
	 *
	 * @return adjDao - Attribute returned
	*/
	public AdjDao getAdjDao() {
		return adjDao;
	}

	/**
	 * Set attribute adjDao.
	 *
	 * @param  adjDaoArg - Set value
	 */
	public void setAdjDao(AdjDao adjDaoArg) {
		adjDao = adjDaoArg;
	}


}