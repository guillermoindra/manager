<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<artifactId>server-operador</artifactId>
	<packaging>jar</packaging>
	<name>gestor-server-operador</name>

	<parent>
		<artifactId>gestor</artifactId>
		<groupId>es.indra.server</groupId>
		<version>1.0.0-SNAPSHOT</version>
	</parent>

	<properties>
		<maven.compiler.source>${java.version}</maven.compiler.source>
		<maven.compiler.target>${java.version}</maven.compiler.target>
		<springfox-version>2.7.0</springfox-version>
	</properties>


	<build>
		<sourceDirectory>src/main/java</sourceDirectory>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<executions>
					<execution>
						<goals>
							<goal>repackage</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

		</plugins>
	</build>
	
	

	<dependencyManagement>
		<dependencies>
			<dependency>
				<!-- Import dependency management from Spring Boot -->
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-dependencies</artifactId>
				<version>${spring-boot.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<dependencies>
	
		<dependency>
			<artifactId>common</artifactId>
			<groupId>es.indra.server</groupId>
			<version>1.0.0-SNAPSHOT</version>
		</dependency>
		
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<dependency>
		    <groupId>org.springframework.boot</groupId>
		    <artifactId>spring-boot-starter-logging</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-tomcat</artifactId>
		</dependency>
		
		
		<!--SpringFox dependencies -->
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger2</artifactId>
			<version>${springfox-version}</version>
		</dependency>
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger-ui</artifactId>
			<version>${springfox-version}</version>
		</dependency>

		<dependency>
			<groupId>com.fasterxml.jackson.datatype</groupId>
			<artifactId>jackson-datatype-jsr310</artifactId>
		</dependency>
		<!-- Bean Validation API support -->
		<dependency>
			<groupId>javax.validation</groupId>
			<artifactId>validation-api</artifactId>
		</dependency>

		<!-- Actuator -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>		
						
		<!-- https://mvnrepository.com/artifact/org.springframework/spring-orm -->
		<dependency>
		    <groupId>org.springframework</groupId>
		    <artifactId>spring-orm</artifactId>
		    <!-- version>3.1.1.RELEASE</version-->
		</dependency>

		
		<dependency>
			<groupId>com.microsoft.sqlserver</groupId>
			<artifactId>mssql-jdbc</artifactId>
		</dependency>
		
		<dependency>
		    <groupId>commons-io</groupId>
		    <artifactId>commons-io</artifactId>
		    <version>2.4</version>
		</dependency>
		
		<!-- https://mvnrepository.com/artifact/c3p0/c3p0 -->
		<dependency>
		    <groupId>c3p0</groupId>
		    <artifactId>c3p0</artifactId>
		    <version>0.9.1.2</version>
		</dependency>
		
		<!-- https://mvnrepository.com/artifact/org.springframework.data/spring-data-jpa -->
		<dependency>
		    <groupId>org.springframework.data</groupId>
		    <artifactId>spring-data-jpa</artifactId>
		</dependency>
				
		
		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-core</artifactId>
			<!-- version>3.6.10.Final</version-->
		</dependency>
				
		
	</dependencies>

	<profiles>
		<profile>
			<id>generar-docker</id>
			<build>
				<plugins>
					<plugin>
						<groupId>io.fabric8</groupId>
						<artifactId>docker-maven-plugin</artifactId>
						<version>0.25.2</version>

						<executions>
							<execution>
								<id>start</id>
								<phase>install</phase>
								<goals>
									<goal>stop</goal>
									<goal>remove</goal>
									<goal>build</goal>
									<goal>start</goal>
								</goals>
							</execution>
						</executions>

						<configuration>
							<dockerHost>${docker.host}:2375</dockerHost>
							<verbose>true</verbose>

							<images>
								<image>
									<name>gestor/${docker.server.name}:${project.version}</name>
									<alias>${docker.server.alias}</alias>
									<build>
										<dockerFile>Dockerfile</dockerFile>
										<dockerFileDir>${project.basedir}/src/test/docker/</dockerFileDir>

										<assembly>
											<descriptorRef>artifact</descriptorRef>
										</assembly>
										<tags>
											<tag>latest</tag>
											<tag>${project.version}</tag>
										</tags>
									</build>
									<run>
										<namingStrategy>alias</namingStrategy>
										<!-- <links> -->
										<!-- <link>some-rabbit</link> -->
										<!-- <link>some-mysql</link> -->
										<!-- </links> -->
										<ports>
											<port>
												8080:8080
											</port>
											<port>
												8181:15672
											</port>
										</ports>
									</run>
								</image>
							</images>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
</project>
