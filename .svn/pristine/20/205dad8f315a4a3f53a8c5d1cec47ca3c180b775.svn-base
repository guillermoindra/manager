package es.indra.server.via.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * TransitJson
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-29T16:46:35.310+02:00")

public class TransitJson   {
  @JsonProperty("idTransaction")
  private Long idTransaction = null;

  @JsonProperty("idIntermediador")
  private Integer idIntermediador = null;

  @JsonProperty("trxDate")
  private OffsetDateTime trxDate = null;

  @JsonProperty("idClient")
  private String idClient = null;

  @JsonProperty("plate")
  private String plate = null;

  @JsonProperty("tip")
  private String tip = null;

  @JsonProperty("epc")
  private String epc = null;

  @JsonProperty("station")
  private String station = null;

  @JsonProperty("lane")
  private String lane = null;

  @JsonProperty("amount")
  private Integer amount = null;

  @JsonProperty("numAxles")
  private Integer numAxles = null;

  @JsonProperty("numDoubleWheel")
  private Integer numDoubleWheel = null;

  @JsonProperty("detectedClass")
  private Integer detectedClass = null;

  @JsonProperty("chargedClass")
  private String chargedClass = null;

  @JsonProperty("readType")
  private Integer readType = null;

  @JsonProperty("direction")
  private String direction = null;

  @JsonProperty("plateOCR")
  private String plateOCR = null;

  @JsonProperty("isDiscrepancy")
  private Boolean isDiscrepancy = null;

  @JsonProperty("dateList")
  private OffsetDateTime dateList = null;

  @JsonProperty("idList")
  private String idList = null;

  @JsonProperty("isDiscrepancyPlate")
  private Boolean isDiscrepancyPlate = null;

  public TransitJson idTransaction(Long idTransaction) {
    this.idTransaction = idTransaction;
    return this;
  }

  /**
   * Identificador único del tránsito.
   * @return idTransaction
  **/
  @ApiModelProperty(value = "Identificador único del tránsito.")


  public Long getIdTransaction() {
    return idTransaction;
  }

  public void setIdTransaction(Long idTransaction) {
    this.idTransaction = idTransaction;
  }

  public TransitJson idIntermediador(Integer idIntermediador) {
    this.idIntermediador = idIntermediador;
    return this;
  }

  /**
   * Identificador del intermediador de destino.
   * @return idIntermediador
  **/
  @ApiModelProperty(value = "Identificador del intermediador de destino.")


  public Integer getIdIntermediador() {
    return idIntermediador;
  }

  public void setIdIntermediador(Integer idIntermediador) {
    this.idIntermediador = idIntermediador;
  }

  public TransitJson trxDate(OffsetDateTime trxDate) {
    this.trxDate = trxDate;
    return this;
  }

  /**
   * Fecha y hora de la transacción.
   * @return trxDate
  **/
  @ApiModelProperty(value = "Fecha y hora de la transacción.")

  @Valid

  public OffsetDateTime getTrxDate() {
    return trxDate;
  }

  public void setTrxDate(OffsetDateTime trxDate) {
    this.trxDate = trxDate;
  }

  public TransitJson idClient(String idClient) {
    this.idClient = idClient;
    return this;
  }

  /**
   * Identificador del cliente. Se sugiere que no empiece por ceros.
   * @return idClient
  **/
  @ApiModelProperty(value = "Identificador del cliente. Se sugiere que no empiece por ceros.")

@Pattern(regexp="^[\\S]+$") @Size(min=1,max=100) 
  public String getIdClient() {
    return idClient;
  }

  public void setIdClient(String idClient) {
    this.idClient = idClient;
  }

  public TransitJson plate(String plate) {
    this.plate = plate;
    return this;
  }

  /**
   * Placa del vehículo.
   * @return plate
  **/
  @ApiModelProperty(value = "Placa del vehículo.")

@Pattern(regexp="^[a-zA-Z0-9\\-]+$") @Size(min=6,max=10) 
  public String getPlate() {
    return plate;
  }

  public void setPlate(String plate) {
    this.plate = plate;
  }

  public TransitJson tip(String tip) {
    this.tip = tip;
    return this;
  }

  /**
   * Código tid del tag.
   * @return tip
  **/
  @ApiModelProperty(value = "Código tid del tag.")

@Pattern(regexp="^[\\S]+$") @Size(min=24,max=32) 
  public String getTip() {
    return tip;
  }

  public void setTip(String tip) {
    this.tip = tip;
  }

  public TransitJson epc(String epc) {
    this.epc = epc;
    return this;
  }

  /**
   * Código del EPC asignado al tag según los rangos habilitados.
   * @return epc
  **/
  @ApiModelProperty(value = "Código del EPC asignado al tag según los rangos habilitados.")

@Pattern(regexp="^[0-9]+$") @Size(min=24,max=24) 
  public String getEpc() {
    return epc;
  }

  public void setEpc(String epc) {
    this.epc = epc;
  }

  public TransitJson station(String station) {
    this.station = station;
    return this;
  }

  /**
   * Código de la estación de peaje.
   * @return station
  **/
  @ApiModelProperty(value = "Código de la estación de peaje.")

@Pattern(regexp="^[\\S]+$") @Size(min=1,max=3) 
  public String getStation() {
    return station;
  }

  public void setStation(String station) {
    this.station = station;
  }

  public TransitJson lane(String lane) {
    this.lane = lane;
    return this;
  }

  /**
   * Código de carril de tránsito.
   * @return lane
  **/
  @ApiModelProperty(value = "Código de carril de tránsito.")

@Pattern(regexp="^[\\S]+$") @Size(min=1,max=10) 
  public String getLane() {
    return lane;
  }

  public void setLane(String lane) {
    this.lane = lane;
  }

  public TransitJson amount(Integer amount) {
    this.amount = amount;
    return this;
  }

  /**
   * Tarifa aplicada a la transacción. Deben ser mayores o igual a 0 (cero para el caso de vehículos exentos).
   * minimum: 0
   * @return amount
  **/
  @ApiModelProperty(value = "Tarifa aplicada a la transacción. Deben ser mayores o igual a 0 (cero para el caso de vehículos exentos).")

@Min(0)
  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public TransitJson numAxles(Integer numAxles) {
    this.numAxles = numAxles;
    return this;
  }

  /**
   * Ejes totales detectados.
   * minimum: 1
   * @return numAxles
  **/
  @ApiModelProperty(value = "Ejes totales detectados.")

@Min(1)
  public Integer getNumAxles() {
    return numAxles;
  }

  public void setNumAxles(Integer numAxles) {
    this.numAxles = numAxles;
  }

  public TransitJson numDoubleWheel(Integer numDoubleWheel) {
    this.numDoubleWheel = numDoubleWheel;
    return this;
  }

  /**
   * Ejes doble ruedas detectados.
   * minimum: 0
   * @return numDoubleWheel
  **/
  @ApiModelProperty(value = "Ejes doble ruedas detectados.")

@Min(0)
  public Integer getNumDoubleWheel() {
    return numDoubleWheel;
  }

  public void setNumDoubleWheel(Integer numDoubleWheel) {
    this.numDoubleWheel = numDoubleWheel;
  }

  public TransitJson detectedClass(Integer detectedClass) {
    this.detectedClass = detectedClass;
    return this;
  }

  /**
   * Categoría DAC según Ministerio de Transporte.
   * @return detectedClass
  **/
  @ApiModelProperty(value = "Categoría DAC según Ministerio de Transporte.")


  public Integer getDetectedClass() {
    return detectedClass;
  }

  public void setDetectedClass(Integer detectedClass) {
    this.detectedClass = detectedClass;
  }

  public TransitJson chargedClass(String chargedClass) {
    this.chargedClass = chargedClass;
    return this;
  }

  /**
   * Código de la categoría según la tabla de categorías de la estación de peaje.
   * @return chargedClass
  **/
  @ApiModelProperty(value = "Código de la categoría según la tabla de categorías de la estación de peaje.")

@Size(min=1,max=10) 
  public String getChargedClass() {
    return chargedClass;
  }

  public void setChargedClass(String chargedClass) {
    this.chargedClass = chargedClass;
  }

  public TransitJson readType(Integer readType) {
    this.readType = readType;
    return this;
  }

  /**
   * Forma de registro vehículo: 1 – Antena, 2 – Manual por placa, 3 – Manual por tag, 4 – Manual por auditoría, 5 – Placa OCR.
   * minimum: 1
   * maximum: 5
   * @return readType
  **/
  @ApiModelProperty(value = "Forma de registro vehículo: 1 – Antena, 2 – Manual por placa, 3 – Manual por tag, 4 – Manual por auditoría, 5 – Placa OCR.")

@Min(1) @Max(5) 
  public Integer getReadType() {
    return readType;
  }

  public void setReadType(Integer readType) {
    this.readType = readType;
  }

  public TransitJson direction(String direction) {
    this.direction = direction;
    return this;
  }

  /**
   * Código del sentido que transita el vehículo. Su estructura debe ser:\\n“3 caracteres origen”-“3 caracteres destino” en mayúscula para mantener el estándar.
   * @return direction
  **/
  @ApiModelProperty(value = "Código del sentido que transita el vehículo. Su estructura debe ser:\\n“3 caracteres origen”-“3 caracteres destino” en mayúscula para mantener el estándar.")

@Pattern(regexp="[A-Z]{3}\\-[A-Z]{3}") @Size(min=7,max=7) 
  public String getDirection() {
    return direction;
  }

  public void setDirection(String direction) {
    this.direction = direction;
  }

  public TransitJson plateOCR(String plateOCR) {
    this.plateOCR = plateOCR;
    return this;
  }

  /**
   * Placa identificada por el sensor OCR.
   * @return plateOCR
  **/
  @ApiModelProperty(value = "Placa identificada por el sensor OCR.")

@Pattern(regexp="^[a-zA-Z0-9\\-]+$") @Size(min=6,max=10) 
  public String getPlateOCR() {
    return plateOCR;
  }

  public void setPlateOCR(String plateOCR) {
    this.plateOCR = plateOCR;
  }

  public TransitJson isDiscrepancy(Boolean isDiscrepancy) {
    this.isDiscrepancy = isDiscrepancy;
    return this;
  }

  /**
   * Indica si existe diferencia entre la categoría registrada y la detectada  true – Si hay discrepancia, false – No hay discrepancias.
   * @return isDiscrepancy
  **/
  @ApiModelProperty(value = "Indica si existe diferencia entre la categoría registrada y la detectada  true – Si hay discrepancia, false – No hay discrepancias.")


  public Boolean isIsDiscrepancy() {
    return isDiscrepancy;
  }

  public void setIsDiscrepancy(Boolean isDiscrepancy) {
    this.isDiscrepancy = isDiscrepancy;
  }

  public TransitJson dateList(OffsetDateTime dateList) {
    this.dateList = dateList;
    return this;
  }

  /**
   * Fecha en el que se actualizó el saldo. Incluir milisegundos.
   * @return dateList
  **/
  @ApiModelProperty(value = "Fecha en el que se actualizó el saldo. Incluir milisegundos.")

  @Valid

  public OffsetDateTime getDateList() {
    return dateList;
  }

  public void setDateList(OffsetDateTime dateList) {
    this.dateList = dateList;
  }

  public TransitJson idList(String idList) {
    this.idList = idList;
    return this;
  }

  /**
   * La lista de usuario es un recurso identificado por el patrón: CODIGOINTERMEDIADOR_YYYYMMDD_NNNNN[X]. YYYYMMDD corresponde al día de generación de la lista, NNNNN es un consecutivo para el día, y el caracter X se refiere a T=total, N=listas negras y P=parciales (tipo de lista).  Ejemplo: 10012_20170801_00001P.
   * @return idList
  **/
  @ApiModelProperty(value = "La lista de usuario es un recurso identificado por el patrón: CODIGOINTERMEDIADOR_YYYYMMDD_NNNNN[X]. YYYYMMDD corresponde al día de generación de la lista, NNNNN es un consecutivo para el día, y el caracter X se refiere a T=total, N=listas negras y P=parciales (tipo de lista).  Ejemplo: 10012_20170801_00001P.")


  public String getIdList() {
    return idList;
  }

  public void setIdList(String idList) {
    this.idList = idList;
  }

  public TransitJson isDiscrepancyPlate(Boolean isDiscrepancyPlate) {
    this.isDiscrepancyPlate = isDiscrepancyPlate;
    return this;
  }

  /**
   * Indica si existe diferencia entre la placa registrada y la detectada. true – Si hay discrepancia false – No hay discrepancia.
   * @return isDiscrepancyPlate
  **/
  @ApiModelProperty(value = "Indica si existe diferencia entre la placa registrada y la detectada. true – Si hay discrepancia false – No hay discrepancia.")


  public Boolean isIsDiscrepancyPlate() {
    return isDiscrepancyPlate;
  }

  public void setIsDiscrepancyPlate(Boolean isDiscrepancyPlate) {
    this.isDiscrepancyPlate = isDiscrepancyPlate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TransitJson transit = (TransitJson) o;
    return Objects.equals(this.idTransaction, transit.idTransaction) &&
        Objects.equals(this.idIntermediador, transit.idIntermediador) &&
        Objects.equals(this.trxDate, transit.trxDate) &&
        Objects.equals(this.idClient, transit.idClient) &&
        Objects.equals(this.plate, transit.plate) &&
        Objects.equals(this.tip, transit.tip) &&
        Objects.equals(this.epc, transit.epc) &&
        Objects.equals(this.station, transit.station) &&
        Objects.equals(this.lane, transit.lane) &&
        Objects.equals(this.amount, transit.amount) &&
        Objects.equals(this.numAxles, transit.numAxles) &&
        Objects.equals(this.numDoubleWheel, transit.numDoubleWheel) &&
        Objects.equals(this.detectedClass, transit.detectedClass) &&
        Objects.equals(this.chargedClass, transit.chargedClass) &&
        Objects.equals(this.readType, transit.readType) &&
        Objects.equals(this.direction, transit.direction) &&
        Objects.equals(this.plateOCR, transit.plateOCR) &&
        Objects.equals(this.isDiscrepancy, transit.isDiscrepancy) &&
        Objects.equals(this.dateList, transit.dateList) &&
        Objects.equals(this.idList, transit.idList) &&
        Objects.equals(this.isDiscrepancyPlate, transit.isDiscrepancyPlate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idTransaction, idIntermediador, trxDate, idClient, plate, tip, epc, station, lane, amount, numAxles, numDoubleWheel, detectedClass, chargedClass, readType, direction, plateOCR, isDiscrepancy, dateList, idList, isDiscrepancyPlate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TransitJson {\n");
    
    sb.append("    idTransaction: ").append(toIndentedString(idTransaction)).append("\n");
    sb.append("    idIntermediador: ").append(toIndentedString(idIntermediador)).append("\n");
    sb.append("    trxDate: ").append(toIndentedString(trxDate)).append("\n");
    sb.append("    idClient: ").append(toIndentedString(idClient)).append("\n");
    sb.append("    plate: ").append(toIndentedString(plate)).append("\n");
    sb.append("    tip: ").append(toIndentedString(tip)).append("\n");
    sb.append("    epc: ").append(toIndentedString(epc)).append("\n");
    sb.append("    station: ").append(toIndentedString(station)).append("\n");
    sb.append("    lane: ").append(toIndentedString(lane)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    numAxles: ").append(toIndentedString(numAxles)).append("\n");
    sb.append("    numDoubleWheel: ").append(toIndentedString(numDoubleWheel)).append("\n");
    sb.append("    detectedClass: ").append(toIndentedString(detectedClass)).append("\n");
    sb.append("    chargedClass: ").append(toIndentedString(chargedClass)).append("\n");
    sb.append("    readType: ").append(toIndentedString(readType)).append("\n");
    sb.append("    direction: ").append(toIndentedString(direction)).append("\n");
    sb.append("    plateOCR: ").append(toIndentedString(plateOCR)).append("\n");
    sb.append("    isDiscrepancy: ").append(toIndentedString(isDiscrepancy)).append("\n");
    sb.append("    dateList: ").append(toIndentedString(dateList)).append("\n");
    sb.append("    idList: ").append(toIndentedString(idList)).append("\n");
    sb.append("    isDiscrepancyPlate: ").append(toIndentedString(isDiscrepancyPlate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

