package es.indra.server.operador.services.impl;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import es.indra.server.operador.daos.AdjDao;
import es.indra.server.operador.entity.SigtDscCodAgency;
import es.indra.server.operador.entity.SigtDscNotifType;
import es.indra.server.operador.entity.SigtOpToIntNotif;
import es.indra.server.operador.entity.SigtOpTrxAdjDscStatus;
import es.indra.server.operador.entity.SigtOpTrxAdjPend;
import es.indra.server.operador.entity.SigtOpTrxAdjPro;
import es.indra.server.operador.model.ConfirmacionProcesamientoAjuste;
import es.indra.server.operador.services.AdjService;
import es.indra.server.operador.utils.GenericConstant;
import es.indra.server.operador.utils.GenericFilterDTO;


/**
 * This class is the implementation of the AdjService interface.
 * 
 * @author rharo
 */
@Service
@Transactional
public class AdjServiceImp extends BaseServiceImp implements AdjService {

	Logger LOGGER = LoggerFactory.getLogger(AdjServiceImp.class);
	
	/** Adjustment DAO */
	private AdjDao adjDao;

	@Override
	public SigtOpTrxAdjPend findSigtOpTrxAdjPend(GenericFilterDTO genericFilterDTOArg) throws Exception {
		return adjDao.findSigtOpTrxAdjPend(genericFilterDTOArg);
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public String updateConfirmedAdj(List<ConfirmacionProcesamientoAjuste> listStatusAdjArg, Date currentDateArg, HttpServletRequest request) throws Exception {

		String auditMessage = new String();

		if(!listStatusAdjArg.isEmpty()){
			
			// notification data
			SigtOpToIntNotif sigtOpToIntNotif = createIntNotif(listStatusAdjArg.size(),currentDateArg);
	
			//process each transaction
			for (ConfirmacionProcesamientoAjuste confirmacionProcesamientoAjuste : listStatusAdjArg) {
	
				LOGGER.debug(confirmacionProcesamientoAjuste.toString()+"\n");
				auditMessage += confirmacionProcesamientoAjuste.toString() + "\n";
				
				String dataTransit[] = confirmacionProcesamientoAjuste.getCodigoAjuste().split("_");
	
				Integer operatorCode = Integer.parseInt(dataTransit[0].toString().trim());
				Long identifier =  Long.parseLong(dataTransit[1].toString().trim()); 
				
				
				//find SigtOpTrxPend by filter
				GenericFilterDTO genericFilterDTO = new GenericFilterDTO();
				genericFilterDTO.setIdentifier(identifier);
				genericFilterDTO.setStatus(GenericConstant.SigtOpTrxAdjDscStatus_ENVIADO);
				SigtOpTrxAdjPend sigtOpTrxAdjPend = (SigtOpTrxAdjPend) findSigtOpTrxAdjPend(genericFilterDTO);
				
				if(sigtOpTrxAdjPend != null && operatorCode.equals(GenericConstant.OPERATOR_CODE)){
					
					SigtOpTrxAdjPro  sigtOpTrxAdjPro =  new SigtOpTrxAdjPro(
							
							sigtOpTrxAdjPend.getIdentifier(),
							sigtOpTrxAdjPend.getSigtTrxAdjDscOptType(),
							sigtOpTrxAdjPend.getSigtDscVehicleClass(),
							sigtOpTrxAdjPend.getSigtOpToIntNotif(),
							sigtOpTrxAdjPend.getSigtOpToIntNotifSigt(),
							sigtOpTrxAdjPend.getSigtTrxAdjDscReason(),
							sigtOpToIntNotif,						
							sigtOpTrxAdjPend.getSigtDscCodAgency(),
							confirmacionProcesamientoAjuste.getCodigoRespuesta(),
							sigtOpTrxAdjPend.getTrxId(),
							sigtOpTrxAdjPend.getTrxDate(),
							sigtOpTrxAdjPend.getTrxCollectionDate(),
							sigtOpTrxAdjPend.getTrxAdjustedId(),
							sigtOpTrxAdjPend.getAdjustmentAmount(),
							sigtOpTrxAdjPend.getAmount(),
							confirmacionProcesamientoAjuste.getTokenConfirmacion(),
							confirmacionProcesamientoAjuste.getDescripcion(),
							currentDateArg,
							GenericConstant.MODIFICATION_USER,
							GenericConstant.APPLICATION_ID,
							null,
							(byte) 0, 
							(byte) 0);
					
					sigtOpTrxAdjPro.setUuid(sigtOpTrxAdjPend.getUuid());
					sigtOpTrxAdjPro.setUuidResponse(request.getAttribute("requestUUID").toString());
					//Save the new SigtOpTrxAdjPro Object, and delete the SigtOpTrxAdjPend object by PK
					adjDao.save(sigtOpTrxAdjPro);				
					
					SigtOpTrxAdjDscStatus sigtOpTrxAdjDscStatus = (SigtOpTrxAdjDscStatus) getDescriptorsById(GenericConstant.SigtOpTrxAdjDscStatus_COMPLETADO, SigtOpTrxAdjDscStatus.class);
					sigtOpTrxAdjPend.setSigtOpTrxAdjDscStatus(sigtOpTrxAdjDscStatus);
					sigtOpTrxAdjPend.setModificationUser(GenericConstant.MODIFICATION_USER);
					sigtOpTrxAdjPend.setApplication(GenericConstant.APPLICATION_ID);
					sigtOpTrxAdjPend.setModificationDate(currentDateArg);
					sigtOpTrxAdjPend.setModification((byte) 1);
					sigtOpTrxAdjPend.setOptimistLock((byte) (sigtOpTrxAdjPend.getOptimistLock() + 1));
					
					adjDao.update(sigtOpTrxAdjPend);
	
				} else {
					LOGGER.warn("V1ApiServiceImpl.v1ConfirmacionesAjustesPut: Adjustment not sent :  " + confirmacionProcesamientoAjuste.getCodigoAjuste());
				}
	
			}
		
		} else {
			LOGGER.warn("V1ApiServiceImpl.v1ConfirmacionesAjustesPut: received list empty ");			
		}
		
		return auditMessage;
	}

	/**
	 * Method that create a notification.
	 * 
	 *@param listSize - list size
	 * @param currentDateArg - current date
	 * @return  SigtOpToIntNotif object
	 */
	private SigtOpToIntNotif createIntNotif(int listSize,Date currentDateArg) throws Exception {
		
		SigtDscCodAgency sigtDscCodAgency = (SigtDscCodAgency) super.getDescriptorsByExternalCode(
				GenericConstant.OPERATOR_CODE.toString(), SigtDscCodAgency.class);
		
		SigtDscNotifType sigtDscNotifType = (SigtDscNotifType)
				super.getDescriptorsById(GenericConstant.SigtDscNotifType_CONFIRMACION_AJUSTES,
				SigtDscNotifType.class);
		
		SigtOpToIntNotif sigtOpToIntNotif = new SigtOpToIntNotif();
		sigtOpToIntNotif.setSigtDscNotifType(sigtDscNotifType);
		sigtOpToIntNotif.setSigtDscCodAgency(sigtDscCodAgency);
		sigtOpToIntNotif.setNumRecords(listSize);
		sigtOpToIntNotif.setApplication(GenericConstant.APPLICATION_ID);
		sigtOpToIntNotif.setModificationUser(GenericConstant.MODIFICATION_USER);
		sigtOpToIntNotif.setCreationDate(currentDateArg);
		sigtOpToIntNotif.setModificationDate(null);
		sigtOpToIntNotif.setModification((byte) 0);
		sigtOpToIntNotif.setOptimistLock((byte) 0);
		
		Long  idenNotif = (Long) adjDao.save(sigtOpToIntNotif);
		sigtOpToIntNotif.setIdentifier(idenNotif);

		return sigtOpToIntNotif;
	}
	
	/**
	 * Return adjDao attribute.
	 *
	 * @return adjDao - Attribute returned
	*/
	public AdjDao getAdjDao() {
		return adjDao;
	}

	/**
	 * Set attribute adjDao.
	 *
	 * @param  adjDaoArg - Set value
	 */
	public void setAdjDao(AdjDao adjDaoArg) {
		adjDao = adjDaoArg;
	}


}