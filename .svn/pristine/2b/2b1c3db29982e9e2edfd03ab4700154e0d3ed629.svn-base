package es.indra.server.operador.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import es.indra.server.operador.bdModel.Audit;
import es.indra.server.operador.daos.AuditDao;
import es.indra.server.operador.daos.DescriptorsDao;
import es.indra.server.operador.log.LogController;
import es.indra.server.operador.services.BaseService;


/**
 * This class is the implementation of the BaseService interface.
 * 
 * @author acoboj
 */
@Service
@Transactional
public class BaseServiceImp implements BaseService {

	/** audit DAO */
	private AuditDao auditDao;
	/** descriptors DAO */
	private DescriptorsDao descriptorsDao;
		
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void registerAudit(Audit audit) throws Exception {
		auditDao.registerAudit(audit);
		LogController.getLog().info("BaseServiceImp.registerAudit: Saving the audit object");
	}

	@Override
	public Object getDescriptorsById(Integer id, Class<?> c) throws Exception {
		return descriptorsDao.getDescriptorsById(id, c);
	}

	@Override
	public Object getDescriptorsByExternalCode(String externalCode, Class<?> c) throws Exception {
		return descriptorsDao.getDescriptorsByExternalCode(externalCode, c);
	}

	/**
	 * Return auditDao attribute.
	 *
	 * @return auditDao - Attribute returned
	*/
	
	public AuditDao getAuditDao() {
		return auditDao;
	}

	/**
	 * Set attribute auditDao.
	 *
	 * @param  auditDao - Set value
	 */
	
	public void setAuditDao(AuditDao auditDao) {
		this.auditDao = auditDao;
	}

	/**
	 * Return descriptorsDao attribute.
	 *
	 * @return descriptorsDao - Attribute returned
	*/
	
	public DescriptorsDao getDescriptorsDao() {
		return descriptorsDao;
	}

	/**
	 * Set attribute descriptorsDao.
	 *
	 * @param  descriptorsDao - Set value
	 */
	
	public void setDescriptorsDao(DescriptorsDao descriptorsDao) {
		this.descriptorsDao = descriptorsDao;
	}
	
	
	

	
}
