package es.indra.server.operador.services.impl;

import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import es.indra.server.operador.bdModel.GenericFilterDTO;
import es.indra.server.operador.bdModel.SigtDscCodAgency;
import es.indra.server.operador.bdModel.SigtDscNotifType;
import es.indra.server.operador.bdModel.SigtDscTrxTestResponseCode;
import es.indra.server.operador.bdModel.SigtOpDscTrxTestNotifStatus;
import es.indra.server.operador.bdModel.SigtOpToIntNotif;
import es.indra.server.operador.bdModel.SigtOpTrxPend;
import es.indra.server.operador.bdModel.SigtOpTrxPro;
import es.indra.server.operador.bdModel.SigtOpTrxTestNotif;
import es.indra.server.operador.bdModel.SigtOpTrxTestResponse;
import es.indra.server.operador.daos.TrxDao;
import es.indra.server.operador.model.ConfirmacionProcesamientoPaso;
import es.indra.server.operador.model.Ticket;
import es.indra.server.operador.services.TrxService;
import es.indra.server.operador.utils.GenericConstant;

/**
 * This class is the implementation of the TrxService interface.
 * 
 * @author rharo
 */
@Service
@Transactional
public class TrxServiceImp extends BaseServiceImp implements TrxService {

	Logger LOGGER = LoggerFactory.getLogger(TrxServiceImp.class);
	
	/** Transaction DAO */
	private TrxDao trxDao;

	@Override
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public Ticket saveSigtOpTrxTestNotif(Date currentDate, String transitCode) throws Exception {
		LOGGER.debug("Transit code: "+transitCode);

		SigtOpDscTrxTestNotifStatus sigtOpDscTrxTestNotifStatus = (SigtOpDscTrxTestNotifStatus) getDescriptorsById(GenericConstant.SigtOpDscTrxTestNotifStatus_PENDIENTE_ENVIAR, SigtOpDscTrxTestNotifStatus.class);
		SigtDscCodAgency sigtDscCodAgencyBD =  (SigtDscCodAgency) getDescriptorsByExternalCode(GenericConstant.INTERMEDIATOR_CODE.toString(), SigtDscCodAgency.class);

		
		String[] split = transitCode.split("_");
		String idTransit = split[1];
		
		LOGGER.debug("SigtOpTrxTestNotif.idTransit: "+idTransit);

		SigtOpTrxTestNotif sigtOpTrxTestNotif = new SigtOpTrxTestNotif(
				null, 
				sigtOpDscTrxTestNotifStatus, 
				idTransit, 
				sigtDscCodAgencyBD,
				currentDate, 
				null, 
				currentDate, 
				GenericConstant.MODIFICATION_USER, 
				GenericConstant.APPLICATION_ID, 
				currentDate, 
				(byte) 0, 
				(byte) 0);
		
		trxDao.save(sigtOpTrxTestNotif);
		trxDao.doFlush();
		LOGGER.debug("Saved sigtOpTrxTestNotif");

		
		SigtDscCodAgency sigtDscCodAgency = (SigtDscCodAgency) getDescriptorsByExternalCode(
				GenericConstant.OPERATOR_CODE.toString(), SigtDscCodAgency.class);
		SigtDscTrxTestResponseCode sigtDscTrxTestResponseCode = (SigtDscTrxTestResponseCode) getDescriptorsById(
					GenericConstant.SigtDscTrxTestResponseCode_TICKET_PENDIENTE_TRATAMIENTO,
					SigtDscTrxTestResponseCode.class);

		LOGGER.debug("SigtOpTrxTestResponse.idTransit: "+idTransit);
		SigtOpTrxTestResponse sigtOpTrxTestResponse = new SigtOpTrxTestResponse(null, sigtDscTrxTestResponseCode,
				sigtDscCodAgency, idTransit, null, currentDate, GenericConstant.MODIFICATION_USER,
				GenericConstant.APPLICATION_ID, currentDate, (byte) 0, (byte) 0);
		Long sigtOpTrxTestResponseId = (Long) trxDao.save(sigtOpTrxTestResponse);
		LOGGER.debug("Saved sigtOpTrxTestNotif");

		
		Ticket ticket = new Ticket();
		ticket.setCodigo(GenericConstant.OPERATOR_CODE + "_" + sigtOpTrxTestResponseId);
		ticket.setFecha(OffsetDateTime.now());
		ticket.setReferencia(transitCode);
		LOGGER.debug("Ticket: "+ticket.toString());
		
		return ticket;
	}
	
	@Override
	public SigtOpTrxPend findSigtOpTrxPend(GenericFilterDTO genericFilterDTOArg) throws Exception {
		return trxDao.findSigtOpTrxPend(genericFilterDTOArg);
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public String updateConfirmedTrx(List<ConfirmacionProcesamientoPaso> listStatusTransitsArg, Date currentDateArg) throws Exception {

		String auditMessage = new String();

		if(!listStatusTransitsArg.isEmpty()){
			
		// notification data
		SigtOpToIntNotif sigtOpToIntNotif = createIntNotif(listStatusTransitsArg.size(),currentDateArg);

		//process each transaction
		LOGGER.debug("Processing transit confirmations...");

		for (ConfirmacionProcesamientoPaso confirmacionProcesamientoPaso : listStatusTransitsArg) {

			auditMessage += confirmacionProcesamientoPaso.toString() + "\n";

			
			LOGGER.debug(confirmacionProcesamientoPaso.toString());
			
			String dataTransit[] = confirmacionProcesamientoPaso.getCodigoPaso().split("_");

			Integer operatorCode = Integer.parseInt(dataTransit[0].toString().trim());
			Long identifier =  Long.parseLong(dataTransit[1].toString().trim()); 
			
			
			//find SigtOpTrxPend by filter
			GenericFilterDTO genericFilterDTO = new GenericFilterDTO();
			genericFilterDTO.setIdentifier(identifier);
			genericFilterDTO.setStatus(GenericConstant.SigtOpTrxDscStatus_ENVIADO);
			SigtOpTrxPend sigtOpTrxPend = (SigtOpTrxPend) findSigtOpTrxPend(genericFilterDTO);
			
			if(sigtOpTrxPend != null && operatorCode.equals(GenericConstant.OPERATOR_CODE)){
												
				SigtOpTrxPro  sigtOpTrxPro =  new SigtOpTrxPro(
						sigtOpTrxPend.getIdentifier(),
						sigtOpTrxPend.getSigtDscVehicleClassByChargedClass(),
						sigtOpTrxPend.getSigtOpToIntNotif(),
						sigtOpTrxPend.getSigtOpToIntNotifSigt(),
						sigtOpTrxPend.getSigtDscPlacesByLaneId(),
						sigtOpToIntNotif,
						sigtOpTrxPend.getSigtDscVehicleClassByDetectedClass(),
						sigtOpTrxPend.getSigtDscCodAgency(),
						confirmacionProcesamientoPaso.getCodigoRespuesta(),
						sigtOpTrxPend.getSigtDscPlacesByStationId(),
						sigtOpTrxPend.getSigtTrxDscReadType(),
						sigtOpTrxPend.getTrxId(),
						sigtOpTrxPend.getTrxDate(),
						sigtOpTrxPend.getTrxCollectionDate(),
						sigtOpTrxPend.getClientId(),
						sigtOpTrxPend.getPlate(),
						sigtOpTrxPend.getTid(),
						sigtOpTrxPend.getEpc(),
						sigtOpTrxPend.getAmount(),
						sigtOpTrxPend.getNumAxles(),
						sigtOpTrxPend.getNumDoubleWheel(),
						sigtOpTrxPend.getDirection(),
						sigtOpTrxPend.getPlateOcr(),
						sigtOpTrxPend.isIsDiscrepancy(),
					    sigtOpTrxPend.getListDate(),
						sigtOpTrxPend.getIdList(),
						sigtOpTrxPend.isIsPlateDiscrepancy(),
						confirmacionProcesamientoPaso.getTokenConfirmacion(),
						confirmacionProcesamientoPaso.getDescripcion(),
						currentDateArg,
						GenericConstant.MODIFICATION_USER,
						GenericConstant.APPLICATION_ID,
						null,
						(byte) 0, 
						(byte) 0);
				
				//Save the new SigtOpTrxPro Object, and delete the SigtOpTrxPend object by PK
				trxDao.save(sigtOpTrxPro);
				//trxDao.deleteByPK(SigtOpTrxPend.class,identifier); //Descomentar en produción

			}else{
				
				LOGGER
				.warn("V1ApiServiceImpl.v1ConfirmacionesPasosPut: Transaction not sent :  "
						+ confirmacionProcesamientoPaso.getCodigoPaso());
			}

		}

		}else{
			LOGGER
			.warn("V1ApiServiceImpl.v1ConfirmacionesPasosPut: received list empty ");
			
		}
		
		return auditMessage;
	}

	/**
	 * Method that create a notification.
	 * 
	 * @param listSizeArg - list size
	 * @param currentDateArg - current date
	 * @return  SigtOpToIntNotif object
	 */
	private SigtOpToIntNotif createIntNotif(int listSizeArg,Date currentDateArg) throws Exception {
		
		SigtDscCodAgency sigtDscCodAgency = (SigtDscCodAgency) super.getDescriptorsByExternalCode(
				GenericConstant.OPERATOR_CODE.toString(), SigtDscCodAgency.class);
		
		SigtDscNotifType sigtDscNotifType = (SigtDscNotifType)
				super.getDescriptorsById(GenericConstant.SigtDscNotifType_CONFIRMACION_VIAJES,
				SigtDscNotifType.class);
		
		SigtOpToIntNotif sigtOpToIntNotif = new SigtOpToIntNotif();
		sigtOpToIntNotif.setSigtDscNotifType(sigtDscNotifType);
		sigtOpToIntNotif.setSigtDscCodAgency(sigtDscCodAgency);
		sigtOpToIntNotif.setNumRecords(listSizeArg);
		sigtOpToIntNotif.setApplication(GenericConstant.APPLICATION_ID);
		sigtOpToIntNotif.setModificationUser(GenericConstant.MODIFICATION_USER);
		sigtOpToIntNotif.setCreationDate(currentDateArg);
		sigtOpToIntNotif.setModificationDate(currentDateArg);
		sigtOpToIntNotif.setModification((byte) 0);
		sigtOpToIntNotif.setOptimistLock((byte) 0);
		
		Long  idenNotif = (Long) trxDao.save(sigtOpToIntNotif);
		sigtOpToIntNotif.setIdentifier(idenNotif);

		return sigtOpToIntNotif;
	}

	/**
	 * Return trxDao attribute.
	 *
	 * @return trxDao - Attribute returned
	 */
	public TrxDao getTrxDao() {
		return trxDao;
	}

	/**
	 * Set attribute trxDao.
	 *
	 * @param trxDaoArg
	 *            - Set value
	 */
	public void setTrxDao(TrxDao trxDaoArg) {
		trxDao = trxDaoArg;
	}


}