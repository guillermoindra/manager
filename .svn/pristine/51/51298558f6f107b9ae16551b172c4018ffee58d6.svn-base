package es.indra.server.operador.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Representa la información de un paso de vehículo. El paso es generado por un Operador OP IP/REV.
 */
@ApiModel(description = "Representa la información de un paso de vehículo. El paso es generado por un Operador OP IP/REV.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-25T10:51:09.761+02:00")

public class Paso   {
  @JsonProperty("codigoPaso")
  private String codigoPaso = null;

  @JsonProperty("codigoOperador")
  private Integer codigoOperador = null;

  @JsonProperty("fechaHora")
  private OffsetDateTime fechaHora = null;

  @JsonProperty("fechaRecaudo")
  private OffsetDateTime fechaRecaudo = null;

  @JsonProperty("codigoCliente")
  private String codigoCliente = null;

  @JsonProperty("placa")
  private String placa = null;

  @JsonProperty("tid")
  private String tid = null;

  @JsonProperty("epc")
  private String epc = null;

  @JsonProperty("estacion")
  private String estacion = null;

  @JsonProperty("carril")
  private String carril = null;

  @JsonProperty("valor")
  private BigDecimal valor = null;

  @JsonProperty("cantidadEjes")
  private Integer cantidadEjes = null;

  @JsonProperty("cantidadDobleRuedas")
  private Integer cantidadDobleRuedas = null;

  @JsonProperty("categoriaDAC")
  private Integer categoriaDAC = null;

  @JsonProperty("categoriaCobrada")
  private String categoriaCobrada = null;

  @JsonProperty("tipoLectura")
  private Integer tipoLectura = null;

  @JsonProperty("sentido")
  private String sentido = null;

  @JsonProperty("placaOCR")
  private String placaOCR = null;

  @JsonProperty("existeDescrepancia")
  private Boolean existeDescrepancia = null;

  @JsonProperty("fechaActualizacionUsuario")
  private OffsetDateTime fechaActualizacionUsuario = null;

  @JsonProperty("codigoListaUsuario")
  private String codigoListaUsuario = null;

  @JsonProperty("existeDiscrepanciaPlaca")
  private Boolean existeDiscrepanciaPlaca = null;

  public Paso codigoPaso(String codigoPaso) {
    this.codigoPaso = codigoPaso;
    return this;
  }

  /**
   * Get codigoPaso
   * @return codigoPaso
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getCodigoPaso() {
    return codigoPaso;
  }

  public void setCodigoPaso(String codigoPaso) {
    this.codigoPaso = codigoPaso;
  }

  public Paso codigoOperador(Integer codigoOperador) {
    this.codigoOperador = codigoOperador;
    return this;
  }

  /**
   * Get codigoOperador
   * @return codigoOperador
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Integer getCodigoOperador() {
    return codigoOperador;
  }

  public void setCodigoOperador(Integer codigoOperador) {
    this.codigoOperador = codigoOperador;
  }

  public Paso fechaHora(OffsetDateTime fechaHora) {
    this.fechaHora = fechaHora;
    return this;
  }

  /**
   * Fecha y hora de la transacción.
   * @return fechaHora
  **/
  @ApiModelProperty(required = true, value = "Fecha y hora de la transacción.")
  @NotNull

  @Valid

  public OffsetDateTime getFechaHora() {
    return fechaHora;
  }

  public void setFechaHora(OffsetDateTime fechaHora) {
    this.fechaHora = fechaHora;
  }

  public Paso fechaRecaudo(OffsetDateTime fechaRecaudo) {
    this.fechaRecaudo = fechaRecaudo;
    return this;
  }

  /**
   * Fecha de recaudo de acuerdo al operador.
   * @return fechaRecaudo
  **/
  @ApiModelProperty(required = true, value = "Fecha de recaudo de acuerdo al operador.")
  @NotNull

  @Valid

  public OffsetDateTime getFechaRecaudo() {
    return fechaRecaudo;
  }

  public void setFechaRecaudo(OffsetDateTime fechaRecaudo) {
    this.fechaRecaudo = fechaRecaudo;
  }

  public Paso codigoCliente(String codigoCliente) {
    this.codigoCliente = codigoCliente;
    return this;
  }

  /**
   * Identificador del cliente. Se sugiere que no empiece por ceros.
   * @return codigoCliente
  **/
  @ApiModelProperty(required = true, value = "Identificador del cliente. Se sugiere que no empiece por ceros.")
  @NotNull

@Pattern(regexp="^[\\S]+$") @Size(min=1,max=100) 
  public String getCodigoCliente() {
    return codigoCliente;
  }

  public void setCodigoCliente(String codigoCliente) {
    this.codigoCliente = codigoCliente;
  }

  public Paso placa(String placa) {
    this.placa = placa;
    return this;
  }

  /**
   * Placa del vehículo.
   * @return placa
  **/
  @ApiModelProperty(required = true, value = "Placa del vehículo.")
  @NotNull

@Pattern(regexp="^[a-zA-Z0-9\\-]+$") @Size(min=6,max=10) 
  public String getPlaca() {
    return placa;
  }

  public void setPlaca(String placa) {
    this.placa = placa;
  }

  public Paso tid(String tid) {
    this.tid = tid;
    return this;
  }

  /**
   * Código tid del tag.
   * @return tid
  **/
  @ApiModelProperty(required = true, value = "Código tid del tag.")
  @NotNull

@Pattern(regexp="^[\\S]+$") @Size(min=24,max=32) 
  public String getTid() {
    return tid;
  }

  public void setTid(String tid) {
    this.tid = tid;
  }

  public Paso epc(String epc) {
    this.epc = epc;
    return this;
  }

  /**
   * Código del EPC asignado al tag según los rangos habilitados.
   * @return epc
  **/
  @ApiModelProperty(required = true, value = "Código del EPC asignado al tag según los rangos habilitados.")
  @NotNull

@Pattern(regexp="^[0-9]+$") @Size(min=24,max=24) 
  public String getEpc() {
    return epc;
  }

  public void setEpc(String epc) {
    this.epc = epc;
  }

  public Paso estacion(String estacion) {
    this.estacion = estacion;
    return this;
  }

  /**
   * Código de la estación de peaje
   * @return estacion
  **/
  @ApiModelProperty(required = true, value = "Código de la estación de peaje")
  @NotNull

@Pattern(regexp="^[\\S]+$") @Size(min=1,max=3) 
  public String getEstacion() {
    return estacion;
  }

  public void setEstacion(String estacion) {
    this.estacion = estacion;
  }

  public Paso carril(String carril) {
    this.carril = carril;
    return this;
  }

  /**
   * Código de carril de tránsito.
   * @return carril
  **/
  @ApiModelProperty(required = true, value = "Código de carril de tránsito.")
  @NotNull

@Pattern(regexp="^[\\S]+$") @Size(min=1,max=10) 
  public String getCarril() {
    return carril;
  }

  public void setCarril(String carril) {
    this.carril = carril;
  }

  public Paso valor(BigDecimal valor) {
    this.valor = valor;
    return this;
  }

  /**
   * Tarifa aplicada a la transacción. Deben ser mayores o igual a 0 (cero para el caso de vehículos exentos).
   * minimum: 0
   * @return valor
  **/
  @ApiModelProperty(required = true, value = "Tarifa aplicada a la transacción. Deben ser mayores o igual a 0 (cero para el caso de vehículos exentos).")
  @NotNull

  @Valid
@DecimalMin("0")
  public BigDecimal getValor() {
    return valor;
  }

  public void setValor(BigDecimal valor) {
    this.valor = valor;
  }

  public Paso cantidadEjes(Integer cantidadEjes) {
    this.cantidadEjes = cantidadEjes;
    return this;
  }

  /**
   * Ejes totales detectados.
   * minimum: 1
   * @return cantidadEjes
  **/
  @ApiModelProperty(required = true, value = "Ejes totales detectados.")
  @NotNull

@Min(1)
  public Integer getCantidadEjes() {
    return cantidadEjes;
  }

  public void setCantidadEjes(Integer cantidadEjes) {
    this.cantidadEjes = cantidadEjes;
  }

  public Paso cantidadDobleRuedas(Integer cantidadDobleRuedas) {
    this.cantidadDobleRuedas = cantidadDobleRuedas;
    return this;
  }

  /**
   * Ejes doble ruedas detectados.
   * minimum: 0
   * @return cantidadDobleRuedas
  **/
  @ApiModelProperty(required = true, value = "Ejes doble ruedas detectados.")
  @NotNull

@Min(0)
  public Integer getCantidadDobleRuedas() {
    return cantidadDobleRuedas;
  }

  public void setCantidadDobleRuedas(Integer cantidadDobleRuedas) {
    this.cantidadDobleRuedas = cantidadDobleRuedas;
  }

  public Paso categoriaDAC(Integer categoriaDAC) {
    this.categoriaDAC = categoriaDAC;
    return this;
  }

  /**
   * Categoría DAC según Ministerio de Transporte.
   * @return categoriaDAC
  **/
  @ApiModelProperty(required = true, value = "Categoría DAC según Ministerio de Transporte.")
  @NotNull


  public Integer getCategoriaDAC() {
    return categoriaDAC;
  }

  public void setCategoriaDAC(Integer categoriaDAC) {
    this.categoriaDAC = categoriaDAC;
  }

  public Paso categoriaCobrada(String categoriaCobrada) {
    this.categoriaCobrada = categoriaCobrada;
    return this;
  }

  /**
   * Código de la categoría según la tabla de categorías de la estación de peaje.
   * @return categoriaCobrada
  **/
  @ApiModelProperty(required = true, value = "Código de la categoría según la tabla de categorías de la estación de peaje.")
  @NotNull

@Size(min=1,max=10) 
  public String getCategoriaCobrada() {
    return categoriaCobrada;
  }

  public void setCategoriaCobrada(String categoriaCobrada) {
    this.categoriaCobrada = categoriaCobrada;
  }

  public Paso tipoLectura(Integer tipoLectura) {
    this.tipoLectura = tipoLectura;
    return this;
  }

  /**
   * Forma de registro vehículo: 1 – Antena, 2 – Manual por placa, 3 – Manual por tag, 4 – Manual por auditoría, 5 – Placa OCR.
   * minimum: 1
   * maximum: 5
   * @return tipoLectura
  **/
  @ApiModelProperty(required = true, value = "Forma de registro vehículo: 1 – Antena, 2 – Manual por placa, 3 – Manual por tag, 4 – Manual por auditoría, 5 – Placa OCR.")
  @NotNull

@Min(1) @Max(5) 
  public Integer getTipoLectura() {
    return tipoLectura;
  }

  public void setTipoLectura(Integer tipoLectura) {
    this.tipoLectura = tipoLectura;
  }

  public Paso sentido(String sentido) {
    this.sentido = sentido;
    return this;
  }

  /**
   * Código del sentido que transita el vehículo. Su estructura debe ser: “3 caracteres origen”-“3 caracteres destino” en mayúscula para mantener el estándar.
   * @return sentido
  **/
  @ApiModelProperty(example = "BOG-MED", required = true, value = "Código del sentido que transita el vehículo. Su estructura debe ser: “3 caracteres origen”-“3 caracteres destino” en mayúscula para mantener el estándar.")
  @NotNull

@Pattern(regexp="[A-Z]{3}\\-[A-Z]{3}") @Size(min=7,max=7) 
  public String getSentido() {
    return sentido;
  }

  public void setSentido(String sentido) {
    this.sentido = sentido;
  }

  public Paso placaOCR(String placaOCR) {
    this.placaOCR = placaOCR;
    return this;
  }

  /**
   * Placa identificada por el sensor OCR
   * @return placaOCR
  **/
  @ApiModelProperty(value = "Placa identificada por el sensor OCR")

@Pattern(regexp="^[a-zA-Z0-9\\-]+$") @Size(min=6,max=10) 
  public String getPlacaOCR() {
    return placaOCR;
  }

  public void setPlacaOCR(String placaOCR) {
    this.placaOCR = placaOCR;
  }

  public Paso existeDescrepancia(Boolean existeDescrepancia) {
    this.existeDescrepancia = existeDescrepancia;
    return this;
  }

  /**
   * Indica si existe diferencia entre la categoría registrada y la detectada  true – Si hay discrepancia, false – No hay discrepancia.
   * @return existeDescrepancia
  **/
  @ApiModelProperty(required = true, value = "Indica si existe diferencia entre la categoría registrada y la detectada  true – Si hay discrepancia, false – No hay discrepancia.")
  @NotNull


  public Boolean isExisteDescrepancia() {
    return existeDescrepancia;
  }

  public void setExisteDescrepancia(Boolean existeDescrepancia) {
    this.existeDescrepancia = existeDescrepancia;
  }

  public Paso fechaActualizacionUsuario(OffsetDateTime fechaActualizacionUsuario) {
    this.fechaActualizacionUsuario = fechaActualizacionUsuario;
    return this;
  }

  /**
   * Fecha en el que se actualizó el saldo. Incluir milisegundos.
   * @return fechaActualizacionUsuario
  **/
  @ApiModelProperty(required = true, value = "Fecha en el que se actualizó el saldo. Incluir milisegundos.")
  @NotNull

  @Valid

  public OffsetDateTime getFechaActualizacionUsuario() {
    return fechaActualizacionUsuario;
  }

  public void setFechaActualizacionUsuario(OffsetDateTime fechaActualizacionUsuario) {
    this.fechaActualizacionUsuario = fechaActualizacionUsuario;
  }

  public Paso codigoListaUsuario(String codigoListaUsuario) {
    this.codigoListaUsuario = codigoListaUsuario;
    return this;
  }

  /**
   * Get codigoListaUsuario
   * @return codigoListaUsuario
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getCodigoListaUsuario() {
    return codigoListaUsuario;
  }

  public void setCodigoListaUsuario(String codigoListaUsuario) {
    this.codigoListaUsuario = codigoListaUsuario;
  }

  public Paso existeDiscrepanciaPlaca(Boolean existeDiscrepanciaPlaca) {
    this.existeDiscrepanciaPlaca = existeDiscrepanciaPlaca;
    return this;
  }

  /**
   * Indica si existe diferencia entre la placa registrada y la detectada. true – Si hay discrepancia false – No hay discrepancia
   * @return existeDiscrepanciaPlaca
  **/
  @ApiModelProperty(required = true, value = "Indica si existe diferencia entre la placa registrada y la detectada. true – Si hay discrepancia false – No hay discrepancia")
  @NotNull


  public Boolean isExisteDiscrepanciaPlaca() {
    return existeDiscrepanciaPlaca;
  }

  public void setExisteDiscrepanciaPlaca(Boolean existeDiscrepanciaPlaca) {
    this.existeDiscrepanciaPlaca = existeDiscrepanciaPlaca;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Paso paso = (Paso) o;
    return Objects.equals(this.codigoPaso, paso.codigoPaso) &&
        Objects.equals(this.codigoOperador, paso.codigoOperador) &&
        Objects.equals(this.fechaHora, paso.fechaHora) &&
        Objects.equals(this.fechaRecaudo, paso.fechaRecaudo) &&
        Objects.equals(this.codigoCliente, paso.codigoCliente) &&
        Objects.equals(this.placa, paso.placa) &&
        Objects.equals(this.tid, paso.tid) &&
        Objects.equals(this.epc, paso.epc) &&
        Objects.equals(this.estacion, paso.estacion) &&
        Objects.equals(this.carril, paso.carril) &&
        Objects.equals(this.valor, paso.valor) &&
        Objects.equals(this.cantidadEjes, paso.cantidadEjes) &&
        Objects.equals(this.cantidadDobleRuedas, paso.cantidadDobleRuedas) &&
        Objects.equals(this.categoriaDAC, paso.categoriaDAC) &&
        Objects.equals(this.categoriaCobrada, paso.categoriaCobrada) &&
        Objects.equals(this.tipoLectura, paso.tipoLectura) &&
        Objects.equals(this.sentido, paso.sentido) &&
        Objects.equals(this.placaOCR, paso.placaOCR) &&
        Objects.equals(this.existeDescrepancia, paso.existeDescrepancia) &&
        Objects.equals(this.fechaActualizacionUsuario, paso.fechaActualizacionUsuario) &&
        Objects.equals(this.codigoListaUsuario, paso.codigoListaUsuario) &&
        Objects.equals(this.existeDiscrepanciaPlaca, paso.existeDiscrepanciaPlaca);
  }

  @Override
  public int hashCode() {
    return Objects.hash(codigoPaso, codigoOperador, fechaHora, fechaRecaudo, codigoCliente, placa, tid, epc, estacion, carril, valor, cantidadEjes, cantidadDobleRuedas, categoriaDAC, categoriaCobrada, tipoLectura, sentido, placaOCR, existeDescrepancia, fechaActualizacionUsuario, codigoListaUsuario, existeDiscrepanciaPlaca);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Paso {\n");
    
    sb.append("    codigoPaso: ").append(toIndentedString(codigoPaso)).append("\n");
    sb.append("    codigoOperador: ").append(toIndentedString(codigoOperador)).append("\n");
    sb.append("    fechaHora: ").append(toIndentedString(fechaHora)).append("\n");
    sb.append("    fechaRecaudo: ").append(toIndentedString(fechaRecaudo)).append("\n");
    sb.append("    codigoCliente: ").append(toIndentedString(codigoCliente)).append("\n");
    sb.append("    placa: ").append(toIndentedString(placa)).append("\n");
    sb.append("    tid: ").append(toIndentedString(tid)).append("\n");
    sb.append("    epc: ").append(toIndentedString(epc)).append("\n");
    sb.append("    estacion: ").append(toIndentedString(estacion)).append("\n");
    sb.append("    carril: ").append(toIndentedString(carril)).append("\n");
    sb.append("    valor: ").append(toIndentedString(valor)).append("\n");
    sb.append("    cantidadEjes: ").append(toIndentedString(cantidadEjes)).append("\n");
    sb.append("    cantidadDobleRuedas: ").append(toIndentedString(cantidadDobleRuedas)).append("\n");
    sb.append("    categoriaDAC: ").append(toIndentedString(categoriaDAC)).append("\n");
    sb.append("    categoriaCobrada: ").append(toIndentedString(categoriaCobrada)).append("\n");
    sb.append("    tipoLectura: ").append(toIndentedString(tipoLectura)).append("\n");
    sb.append("    sentido: ").append(toIndentedString(sentido)).append("\n");
    sb.append("    placaOCR: ").append(toIndentedString(placaOCR)).append("\n");
    sb.append("    existeDescrepancia: ").append(toIndentedString(existeDescrepancia)).append("\n");
    sb.append("    fechaActualizacionUsuario: ").append(toIndentedString(fechaActualizacionUsuario)).append("\n");
    sb.append("    codigoListaUsuario: ").append(toIndentedString(codigoListaUsuario)).append("\n");
    sb.append("    existeDiscrepanciaPlaca: ").append(toIndentedString(existeDiscrepanciaPlaca)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

