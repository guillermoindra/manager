package es.indra.server.audit.service.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import es.indra.server.audit.model.Audit;
import es.indra.server.audit.service.AuditService;

@Service
public class FileAuditService implements AuditService {

	private Logger LOGGER = LoggerFactory.getLogger(FileAuditService.class);

	@Value(value = "${file.directory}")
	private String directory;

	public void saveAudit(Audit audit) {

		Date date = audit.getDateReception();
		SimpleDateFormat format = new SimpleDateFormat("_ddMMyyyy-HHmmssSSS_");

		String inputFileName = directory + "audit-" + format.format(date) + "-INPUT.json";
		String outputFileName = directory + "audit-" + format.format(date) + "-OUTPUT.json";
		
		audit.setFileInput(inputFileName);
		audit.setFileOutput(outputFileName);
		
		saveFile(inputFileName, audit.getFileInput());
		saveFile(outputFileName, audit.getFileOutput());
	}

	public void saveFile(String fileName, String json) {
		try {
			Files.write(Paths.get(fileName), json.getBytes());
		} catch (IOException e) {
			LOGGER.error("Error to write the file: {}", json);
		}
	}

}
