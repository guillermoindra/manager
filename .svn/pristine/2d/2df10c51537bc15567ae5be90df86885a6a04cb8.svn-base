package es.indra.server.operador.services;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import es.indra.server.operador.entity.SigtOpTrxPend;
import es.indra.server.operador.model.ConfirmacionProcesamientoPaso;
import es.indra.server.operador.model.Ticket;
import es.indra.server.operador.utils.GenericFilterDTO;

/**
 * Interface for Transactions Service.
 * 
 * @author rharo
 */
public interface TrxService {

	/**
	 * Find the SigtOpTrxPend object in database by filter.
	 * 
	 * @param genericFilterDTOArg - filter DTO
	 * return SigtOpTrxPend object
	 * @throws Exception - the exception
	 */
	SigtOpTrxPend findSigtOpTrxPend(GenericFilterDTO genericFilterDTOArg) throws Exception;

	/**
	 * Update the confirmed transactions, that have been send .
	 * 
	 * @param listStatusTransitsArg - list of Transactions to process
	 * @param currentDateArg - current date
	 * @param request 
	 * @return String with audit
	 * @throws Exception - the exception
	 */
	String updateConfirmedTrx(List<ConfirmacionProcesamientoPaso> listStatusTransitsArg, Date currentDateArg, HttpServletRequest request) throws Exception;

	/**
	 * Save the object in database.
	 * 
	 * @param transitCode - TransitCode
	 * @param currentDate - Date 
	 * @param request 
	 * @return ticket - Ticket Object
	 * @throws Exception - the exception
	 */
	Ticket saveSigtOpTrxTestNotif(Date currentDate, String transitCode, HttpServletRequest request) throws Exception;
}