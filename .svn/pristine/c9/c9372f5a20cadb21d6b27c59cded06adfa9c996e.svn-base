package es.indra.server.operador.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Representa la información de una negación de autorización de paso de vehículo reportado por un Operador OP IP/REV.
 */
@ApiModel(description = "Representa la información de una negación de autorización de paso de vehículo reportado por un Operador OP IP/REV.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-25T10:51:09.761+02:00")

public class NegacionPaso   {
  @JsonProperty("codigoNegacion")
  private String codigoNegacion = null;

  @JsonProperty("codigoOperador")
  private Integer codigoOperador = null;

  @JsonProperty("fechaHora")
  private OffsetDateTime fechaHora = null;

  @JsonProperty("diaContable")
  private LocalDate diaContable = null;

  @JsonProperty("placa")
  private String placa = null;

  @JsonProperty("tid")
  private String tid = null;

  @JsonProperty("epc")
  private String epc = null;

  @JsonProperty("estacion")
  private String estacion = null;

  @JsonProperty("carril")
  private String carril = null;

  @JsonProperty("tipoLectura")
  private Integer tipoLectura = null;

  @JsonProperty("sentido")
  private String sentido = null;

  @JsonProperty("version")
  private OffsetDateTime version = null;

  @JsonProperty("codigoLista")
  private String codigoLista = null;

  @JsonProperty("razon")
  private String razon = null;

  public NegacionPaso codigoNegacion(String codigoNegacion) {
    this.codigoNegacion = codigoNegacion;
    return this;
  }

  /**
   * Get codigoNegacion
   * @return codigoNegacion
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getCodigoNegacion() {
    return codigoNegacion;
  }

  public void setCodigoNegacion(String codigoNegacion) {
    this.codigoNegacion = codigoNegacion;
  }

  public NegacionPaso codigoOperador(Integer codigoOperador) {
    this.codigoOperador = codigoOperador;
    return this;
  }

  /**
   * Get codigoOperador
   * @return codigoOperador
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Integer getCodigoOperador() {
    return codigoOperador;
  }

  public void setCodigoOperador(Integer codigoOperador) {
    this.codigoOperador = codigoOperador;
  }

  public NegacionPaso fechaHora(OffsetDateTime fechaHora) {
    this.fechaHora = fechaHora;
    return this;
  }

  /**
   * Fecha y hora de la transacción.
   * @return fechaHora
  **/
  @ApiModelProperty(required = true, value = "Fecha y hora de la transacción.")
  @NotNull

  @Valid

  public OffsetDateTime getFechaHora() {
    return fechaHora;
  }

  public void setFechaHora(OffsetDateTime fechaHora) {
    this.fechaHora = fechaHora;
  }

  public NegacionPaso diaContable(LocalDate diaContable) {
    this.diaContable = diaContable;
    return this;
  }

  /**
   * Fecha del día contable de la transacción en formato ISO 8601.
   * @return diaContable
  **/
  @ApiModelProperty(required = true, value = "Fecha del día contable de la transacción en formato ISO 8601.")
  @NotNull

  @Valid

  public LocalDate getDiaContable() {
    return diaContable;
  }

  public void setDiaContable(LocalDate diaContable) {
    this.diaContable = diaContable;
  }

  public NegacionPaso placa(String placa) {
    this.placa = placa;
    return this;
  }

  /**
   * Placa del vehículo.
   * @return placa
  **/
  @ApiModelProperty(required = true, value = "Placa del vehículo.")
  @NotNull

@Pattern(regexp="^[a-zA-Z0-9\\-]+$") @Size(min=6,max=10) 
  public String getPlaca() {
    return placa;
  }

  public void setPlaca(String placa) {
    this.placa = placa;
  }

  public NegacionPaso tid(String tid) {
    this.tid = tid;
    return this;
  }

  /**
   * Código tid del tag.
   * @return tid
  **/
  @ApiModelProperty(required = true, value = "Código tid del tag.")
  @NotNull

@Pattern(regexp="^[\\S]+$") @Size(min=24,max=32) 
  public String getTid() {
    return tid;
  }

  public void setTid(String tid) {
    this.tid = tid;
  }

  public NegacionPaso epc(String epc) {
    this.epc = epc;
    return this;
  }

  /**
   * Código del EPC asignado al tag según los rangos habilitados.
   * @return epc
  **/
  @ApiModelProperty(required = true, value = "Código del EPC asignado al tag según los rangos habilitados.")
  @NotNull

@Pattern(regexp="^[0-9]+$") @Size(min=24,max=24) 
  public String getEpc() {
    return epc;
  }

  public void setEpc(String epc) {
    this.epc = epc;
  }

  public NegacionPaso estacion(String estacion) {
    this.estacion = estacion;
    return this;
  }

  /**
   * Código de la estación de peaje.
   * @return estacion
  **/
  @ApiModelProperty(required = true, value = "Código de la estación de peaje.")
  @NotNull

@Pattern(regexp="^[\\S]+$") @Size(max=3) 
  public String getEstacion() {
    return estacion;
  }

  public void setEstacion(String estacion) {
    this.estacion = estacion;
  }

  public NegacionPaso carril(String carril) {
    this.carril = carril;
    return this;
  }

  /**
   * Código de carril del tránsito.
   * @return carril
  **/
  @ApiModelProperty(required = true, value = "Código de carril del tránsito.")
  @NotNull

@Pattern(regexp="^[\\S]+$") @Size(max=10) 
  public String getCarril() {
    return carril;
  }

  public void setCarril(String carril) {
    this.carril = carril;
  }

  public NegacionPaso tipoLectura(Integer tipoLectura) {
    this.tipoLectura = tipoLectura;
    return this;
  }

  /**
   * Forma de registro del vehículo: 1 – Antena, 2 – Manual por placa, 3 – Manual por tag, 4 – Manual por auditoría, 5 – Placa OCR.
   * @return tipoLectura
  **/
  @ApiModelProperty(required = true, value = "Forma de registro del vehículo: 1 – Antena, 2 – Manual por placa, 3 – Manual por tag, 4 – Manual por auditoría, 5 – Placa OCR.")
  @NotNull


  public Integer getTipoLectura() {
    return tipoLectura;
  }

  public void setTipoLectura(Integer tipoLectura) {
    this.tipoLectura = tipoLectura;
  }

  public NegacionPaso sentido(String sentido) {
    this.sentido = sentido;
    return this;
  }

  /**
   * Código del sentido que transita el vehículo. Su estructura debe ser: “3 caracteres origen”-“3 caracteres destino” en mayúscula para mantener el estándar.
   * @return sentido
  **/
  @ApiModelProperty(example = "BOG-MED", required = true, value = "Código del sentido que transita el vehículo. Su estructura debe ser: “3 caracteres origen”-“3 caracteres destino” en mayúscula para mantener el estándar.")
  @NotNull

@Pattern(regexp="[A-Z]{3}\\-[A-Z]{3}") @Size(min=7,max=7) 
  public String getSentido() {
    return sentido;
  }

  public void setSentido(String sentido) {
    this.sentido = sentido;
  }

  public NegacionPaso version(OffsetDateTime version) {
    this.version = version;
    return this;
  }

  /**
   * Código de la versión del registro del usuario con la que fue autorizada la transacción.
   * @return version
  **/
  @ApiModelProperty(required = true, value = "Código de la versión del registro del usuario con la que fue autorizada la transacción.")
  @NotNull

  @Valid

  public OffsetDateTime getVersion() {
    return version;
  }

  public void setVersion(OffsetDateTime version) {
    this.version = version;
  }

  public NegacionPaso codigoLista(String codigoLista) {
    this.codigoLista = codigoLista;
    return this;
  }

  /**
   * Get codigoLista
   * @return codigoLista
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getCodigoLista() {
    return codigoLista;
  }

  public void setCodigoLista(String codigoLista) {
    this.codigoLista = codigoLista;
  }

  public NegacionPaso razon(String razon) {
    this.razon = razon;
    return this;
  }

  /**
   * Razón por la que el paso no es autorizado.
   * @return razon
  **/
  @ApiModelProperty(required = true, value = "Razón por la que el paso no es autorizado.")
  @NotNull

@Size(max=1000) 
  public String getRazon() {
    return razon;
  }

  public void setRazon(String razon) {
    this.razon = razon;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NegacionPaso negacionPaso = (NegacionPaso) o;
    return Objects.equals(this.codigoNegacion, negacionPaso.codigoNegacion) &&
        Objects.equals(this.codigoOperador, negacionPaso.codigoOperador) &&
        Objects.equals(this.fechaHora, negacionPaso.fechaHora) &&
        Objects.equals(this.diaContable, negacionPaso.diaContable) &&
        Objects.equals(this.placa, negacionPaso.placa) &&
        Objects.equals(this.tid, negacionPaso.tid) &&
        Objects.equals(this.epc, negacionPaso.epc) &&
        Objects.equals(this.estacion, negacionPaso.estacion) &&
        Objects.equals(this.carril, negacionPaso.carril) &&
        Objects.equals(this.tipoLectura, negacionPaso.tipoLectura) &&
        Objects.equals(this.sentido, negacionPaso.sentido) &&
        Objects.equals(this.version, negacionPaso.version) &&
        Objects.equals(this.codigoLista, negacionPaso.codigoLista) &&
        Objects.equals(this.razon, negacionPaso.razon);
  }

  @Override
  public int hashCode() {
    return Objects.hash(codigoNegacion, codigoOperador, fechaHora, diaContable, placa, tid, epc, estacion, carril, tipoLectura, sentido, version, codigoLista, razon);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NegacionPaso {\n");
    
    sb.append("    codigoNegacion: ").append(toIndentedString(codigoNegacion)).append("\n");
    sb.append("    codigoOperador: ").append(toIndentedString(codigoOperador)).append("\n");
    sb.append("    fechaHora: ").append(toIndentedString(fechaHora)).append("\n");
    sb.append("    diaContable: ").append(toIndentedString(diaContable)).append("\n");
    sb.append("    placa: ").append(toIndentedString(placa)).append("\n");
    sb.append("    tid: ").append(toIndentedString(tid)).append("\n");
    sb.append("    epc: ").append(toIndentedString(epc)).append("\n");
    sb.append("    estacion: ").append(toIndentedString(estacion)).append("\n");
    sb.append("    carril: ").append(toIndentedString(carril)).append("\n");
    sb.append("    tipoLectura: ").append(toIndentedString(tipoLectura)).append("\n");
    sb.append("    sentido: ").append(toIndentedString(sentido)).append("\n");
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("    codigoLista: ").append(toIndentedString(codigoLista)).append("\n");
    sb.append("    razon: ").append(toIndentedString(razon)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

