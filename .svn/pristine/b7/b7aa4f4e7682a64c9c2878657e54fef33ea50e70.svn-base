package es.indra.server.operador.configuration;

import java.io.IOException;
import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AvailableSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import es.indra.server.operador.daos.impl.AuditDaoImp;
import es.indra.server.operador.daos.impl.DescriptorsDaoImp;
import es.indra.server.operador.services.impl.BaseServiceImp;

/**
 * 
 * @author bytesTree
 * @see <a href="http://www.bytestree.com/">BytesTree</a>
 * 
 */
//@Configuration
//@EnableTransactionManagement
//@PropertySource(value = { "classpath:database.properties" })
public class HibernateConfig {

	@Autowired
	private Environment env;
	
	@Autowired
	private ResourceLoader rl;

	/**
	 * Initialize dataSource
	 * 
	 * @return DataSource
	 */
	@Bean
	public DataSource getDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getRequiredProperty("driver"));
		dataSource.setUrl(env.getRequiredProperty("url"));
		dataSource.setUsername(env.getRequiredProperty("username"));
		dataSource.setPassword(env.getRequiredProperty("password"));
		return dataSource;
	}
	
	/*@Bean
	public BaseServiceImp baseService(DescriptorsDaoImp descriptorsDaoImp) {
		BaseServiceImp baseServiceImp = new BaseServiceImp();
		baseServiceImp.setAuditDao(new AuditDaoImp());
		baseServiceImp.setDescriptorsDao(descriptorsDaoImp);
		return baseServiceImp;
	}*/
	
	
	/**
	 * Initialize hibernate properties
	 * 
	 * @return Properties
	 */
	private Properties getHibernateProperties() {
		Properties properties = new Properties();
		properties.put(AvailableSettings.DIALECT, env.getRequiredProperty("hibernate.dialect"));
		properties.put(AvailableSettings.SHOW_SQL, true);
		//properties.put(AvailableSettings.STATEMENT_BATCH_SIZE, env.getRequiredProperty("hibernate.batch.size"));
		//properties.put(AvailableSettings.HBM2DDL_AUTO, env.getRequiredProperty("hibernate.hbm2ddl.auto"));
		//properties.put(AvailableSettings.CURRENT_SESSION_CONTEXT_CLASS, env.getRequiredProperty("hibernate.current.session.context.class"));
		return properties;
	}
	
	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(getDataSource());
		sessionFactory.setMappingLocations(loadResources());//PackagesToScan(new String[] { "com.bytestree.model" });
		sessionFactory.setHibernateProperties(getHibernateProperties());
		return sessionFactory;
	}
			
	public Resource[] loadResources() {
	    Resource[] resources = null;
	    try {
	        resources = ResourcePatternUtils.getResourcePatternResolver(rl)
	                .getResources("classpath:/mappings/*.hbm.xml");
	    } catch (IOException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    }
	    return resources;
	}


	/**
	 * Initialize Transaction Manager
	 * 
	 * @param sessionFactory
	 * @return HibernateTransactionManager
	 */
	@Bean
	public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager txManager = new HibernateTransactionManager();
		txManager.setSessionFactory(sessionFactory);
		return txManager;
	}
	
	
}
