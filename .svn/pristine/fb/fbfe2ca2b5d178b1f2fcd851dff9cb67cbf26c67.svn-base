package es.indra.server.via.api;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.indra.server.via.entity.SigtDscCodAgency;
import es.indra.server.via.entity.SigtDscPlaces;
import es.indra.server.via.entity.SigtDscVehicleClass;
import es.indra.server.via.entity.SigtOpTrxDscStatus;
import es.indra.server.via.entity.SigtOpTrxPend;
import es.indra.server.via.model.TransitJson;
import es.indra.server.via.model.TransitsJson;
import es.indra.server.via.service.DescriptorService;
import es.indra.server.via.service.SqlService;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-23T09:38:53.430+02:00")
@Controller
public class ViaApiController implements ViaApi {
	
	Logger LOGGER = LoggerFactory.getLogger(ViaApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;
    
    @Autowired
    private SqlService sqlTransitService;  
    
    @Autowired
    private DescriptorService descriptorService ;
    
    @Autowired
    public ViaApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @Override
    public Optional<ObjectMapper> getObjectMapper() {
        return Optional.ofNullable(objectMapper);
    }

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.ofNullable(request);
    }

	@Override
	public ResponseEntity<Void> viaTransitsPost(@Valid @RequestBody TransitsJson body) {
		LOGGER.info("Controller transitsPost (Num Transits): " + body.size());
		
//		auditApi.sendAudit(body.toString());
		
		try {
	    
			for (int i = 0; i < body.size(); i++) {
				TransitJson transitDto = body.get(i);
				SigtOpTrxPend transit = new SigtOpTrxPend();
				
				SigtDscCodAgency codAgency = (SigtDscCodAgency) descriptorService.getDescriptorsByCodAgency(Long.valueOf(transitDto.getIdIntermediador()), SigtDscCodAgency.class) ;
				transit.setCodAgency(codAgency);
				
				transit.setTrxId(transitDto.getIdTransaction());
				transit.setPlate(transitDto.getPlate());
				transit.setEpc(transitDto.getEpc());
				transit.setTid(transitDto.getTid());
				
				SigtDscPlaces lane = (SigtDscPlaces) descriptorService.getDescriptorsByIdPlace(Long.parseLong(transitDto.getLane()), SigtDscPlaces.class) ;
				transit.setLaneId(lane);
				if (lane != null) transit.setLaneExternalId(lane.getExternalCode()); 
				
				lane = (SigtDscPlaces) descriptorService.getDescriptorsByIdPlace(Long.parseLong(transitDto.getStation()), SigtDscPlaces.class) ;
				transit.setStationId(lane);	
				
				SigtDscVehicleClass vehicleClass = (SigtDscVehicleClass) descriptorService.getDescriptorsByIdVehicle(Long.valueOf(transitDto.getDetectedClass()), SigtDscVehicleClass.class) ;
				transit.setDetectedClass(vehicleClass);
				
				vehicleClass = (SigtDscVehicleClass) descriptorService.getDescriptorsByIdVehicle(Long.valueOf(transitDto.getChargedClass()), SigtDscVehicleClass.class) ;
				transit.setChargedClass(vehicleClass) ;
				
				transit.setIdNotif(null);
				transit.setIdNotifSigt(null);			
				transit.setNumSend(0);
				
				SigtOpTrxDscStatus status = (SigtOpTrxDscStatus) descriptorService.getDescriptorsByIdStatus((long) 1, SigtOpTrxDscStatus.class) ;
				transit.setStatus(status);
				
				transit.setReadType(transitDto.getReadType());
				transit.setTrxDate(new Date(transitDto.getTrxDate().toInstant().toEpochMilli()));		
				transit.setClientId(transitDto.getIdClient());		
				transit.setAmount(new BigDecimal(transitDto.getAmount()));		
				transit.setNumAxles(transitDto.getNumAxles());
				transit.setNumDoubleWheel(transitDto.getNumDoubleWheel());
				transit.setDirection(transitDto.getDirection());
				transit.setPlateOcr(transitDto.getPlateOCR());
				transit.setIsDiscrepancy(transitDto.isIsDiscrepancy());		
				transit.setListDate(new Date(transitDto.getDateList().toInstant().toEpochMilli()));			
				transit.setIdList(transitDto.getIdList());
				transit.setIsPlateDiscrepancy(transitDto.isIsDiscrepancyPlate());
				transit.setCreationDate(new Date());			
				transit.setModificationUser("WS_VIA");
				//Por definir
				transit.setApplication(0);
				transit.setModificationDate(null);
				transit.setModification((byte) 1);
				transit.setOptimistLock((byte) 1);
				
				
				//if (i==2) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	
				LOGGER.info("Save (Transits): " + transit.getTrxId());
				sqlTransitService.saveTransit(transit);
			}	
		
		} catch (Exception e) {			
			LOGGER.error("viaTransitsPost: General error occurred during execution!. " + e.getMessage());
			LOGGER.error(GenericUtils.stackTraceStr(e));
			new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<>(HttpStatus.OK);
		//return TransitsApi.super.transitsPost(body);
	}
	
	/*@ModelAttribute
	protected void logging(HttpServletRequest request, HttpServletResponse response) { 
		LOGGER.info("logging: " + request.getRequestURI() + " " + response.getStatus());
	}*/
    
    

}
