<?xml version="1.0" encoding="UTF-8"?>
<configuration debug="true" scan="true" scanPeriod="30 seconds">


	<include resource="org/springframework/boot/logging/logback/defaults.xml" />

	<property name="LOG_PATH" value="logs" />
	<property name="LOG_ARCHIVE" value="${LOG_PATH}/archive" />
	<timestamp key="timestamp-by-second" datePattern="yyyy-MM-dd'T'HHmmss" />
	<timestamp key="timestamp-by-day" datePattern="yyyy-MM-dd" />

	<appender name="Console-Appender" class="ch.qos.logback.core.ConsoleAppender" level="warn">
		<layout>
			<pattern>%yellow(%d{yyyy-MM-dd HH:mm:ss.SSS}) %magenta([%thread]) %highlight(%-2level) %logger{36}.%M - %msg%n</pattern>
		</layout>
	</appender>

	<appender name="File-Appender" class="ch.qos.logback.core.FileAppender">
		<file>${LOG_PATH}/logfile-${timestamp-by-day}.log</file>
		<encoder>
			<pattern>%d{dd-MM-yyyy HH:mm:ss.SSS} [%thread] %-5level %logger{36}.%M - %msg%n</pattern>
			<outputPatternAsHeader>true</outputPatternAsHeader>
		</encoder>
	</appender>

	<appender name="FileTest-Appender" class="ch.qos.logback.core.FileAppender">
		<file>${LOG_PATH}/logfileTest-${timestamp-by-day}.log</file>
		<encoder>
			<pattern>%d{dd-MM-yyyy HH:mm:ss.SSS} [%thread] %-5level %logger{36}.%M - %msg%n</pattern>
			<outputPatternAsHeader>true</outputPatternAsHeader>
		</encoder>
	</appender>

	<appender name="Spring-Appender" class="ch.qos.logback.core.FileAppender">
		<file>${LOG_PATH}/logfileSpring-${timestamp-by-day}.log</file>
		<encoder>
			<pattern>%d{dd-MM-yyyy HH:mm:ss.SSS} [%thread] %-5level %logger{36}.%M - %msg%n</pattern>
			<outputPatternAsHeader>true</outputPatternAsHeader>
		</encoder>
	</appender>

	<appender name="RollingFile-Appender"
		class="ch.qos.logback.core.rolling.RollingFileAppender">
		<file>${LOG_PATH}/rollingfile.log</file>
		<rollingPolicy
			class="ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy">
			<fileNamePattern>${LOG_ARCHIVE}/rollingfile.log_%d{yyyy-MM-dd}_%i.log.zip
			</fileNamePattern>
			<!-- each file should be at most 100MB, keep 30 days worth of history, 
				but at most 1GB -->
			<maxFileSize>100MB</maxFileSize>
			<maxHistory>30</maxHistory>
			<totalSizeCap>1GB</totalSizeCap>
		</rollingPolicy>
		<encoder>
			<pattern>%d{dd-MM-yyyy HH:mm:ss.SSS} [%thread] %-5level %logger{36}.%M - %msg%n</pattern>
		</encoder>
	</appender>

	<appender name="Async-Appender" class="ch.qos.logback.classic.AsyncAppender">
		<appender-ref ref="RollingFile-Appender" />
	</appender>

	<logger name="org.springframework" level="info">
		<appender-ref ref="Spring-Appender" />
		<appender-ref ref="Async-Appender" />
		<appender-ref ref="Console-Appender" />	
	</logger>
		
	<logger name="es.indra.server.via.test" level="debug" additivity="true">		
		<appender-ref ref="FileTest-Appender" />
		<appender-ref ref="Async-Appender" />
	</logger>
	
	<logger name="es.indra.server.via" level="debug">		
		<appender-ref ref="File-Appender" />
		<appender-ref ref="Async-Appender" />
		<appender-ref ref="Console-Appender" />		
	</logger>
	
	<logger name="es.indra.common" level="debug">		
		<appender-ref ref="File-Appender" />
		<appender-ref ref="Async-Appender" />
		<appender-ref ref="Console-Appender" />		
	</logger>
	
	
	<root>	
	</root>

</configuration>

