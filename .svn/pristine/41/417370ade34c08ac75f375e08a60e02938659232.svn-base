package es.indra.server.operador.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * SigtOpTrxPend class, implements the object for transactions pending to send.
 * 
 */
@Entity
@Table(name="SIGT_OP_TRX_PEND")
public class SigtOpTrxPend implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="IDENTIFIER")
	private Long identifier;
	
	@Column(name = "UUID", nullable = false, length = 100)
	private String uuid;
	
	@ManyToOne(fetch = FetchType.LAZY) // default is EAGER
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "CHARGED_CLASS")
	private SigtDscVehicleClass sigtDscVehicleClassByChargedClass;
	
	@ManyToOne(fetch = FetchType.LAZY) // default is EAGER
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "ID_NOTIF")
	private SigtOpToIntNotif sigtOpToIntNotif;
	
	@ManyToOne(fetch = FetchType.LAZY) // default is EAGER
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "ID_NOTIF_SIGT")
	private SigtOpToIntNotif sigtOpToIntNotifSigt;
	
	@ManyToOne(fetch = FetchType.LAZY) // default is EAGER
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "LANE_ID", nullable = false)
	private SigtDscPlaces sigtDscPlacesByLaneId;
	
	@ManyToOne(fetch = FetchType.LAZY) // default is EAGER
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "STATUS", nullable = false)
	private SigtOpTrxDscStatus sigtOpTrxDscStatus;
	
	@ManyToOne(fetch = FetchType.LAZY) // default is EAGER
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "DETECTED_CLASS")
	private SigtDscVehicleClass sigtDscVehicleClassByDetectedClass;
	
	@ManyToOne(fetch = FetchType.LAZY) // default is EAGER
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "COD_AGENCY", nullable = false)
	private SigtDscCodAgency sigtDscCodAgency;
	
	@ManyToOne(fetch = FetchType.LAZY) // default is EAGER
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "STATION_ID", nullable = false)
	private SigtDscPlaces sigtDscPlacesByStationId;
	
	@ManyToOne(fetch = FetchType.LAZY) // default is EAGER
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "READ_TYPE")
	private SigtTrxDscReadType sigtTrxDscReadType;
	
	@Column(name = "TRX_ID", nullable = false)
	private Long trxId;
	
	@Column(name = "TRX_DATE", length = 23, nullable = false)
	private Date trxDate;
	
	//@Column(name = "TRX_COLLECTION_DATE", length = 23)
	//private Date trxCollectionDate;
	
	@Column(name = "CLIENT_ID", length = 50)
	private String clientId;
	
	@Column(name = "PLATE", length = 50)
	private String plate;
	
	@Column(name = "TID", length = 50, nullable = false)
	private String tid;
	
	@Column(name = "EPC", length = 50)
	private String epc;
	
	@Column(name = "LANE_EXTERNAL_ID", length = 50)
	private String laneExternalId;
	
	@Column(name = "NUM_SEND", nullable = false)
	private Integer numSend;
	
	@Column(name = "AMOUNT", precision = 18, nullable = false)
	private BigDecimal amount;
	
	@Column(name = "NUM_AXLES", nullable = false)
	private Integer numAxles;
	
	@Column(name = "NUM_DOUBLE_WHEEL", nullable = false)
	private Integer numDoubleWheel;
	
	@Column(name = "DIRECTION", length = 50)
	private String direction;
	
	@Column(name = "PLATE_OCR", length = 50)
	private String plateOcr;
	
	@Column(name = "IS_DISCREPANCY", nullable = false)
	private Boolean isDiscrepancy;
	
	@Column(name = "LIST_DATE", length = 23)
	private Date listDate;
	
	@Column(name = "ID_LIST", length = 100)
	private String idList;
	
	@Column(name = "IS_PLATE_DISCREPANCY", nullable = false)
	private Boolean isPlateDiscrepancy;
	
	@Column(name = "CREATION_DATE", length = 23, nullable = false)
	private Date creationDate;
	
	@Column(name = "MODIFICATION_USER", length = 50)
	private String modificationUser;
	
	@Column(name = "APPLICATION", nullable = false)
	private Integer application;
	
	@Column(name = "MODIFICATION_DATE", length = 23, nullable = false)
	private Date modificationDate;
	
	@Column(name = "MODIFICATION", nullable = false)
	private Byte modification;
	
	@Column(name = "OPTIMIST_LOCK", nullable = false)
	private Byte optimistLock;

	public SigtOpTrxPend() {
	}
	
	
	
	public SigtOpTrxPend(Long identifier, String uuid, SigtDscVehicleClass sigtDscVehicleClassByChargedClass,
			SigtOpToIntNotif sigtOpToIntNotif, SigtOpToIntNotif sigtOpToIntNotifSigt,
			SigtDscPlaces sigtDscPlacesByLaneId, SigtOpTrxDscStatus sigtOpTrxDscStatus,
			SigtDscVehicleClass sigtDscVehicleClassByDetectedClass, SigtDscCodAgency sigtDscCodAgency,
			SigtDscPlaces sigtDscPlacesByStationId, SigtTrxDscReadType sigtTrxDscReadType, Long trxId, Date trxDate,
			String clientId, String plate, String tid, String epc, String laneExternalId,
			Integer numSend, BigDecimal amount, Integer numAxles, Integer numDoubleWheel, String direction,
			String plateOcr, Boolean isDiscrepancy, Date listDate, String idList, Boolean isPlateDiscrepancy,
			Date creationDate, String modificationUser, Integer application, Date modificationDate, Byte modification,
			Byte optimistLock) {
		super();
		this.identifier = identifier;
		this.uuid = uuid;
		this.sigtDscVehicleClassByChargedClass = sigtDscVehicleClassByChargedClass;
		this.sigtOpToIntNotif = sigtOpToIntNotif;
		this.sigtOpToIntNotifSigt = sigtOpToIntNotifSigt;
		this.sigtDscPlacesByLaneId = sigtDscPlacesByLaneId;
		this.sigtOpTrxDscStatus = sigtOpTrxDscStatus;
		this.sigtDscVehicleClassByDetectedClass = sigtDscVehicleClassByDetectedClass;
		this.sigtDscCodAgency = sigtDscCodAgency;
		this.sigtDscPlacesByStationId = sigtDscPlacesByStationId;
		this.sigtTrxDscReadType = sigtTrxDscReadType;
		this.trxId = trxId;
		this.trxDate = trxDate;
		this.clientId = clientId;
		this.plate = plate;
		this.tid = tid;
		this.epc = epc;
		this.laneExternalId = laneExternalId;
		this.numSend = numSend;
		this.amount = amount;
		this.numAxles = numAxles;
		this.numDoubleWheel = numDoubleWheel;
		this.direction = direction;
		this.plateOcr = plateOcr;
		this.isDiscrepancy = isDiscrepancy;
		this.listDate = listDate;
		this.idList = idList;
		this.isPlateDiscrepancy = isPlateDiscrepancy;
		this.creationDate = creationDate;
		this.modificationUser = modificationUser;
		this.application = application;
		this.modificationDate = modificationDate;
		this.modification = modification;
		this.optimistLock = optimistLock;
	}



	public Long getIdentifier() {
		return this.identifier;
	}

	public void setIdentifier(Long identifier) {
		this.identifier = identifier;
	}

	public SigtDscVehicleClass getSigtDscVehicleClassByChargedClass() {
		return this.sigtDscVehicleClassByChargedClass;
	}

	public void setSigtDscVehicleClassByChargedClass(SigtDscVehicleClass sigtDscVehicleClassByChargedClass) {
		this.sigtDscVehicleClassByChargedClass = sigtDscVehicleClassByChargedClass;
	}

	public SigtOpToIntNotif getSigtOpToIntNotif() {
		return this.sigtOpToIntNotif;
	}

	public void setSigtOpToIntNotifSigt(SigtOpToIntNotif sigtOpToIntNotifSigt) {
		this.sigtOpToIntNotifSigt = sigtOpToIntNotifSigt;
	}
	
	public SigtOpToIntNotif getSigtOpToIntNotifSigt() {
		return this.sigtOpToIntNotifSigt;
	}

	public void setSigtOpToIntNotif(SigtOpToIntNotif sigtOpToIntNotif) {
		this.sigtOpToIntNotif = sigtOpToIntNotif;
	}

	public SigtDscPlaces getSigtDscPlacesByLaneId() {
		return this.sigtDscPlacesByLaneId;
	}

	public void setSigtDscPlacesByLaneId(SigtDscPlaces sigtDscPlacesByLaneId) {
		this.sigtDscPlacesByLaneId = sigtDscPlacesByLaneId;
	}

	public SigtOpTrxDscStatus getSigtOpTrxDscStatus() {
		return this.sigtOpTrxDscStatus;
	}

	public void setSigtOpTrxDscStatus(SigtOpTrxDscStatus sigtOpTrxDscStatus) {
		this.sigtOpTrxDscStatus = sigtOpTrxDscStatus;
	}

	public SigtDscVehicleClass getSigtDscVehicleClassByDetectedClass() {
		return this.sigtDscVehicleClassByDetectedClass;
	}

	public void setSigtDscVehicleClassByDetectedClass(SigtDscVehicleClass sigtDscVehicleClassByDetectedClass) {
		this.sigtDscVehicleClassByDetectedClass = sigtDscVehicleClassByDetectedClass;
	}

	public SigtDscCodAgency getSigtDscCodAgency() {
		return this.sigtDscCodAgency;
	}

	public void setSigtDscCodAgency(SigtDscCodAgency sigtDscCodAgency) {
		this.sigtDscCodAgency = sigtDscCodAgency;
	}

	public SigtDscPlaces getSigtDscPlacesByStationId() {
		return this.sigtDscPlacesByStationId;
	}

	public void setSigtDscPlacesByStationId(SigtDscPlaces sigtDscPlacesByStationId) {
		this.sigtDscPlacesByStationId = sigtDscPlacesByStationId;
	}

	public SigtTrxDscReadType getSigtTrxDscReadType() {
		return this.sigtTrxDscReadType;
	}

	public void setSigtTrxDscReadType(SigtTrxDscReadType sigtTrxDscReadType) {
		this.sigtTrxDscReadType = sigtTrxDscReadType;
	}

	public Long getTrxId() {
		return this.trxId;
	}

	public void setTrxId(Long trxId) {
		this.trxId = trxId;
	}

	public Date getTrxDate() {
		return this.trxDate;
	}

	public void setTrxDate(Date trxDate) {
		this.trxDate = trxDate;
	}
	
	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getPlate() {
		return this.plate;
	}

	public void setPlate(String plate) {
		this.plate = plate;
	}

	public String getTid() {
		return this.tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getEpc() {
		return this.epc;
	}

	public void setEpc(String epc) {
		this.epc = epc;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getNumAxles() {
		return this.numAxles;
	}

	public void setNumAxles(Integer numAxles) {
		this.numAxles = numAxles;
	}

	public Integer getNumDoubleWheel() {
		return this.numDoubleWheel;
	}

	public void setNumDoubleWheel(Integer numDoubleWheel) {
		this.numDoubleWheel = numDoubleWheel;
	}

	public String getDirection() {
		return this.direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getPlateOcr() {
		return this.plateOcr;
	}

	public void setPlateOcr(String plateOcr) {
		this.plateOcr = plateOcr;
	}

	public Boolean isIsDiscrepancy() {
		return this.isDiscrepancy;
	}

	public void setIsDiscrepancy(Boolean isDiscrepancy) {
		this.isDiscrepancy = isDiscrepancy;
	}

	public Date getListDate() {
		return this.listDate;
	}

	public void setListDate(Date listDate) {
		this.listDate = listDate;
	}

	public String getIdList() {
		return this.idList;
	}

	public void setIdList(String idList) {
		this.idList = idList;
	}

	public Boolean isIsPlateDiscrepancy() {
		return this.isPlateDiscrepancy;
	}

	public void setIsPlateDiscrepancy(Boolean isPlateDiscrepancy) {
		this.isPlateDiscrepancy = isPlateDiscrepancy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getModificationUser() {
		return this.modificationUser;
	}

	public void setModificationUser(String modificationUser) {
		this.modificationUser = modificationUser;
	}

	public Integer getApplication() {
		return this.application;
	}

	public void setApplication(Integer application) {
		this.application = application;
	}

	public Date getModificationDate() {
		return this.modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Byte getModification() {
		return this.modification;
	}

	public void setModification(Byte modification) {
		this.modification = modification;
	}

	public Byte getOptimistLock() {
		return this.optimistLock;
	}

	public void setOptimistLock(Byte optimistLock) {
		this.optimistLock = optimistLock;
	}

	public String getLaneExternalId() {
		return laneExternalId;
	}

	public void setLaneExternalId(String laneExternalId) {
		this.laneExternalId = laneExternalId;
	}



	public Integer getNumSend() {
		return numSend;
	}



	public void setNumSend(Integer numSend) {
		this.numSend = numSend;
	}



	public String getUuid() {
		return uuid;
	}



	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
