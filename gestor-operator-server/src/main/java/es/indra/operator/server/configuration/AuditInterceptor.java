package es.indra.operator.server.configuration;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

@Component
public class AuditInterceptor extends HandlerInterceptorAdapter {

	private static final Logger logger = LoggerFactory.getLogger(AuditInterceptor.class);

	public AuditInterceptor() {
		super();
	}
	
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestId = UUID.randomUUID().toString();
        log(request,response, requestId);
        long startTime = System.currentTimeMillis();
        request.setAttribute("startTime", startTime);
        request.setAttribute("requestUUID", requestId);
        return true;
    }

     @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        super.afterCompletion(request, response, handler, ex);
        long startTime = (Long)request.getAttribute("startTime");
        long endTime = System.currentTimeMillis();
        long executeTime = endTime - startTime;
        logger.info("requestId {}, Handle :{} , request take time: {} Status : {}",request.getAttribute("requestUUID"), handler, executeTime, response.getStatus());
    }
     
	private void log(HttpServletRequest request, HttpServletResponse response, String requestId) {
	    logger.info("requestId {}, host {}  HttpMethod: {}, URI : {} Status : {}",requestId, request.getHeader("host"), 
	    		request.getMethod(), request.getRequestURI(), response.getStatus() );
	}
	
	
	
}
