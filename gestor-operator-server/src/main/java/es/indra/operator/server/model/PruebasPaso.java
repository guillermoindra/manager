package es.indra.operator.server.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;

import es.indra.operator.server.model.Ticket;

import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Representa las pruebas de paso de un vehículo.
 */
@ApiModel(description = "Representa las pruebas de paso de un vehículo.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-25T10:51:09.761+02:00")

public class PruebasPaso   {
  @JsonProperty("ticket")
  private Ticket ticket = null;

  @JsonProperty("codigoPaso")
  private String codigoPaso = null;

  @JsonProperty("codigoRespuesta")
  private Integer codigoRespuesta = null;

  @JsonProperty("pruebas")
  @Valid
  private List<byte[]> pruebas = new ArrayList<>();

  public PruebasPaso ticket(Ticket ticket) {
    this.ticket = ticket;
    return this;
  }

  /**
   * Get ticket
   * @return ticket
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public Ticket getTicket() {
    return ticket;
  }

  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
  }

  public PruebasPaso codigoPaso(String codigoPaso) {
    this.codigoPaso = codigoPaso;
    return this;
  }

  /**
   * Get codigoPaso
   * @return codigoPaso
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getCodigoPaso() {
    return codigoPaso;
  }

  public void setCodigoPaso(String codigoPaso) {
    this.codigoPaso = codigoPaso;
  }

  public PruebasPaso codigoRespuesta(Integer codigoRespuesta) {
    this.codigoRespuesta = codigoRespuesta;
    return this;
  }

  /**
   * 1 – Ok, 2001 – Transacción no encontrada.
   * @return codigoRespuesta
  **/
  @ApiModelProperty(required = true, value = "1 – Ok, 2001 – Transacción no encontrada.")
  @NotNull


  public Integer getCodigoRespuesta() {
    return codigoRespuesta;
  }

  public void setCodigoRespuesta(Integer codigoRespuesta) {
    this.codigoRespuesta = codigoRespuesta;
  }

  public PruebasPaso pruebas(List<byte[]> pruebas) {
    this.pruebas = pruebas;
    return this;
  }

  public PruebasPaso addPruebasItem(byte[] pruebasItem) {
    this.pruebas.add(pruebasItem);
    return this;
  }

  /**
   * Lista de fotografias. Cada elemento del arreglo corresponde a un string como representación en Base64 del contenido del archivo.
   * @return pruebas
  **/
  @ApiModelProperty(required = true, value = "Lista de fotografias. Cada elemento del arreglo corresponde a un string como representación en Base64 del contenido del archivo.")
  @NotNull


  public List<byte[]> getPruebas() {
    return pruebas;
  }

  public void setPruebas(List<byte[]> pruebas) {
    this.pruebas = pruebas;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PruebasPaso pruebasPaso = (PruebasPaso) o;
    return Objects.equals(this.ticket, pruebasPaso.ticket) &&
        Objects.equals(this.codigoPaso, pruebasPaso.codigoPaso) &&
        Objects.equals(this.codigoRespuesta, pruebasPaso.codigoRespuesta) &&
        Objects.equals(this.pruebas, pruebasPaso.pruebas);
  }

  @Override
  public int hashCode() {
    return Objects.hash(ticket, codigoPaso, codigoRespuesta, pruebas);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PruebasPaso {\n");
    
    sb.append("    ticket: ").append(toIndentedString(ticket)).append("\n");
    sb.append("    codigoPaso: ").append(toIndentedString(codigoPaso)).append("\n");
    sb.append("    codigoRespuesta: ").append(toIndentedString(codigoRespuesta)).append("\n");
    sb.append("    pruebas: ").append(toIndentedString(pruebas)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

