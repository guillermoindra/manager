package es.indra.operator.server.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.indra.operator.server.entity.SigtDscCodAgency;
import es.indra.operator.server.entity.SigtDscCodListType;
import es.indra.operator.server.entity.SigtOpBalance;
import es.indra.operator.server.entity.SigtOpDscLstNotifStatus;
import es.indra.operator.server.entity.SigtOpLstNotif;
import es.indra.operator.server.model.ConfirmacionProcesamientoAjuste;
import es.indra.operator.server.model.ConfirmacionProcesamientoPaso;
import es.indra.operator.server.model.ListaUsuario;
import es.indra.operator.server.model.Saldo;
import es.indra.operator.server.model.Ticket;
import es.indra.operator.server.services.AdjService;
import es.indra.operator.server.services.BalanceService;
import es.indra.operator.server.services.BaseService;
import es.indra.operator.server.services.LstService;
import es.indra.operator.server.services.TrxService;
import es.indra.operator.server.utils.GenericConstant;
import es.indra.operator.server.utils.GenericUtils;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-25T10:51:09.761+02:00")

@Controller
public class V1ApiController implements V1Api {
	   	
	Logger LOGGER = LoggerFactory.getLogger(V1ApiController.class);
	
	@Autowired
	private BaseService baseService;
	@Autowired
	private LstService lstService;
	@Autowired
	private TrxService trxService;
	@Autowired
	private BalanceService balanceService;
	@Autowired
	private AdjService adjService;

	private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public V1ApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @Override
    public Optional<ObjectMapper> getObjectMapper() {
        return Optional.ofNullable(objectMapper);
    }

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.ofNullable(request);
    }
    
    @Override
	public ResponseEntity<Void> v1ConfirmacionesAjustesPut(@Valid @RequestBody List<ConfirmacionProcesamientoAjuste> body) {
    	
    	LOGGER.debug("V1ApiServiceImpl.v1ConfirmacionesAjustesPut: Begin of method... ");
		// TODO Auto-generated method stub
    	Date currentDate = new Date();
		//String auditMessage = new String();
		
		try {
			/*if (!validateToken(authorization)) {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}*/
			
			//update confirmed adjustments
			/*auditMessage =  */adjService.updateConfirmedAdj(body, currentDate, request);
			
			//Audit audit = new Audit(currentDate, "v1ConfirmacionesAjustesPut", auditMessage);
			//baseService.registerAudit(audit);
		
		} catch (Exception e) {
			LOGGER.error("V1ApiServiceImpl.v1ConfirmacionesAjustesPut: General error occurred during execution!. " + e.getMessage());
			LOGGER.error(GenericUtils.stackTraceStr(e));
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			//return Response.serverError().entity(
			//		new ApiResponseMessage(ApiResponseMessage.ERROR, "General error occurred during execution!"))
			//		.build();
		}

		return new ResponseEntity<>(HttpStatus.OK);
		//return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "Operation realized successfully!"))
		//		.build();
		
		//return V1Api.super.v1ConfirmacionesAjustesPut(body);
	}

	@Override
	public ResponseEntity<Void> v1ConfirmacionesPasosPut(@Valid @RequestBody List<ConfirmacionProcesamientoPaso> body) {
		
		LOGGER.debug("V1ApiServiceImpl.v1ConfirmacionesPasosPut: Begin of method... ");
		// TODO Auto-generated method stub
		Date currentDate = new Date();
		//String auditMessage = new String();
		
		try {
			/*if (!validateToken(authorization)) {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}*/

			//update confirmed transactions, and return string with audit 
			/*auditMessage = */trxService.updateConfirmedTrx(body,currentDate,request);
		
			//Audit audit = new Audit(currentDate, "v1ConfirmacionesPasosPut", auditMessage);
			//baseService.registerAudit(audit);
			
		} catch (Exception e) {
			LOGGER.error("V1ApiServiceImpl.v1ConfirmacionesPasosPut: General error occurred during execution!. " + e.getMessage());
			LOGGER.error(GenericUtils.stackTraceStr(e));
			/*return Response.serverError().entity(
					new ApiResponseMessage(ApiResponseMessage.ERROR, "General error occurred during execution!"))
					.build();*/
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<>(HttpStatus.OK);

		/*return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "Operation realized successfully!"))
				.build();*/

		//return V1Api.super.v1ConfirmacionesPasosPut(body);
	}

	@Override
	public ResponseEntity<Void> v1ListasUsuariosPut(@Valid @RequestBody ListaUsuario body) {
		
		LOGGER.debug("V1ApiServiceImpl.v1ListasUsuariosPut: Begin of method... ");
		// TODO Auto-generated method stub
		Date currentDate = new Date();
		//String auditMessage = new String();
		
		
		try {	
			/*if (!validateToken(authorization)) {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}*/
			
			SigtOpDscLstNotifStatus sigtOpDscLstNotifStatusBD = (SigtOpDscLstNotifStatus) baseService
					.getDescriptorsById(GenericConstant.SigtOpDscLstNotifStatus_PENDIENTE_SOLICITAR,
							SigtOpDscLstNotifStatus.class);
			SigtDscCodAgency sigtDscCodAgencyBD = (SigtDscCodAgency) baseService
					.getDescriptorsByExternalCode(body.getCodigoIntermediador().toString(), SigtDscCodAgency.class);
			SigtDscCodListType sigtDscCodListTypeBD = (SigtDscCodListType) baseService
					.getDescriptorsByExternalCode(body.getTipoLista().toString(), SigtDscCodListType.class);

	
			SigtOpLstNotif sigtOpLstNotif = new SigtOpLstNotif(
					null, 
					sigtOpDscLstNotifStatusBD, 
					sigtDscCodAgencyBD, 
					sigtDscCodListTypeBD, 
					body.getCodigoLista(), 
					new Date(body.getFechaEmision().toInstant().toEpochMilli()),
					body.getCodigoListaAnterior(), 
					body.getNumeroRegistros(), 
					null, 
					null, 
					currentDate, 
					null, 
					currentDate,
					GenericConstant.MODIFICATION_USER, 
					GenericConstant.APPLICATION_ID, 
					currentDate, 
					(byte) 0, 
					(byte) 0, 
					null, 
					null, 
					null);
			
			lstService.saveSigtOpLstNotif(sigtOpLstNotif);

			//auditMessage = body.toString();
			//Audit audit = new Audit(currentDate, "v1ListasUsuariosPut", auditMessage);
			//baseService.registerAudit(audit);
		} catch (Exception e) {
			LOGGER.error("V1ApiServiceImpl.v1ListasUsuariosPut: General error occurred during execution!. "	+ e.getMessage());
			LOGGER.error(GenericUtils.stackTraceStr(e));
			/*return Response.serverError().entity(
					new ApiResponseMessage(ApiResponseMessage.ERROR, "General error occurred during execution!"))
					.build();*/
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<>(HttpStatus.OK);

		//return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "Operation realized successfully!"))
		//		.build();
		
		//return V1Api.super.v1ListasUsuariosPut(body);
	}

	@Override
	public ResponseEntity<Ticket> v1PasosCodigoPasoSolicitarPruebasPut(String codigoPaso) {
		
		LOGGER.debug("V1ApiServiceImpl.v1PasosCodigoPasoSolicitarPruebasPut: Begin of method... ");
		// TODO Auto-generated method stub
		Date currentDate = new Date();
		//String auditMessage = codigoPaso;
		Ticket ticket = null;
		
		try {
			/*if (!validateToken(authorization)) {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}*/
			
			
			ticket = trxService.saveSigtOpTrxTestNotif(currentDate, codigoPaso, request);
								
			//Audit audit = new Audit(currentDate, "v1PasosCodigoPasoSolicitarPruebasPut", auditMessage);
			//baseService.registerAudit(audit);
		} catch (Exception e) {
			LOGGER.error("V1ApiServiceImpl.v1PasosCodigoPasoSolicitarPruebasPut: General error occurred during execution!. " + e.getMessage());
			LOGGER.error(GenericUtils.stackTraceStr(e));
			//return Response.serverError().entity(
			//		new ApiResponseMessage(ApiResponseMessage.ERROR, "General error occurred during execution!"))
			//		.build();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<>(ticket, HttpStatus.OK);
		
		
        //return Response.ok().status(Response.Status.OK).entity(ticket).build();
		//return V1Api.super.v1PasosCodigoPasoSolicitarPruebasPut(codigoPaso);
	}

	@Override
	public ResponseEntity<Void> v1SaldosPut(@Valid @RequestBody List<Saldo> body) {
		
		LOGGER.debug("V1ApiServiceImpl.v1SaldosPut: Begin of method... ");
		// TODO Auto-generated method stub
		Date currentDate = new Date();
		//String auditMessage = new String();
		
		try {
			/*if (!validateToken(authorization)) {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}*/
			
			List<SigtOpBalance> sigtOpBalances = new ArrayList<SigtOpBalance> ();
			for (Saldo saldo : body) {
				SigtDscCodAgency sigtDscCodAgencyBD =  (SigtDscCodAgency) baseService
						.getDescriptorsByExternalCode(saldo.getCodigoIntermediador().toString(), SigtDscCodAgency.class);
				
		
				SigtOpBalance sigtOpBalance = new SigtOpBalance(
						null,
						sigtDscCodAgencyBD,
						saldo.getPlaca(),
						saldo.getTid(),
						saldo.getEpc(),
						saldo.getSaldo(),
						saldo.isSaldoBajo(),
						new Date(saldo.getVersion().toInstant().toEpochMilli()),
						currentDate,
						GenericConstant.MODIFICATION_USER,
						GenericConstant.APPLICATION_ID,
						currentDate,
						(byte) 0,
						(byte) 0);
				
				sigtOpBalances.add(sigtOpBalance);
				//auditMessage = auditMessage + saldo.toString()+"\n";				
			}
			
			//Transactional operation
			balanceService.saveSigtOpBalances(sigtOpBalances);
			
			//Audit audit = new Audit(currentDate, "v1SaldosPut", auditMessage);
			//baseService.registerAudit(audit);
		} catch (Exception e) {
			LOGGER.error("V1ApiServiceImpl.v1SaldosPut: General error occurred during execution!. "	+ e.getMessage());
			LOGGER.error(GenericUtils.stackTraceStr(e));
			//return Response.serverError().entity(
			//		new ApiResponseMessage(ApiResponseMessage.ERROR, "General error occurred during execution!"))
			//		.build();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<>(HttpStatus.OK);

		//return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "Operation realized successfully!")).build();	
		//return V1Api.super.v1SaldosPut(body);
	}

}
