package es.indra.operator.server.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Representa información de un registro de datos para una lista.
 */
@ApiModel(description = "Representa información de un registro de datos para una lista.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-25T10:51:09.761+02:00")

public class Usuario   {
  @JsonProperty("codigoIntermediador")
  private Integer codigoIntermediador = null;

  @JsonProperty("placa")
  private String placa = null;

  @JsonProperty("tid")
  private String tid = null;

  @JsonProperty("epc")
  private String epc = null;

  /**
   * Código de categoría. Según tabla de categorías:  A (Automóvil, camioneta o campero), B1 (Microbús (sin doble ruedas)), B2 (Bus o buseta de 2 ejes), C1 (Camión liviano de 2 ejes), C2 (Camión pesado de 2 ejes), C3 (Camión de 3 ejes), C4 (Camión de 4 ejes), C5 (Camión de 5 ejes), C6 (Camión de 6 ejes o más).
   */
  public enum CategoriaEnum {
    A("A"),
    
    B1("B1"),
    
    B2("B2"),
    
    C1("C1"),
    
    C2("C2"),
    
    C3("C3"),
    
    C4("C4"),
    
    C5("C5"),
    
    C6("C6");

    private String value;

    CategoriaEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static CategoriaEnum fromValue(String text) {
      for (CategoriaEnum b : CategoriaEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("categoria")
  private CategoriaEnum categoria = null;

  @JsonProperty("estado")
  private Integer estado = null;

  @JsonProperty("estadoSaldo")
  private Integer estadoSaldo = null;

  @JsonProperty("saldo")
  private BigDecimal saldo = null;

  @JsonProperty("saldoBajo")
  private Boolean saldoBajo = null;

  @JsonProperty("numeroCliente")
  private String numeroCliente = null;

  /**
   * Permite definir la modalidad del usuario.
   */
  public enum ModalidadEnum {
    PREPAGOSIMPLE("PrepagoSimple"),
    
    PREPAGOCARGORECURRENTE("PrepagoCargoRecurrente"),
    
    PAGOINMEDIATO("PagoInmediato"),
    
    POSPAGO("Pospago");

    private String value;

    ModalidadEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static ModalidadEnum fromValue(String text) {
      for (ModalidadEnum b : ModalidadEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("modalidad")
  private ModalidadEnum modalidad = null;

  @JsonProperty("version")
  private OffsetDateTime version = null;

  public Usuario codigoIntermediador(Integer codigoIntermediador) {
    this.codigoIntermediador = codigoIntermediador;
    return this;
  }

  /**
   * Get codigoIntermediador
   * @return codigoIntermediador
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Integer getCodigoIntermediador() {
    return codigoIntermediador;
  }

  public void setCodigoIntermediador(Integer codigoIntermediador) {
    this.codigoIntermediador = codigoIntermediador;
  }

  public Usuario placa(String placa) {
    this.placa = placa;
    return this;
  }

  /**
   * Placa del vehículo.
   * @return placa
  **/
  @ApiModelProperty(required = true, value = "Placa del vehículo.")
  @NotNull

@Pattern(regexp="^[a-zA-Z0-9\\-]+$") @Size(min=6,max=10) 
  public String getPlaca() {
    return placa;
  }

  public void setPlaca(String placa) {
    this.placa = placa;
  }

  public Usuario tid(String tid) {
    this.tid = tid;
    return this;
  }

  /**
   * Código tid del tag. Las longitudes válidas son de 24 y 32 caracteres (96 bits), pero debe ser ampliable a futuro para la próxima generación de 128 bits. Caracteres hexadecimales Ejemplo: Un tag de 24 caracteres hexadecimales: E2003412012BC1FFEEE2EC14  Un tag que no tenga las longitudes aquí especificadas, no será válido.
   * @return tid
  **/
  @ApiModelProperty(required = true, value = "Código tid del tag. Las longitudes válidas son de 24 y 32 caracteres (96 bits), pero debe ser ampliable a futuro para la próxima generación de 128 bits. Caracteres hexadecimales Ejemplo: Un tag de 24 caracteres hexadecimales: E2003412012BC1FFEEE2EC14  Un tag que no tenga las longitudes aquí especificadas, no será válido.")
  @NotNull

@Pattern(regexp="^[\\S]+$") @Size(min=24,max=32) 
  public String getTid() {
    return tid;
  }

  public void setTid(String tid) {
    this.tid = tid;
  }

  public Usuario epc(String epc) {
    this.epc = epc;
    return this;
  }

  /**
   * Código del EPC asignado al tag según los rangos habilitados.
   * @return epc
  **/
  @ApiModelProperty(required = true, value = "Código del EPC asignado al tag según los rangos habilitados.")
  @NotNull

@Pattern(regexp="^[0-9]+$") @Size(min=24,max=24) 
  public String getEpc() {
    return epc;
  }

  public void setEpc(String epc) {
    this.epc = epc;
  }

  public Usuario categoria(CategoriaEnum categoria) {
    this.categoria = categoria;
    return this;
  }

  /**
   * Código de categoría. Según tabla de categorías:  A (Automóvil, camioneta o campero), B1 (Microbús (sin doble ruedas)), B2 (Bus o buseta de 2 ejes), C1 (Camión liviano de 2 ejes), C2 (Camión pesado de 2 ejes), C3 (Camión de 3 ejes), C4 (Camión de 4 ejes), C5 (Camión de 5 ejes), C6 (Camión de 6 ejes o más).
   * @return categoria
  **/
  @ApiModelProperty(required = true, value = "Código de categoría. Según tabla de categorías:  A (Automóvil, camioneta o campero), B1 (Microbús (sin doble ruedas)), B2 (Bus o buseta de 2 ejes), C1 (Camión liviano de 2 ejes), C2 (Camión pesado de 2 ejes), C3 (Camión de 3 ejes), C4 (Camión de 4 ejes), C5 (Camión de 5 ejes), C6 (Camión de 6 ejes o más).")
  @NotNull

@Size(min=1,max=2) 
  public CategoriaEnum getCategoria() {
    return categoria;
  }

  public void setCategoria(CategoriaEnum categoria) {
    this.categoria = categoria;
  }

  public Usuario estado(Integer estado) {
    this.estado = estado;
    return this;
  }

  /**
   * Estado del vehículo: 1 – Matriculado (ACTIVO), 2 – Suspendido (INACTIVO), 3 – Cancelado (el usuario ha sido des-matriculado del sistema).
   * @return estado
  **/
  @ApiModelProperty(required = true, value = "Estado del vehículo: 1 – Matriculado (ACTIVO), 2 – Suspendido (INACTIVO), 3 – Cancelado (el usuario ha sido des-matriculado del sistema).")
  @NotNull


  public Integer getEstado() {
    return estado;
  }

  public void setEstado(Integer estado) {
    this.estado = estado;
  }

  public Usuario estadoSaldo(Integer estadoSaldo) {
    this.estadoSaldo = estadoSaldo;
    return this;
  }

  /**
   * Este campo se utiliza para indicar el estado del saldo de un usuario.  1 – Con saldo. Siempre deja pasar al vehículo, 2 – Sin saldo. Nunca deja pasar al vehículo, 3 – Use valor del saldo. Deja pasar al vehículo según el saldo y el valor del peaje, 4 – Vehículo exento ley 787 ministerio (para uso exclusivo del ministerio de transporte).
   * @return estadoSaldo
  **/
  @ApiModelProperty(required = true, value = "Este campo se utiliza para indicar el estado del saldo de un usuario.  1 – Con saldo. Siempre deja pasar al vehículo, 2 – Sin saldo. Nunca deja pasar al vehículo, 3 – Use valor del saldo. Deja pasar al vehículo según el saldo y el valor del peaje, 4 – Vehículo exento ley 787 ministerio (para uso exclusivo del ministerio de transporte).")
  @NotNull


  public Integer getEstadoSaldo() {
    return estadoSaldo;
  }

  public void setEstadoSaldo(Integer estadoSaldo) {
    this.estadoSaldo = estadoSaldo;
  }

  public Usuario saldo(BigDecimal saldo) {
    this.saldo = saldo;
    return this;
  }

  /**
   * Saldo actual para el vehículo.
   * @return saldo
  **/
  @ApiModelProperty(required = true, value = "Saldo actual para el vehículo.")
  @NotNull

  @Valid

  public BigDecimal getSaldo() {
    return saldo;
  }

  public void setSaldo(BigDecimal saldo) {
    this.saldo = saldo;
  }

  public Usuario saldoBajo(Boolean saldoBajo) {
    this.saldoBajo = saldoBajo;
    return this;
  }

  /**
   * Determina si el usuario tiene o no saldo escaso. true = saldo escaso, false en caso contrario.  El saldo escaso es una convención entre el INT IP/REV y su usuario y equivale a un umbral de valor económico.
   * @return saldoBajo
  **/
  @ApiModelProperty(required = true, value = "Determina si el usuario tiene o no saldo escaso. true = saldo escaso, false en caso contrario.  El saldo escaso es una convención entre el INT IP/REV y su usuario y equivale a un umbral de valor económico.")
  @NotNull


  public Boolean isSaldoBajo() {
    return saldoBajo;
  }

  public void setSaldoBajo(Boolean saldoBajo) {
    this.saldoBajo = saldoBajo;
  }

  public Usuario numeroCliente(String numeroCliente) {
    this.numeroCliente = numeroCliente;
    return this;
  }

  /**
   * Número de contrato o referencia del usuario en el intermediador para consultas o disputas.
   * @return numeroCliente
  **/
  @ApiModelProperty(value = "Número de contrato o referencia del usuario en el intermediador para consultas o disputas.")

@Pattern(regexp="^[\\S]+$") @Size(max=100) 
  public String getNumeroCliente() {
    return numeroCliente;
  }

  public void setNumeroCliente(String numeroCliente) {
    this.numeroCliente = numeroCliente;
  }

  public Usuario modalidad(ModalidadEnum modalidad) {
    this.modalidad = modalidad;
    return this;
  }

  /**
   * Permite definir la modalidad del usuario.
   * @return modalidad
  **/
  @ApiModelProperty(required = true, value = "Permite definir la modalidad del usuario.")
  @NotNull


  public ModalidadEnum getModalidad() {
    return modalidad;
  }

  public void setModalidad(ModalidadEnum modalidad) {
    this.modalidad = modalidad;
  }

  public Usuario version(OffsetDateTime version) {
    this.version = version;
    return this;
  }

  /**
   * Versión del registro, siempre debe ser creciente con respecto a la versión anterior de la información del usuario.  El código de versión debe ser expresado como una fecha de actualización del registro por parte del INT IP/REV.  En cualquier caso, el INT IP/REV debe garantizar que sea único y creciente.
   * @return version
  **/
  @ApiModelProperty(required = true, value = "Versión del registro, siempre debe ser creciente con respecto a la versión anterior de la información del usuario.  El código de versión debe ser expresado como una fecha de actualización del registro por parte del INT IP/REV.  En cualquier caso, el INT IP/REV debe garantizar que sea único y creciente.")
  @NotNull

  @Valid

  public OffsetDateTime getVersion() {
    return version;
  }

  public void setVersion(OffsetDateTime version) {
    this.version = version;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Usuario usuario = (Usuario) o;
    return Objects.equals(this.codigoIntermediador, usuario.codigoIntermediador) &&
        Objects.equals(this.placa, usuario.placa) &&
        Objects.equals(this.tid, usuario.tid) &&
        Objects.equals(this.epc, usuario.epc) &&
        Objects.equals(this.categoria, usuario.categoria) &&
        Objects.equals(this.estado, usuario.estado) &&
        Objects.equals(this.estadoSaldo, usuario.estadoSaldo) &&
        Objects.equals(this.saldo, usuario.saldo) &&
        Objects.equals(this.saldoBajo, usuario.saldoBajo) &&
        Objects.equals(this.numeroCliente, usuario.numeroCliente) &&
        Objects.equals(this.modalidad, usuario.modalidad) &&
        Objects.equals(this.version, usuario.version);
  }

  @Override
  public int hashCode() {
    return Objects.hash(codigoIntermediador, placa, tid, epc, categoria, estado, estadoSaldo, saldo, saldoBajo, numeroCliente, modalidad, version);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Usuario {\n");
    
    sb.append("    codigoIntermediador: ").append(toIndentedString(codigoIntermediador)).append("\n");
    sb.append("    placa: ").append(toIndentedString(placa)).append("\n");
    sb.append("    tid: ").append(toIndentedString(tid)).append("\n");
    sb.append("    epc: ").append(toIndentedString(epc)).append("\n");
    sb.append("    categoria: ").append(toIndentedString(categoria)).append("\n");
    sb.append("    estado: ").append(toIndentedString(estado)).append("\n");
    sb.append("    estadoSaldo: ").append(toIndentedString(estadoSaldo)).append("\n");
    sb.append("    saldo: ").append(toIndentedString(saldo)).append("\n");
    sb.append("    saldoBajo: ").append(toIndentedString(saldoBajo)).append("\n");
    sb.append("    numeroCliente: ").append(toIndentedString(numeroCliente)).append("\n");
    sb.append("    modalidad: ").append(toIndentedString(modalidad)).append("\n");
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

