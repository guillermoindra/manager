package es.indra.operator.server.entity;
// Generated 23-oct-2017 16:13:45 by Hibernate Tools 3.6.0.Final

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * SigtDscCodAgency generated by hbm2java
 */
@Entity
@Table(name="SIGT_DSC_COD_AGENCY")
public class SigtDscCodAgency implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="ID")
	private Integer id;
	
	@Column(name = "DESCRIPTION", length = 100, nullable=false)
	private String description;
	
	@Column(name = "WS_URL", length = 50)
	private String wsUrl;

	@Column(name = "EXTERNAL_CODE", length = 50)
	private String externalCode;
	
	@OneToMany(mappedBy="sigtDscCodAgency", fetch=FetchType.LAZY) // default fetch type is LAZY
	@Fetch(FetchMode.SELECT)
	private Set<SigtOpLstDtlPend> sigtOpLstDtlPends = new HashSet(0);
	
	@OneToMany(mappedBy="sigtDscCodAgency", fetch=FetchType.LAZY) // default fetch type is LAZY
	@Fetch(FetchMode.SELECT)
	private Set<SigtOpLstDtlPro> sigtOpLstDtlPros = new HashSet(0);
	
	@OneToMany(mappedBy="sigtDscCodAgency", fetch=FetchType.LAZY) // default fetch type is LAZY
	@Fetch(FetchMode.SELECT)
	private Set<SigtOpLstNotif> sigtOpLstNotifs = new HashSet(0);

	public SigtDscCodAgency() {
	}

	public SigtDscCodAgency(Integer id, String description) {
		this.id = id;
		this.description = description;
	}

	public SigtDscCodAgency(Integer id, String description, String externalCode, Set sigtOpLstDtlPends,
			Set sigtOpLstDtlPros, Set sigtOpLstNotifs) {
		this.id = id;
		this.description = description;
		this.externalCode = externalCode;
		this.sigtOpLstDtlPends = sigtOpLstDtlPends;
		this.sigtOpLstDtlPros = sigtOpLstDtlPros;
		this.sigtOpLstNotifs = sigtOpLstNotifs;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getWsUrl() {
		return wsUrl;
	}

	public void setWsUrl(String wsUrl) {
		this.wsUrl = wsUrl;
	}
	
	public String getExternalCode() {
		return this.externalCode;
	}

	public void setExternalCode(String externalCode) {
		this.externalCode = externalCode;
	}

	public Set getSigtOpLstDtlPends() {
		return this.sigtOpLstDtlPends;
	}

	public void setSigtOpLstDtlPends(Set sigtOpLstDtlPends) {
		this.sigtOpLstDtlPends = sigtOpLstDtlPends;
	}

	public Set getSigtOpLstDtlPros() {
		return this.sigtOpLstDtlPros;
	}

	public void setSigtOpLstDtlPros(Set sigtOpLstDtlPros) {
		this.sigtOpLstDtlPros = sigtOpLstDtlPros;
	}

	public Set getSigtOpLstNotifs() {
		return this.sigtOpLstNotifs;
	}

	public void setSigtOpLstNotifs(Set sigtOpLstNotifs) {
		this.sigtOpLstNotifs = sigtOpLstNotifs;
	}

}
