package es.indra.operator.server.services;

import java.util.List;

import es.indra.operator.server.entity.SigtOpBalance;


/**
 * Interface for Balance Service.
 * 
 * @author acoboj
 */
public interface BalanceService {
	
	/**
	 * Save the sigtOpBalances list in database.
	 * 
	 * @param sigtOpBalances - the object dto list arg
	 * @throws Exception - the exception
	 */
	void saveSigtOpBalances(List<SigtOpBalance> sigtOpBalances) throws Exception;
}
