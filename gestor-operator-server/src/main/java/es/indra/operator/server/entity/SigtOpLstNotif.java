package es.indra.operator.server.entity;
// Generated 23-oct-2017 16:13:45 by Hibernate Tools 3.6.0.Final

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * SigtOpLstNotif generated by hbm2java
 */
@Entity
@Table(name="SIGT_OP_LST_NOTIF")
public class SigtOpLstNotif implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="IDENTIFIER")
	private Long identifier;
	
	@ManyToOne(fetch = FetchType.LAZY) // default is EAGER
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "STATUS", nullable = false)
	private SigtOpDscLstNotifStatus sigtOpDscLstNotifStatus;
	
	@ManyToOne(fetch = FetchType.LAZY) // default is EAGER
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "COD_AGENCY", nullable = false)
	private SigtDscCodAgency sigtDscCodAgency;
	
	@ManyToOne(fetch = FetchType.LAZY) // default is EAGER
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "COD_LIST_TYPE", nullable = false)
	private SigtDscCodListType sigtDscCodListType;
	
	@Column(name = "ID_LIST", length = 100, nullable = false)
	private String idList;
	
	@Column(name = "PUBLISH_DATE", length = 23, nullable = false)
	private Date publishDate;
	
	@Column(name = "ID_LIST_PREV", length = 100, nullable = false)
	private String idListPrev;
	
	@Column(name = "NUM_RECORD", nullable = false)
	private Integer numRecord;
	
	@Column(name = "NUM_ACCEPTED")
	private Integer numAccepted;
	
	@Column(name = "NUM_REJECTED")
	private Integer numRejected;
	
	@Column(name = "PROCESSED_DATE", length = 23)
	private Date processedDate;
	
	@Column(name = "UUID", length = 100)
	private String uuid;
	
	@Column(name = "CREATION_DATE", length = 23, nullable = false)
	private Date creationDate;
	
	@Column(name = "MODIFICATION_USER", length = 50)
	private String modificationUser;
	
	@Column(name = "APPLICATION", nullable = false)
	private Integer application;
	
	@Column(name = "MODIFICATION_DATE", length = 23, nullable = false)
	private Date modificationDate;
	
	@Column(name = "MODIFICATION", nullable = false)
	private Byte modification;
	
	@Column(name = "OPTIMIST_LOCK", nullable = false)
	private Byte optimistLock;
	
	@OneToMany(mappedBy="sigtOpLstNotif", fetch=FetchType.LAZY) // default fetch type is LAZY
	@Fetch(FetchMode.SELECT)
	private Set<SigtOpLstDtlPro> sigtOpLstDtlPros = new HashSet(0);
	
	@OneToMany(mappedBy="sigtOpLstNotif", fetch=FetchType.LAZY) // default fetch type is LAZY
	@Fetch(FetchMode.SELECT)
	private Set<SigtOpLstDtlPend> sigtOpLstDtlPends = new HashSet(0);
	
	@OneToMany(mappedBy="sigtOpLstNotif", fetch=FetchType.LAZY) // default fetch type is LAZY
	@Fetch(FetchMode.SELECT)
	private Set<SigtOpLstDtlErr> sigtOpLstDtlErrs = new HashSet(0);

	public SigtOpLstNotif() {
	}

	public SigtOpLstNotif(Long identifier, SigtOpDscLstNotifStatus sigtOpDscLstNotifStatus,
			SigtDscCodAgency sigtDscCodAgency, SigtDscCodListType sigtDscCodListType, String idList,
			Date publishDate, String idListPrev, Integer numRecord, Date creationDate, Integer application,
			Date modificationDate, Byte modification, Byte optimistLock) {
		this.identifier = identifier;
		this.sigtOpDscLstNotifStatus = sigtOpDscLstNotifStatus;
		this.sigtDscCodAgency = sigtDscCodAgency;
		this.sigtDscCodListType = sigtDscCodListType;
		this.idList = idList;
		this.publishDate = publishDate;
		this.idListPrev = idListPrev;
		this.numRecord = numRecord;
		this.creationDate = creationDate;
		this.application = application;
		this.modificationDate = modificationDate;
		this.modification = modification;
		this.optimistLock = optimistLock;
	}

	public SigtOpLstNotif(Long identifier, SigtOpDscLstNotifStatus sigtOpDscLstNotifStatus,
			SigtDscCodAgency sigtDscCodAgency, SigtDscCodListType sigtDscCodListType, String idList,
			Date publishDate, String idListPrev, Integer numRecord, Integer numAccepted, Integer numRejected,
			Date processedDate, String uuid, Date creationDate, String modificationUser, Integer application,
			Date modificationDate, Byte modification, Byte optimistLock, Set sigtOpLstDtlPros, Set sigtOpLstDtlPends,
			Set sigtOpLstDtlErrs) {
		this.identifier = identifier;
		this.sigtOpDscLstNotifStatus = sigtOpDscLstNotifStatus;
		this.sigtDscCodAgency = sigtDscCodAgency;
		this.sigtDscCodListType = sigtDscCodListType;
		this.idList = idList;
		this.publishDate = publishDate;
		this.idListPrev = idListPrev;
		this.numRecord = numRecord;
		this.numAccepted = numAccepted;
		this.numRejected = numRejected;
		this.processedDate = processedDate;
		this.uuid = uuid;
		this.creationDate = creationDate;
		this.modificationUser = modificationUser;
		this.application = application;
		this.modificationDate = modificationDate;
		this.modification = modification;
		this.optimistLock = optimistLock;
		this.sigtOpLstDtlPros = sigtOpLstDtlPros;
		this.sigtOpLstDtlPends = sigtOpLstDtlPends;
		this.sigtOpLstDtlErrs = sigtOpLstDtlErrs;
	}

	public Long getIdentifier() {
		return this.identifier;
	}

	public void setIdentifier(Long identifier) {
		this.identifier = identifier;
	}

	public SigtOpDscLstNotifStatus getSigtOpDscLstNotifStatus() {
		return this.sigtOpDscLstNotifStatus;
	}

	public void setSigtOpDscLstNotifStatus(SigtOpDscLstNotifStatus sigtOpDscLstNotifStatus) {
		this.sigtOpDscLstNotifStatus = sigtOpDscLstNotifStatus;
	}

	public SigtDscCodAgency getSigtDscCodAgency() {
		return this.sigtDscCodAgency;
	}

	public void setSigtDscCodAgency(SigtDscCodAgency sigtDscCodAgency) {
		this.sigtDscCodAgency = sigtDscCodAgency;
	}

	public SigtDscCodListType getSigtDscCodListType() {
		return this.sigtDscCodListType;
	}

	public void setSigtDscCodListType(SigtDscCodListType sigtDscCodListType) {
		this.sigtDscCodListType = sigtDscCodListType;
	}

	public String getIdList() {
		return this.idList;
	}

	public void setIdList(String idList) {
		this.idList = idList;
	}

	public Date getPublishDate() {
		return this.publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public String getIdListPrev() {
		return this.idListPrev;
	}

	public void setIdListPrev(String idListPrev) {
		this.idListPrev = idListPrev;
	}

	public Integer getNumRecord() {
		return this.numRecord;
	}

	public void setNumRecord(Integer numRecord) {
		this.numRecord = numRecord;
	}

	public Integer getNumAccepted() {
		return this.numAccepted;
	}

	public void setNumAccepted(Integer numAccepted) {
		this.numAccepted = numAccepted;
	}

	public Integer getNumRejected() {
		return this.numRejected;
	}

	public void setNumRejected(Integer numRejected) {
		this.numRejected = numRejected;
	}

	public Date getProcessedDate() {
		return this.processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

	public String getUuid() {
		return this.uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getModificationUser() {
		return this.modificationUser;
	}

	public void setModificationUser(String modificationUser) {
		this.modificationUser = modificationUser;
	}

	public Integer getApplication() {
		return this.application;
	}

	public void setApplication(Integer application) {
		this.application = application;
	}

	public Date getModificationDate() {
		return this.modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Byte getModification() {
		return this.modification;
	}

	public void setModification(Byte modification) {
		this.modification = modification;
	}

	public Byte getOptimistLock() {
		return this.optimistLock;
	}

	public void setOptimistLock(Byte optimistLock) {
		this.optimistLock = optimistLock;
	}

	public Set getSigtOpLstDtlPros() {
		return this.sigtOpLstDtlPros;
	}

	public void setSigtOpLstDtlPros(Set sigtOpLstDtlPros) {
		this.sigtOpLstDtlPros = sigtOpLstDtlPros;
	}

	public Set getSigtOpLstDtlPends() {
		return this.sigtOpLstDtlPends;
	}

	public void setSigtOpLstDtlPends(Set sigtOpLstDtlPends) {
		this.sigtOpLstDtlPends = sigtOpLstDtlPends;
	}

	public Set getSigtOpLstDtlErrs() {
		return this.sigtOpLstDtlErrs;
	}

	public void setSigtOpLstDtlErrs(Set sigtOpLstDtlErrs) {
		this.sigtOpLstDtlErrs = sigtOpLstDtlErrs;
	}

}
