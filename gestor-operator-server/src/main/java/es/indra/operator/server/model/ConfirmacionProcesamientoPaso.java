package es.indra.operator.server.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Representa la información de una confirmación por parte del Intermediador al Operador del procesamiento de un paso de vehículo. Esta confirmación de procesamiento de un paso puede reportar el resultado como exitoso o fallido.
 */
@ApiModel(description = "Representa la información de una confirmación por parte del Intermediador al Operador del procesamiento de un paso de vehículo. Esta confirmación de procesamiento de un paso puede reportar el resultado como exitoso o fallido.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-25T10:51:09.761+02:00")

public class ConfirmacionProcesamientoPaso   {
  @JsonProperty("codigoPaso")
  private String codigoPaso = null;

  @JsonProperty("codigoIntermediador")
  private Integer codigoIntermediador = null;

  @JsonProperty("tokenConfirmacion")
  private String tokenConfirmacion = null;

  @JsonProperty("codigoRespuesta")
  private Integer codigoRespuesta = null;

  @JsonProperty("descripcion")
  private String descripcion = null;

  public ConfirmacionProcesamientoPaso codigoPaso(String codigoPaso) {
    this.codigoPaso = codigoPaso;
    return this;
  }

  /**
   * Get codigoPaso
   * @return codigoPaso
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getCodigoPaso() {
    return codigoPaso;
  }

  public void setCodigoPaso(String codigoPaso) {
    this.codigoPaso = codigoPaso;
  }

  public ConfirmacionProcesamientoPaso codigoIntermediador(Integer codigoIntermediador) {
    this.codigoIntermediador = codigoIntermediador;
    return this;
  }

  /**
   * Get codigoIntermediador
   * @return codigoIntermediador
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Integer getCodigoIntermediador() {
    return codigoIntermediador;
  }

  public void setCodigoIntermediador(Integer codigoIntermediador) {
    this.codigoIntermediador = codigoIntermediador;
  }

  public ConfirmacionProcesamientoPaso tokenConfirmacion(String tokenConfirmacion) {
    this.tokenConfirmacion = tokenConfirmacion;
    return this;
  }

  /**
   * Get tokenConfirmacion
   * @return tokenConfirmacion
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getTokenConfirmacion() {
    return tokenConfirmacion;
  }

  public void setTokenConfirmacion(String tokenConfirmacion) {
    this.tokenConfirmacion = tokenConfirmacion;
  }

  public ConfirmacionProcesamientoPaso codigoRespuesta(Integer codigoRespuesta) {
    this.codigoRespuesta = codigoRespuesta;
    return this;
  }

  /**
   * 1 – Aceptada, 1001 – Valor inválido, en atributos, 1002 – Atributo faltante, 2001 – Vehículo no encontrado.
   * @return codigoRespuesta
  **/
  @ApiModelProperty(required = true, value = "1 – Aceptada, 1001 – Valor inválido, en atributos, 1002 – Atributo faltante, 2001 – Vehículo no encontrado.")
  @NotNull


  public Integer getCodigoRespuesta() {
    return codigoRespuesta;
  }

  public void setCodigoRespuesta(Integer codigoRespuesta) {
    this.codigoRespuesta = codigoRespuesta;
  }

  public ConfirmacionProcesamientoPaso descripcion(String descripcion) {
    this.descripcion = descripcion;
    return this;
  }

  /**
   * Descripción de fallos.
   * @return descripcion
  **/
  @ApiModelProperty(value = "Descripción de fallos.")

@Size(max=200) 
  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConfirmacionProcesamientoPaso confirmacionProcesamientoPaso = (ConfirmacionProcesamientoPaso) o;
    return Objects.equals(this.codigoPaso, confirmacionProcesamientoPaso.codigoPaso) &&
        Objects.equals(this.codigoIntermediador, confirmacionProcesamientoPaso.codigoIntermediador) &&
        Objects.equals(this.tokenConfirmacion, confirmacionProcesamientoPaso.tokenConfirmacion) &&
        Objects.equals(this.codigoRespuesta, confirmacionProcesamientoPaso.codigoRespuesta) &&
        Objects.equals(this.descripcion, confirmacionProcesamientoPaso.descripcion);
  }

  @Override
  public int hashCode() {
    return Objects.hash(codigoPaso, codigoIntermediador, tokenConfirmacion, codigoRespuesta, descripcion);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ConfirmacionProcesamientoPaso {\n");
    
    sb.append("    codigoPaso: ").append(toIndentedString(codigoPaso)).append("\n");
    sb.append("    codigoIntermediador: ").append(toIndentedString(codigoIntermediador)).append("\n");
    sb.append("    tokenConfirmacion: ").append(toIndentedString(tokenConfirmacion)).append("\n");
    sb.append("    codigoRespuesta: ").append(toIndentedString(codigoRespuesta)).append("\n");
    sb.append("    descripcion: ").append(toIndentedString(descripcion)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

