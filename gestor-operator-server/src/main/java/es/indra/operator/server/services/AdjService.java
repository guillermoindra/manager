package es.indra.operator.server.services;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import es.indra.operator.server.entity.SigtOpTrxAdjPend;
import es.indra.operator.server.model.ConfirmacionProcesamientoAjuste;
import es.indra.operator.server.utils.GenericFilterDTO;

/**
 * Interface for Adjustment Service.
 * 
 * @author rharo
 */
public interface AdjService {
	
	/**
	 * Update the confirmed adjustments, that have been send .
	 * 
	 * @param listStatusAdjArg - list of adjustments to process
	 * @param currentDateArg - current date
	 * @param request 
	 * @return String with audit
	 * @throws Exception - the exception
	 */
	String updateConfirmedAdj(List<ConfirmacionProcesamientoAjuste> listStatusAdjArg, Date currentDateArg, HttpServletRequest request) throws Exception;
	
	/**
	 * Find the SigtOpTrxAdjPend object in database by filter.
	 * 
	 * @param genericFilterDTOArg - filter DTO
	 * return SigtOpTrxAdjPend object
	 * @throws Exception - the exception
	 */
	SigtOpTrxAdjPend findSigtOpTrxAdjPend(GenericFilterDTO genericFilterDTOArg) throws Exception;

}