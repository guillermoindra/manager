package es.indra.operator.server.entity;
// Generated 25-oct-2017 11:58:10 by Hibernate Tools 3.6.0.Final

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * SigtOpTrxAdjPend generated by hbm2java
 */
@Entity
@Table(name="SIGT_OP_TRX_ADJ_PEND")
public class SigtOpTrxAdjPend implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="IDENTIFIER")
	private Long identifier;
	
	@Column(name = "UUID", nullable = false, length = 100)
	private String uuid = null;
	
	@ManyToOne(fetch = FetchType.LAZY) // default is EAGER
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "ADJUSTMENT_TYPE", nullable = false)
	private SigtTrxAdjDscOptType sigtTrxAdjDscOptType;
	
	@ManyToOne(fetch = FetchType.LAZY) // default is EAGER
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "CHARGED_CLASS")
	private SigtDscVehicleClass sigtDscVehicleClass;
	
	@ManyToOne(fetch = FetchType.LAZY) // default is EAGER
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "ID_NOTIF")
	private SigtOpToIntNotif sigtOpToIntNotif;
	
	@ManyToOne(fetch = FetchType.LAZY) // default is EAGER
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "ID_NOTIF_SIGT")
	private SigtOpToIntNotif sigtOpToIntNotifSigt;
	
	@ManyToOne(fetch = FetchType.LAZY) // default is EAGER
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "ADJUSTMENT_REASON", nullable = false)
	private SigtTrxAdjDscReason sigtTrxAdjDscReason;
	
	@ManyToOne(fetch = FetchType.LAZY) // default is EAGER
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "STATUS", nullable = false)
	private SigtOpTrxAdjDscStatus sigtOpTrxAdjDscStatus;
	
	@ManyToOne(fetch = FetchType.LAZY) // default is EAGER
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "COD_AGENCY", nullable = false)
	private SigtDscCodAgency sigtDscCodAgency;
	
	@Column(name = "TRX_ID", nullable = false)
	private Long trxId;
	
	@Column(name = "TRX_DATE", length = 23, nullable = false)
	private Date trxDate;
	
	@Column(name = "TRX_COLLECTION_DATE", length = 23, nullable = false)
	private Date trxCollectionDate;
	
	@Column(name = "TRX_ADJUSTED_ID", nullable = false)
	private Long trxAdjustedId;
	
	@Column(name = "ADJUSTMENT_AMOUNT", precision = 18, nullable = false)
	private BigDecimal adjustmentAmount;
	
	@Column(name = "AMOUNT", precision = 18, nullable = false)
	private BigDecimal amount;
	
	@Column(name = "CREATION_DATE", length = 23, nullable = false)
	private Date creationDate;
	
	@Column(name = "MODIFICATION_USER", length = 50)
	private String modificationUser;
	
	@Column(name = "APPLICATION", nullable = false)
	private Integer application;
	
	@Column(name = "MODIFICATION_DATE", length = 23, nullable = false)
	private Date modificationDate;
	
	@Column(name = "MODIFICATION", nullable = false)
	private Byte modification;
	
	@Column(name = "OPTIMIST_LOCK", nullable = false)
	private Byte optimistLock;

	public SigtOpTrxAdjPend() {
	}

	

	public SigtOpTrxAdjPend(Long identifier, SigtTrxAdjDscOptType sigtTrxAdjDscOptType,
			SigtDscVehicleClass sigtDscVehicleClass, SigtOpToIntNotif sigtOpToIntNotif,
			SigtOpToIntNotif sigtOpToIntNotifSigt, SigtTrxAdjDscReason sigtTrxAdjDscReason,
			SigtOpTrxAdjDscStatus sigtOpTrxAdjDscStatus, SigtDscCodAgency sigtDscCodAgency, Long trxId, Date trxDate,
			Date trxCollectionDate, Long trxAdjustedId, BigDecimal adjustmentAmount, BigDecimal amount,
			Date creationDate, String modificationUser, Integer application, Date modificationDate, Byte modification,
			Byte optimistLock) {
		super();
		this.identifier = identifier;
		this.sigtTrxAdjDscOptType = sigtTrxAdjDscOptType;
		this.sigtDscVehicleClass = sigtDscVehicleClass;
		this.sigtOpToIntNotif = sigtOpToIntNotif;
		this.sigtOpToIntNotifSigt = sigtOpToIntNotifSigt;
		this.sigtTrxAdjDscReason = sigtTrxAdjDscReason;
		this.sigtOpTrxAdjDscStatus = sigtOpTrxAdjDscStatus;
		this.sigtDscCodAgency = sigtDscCodAgency;
		this.trxId = trxId;
		this.trxDate = trxDate;
		this.trxCollectionDate = trxCollectionDate;
		this.trxAdjustedId = trxAdjustedId;
		this.adjustmentAmount = adjustmentAmount;
		this.amount = amount;
		this.creationDate = creationDate;
		this.modificationUser = modificationUser;
		this.application = application;
		this.modificationDate = modificationDate;
		this.modification = modification;
		this.optimistLock = optimistLock;
	}



	public Long getIdentifier() {
		return this.identifier;
	}

	public void setIdentifier(Long identifier) {
		this.identifier = identifier;
	}

	public SigtTrxAdjDscOptType getSigtTrxAdjDscOptType() {
		return this.sigtTrxAdjDscOptType;
	}

	public void setSigtTrxAdjDscOptType(SigtTrxAdjDscOptType sigtTrxAdjDscOptType) {
		this.sigtTrxAdjDscOptType = sigtTrxAdjDscOptType;
	}

	public SigtDscVehicleClass getSigtDscVehicleClass() {
		return this.sigtDscVehicleClass;
	}

	public void setSigtDscVehicleClass(SigtDscVehicleClass sigtDscVehicleClass) {
		this.sigtDscVehicleClass = sigtDscVehicleClass;
	}

	public SigtOpToIntNotif getSigtOpToIntNotif() {
		return this.sigtOpToIntNotif;
	}

	public void setSigtOpToIntNotif(SigtOpToIntNotif sigtOpToIntNotif) {
		this.sigtOpToIntNotif = sigtOpToIntNotif;
	}

	public SigtTrxAdjDscReason getSigtTrxAdjDscReason() {
		return this.sigtTrxAdjDscReason;
	}

	public void setSigtTrxAdjDscReason(SigtTrxAdjDscReason sigtTrxAdjDscReason) {
		this.sigtTrxAdjDscReason = sigtTrxAdjDscReason;
	}

	public SigtOpTrxAdjDscStatus getSigtOpTrxAdjDscStatus() {
		return this.sigtOpTrxAdjDscStatus;
	}

	public void setSigtOpTrxAdjDscStatus(SigtOpTrxAdjDscStatus sigtOpTrxAdjDscStatus) {
		this.sigtOpTrxAdjDscStatus = sigtOpTrxAdjDscStatus;
	}

	public SigtDscCodAgency getSigtDscCodAgency() {
		return this.sigtDscCodAgency;
	}

	public void setSigtDscCodAgency(SigtDscCodAgency sigtDscCodAgency) {
		this.sigtDscCodAgency = sigtDscCodAgency;
	}

	public Long getTrxId() {
		return this.trxId;
	}

	public void setTrxId(Long trxId) {
		this.trxId = trxId;
	}

	public Date getTrxDate() {
		return this.trxDate;
	}

	public void setTrxDate(Date trxDate) {
		this.trxDate = trxDate;
	}

	public Date getTrxCollectionDate() {
		return this.trxCollectionDate;
	}

	public void setTrxCollectionDate(Date trxCollectionDate) {
		this.trxCollectionDate = trxCollectionDate;
	}

	public Long getTrxAdjustedId() {
		return this.trxAdjustedId;
	}

	public void setTrxAdjustedId(Long trxAdjustedId) {
		this.trxAdjustedId = trxAdjustedId;
	}

	public BigDecimal getAdjustmentAmount() {
		return this.adjustmentAmount;
	}

	public void setAdjustmentAmount(BigDecimal adjustmentAmount) {
		this.adjustmentAmount = adjustmentAmount;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getModificationUser() {
		return this.modificationUser;
	}

	public void setModificationUser(String modificationUser) {
		this.modificationUser = modificationUser;
	}

	public Integer getApplication() {
		return this.application;
	}

	public void setApplication(Integer application) {
		this.application = application;
	}

	public Date getModificationDate() {
		return this.modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Byte getModification() {
		return this.modification;
	}

	public void setModification(Byte modification) {
		this.modification = modification;
	}

	public Byte getOptimistLock() {
		return this.optimistLock;
	}

	public void setOptimistLock(Byte optimistLock) {
		this.optimistLock = optimistLock;
	}

	public SigtOpToIntNotif getSigtOpToIntNotifSigt() {
		return sigtOpToIntNotifSigt;
	}

	public void setSigtOpToIntNotifSigt(SigtOpToIntNotif sigtOpToIntNotifSigt) {
		this.sigtOpToIntNotifSigt = sigtOpToIntNotifSigt;
	}



	public String getUuid() {
		return uuid;
	}



	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
