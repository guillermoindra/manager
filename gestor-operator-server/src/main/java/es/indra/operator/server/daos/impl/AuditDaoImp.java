package es.indra.operator.server.daos.impl;

import org.springframework.transaction.annotation.Transactional;

import es.indra.operator.server.daos.AuditDao;
import es.indra.operator.server.utils.Audit;


/**
 * This class is the implementation of the AuditDao interface.
 * 
 * @author acoboj
 */
@Transactional
public class AuditDaoImp implements AuditDao{

	@Override
	public void registerAudit(Audit audit) {
		/*LogAudit.getLog().info("****************************************");
		LogAudit.getLog().info("Audit id: "+audit.getIdentifier());
		LogAudit.getLog().info("Audit Date: "+audit.getCreationDate());
		LogAudit.getLog().info("Audit message:\n"+audit.getMessage());
		LogAudit.getLog().info("****************************************");*/
	}

}
