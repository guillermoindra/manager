package es.indra.operator.server.daos;

import es.indra.operator.server.entity.SigtOpTrxAdjPend;
import es.indra.operator.server.utils.GenericFilterDTO;

/**
 * Interface for Adjustment DAO.
 * 
 * @author rharo
 */
public interface AdjDao extends BaseDao<Object>{
	
	/**
	 * Find the SigtOpTrxAdjPend object in database by filter.
	 * 
	 * @param genericFilterDTOArg - filter DTO
	 * return SigtOpTrxAdjPend object
	 * @throws Exception - the exception
	 */
	SigtOpTrxAdjPend findSigtOpTrxAdjPend(GenericFilterDTO genericFilterDTOArg);
}