package es.indra.operator.server.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;

import es.indra.operator.server.model.ErroresProcesamientoLista;

import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Representa la información de errores en el procesamiento de una lista de usuario. Esta notificación es realizada por el operador al intermediador.
 */
@ApiModel(description = "Representa la información de errores en el procesamiento de una lista de usuario. Esta notificación es realizada por el operador al intermediador.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-25T10:51:09.761+02:00")

public class NotificacionErrorProcesamientoListas   {
  @JsonProperty("codigoLista")
  private String codigoLista = null;

  @JsonProperty("codigoOperador")
  private Integer codigoOperador = null;

  @JsonProperty("errores")
  private ErroresProcesamientoLista errores = null;

  public NotificacionErrorProcesamientoListas codigoLista(String codigoLista) {
    this.codigoLista = codigoLista;
    return this;
  }

  /**
   * Get codigoLista
   * @return codigoLista
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getCodigoLista() {
    return codigoLista;
  }

  public void setCodigoLista(String codigoLista) {
    this.codigoLista = codigoLista;
  }

  public NotificacionErrorProcesamientoListas codigoOperador(Integer codigoOperador) {
    this.codigoOperador = codigoOperador;
    return this;
  }

  /**
   * Get codigoOperador
   * @return codigoOperador
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Integer getCodigoOperador() {
    return codigoOperador;
  }

  public void setCodigoOperador(Integer codigoOperador) {
    this.codigoOperador = codigoOperador;
  }

  public NotificacionErrorProcesamientoListas errores(ErroresProcesamientoLista errores) {
    this.errores = errores;
    return this;
  }

  /**
   * Get errores
   * @return errores
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public ErroresProcesamientoLista getErrores() {
    return errores;
  }

  public void setErrores(ErroresProcesamientoLista errores) {
    this.errores = errores;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NotificacionErrorProcesamientoListas notificacionErrorProcesamientoListas = (NotificacionErrorProcesamientoListas) o;
    return Objects.equals(this.codigoLista, notificacionErrorProcesamientoListas.codigoLista) &&
        Objects.equals(this.codigoOperador, notificacionErrorProcesamientoListas.codigoOperador) &&
        Objects.equals(this.errores, notificacionErrorProcesamientoListas.errores);
  }

  @Override
  public int hashCode() {
    return Objects.hash(codigoLista, codigoOperador, errores);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NotificacionErrorProcesamientoListas {\n");
    
    sb.append("    codigoLista: ").append(toIndentedString(codigoLista)).append("\n");
    sb.append("    codigoOperador: ").append(toIndentedString(codigoOperador)).append("\n");
    sb.append("    errores: ").append(toIndentedString(errores)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

