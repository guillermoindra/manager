package es.indra.operator.server.daos.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import es.indra.operator.server.daos.AdjDao;
import es.indra.operator.server.entity.SigtOpTrxAdjPend;
import es.indra.operator.server.utils.GenericFilterDTO;
import es.indra.operator.server.utils.GenericUtils;

/**
 * This class is the implementation of the AdjDAO interface.
 * 
 * @author rharo
 */
@Transactional
public class AdjDaoImp extends BaseDaoImp<Object> implements AdjDao {

	@Override
	public SigtOpTrxAdjPend findSigtOpTrxAdjPend(GenericFilterDTO genericFilterDTOArg){
		
		ArrayList<Object> params = new ArrayList<Object>();
		String hql = " SELECT s FROM SigtOpTrxAdjPend s INNER JOIN FETCH s.sigtTrxAdjDscOptType LEFT JOIN FETCH s.sigtDscVehicleClass "
				+ " LEFT JOIN FETCH s.sigtOpToIntNotif  INNER JOIN FETCH s.sigtTrxAdjDscReason INNER JOIN FETCH s.sigtOpTrxAdjDscStatus "
				+ "INNER JOIN FETCH s.sigtDscCodAgency WHERE s.identifier = ? AND s.sigtOpTrxAdjDscStatus.id = ?";
		params.add(genericFilterDTOArg.getIdentifier());
		params.add(genericFilterDTOArg.getStatus());
		
		List<SigtOpTrxAdjPend> finded = GenericUtils.castList(SigtOpTrxAdjPend.class, super.find(hql, params));
		if(finded != null && !finded.isEmpty()){
			return finded.get(0);
		}
		return null;	
	}
}
