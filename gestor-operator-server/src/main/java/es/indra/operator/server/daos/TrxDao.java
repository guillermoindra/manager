package es.indra.operator.server.daos;

import es.indra.operator.server.entity.SigtOpTrxPend;
import es.indra.operator.server.utils.GenericFilterDTO;

/**
 * Interface for Transactions DAO.
 * 
 * @author rharo
 */
public interface TrxDao extends BaseDao<Object>{
	
	/**
	 * Find the SigtOpTrxPend object in database by filter.
	 * 
	 * @param genericFilterDTOArg - filter DTO
	 * return SigtOpTrxPend object
	 * @throws Exception - the exception
	 */
	SigtOpTrxPend findSigtOpTrxPend(GenericFilterDTO genericFilterDTOArg);
}