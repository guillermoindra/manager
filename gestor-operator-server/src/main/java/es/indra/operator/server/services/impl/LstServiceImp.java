package es.indra.operator.server.services.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.indra.operator.server.daos.LstDao;
import es.indra.operator.server.entity.SigtOpLstNotif;
import es.indra.operator.server.services.LstService;

/**
 * This class is the implementation of the LstService interface.
 * 
 * @author acoboj
 */
@Service
@Transactional
public class LstServiceImp extends BaseServiceImp implements LstService {

	/** Lst DAO */
	private LstDao lstDao;

	@Override
	public void saveSigtOpLstNotif(SigtOpLstNotif sigtOpLstNotif) throws Exception {
		lstDao.save(sigtOpLstNotif);
	}

	/**
	 * Return lstDao attribute.
	 *
	 * @return lstDao - Attribute returned
	*/
	
	public LstDao getLstDao() {
		return lstDao;
	}

	/**
	 * Set attribute lstDao.
	 *
	 * @param  lstDao - Set value
	 */
	
	public void setLstDao(LstDao lstDao) {
		this.lstDao = lstDao;
	}

	
	
}
