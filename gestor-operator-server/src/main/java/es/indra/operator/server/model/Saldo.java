package es.indra.operator.server.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Representa la información mínima de un saldo de un usuario. 
 */
@ApiModel(description = "Representa la información mínima de un saldo de un usuario. ")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-25T10:51:09.761+02:00")

public class Saldo   {
  @JsonProperty("codigoIntermediador")
  private Integer codigoIntermediador = null;

  @JsonProperty("placa")
  private String placa = null;

  @JsonProperty("tid")
  private String tid = null;

  @JsonProperty("epc")
  private String epc = null;

  @JsonProperty("saldo")
  private BigDecimal saldo = null;

  @JsonProperty("saldoBajo")
  private Boolean saldoBajo = null;

  @JsonProperty("version")
  private OffsetDateTime version = null;

  public Saldo codigoIntermediador(Integer codigoIntermediador) {
    this.codigoIntermediador = codigoIntermediador;
    return this;
  }

  /**
   * Get codigoIntermediador
   * @return codigoIntermediador
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Integer getCodigoIntermediador() {
    return codigoIntermediador;
  }

  public void setCodigoIntermediador(Integer codigoIntermediador) {
    this.codigoIntermediador = codigoIntermediador;
  }

  public Saldo placa(String placa) {
    this.placa = placa;
    return this;
  }

  /**
   * Placa del vehículo.
   * @return placa
  **/
  @ApiModelProperty(required = true, value = "Placa del vehículo.")
  @NotNull

@Pattern(regexp="^[a-zA-Z0-9\\-]+$") @Size(min=6,max=10) 
  public String getPlaca() {
    return placa;
  }

  public void setPlaca(String placa) {
    this.placa = placa;
  }

  public Saldo tid(String tid) {
    this.tid = tid;
    return this;
  }

  /**
   * Código tid del tag. Las longitudes válidas son de 24 y 32 caracteres (96 bits), pero debe ser ampliable a futuro para la próxima generación de 128 bits. Caracteres hexadecimales Ejemplo: Un tag de 24 caracteres hexadecimales: E2003412012BC1FFEEE2EC14  Un tag que no tenga las longitudes aquí especificadas, no será válido.
   * @return tid
  **/
  @ApiModelProperty(required = true, value = "Código tid del tag. Las longitudes válidas son de 24 y 32 caracteres (96 bits), pero debe ser ampliable a futuro para la próxima generación de 128 bits. Caracteres hexadecimales Ejemplo: Un tag de 24 caracteres hexadecimales: E2003412012BC1FFEEE2EC14  Un tag que no tenga las longitudes aquí especificadas, no será válido.")
  @NotNull

@Pattern(regexp="^[\\S]+$") @Size(min=24,max=32) 
  public String getTid() {
    return tid;
  }

  public void setTid(String tid) {
    this.tid = tid;
  }

  public Saldo epc(String epc) {
    this.epc = epc;
    return this;
  }

  /**
   * Código del EPC asignado al tag según los rangos habilitados
   * @return epc
  **/
  @ApiModelProperty(required = true, value = "Código del EPC asignado al tag según los rangos habilitados")
  @NotNull

@Pattern(regexp="^[0-9]+$") @Size(min=24,max=24) 
  public String getEpc() {
    return epc;
  }

  public void setEpc(String epc) {
    this.epc = epc;
  }

  public Saldo saldo(BigDecimal saldo) {
    this.saldo = saldo;
    return this;
  }

  /**
   * Saldo asociado a la placa.
   * @return saldo
  **/
  @ApiModelProperty(required = true, value = "Saldo asociado a la placa.")
  @NotNull

  @Valid

  public BigDecimal getSaldo() {
    return saldo;
  }

  public void setSaldo(BigDecimal saldo) {
    this.saldo = saldo;
  }

  public Saldo saldoBajo(Boolean saldoBajo) {
    this.saldoBajo = saldoBajo;
    return this;
  }

  /**
   * Determina si el usuario tiene o no saldo escaso. true = saldo escaso, false en caso contrario.  El saldo escaso es una convención entre el INT IP/REV y su usuario y equivale a un umbral de valor económico.
   * @return saldoBajo
  **/
  @ApiModelProperty(required = true, value = "Determina si el usuario tiene o no saldo escaso. true = saldo escaso, false en caso contrario.  El saldo escaso es una convención entre el INT IP/REV y su usuario y equivale a un umbral de valor económico.")
  @NotNull


  public Boolean isSaldoBajo() {
    return saldoBajo;
  }

  public void setSaldoBajo(Boolean saldoBajo) {
    this.saldoBajo = saldoBajo;
  }

  public Saldo version(OffsetDateTime version) {
    this.version = version;
    return this;
  }

  /**
   * Versión del registro, siempre debe ser creciente con respecto a la versión anterior de la información del usuario.  El código de versión debe ser expresado como una fecha de actualización del registro por parte del INT IP/REV.  En cualquier caso, el INT IP/REV debe garantizar que sea único y creciente.
   * @return version
  **/
  @ApiModelProperty(required = true, value = "Versión del registro, siempre debe ser creciente con respecto a la versión anterior de la información del usuario.  El código de versión debe ser expresado como una fecha de actualización del registro por parte del INT IP/REV.  En cualquier caso, el INT IP/REV debe garantizar que sea único y creciente.")
  @NotNull

  @Valid

  public OffsetDateTime getVersion() {
    return version;
  }

  public void setVersion(OffsetDateTime version) {
    this.version = version;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Saldo saldo = (Saldo) o;
    return Objects.equals(this.codigoIntermediador, saldo.codigoIntermediador) &&
        Objects.equals(this.placa, saldo.placa) &&
        Objects.equals(this.tid, saldo.tid) &&
        Objects.equals(this.epc, saldo.epc) &&
        Objects.equals(this.saldo, saldo.saldo) &&
        Objects.equals(this.saldoBajo, saldo.saldoBajo) &&
        Objects.equals(this.version, saldo.version);
  }

  @Override
  public int hashCode() {
    return Objects.hash(codigoIntermediador, placa, tid, epc, saldo, saldoBajo, version);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Saldo {\n");
    
    sb.append("    codigoIntermediador: ").append(toIndentedString(codigoIntermediador)).append("\n");
    sb.append("    placa: ").append(toIndentedString(placa)).append("\n");
    sb.append("    tid: ").append(toIndentedString(tid)).append("\n");
    sb.append("    epc: ").append(toIndentedString(epc)).append("\n");
    sb.append("    saldo: ").append(toIndentedString(saldo)).append("\n");
    sb.append("    saldoBajo: ").append(toIndentedString(saldoBajo)).append("\n");
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

