package es.indra.operator.server.configuration;

import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import es.indra.operator.server.daos.AdjDao;
import es.indra.operator.server.daos.AuditDao;
import es.indra.operator.server.daos.BalanceDao;
import es.indra.operator.server.daos.DescriptorsDao;
import es.indra.operator.server.daos.LstDao;
import es.indra.operator.server.daos.TrxDao;
import es.indra.operator.server.daos.impl.AdjDaoImp;
import es.indra.operator.server.daos.impl.AuditDaoImp;
import es.indra.operator.server.daos.impl.BalanceDaoImp;
import es.indra.operator.server.daos.impl.DescriptorsDaoImp;
import es.indra.operator.server.daos.impl.LstDaoImp;
import es.indra.operator.server.daos.impl.TrxDaoImp;
import es.indra.operator.server.services.impl.AdjServiceImp;
import es.indra.operator.server.services.impl.BalanceServiceImp;
import es.indra.operator.server.services.impl.BaseServiceImp;
import es.indra.operator.server.services.impl.LstServiceImp;
import es.indra.operator.server.services.impl.TrxServiceImp;

@Configuration
public class OperatorConfig {
	
	/*
	 * Services
	 */
	@Bean
	public BaseServiceImp baseService(AuditDao auditDao, DescriptorsDao descriptorsDao) {
		BaseServiceImp baseService = new BaseServiceImp();
		baseService.setAuditDao(auditDao);
		baseService.setDescriptorsDao(descriptorsDao);
		return baseService;
	}

	@Bean
	public LstServiceImp lstService(LstDao lstDao, AuditDao auditDao, DescriptorsDao descriptorsDao) {
		LstServiceImp lstService = new LstServiceImp();
		lstService.setAuditDao(auditDao);
		lstService.setDescriptorsDao(descriptorsDao);
		lstService.setLstDao(lstDao);
		return lstService;
	}

	@Bean
	public BalanceServiceImp balanceService(AuditDao auditDao, DescriptorsDao descriptorsDao, BalanceDao balanceDao) {
		BalanceServiceImp balanceService = new BalanceServiceImp();
		balanceService.setAuditDao(auditDao);
		balanceService.setBalanceDao(balanceDao);
		balanceService.setDescriptorsDao(descriptorsDao);
		return balanceService;
	}

	@Bean
	public TrxServiceImp trxService(AuditDao auditDao, DescriptorsDao descriptorsDao, TrxDao trxDao) {
		TrxServiceImp trxService = new TrxServiceImp();
		trxService.setAuditDao(auditDao);
		trxService.setTrxDao(trxDao);
		trxService.setDescriptorsDao(descriptorsDao);
		return trxService;
	}

	@Bean
	public AdjServiceImp adjService(AuditDao auditDao, DescriptorsDao descriptorsDao, AdjDao adjDao) {
		AdjServiceImp adjService = new AdjServiceImp();
		adjService.setAuditDao(auditDao);
		adjService.setAdjDao(adjDao);
		adjService.setDescriptorsDao(descriptorsDao);
		return adjService;
	}

	
	/*
	 * Dao's
	 */
	@Bean
	public AuditDao auditDao() {
		return new AuditDaoImp();
	}
	
	@Bean
	public DescriptorsDaoImp descriptorsDao(SessionFactory sessionFactory) {
		DescriptorsDaoImp descriptorsDao = new DescriptorsDaoImp();
		descriptorsDao.setSessionFactory(sessionFactory);
		return descriptorsDao;
	}

	@Bean
	public LstDaoImp lstDao(SessionFactory sessionFactory) {
		LstDaoImp lstDao = new LstDaoImp();
		lstDao.setSessionFactory(sessionFactory);
		return lstDao;
	}

	@Bean
	public BalanceDaoImp balanceDao(SessionFactory sessionFactory) {
		BalanceDaoImp balanceDao = new BalanceDaoImp();
		balanceDao.setSessionFactory(sessionFactory);
		return balanceDao;
	}

	@Bean
	public TrxDaoImp trxDao(SessionFactory sessionFactory) {
		TrxDaoImp trxDao = new TrxDaoImp();
		trxDao.setSessionFactory(sessionFactory);
		return trxDao;
	}

	@Bean
	public AdjDaoImp adjDao(SessionFactory sessionFactory) {
		AdjDaoImp adjDao = new AdjDaoImp();
		adjDao.setSessionFactory(sessionFactory);
		return adjDao;
	}
}
