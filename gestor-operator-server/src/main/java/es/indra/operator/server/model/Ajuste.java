package es.indra.operator.server.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Representa la información de un ajuste reportado por un Operador OP IP/REV.
 */
@ApiModel(description = "Representa la información de un ajuste reportado por un Operador OP IP/REV.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-25T10:51:09.761+02:00")

public class Ajuste   {
  @JsonProperty("codigoAjuste")
  private String codigoAjuste = null;

  @JsonProperty("codigoOperador")
  private Integer codigoOperador = null;

  @JsonProperty("fechaHora")
  private OffsetDateTime fechaHora = null;

  @JsonProperty("fechaRecaudo")
  private LocalDate fechaRecaudo = null;

  @JsonProperty("codigoPasoReferencia")
  private String codigoPasoReferencia = null;

  @JsonProperty("motivoAjuste")
  private Integer motivoAjuste = null;

  @JsonProperty("valorAjuste")
  private BigDecimal valorAjuste = null;

  @JsonProperty("valor")
  private BigDecimal valor = null;

  @JsonProperty("categoria")
  private String categoria = null;

  @JsonProperty("tipoOperacion")
  private Integer tipoOperacion = null;

  public Ajuste codigoAjuste(String codigoAjuste) {
    this.codigoAjuste = codigoAjuste;
    return this;
  }

  /**
   * Get codigoAjuste
   * @return codigoAjuste
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getCodigoAjuste() {
    return codigoAjuste;
  }

  public void setCodigoAjuste(String codigoAjuste) {
    this.codigoAjuste = codigoAjuste;
  }

  public Ajuste codigoOperador(Integer codigoOperador) {
    this.codigoOperador = codigoOperador;
    return this;
  }

  /**
   * Get codigoOperador
   * @return codigoOperador
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Integer getCodigoOperador() {
    return codigoOperador;
  }

  public void setCodigoOperador(Integer codigoOperador) {
    this.codigoOperador = codigoOperador;
  }

  public Ajuste fechaHora(OffsetDateTime fechaHora) {
    this.fechaHora = fechaHora;
    return this;
  }

  /**
   * Fecha y hora del ajuste.
   * @return fechaHora
  **/
  @ApiModelProperty(required = true, value = "Fecha y hora del ajuste.")
  @NotNull

  @Valid

  public OffsetDateTime getFechaHora() {
    return fechaHora;
  }

  public void setFechaHora(OffsetDateTime fechaHora) {
    this.fechaHora = fechaHora;
  }

  public Ajuste fechaRecaudo(LocalDate fechaRecaudo) {
    this.fechaRecaudo = fechaRecaudo;
    return this;
  }

  /**
   * Fecha de recaudo de según el OP IP/REV.
   * @return fechaRecaudo
  **/
  @ApiModelProperty(required = true, value = "Fecha de recaudo de según el OP IP/REV.")
  @NotNull

  @Valid

  public LocalDate getFechaRecaudo() {
    return fechaRecaudo;
  }

  public void setFechaRecaudo(LocalDate fechaRecaudo) {
    this.fechaRecaudo = fechaRecaudo;
  }

  public Ajuste codigoPasoReferencia(String codigoPasoReferencia) {
    this.codigoPasoReferencia = codigoPasoReferencia;
    return this;
  }

  /**
   * Get codigoPasoReferencia
   * @return codigoPasoReferencia
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getCodigoPasoReferencia() {
    return codigoPasoReferencia;
  }

  public void setCodigoPasoReferencia(String codigoPasoReferencia) {
    this.codigoPasoReferencia = codigoPasoReferencia;
  }

  public Ajuste motivoAjuste(Integer motivoAjuste) {
    this.motivoAjuste = motivoAjuste;
    return this;
  }

  /**
   * Razón por la que se realiza el ajuste:  1 – Cambio de categoría, 2 – Cancelación, 3 - Cambio de Tarifa.
   * @return motivoAjuste
  **/
  @ApiModelProperty(required = true, value = "Razón por la que se realiza el ajuste:  1 – Cambio de categoría, 2 – Cancelación, 3 - Cambio de Tarifa.")
  @NotNull


  public Integer getMotivoAjuste() {
    return motivoAjuste;
  }

  public void setMotivoAjuste(Integer motivoAjuste) {
    this.motivoAjuste = motivoAjuste;
  }

  public Ajuste valorAjuste(BigDecimal valorAjuste) {
    this.valorAjuste = valorAjuste;
    return this;
  }

  /**
   * Valor absoluto del ajuste.
   * minimum: 0
   * @return valorAjuste
  **/
  @ApiModelProperty(required = true, value = "Valor absoluto del ajuste.")
  @NotNull

  @Valid
@DecimalMin("0")
  public BigDecimal getValorAjuste() {
    return valorAjuste;
  }

  public void setValorAjuste(BigDecimal valorAjuste) {
    this.valorAjuste = valorAjuste;
  }

  public Ajuste valor(BigDecimal valor) {
    this.valor = valor;
    return this;
  }

  /**
   * Nuevo valor de la transacción tras haber sido ajustada. Debe ser mayor o igual a 0 (cero para el caso de vehículos exentos).
   * minimum: 0
   * @return valor
  **/
  @ApiModelProperty(required = true, value = "Nuevo valor de la transacción tras haber sido ajustada. Debe ser mayor o igual a 0 (cero para el caso de vehículos exentos).")
  @NotNull

  @Valid
@DecimalMin("0")
  public BigDecimal getValor() {
    return valor;
  }

  public void setValor(BigDecimal valor) {
    this.valor = valor;
  }

  public Ajuste categoria(String categoria) {
    this.categoria = categoria;
    return this;
  }

  /**
   * En caso de cambio de categoría, se debe enviar la nueva categoría.
   * @return categoria
  **/
  @ApiModelProperty(value = "En caso de cambio de categoría, se debe enviar la nueva categoría.")


  public String getCategoria() {
    return categoria;
  }

  public void setCategoria(String categoria) {
    this.categoria = categoria;
  }

  public Ajuste tipoOperacion(Integer tipoOperacion) {
    this.tipoOperacion = tipoOperacion;
    return this;
  }

  /**
   * Indica si el ajuste realizado corresponde a un débito o un crédito. 5 - Crédito, 6 - Débito.
   * @return tipoOperacion
  **/
  @ApiModelProperty(required = true, value = "Indica si el ajuste realizado corresponde a un débito o un crédito. 5 - Crédito, 6 - Débito.")
  @NotNull


  public Integer getTipoOperacion() {
    return tipoOperacion;
  }

  public void setTipoOperacion(Integer tipoOperacion) {
    this.tipoOperacion = tipoOperacion;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Ajuste ajuste = (Ajuste) o;
    return Objects.equals(this.codigoAjuste, ajuste.codigoAjuste) &&
        Objects.equals(this.codigoOperador, ajuste.codigoOperador) &&
        Objects.equals(this.fechaHora, ajuste.fechaHora) &&
        Objects.equals(this.fechaRecaudo, ajuste.fechaRecaudo) &&
        Objects.equals(this.codigoPasoReferencia, ajuste.codigoPasoReferencia) &&
        Objects.equals(this.motivoAjuste, ajuste.motivoAjuste) &&
        Objects.equals(this.valorAjuste, ajuste.valorAjuste) &&
        Objects.equals(this.valor, ajuste.valor) &&
        Objects.equals(this.categoria, ajuste.categoria) &&
        Objects.equals(this.tipoOperacion, ajuste.tipoOperacion);
  }

  @Override
  public int hashCode() {
    return Objects.hash(codigoAjuste, codigoOperador, fechaHora, fechaRecaudo, codigoPasoReferencia, motivoAjuste, valorAjuste, valor, categoria, tipoOperacion);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Ajuste {\n");
    
    sb.append("    codigoAjuste: ").append(toIndentedString(codigoAjuste)).append("\n");
    sb.append("    codigoOperador: ").append(toIndentedString(codigoOperador)).append("\n");
    sb.append("    fechaHora: ").append(toIndentedString(fechaHora)).append("\n");
    sb.append("    fechaRecaudo: ").append(toIndentedString(fechaRecaudo)).append("\n");
    sb.append("    codigoPasoReferencia: ").append(toIndentedString(codigoPasoReferencia)).append("\n");
    sb.append("    motivoAjuste: ").append(toIndentedString(motivoAjuste)).append("\n");
    sb.append("    valorAjuste: ").append(toIndentedString(valorAjuste)).append("\n");
    sb.append("    valor: ").append(toIndentedString(valor)).append("\n");
    sb.append("    categoria: ").append(toIndentedString(categoria)).append("\n");
    sb.append("    tipoOperacion: ").append(toIndentedString(tipoOperacion)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

