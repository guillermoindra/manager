package es.indra.operator.server.services.impl;

import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import es.indra.operator.server.daos.TrxDao;
import es.indra.operator.server.entity.SigtDscCodAgency;
import es.indra.operator.server.entity.SigtDscNotifType;
import es.indra.operator.server.entity.SigtDscTrxTestResponseCode;
import es.indra.operator.server.entity.SigtOpDscTrxTestNotifStatus;
import es.indra.operator.server.entity.SigtOpToIntNotif;
import es.indra.operator.server.entity.SigtOpTrxDscStatus;
import es.indra.operator.server.entity.SigtOpTrxPend;
import es.indra.operator.server.entity.SigtOpTrxPro;
import es.indra.operator.server.entity.SigtOpTrxTestNotif;
import es.indra.operator.server.entity.SigtOpTrxTestResponse;
import es.indra.operator.server.model.ConfirmacionProcesamientoPaso;
import es.indra.operator.server.model.Ticket;
import es.indra.operator.server.services.TrxService;
import es.indra.operator.server.utils.GenericConstant;
import es.indra.operator.server.utils.GenericFilterDTO;

/**
 * This class is the implementation of the TrxService interface.
 * 
 * @author rharo
 */
@Service
@Transactional
public class TrxServiceImp extends BaseServiceImp implements TrxService {

	Logger LOGGER = LoggerFactory.getLogger(TrxServiceImp.class);
	
	/** Transaction DAO */
	private TrxDao trxDao;

	@Override
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public Ticket saveSigtOpTrxTestNotif(Date currentDate, String transitCode, HttpServletRequest request) throws Exception {
		LOGGER.debug("Transit code: "+transitCode);

		SigtOpDscTrxTestNotifStatus sigtOpDscTrxTestNotifStatus = (SigtOpDscTrxTestNotifStatus) getDescriptorsById(GenericConstant.SigtOpDscTrxTestNotifStatus_PENDIENTE_ENVIAR, SigtOpDscTrxTestNotifStatus.class);
		//SigtDscCodAgency sigtDscCodAgencyBD =  (SigtDscCodAgency) getDescriptorsByExternalCode(GenericConstant.INTERMEDIATOR_CODE.toString(), SigtDscCodAgency.class);
	
		String[] split = transitCode.split("_");
		String identifier = split[0];
		String idTransit = split[1];
		
		SigtDscCodAgency sigtDscCodAgencyBD =  (SigtDscCodAgency) getDescriptorsByExternalCode(identifier, SigtDscCodAgency.class);
		
		LOGGER.debug("SigtOpTrxTestNotif.idTransit: "+idTransit);

		SigtOpTrxTestNotif sigtOpTrxTestNotif = new SigtOpTrxTestNotif(
				null, 
				sigtOpDscTrxTestNotifStatus, 
				idTransit, 
				sigtDscCodAgencyBD,
				currentDate, 
				request.getAttribute("requestUUID").toString(), 
				currentDate, 
				GenericConstant.MODIFICATION_USER, 
				GenericConstant.APPLICATION_ID, 
				null, 
				(byte) 0, 
				(byte) 0);
		
		trxDao.save(sigtOpTrxTestNotif);
		trxDao.doFlush();
		LOGGER.debug("Saved sigtOpTrxTestNotif");

		
		SigtDscCodAgency sigtDscCodAgency = (SigtDscCodAgency) getDescriptorsByExternalCode(
				GenericConstant.OPERATOR_CODE.toString(), SigtDscCodAgency.class);
		SigtDscTrxTestResponseCode sigtDscTrxTestResponseCode = (SigtDscTrxTestResponseCode) getDescriptorsById(
					GenericConstant.SigtDscTrxTestResponseCode_TICKET_PENDIENTE_TRATAMIENTO,
					SigtDscTrxTestResponseCode.class);

		LOGGER.debug("SigtOpTrxTestResponse.idTransit: "+idTransit);
		SigtOpTrxTestResponse sigtOpTrxTestResponse = new SigtOpTrxTestResponse(
				null, sigtDscTrxTestResponseCode,
				sigtDscCodAgency, idTransit, 
				request.getAttribute("requestUUID").toString(), 
				currentDate, GenericConstant.MODIFICATION_USER,
				GenericConstant.APPLICATION_ID, null, (byte) 0, (byte) 0);
		Long sigtOpTrxTestResponseId = (Long) trxDao.save(sigtOpTrxTestResponse);
		LOGGER.debug("Saved sigtOpTrxTestNotif");

		
		Ticket ticket = new Ticket();
		ticket.setCodigo(GenericConstant.OPERATOR_CODE + "_" + sigtOpTrxTestResponseId);
		ticket.setFecha(OffsetDateTime.now());
		ticket.setReferencia(transitCode);
		LOGGER.debug("Ticket: "+ticket.toString());
		
		return ticket;
	}
	
	@Override
	public SigtOpTrxPend findSigtOpTrxPend(GenericFilterDTO genericFilterDTOArg) throws Exception {
		return trxDao.findSigtOpTrxPend(genericFilterDTOArg);
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public String updateConfirmedTrx(List<ConfirmacionProcesamientoPaso> listStatusTransitsArg, Date currentDateArg, HttpServletRequest request) throws Exception {

		//String auditMessage = new String();

		if(!listStatusTransitsArg.isEmpty()){
			
			// notification data
			SigtOpToIntNotif sigtOpToIntNotif = createIntNotif(listStatusTransitsArg.size(),currentDateArg);
	
			//process each transaction
			LOGGER.debug("Processing transit confirmations...");
	
			for (ConfirmacionProcesamientoPaso confirmacionProcesamientoPaso : listStatusTransitsArg) {
	
				//auditMessage += confirmacionProcesamientoPaso.toString() + "\n";
	
				
				LOGGER.debug(confirmacionProcesamientoPaso.toString());
				
				String dataTransit[] = confirmacionProcesamientoPaso.getCodigoPaso().split("_");
	
				Integer operatorCode = Integer.parseInt(dataTransit[0].toString().trim());
				Long identifier =  Long.parseLong(dataTransit[1].toString().trim()); 
				
				
				//find SigtOpTrxPend by filter
				GenericFilterDTO genericFilterDTO = new GenericFilterDTO();
				genericFilterDTO.setIdentifier(identifier);
				genericFilterDTO.setStatus(GenericConstant.SigtOpTrxDscStatus_ENVIADO);
				SigtOpTrxPend sigtOpTrxPend = (SigtOpTrxPend) findSigtOpTrxPend(genericFilterDTO);
				
				if(sigtOpTrxPend != null && operatorCode.equals(GenericConstant.OPERATOR_CODE)){
													
					SigtOpTrxPro  sigtOpTrxPro =  new SigtOpTrxPro(
							sigtOpTrxPend.getIdentifier(),
							sigtOpTrxPend.getSigtDscVehicleClassByChargedClass(),
							sigtOpTrxPend.getSigtOpToIntNotif(),
							sigtOpTrxPend.getSigtOpToIntNotifSigt(),
							sigtOpTrxPend.getSigtDscPlacesByLaneId(),
							sigtOpToIntNotif,
							sigtOpTrxPend.getSigtDscVehicleClassByDetectedClass(),
							sigtOpTrxPend.getSigtDscCodAgency(),
							confirmacionProcesamientoPaso.getCodigoRespuesta(),
							sigtOpTrxPend.getSigtDscPlacesByStationId(),
							sigtOpTrxPend.getSigtTrxDscReadType(),
							sigtOpTrxPend.getTrxId(),
							sigtOpTrxPend.getTrxDate(),
							sigtOpTrxPend.getClientId(),
							sigtOpTrxPend.getPlate(),
							sigtOpTrxPend.getTid(),
							sigtOpTrxPend.getEpc(),
							sigtOpTrxPend.getAmount(),
							sigtOpTrxPend.getNumAxles(),
							sigtOpTrxPend.getNumDoubleWheel(),
							sigtOpTrxPend.getDirection(),
							sigtOpTrxPend.getPlateOcr(),
							sigtOpTrxPend.isIsDiscrepancy(),
						    sigtOpTrxPend.getListDate(),
							sigtOpTrxPend.getIdList(),
							sigtOpTrxPend.isIsPlateDiscrepancy(),
							confirmacionProcesamientoPaso.getTokenConfirmacion(),
							confirmacionProcesamientoPaso.getDescripcion(),
							currentDateArg,
							GenericConstant.MODIFICATION_USER,
							GenericConstant.APPLICATION_ID,
							null,
							(byte) 0, 
							(byte) 0);
					
					sigtOpTrxPro.setUuid(sigtOpTrxPend.getUuid());
					sigtOpTrxPro.setUuidResponse(request.getAttribute("requestUUID").toString());
					sigtOpTrxPro.setSigtOpTrxDscStatus(sigtOpTrxPend.getSigtOpTrxDscStatus());
					sigtOpTrxPro.setNumSend(sigtOpTrxPend.getNumSend());
					sigtOpTrxPro.setLaneExternalId(sigtOpTrxPend.getLaneExternalId());
												
					//Save the new SigtOpTrxPro Object, and delete the SigtOpTrxPend object by PK
					trxDao.save(sigtOpTrxPro);
					
					SigtOpTrxDscStatus sigtOpTrxDscStatus = (SigtOpTrxDscStatus) getDescriptorsById(GenericConstant.SigtOpTrxDscStatus_COMPLETADO, SigtOpTrxDscStatus.class);
					sigtOpTrxPend.setSigtOpTrxDscStatus(sigtOpTrxDscStatus);
					sigtOpTrxPend.setModificationUser(GenericConstant.MODIFICATION_USER);
					sigtOpTrxPend.setApplication(GenericConstant.APPLICATION_ID);
					sigtOpTrxPend.setModificationDate(currentDateArg);
					sigtOpTrxPend.setModification((byte) 1);
					sigtOpTrxPend.setOptimistLock((byte) (sigtOpTrxPend.getOptimistLock() + 1));
					
					trxDao.update(sigtOpTrxPend);
	
				} else {
					LOGGER.warn("V1ApiServiceImpl.v1ConfirmacionesPasosPut: Transaction not sent :  " + confirmacionProcesamientoPaso.getCodigoPaso());
				}
	
			}

		} else {
			LOGGER.warn("V1ApiServiceImpl.v1ConfirmacionesPasosPut: received list empty ");
		}
		
		return null; //auditMessage;
	}

	/**
	 * Method that create a notification.
	 * 
	 * @param listSizeArg - list size
	 * @param currentDateArg - current date
	 * @return  SigtOpToIntNotif object
	 */
	private SigtOpToIntNotif createIntNotif(int listSizeArg,Date currentDateArg) throws Exception {
		
		SigtDscCodAgency sigtDscCodAgency = (SigtDscCodAgency) super.getDescriptorsByExternalCode(
				GenericConstant.OPERATOR_CODE.toString(), SigtDscCodAgency.class);
		
		SigtDscNotifType sigtDscNotifType = (SigtDscNotifType)
				super.getDescriptorsById(GenericConstant.SigtDscNotifType_CONFIRMACION_VIAJES,
				SigtDscNotifType.class);
		
		SigtOpToIntNotif sigtOpToIntNotif = new SigtOpToIntNotif();
		sigtOpToIntNotif.setSigtDscNotifType(sigtDscNotifType);
		sigtOpToIntNotif.setSigtDscCodAgency(sigtDscCodAgency);
		sigtOpToIntNotif.setNumRecords(listSizeArg);
		sigtOpToIntNotif.setApplication(GenericConstant.APPLICATION_ID);
		sigtOpToIntNotif.setModificationUser(GenericConstant.MODIFICATION_USER);
		sigtOpToIntNotif.setCreationDate(currentDateArg);
		sigtOpToIntNotif.setModificationDate(null);
		sigtOpToIntNotif.setModification((byte) 0);
		sigtOpToIntNotif.setOptimistLock((byte) 0);
		
		Long  idenNotif = (Long) trxDao.save(sigtOpToIntNotif);
		sigtOpToIntNotif.setIdentifier(idenNotif);

		return sigtOpToIntNotif;
	}

	/**
	 * Return trxDao attribute.
	 *
	 * @return trxDao - Attribute returned
	 */
	public TrxDao getTrxDao() {
		return trxDao;
	}

	/**
	 * Set attribute trxDao.
	 *
	 * @param trxDaoArg
	 *            - Set value
	 */
	public void setTrxDao(TrxDao trxDaoArg) {
		trxDao = trxDaoArg;
	}


}