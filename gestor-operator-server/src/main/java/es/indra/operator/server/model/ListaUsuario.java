package es.indra.operator.server.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Representa una lista de usuarios de un INT IP/REV.
 */
@ApiModel(description = "Representa una lista de usuarios de un INT IP/REV.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-25T10:51:09.761+02:00")

public class ListaUsuario   {
  @JsonProperty("codigoIntermediador")
  private Integer codigoIntermediador = null;

  @JsonProperty("codigoLista")
  private String codigoLista = null;

  /**
   * Identifica si la lista es T=total, P=parcial o N=negra.
   */
  public enum TipoListaEnum {
    T("T"),
    
    P("P"),
    
    N("N");

    private String value;

    TipoListaEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static TipoListaEnum fromValue(String text) {
      for (TipoListaEnum b : TipoListaEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("tipoLista")
  private TipoListaEnum tipoLista = null;

  @JsonProperty("fechaEmision")
  private OffsetDateTime fechaEmision = null;

  @JsonProperty("codigoListaAnterior")
  private String codigoListaAnterior = null;

  @JsonProperty("numeroRegistros")
  private Integer numeroRegistros = null;

  public ListaUsuario codigoIntermediador(Integer codigoIntermediador) {
    this.codigoIntermediador = codigoIntermediador;
    return this;
  }

  /**
   * Get codigoIntermediador
   * @return codigoIntermediador
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Integer getCodigoIntermediador() {
    return codigoIntermediador;
  }

  public void setCodigoIntermediador(Integer codigoIntermediador) {
    this.codigoIntermediador = codigoIntermediador;
  }

  public ListaUsuario codigoLista(String codigoLista) {
    this.codigoLista = codigoLista;
    return this;
  }

  /**
   * Get codigoLista
   * @return codigoLista
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getCodigoLista() {
    return codigoLista;
  }

  public void setCodigoLista(String codigoLista) {
    this.codigoLista = codigoLista;
  }

  public ListaUsuario tipoLista(TipoListaEnum tipoLista) {
    this.tipoLista = tipoLista;
    return this;
  }

  /**
   * Identifica si la lista es T=total, P=parcial o N=negra.
   * @return tipoLista
  **/
  @ApiModelProperty(required = true, value = "Identifica si la lista es T=total, P=parcial o N=negra.")
  @NotNull


  public TipoListaEnum getTipoLista() {
    return tipoLista;
  }

  public void setTipoLista(TipoListaEnum tipoLista) {
    this.tipoLista = tipoLista;
  }

  public ListaUsuario fechaEmision(OffsetDateTime fechaEmision) {
    this.fechaEmision = fechaEmision;
    return this;
  }

  /**
   * Fecha y hora de la generación de la lista por parte del INT IP/REV.
   * @return fechaEmision
  **/
  @ApiModelProperty(required = true, value = "Fecha y hora de la generación de la lista por parte del INT IP/REV.")
  @NotNull

  @Valid

  public OffsetDateTime getFechaEmision() {
    return fechaEmision;
  }

  public void setFechaEmision(OffsetDateTime fechaEmision) {
    this.fechaEmision = fechaEmision;
  }

  public ListaUsuario codigoListaAnterior(String codigoListaAnterior) {
    this.codigoListaAnterior = codigoListaAnterior;
    return this;
  }

  /**
   * Get codigoListaAnterior
   * @return codigoListaAnterior
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getCodigoListaAnterior() {
    return codigoListaAnterior;
  }

  public void setCodigoListaAnterior(String codigoListaAnterior) {
    this.codigoListaAnterior = codigoListaAnterior;
  }

  public ListaUsuario numeroRegistros(Integer numeroRegistros) {
    this.numeroRegistros = numeroRegistros;
    return this;
  }

  /**
   * Cantidad de registros incluidas en la lista.
   * minimum: 1
   * @return numeroRegistros
  **/
  @ApiModelProperty(required = true, value = "Cantidad de registros incluidas en la lista.")
  @NotNull

@Min(1)
  public Integer getNumeroRegistros() {
    return numeroRegistros;
  }

  public void setNumeroRegistros(Integer numeroRegistros) {
    this.numeroRegistros = numeroRegistros;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ListaUsuario listaUsuario = (ListaUsuario) o;
    return Objects.equals(this.codigoIntermediador, listaUsuario.codigoIntermediador) &&
        Objects.equals(this.codigoLista, listaUsuario.codigoLista) &&
        Objects.equals(this.tipoLista, listaUsuario.tipoLista) &&
        Objects.equals(this.fechaEmision, listaUsuario.fechaEmision) &&
        Objects.equals(this.codigoListaAnterior, listaUsuario.codigoListaAnterior) &&
        Objects.equals(this.numeroRegistros, listaUsuario.numeroRegistros);
  }

  @Override
  public int hashCode() {
    return Objects.hash(codigoIntermediador, codigoLista, tipoLista, fechaEmision, codigoListaAnterior, numeroRegistros);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ListaUsuario {\n");
    
    sb.append("    codigoIntermediador: ").append(toIndentedString(codigoIntermediador)).append("\n");
    sb.append("    codigoLista: ").append(toIndentedString(codigoLista)).append("\n");
    sb.append("    tipoLista: ").append(toIndentedString(tipoLista)).append("\n");
    sb.append("    fechaEmision: ").append(toIndentedString(fechaEmision)).append("\n");
    sb.append("    codigoListaAnterior: ").append(toIndentedString(codigoListaAnterior)).append("\n");
    sb.append("    numeroRegistros: ").append(toIndentedString(numeroRegistros)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

