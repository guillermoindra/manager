package es.indra.operator.server.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;



public abstract class GenericUtils {

	public static <T> List<T> castList(Class<? extends T> clazz, Collection<?> c) {
		if(c != null){
			List<T> r = new ArrayList<T>(c.size());
			for(Object o: c)
				r.add(clazz.cast(o));
			return r;
		}else return null;
	}

	public static Date getDateFromStr(String dateStr, String format) throws ParseException {
		if(format == null || format.isEmpty()){
			format = "YYYYMMDD";
		}
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		Date date = formatter.parse(dateStr);
		return date;
	}
	
	 /**
     * Store stack trace into a string.
     * 
     * @param ex - The exception
     * @return String containing the stack trace
     */
    public static String stackTraceStr(Exception ex) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        return sw.toString();
    }
    
    /**
     * Store stack trace into a string.
     * 
     * @param ex - The exception
     * @return String containing the stack trace
     */
    public static String stackTraceStr(Throwable ex) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        return sw.toString();
    }
    

}
