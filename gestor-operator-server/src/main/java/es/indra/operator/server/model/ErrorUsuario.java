package es.indra.operator.server.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Representa la información de error al procesar un usuario de una lista de usuarios. Este error es específico a solamente un registro.
 */
@ApiModel(description = "Representa la información de error al procesar un usuario de una lista de usuarios. Este error es específico a solamente un registro.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-25T10:51:09.761+02:00")

public class ErrorUsuario   {
  @JsonProperty("placa")
  private String placa = null;

  @JsonProperty("tid")
  private String tid = null;

  @JsonProperty("version")
  private OffsetDateTime version = null;

  @JsonProperty("codigoRespuesta")
  private Integer codigoRespuesta = null;

  @JsonProperty("descripcion")
  private String descripcion = null;

  public ErrorUsuario placa(String placa) {
    this.placa = placa;
    return this;
  }

  /**
   * Identifica la placa del vehículo.
   * @return placa
  **/
  @ApiModelProperty(required = true, value = "Identifica la placa del vehículo.")
  @NotNull

@Pattern(regexp="^[a-zA-Z0-9\\-]+$") @Size(min=6,max=10) 
  public String getPlaca() {
    return placa;
  }

  public void setPlaca(String placa) {
    this.placa = placa;
  }

  public ErrorUsuario tid(String tid) {
    this.tid = tid;
    return this;
  }

  /**
   * Código tid del tag.
   * @return tid
  **/
  @ApiModelProperty(required = true, value = "Código tid del tag.")
  @NotNull

@Pattern(regexp="^[\\S]+$") @Size(min=24,max=32) 
  public String getTid() {
    return tid;
  }

  public void setTid(String tid) {
    this.tid = tid;
  }

  public ErrorUsuario version(OffsetDateTime version) {
    this.version = version;
    return this;
  }

  /**
   * Versión del registro reportado en el mensaje original.
   * @return version
  **/
  @ApiModelProperty(required = true, value = "Versión del registro reportado en el mensaje original.")
  @NotNull

  @Valid

  public OffsetDateTime getVersion() {
    return version;
  }

  public void setVersion(OffsetDateTime version) {
    this.version = version;
  }

  public ErrorUsuario codigoRespuesta(Integer codigoRespuesta) {
    this.codigoRespuesta = codigoRespuesta;
    return this;
  }

  /**
   * 1001 – Valor inválido en atributos 1002 – Atributo faltante 1003 – Registro multi INT-IP/REV.
   * @return codigoRespuesta
  **/
  @ApiModelProperty(required = true, value = "1001 – Valor inválido en atributos 1002 – Atributo faltante 1003 – Registro multi INT-IP/REV.")
  @NotNull


  public Integer getCodigoRespuesta() {
    return codigoRespuesta;
  }

  public void setCodigoRespuesta(Integer codigoRespuesta) {
    this.codigoRespuesta = codigoRespuesta;
  }

  public ErrorUsuario descripcion(String descripcion) {
    this.descripcion = descripcion;
    return this;
  }

  /**
   * Descripción de fallo.
   * @return descripcion
  **/
  @ApiModelProperty(value = "Descripción de fallo.")

@Size(max=200) 
  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ErrorUsuario errorUsuario = (ErrorUsuario) o;
    return Objects.equals(this.placa, errorUsuario.placa) &&
        Objects.equals(this.tid, errorUsuario.tid) &&
        Objects.equals(this.version, errorUsuario.version) &&
        Objects.equals(this.codigoRespuesta, errorUsuario.codigoRespuesta) &&
        Objects.equals(this.descripcion, errorUsuario.descripcion);
  }

  @Override
  public int hashCode() {
    return Objects.hash(placa, tid, version, codigoRespuesta, descripcion);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ErrorUsuario {\n");
    
    sb.append("    placa: ").append(toIndentedString(placa)).append("\n");
    sb.append("    tid: ").append(toIndentedString(tid)).append("\n");
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("    codigoRespuesta: ").append(toIndentedString(codigoRespuesta)).append("\n");
    sb.append("    descripcion: ").append(toIndentedString(descripcion)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

