package es.indra.operator.server.services;

import es.indra.operator.server.entity.SigtOpLstNotif;

/**
 * Interface for Lst Service.
 * 
 * @author acoboj
 */
public interface LstService {
	
	/**
	 * Save the SigtOpLstNotif object in database.
	 * 
	 * @param sigtOpLstNotif - the object dto arg
	 * @throws Exception - the exception
	 */
	void saveSigtOpLstNotif(SigtOpLstNotif sigtOpLstNotif) throws Exception;
}
