package es.indra.operator.server.daos.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.jdbc.Work;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import es.indra.operator.server.daos.BaseDao;


/**
 * This class is the implementation of the BaseDao interface.
 * 
 * @author acoboj
 */

@Transactional
public class BaseDaoImp<CLS> extends HibernateDaoSupport implements BaseDao<CLS> {


	public List<?> find(String stringQuery) {
		List<?> list = null;
		list = this.getHibernateTemplate().find(stringQuery);
		return list;
	}
	
	public List<?> find(String stringQuery, int maxResults) {
		List<?> list = null;
		HibernateTemplate ht = this.getHibernateTemplate();  
        ht.setMaxResults(maxResults);  
        list =  ht.find(stringQuery);
        ht.setMaxResults(0); //Restablished to ilimited
        return list;
	}

	public List<?> find(String stringQuery, List<Object> params) {
		List<?> list = null;
		if (params != null && !params.isEmpty()) {
			list = this.getHibernateTemplate().find(stringQuery, params.toArray());
		} else {
			list = this.getHibernateTemplate().find(stringQuery);
		}
		return list;
	}
	
	public List<?> find(String stringQuery, List<Object> params, int maxResults) {
		List<?> list = null;
		HibernateTemplate ht = this.getHibernateTemplate();  
        ht.setMaxResults(maxResults);
		if (params != null && !params.isEmpty()) {
			list = ht.find(stringQuery,	params.toArray());
		} else {
			list = ht.find(stringQuery);
		}
        ht.setMaxResults(0); //Restablished to ilimited
		return list;
	}

	public void update(Object object) {
		this.getHibernateTemplate().update(object);
	}

	public Serializable save(Object object) {
		return this.getHibernateTemplate().save(object);
	}

	public Serializable merge(Object object) {
		return (Serializable) this.getHibernateTemplate().merge(object);
	}

	public Serializable saveOrUpdate(Object o) {
		this.getHibernateTemplate().saveOrUpdate(o);
		return (Serializable) o;
	}
	
	public Serializable findByPK(Serializable pk, Class<?> entityClass) {
		return (Serializable) this.getHibernateTemplate().get(entityClass, pk);
	}

	public void deleteByPK(Class<?> entityClass, Serializable pk) {
		Object entity = this.getHibernateTemplate().get(entityClass, pk);
		this.getHibernateTemplate().delete(entity);
	}

	public void doWork(Work work) {
		this.getSessionFactory().getCurrentSession().doWork(work);
	}

	public void doFlush() {
		this.getSessionFactory().getCurrentSession().flush();
		this.getSessionFactory().getCurrentSession().clear();
	}	
}
