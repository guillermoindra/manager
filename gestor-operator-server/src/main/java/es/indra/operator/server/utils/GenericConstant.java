package es.indra.operator.server.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GenericConstant {
	
	/*
	 * General constant
	 */
	public static  Integer APPLICATION_ID = 1;
	public static  String MODIFICATION_USER = "SIGT_ADMIN";
	//public static  Integer SLEEP_TIME = 10000;
	public static  Integer OPERATOR_CODE = 10027;
	//public static  Integer INTERMEDIATOR_CODE = 10000;	
	
	public static  String PROXY_HOST;
	public static  String PROXY_PORT;
	public static  String PROXY_USER;
	public static  String PROXY_PASSWORD;
	
	/*
	 * Login
	 */
	public static String TENANT;
	public static String AUTHORITY;
	public static String CLIENT_ID;
	public static String USER_NAME;
	public static String PASSWORD;
	
	/*
	 * Lst constants
	 */
	public static  Integer SigtOpDscLstNotifStatus_PENDIENTE_SOLICITAR = 1;
	public static  Integer SigtOpDscLstNotifStatus_SOLICITADA = 2;
	public static  Integer SigtOpDscLstNotifStatus_PENDIENTE_INFORMAR_RECHAZOS = 3;
	public static  Integer SigtOpDscLstNotifStatus_CERRADA = 4;
	
	public static  Integer SigtIntDscLstNotifStatus_PENDIENTE_ENVIAR = 1;
	public static  Integer SigtIntDscLstNotifStatus_NOTIFICACION_ENVIADA = 2;
	public static  Integer SigtIntDscLstNotifStatus_DETALLES_ASOCIADOS_ENVIADOS = 3;
	public static  Integer SigtIntDscLstNotifStatus_NOTIFICACION_CON_ERRORES = 4;
	public static  Integer SIGT_INT_LST_DTL_MAX_RESULT = 20;
	
	/*
	 * Transit constants
	 */
	public static  Integer SigtIntTrxDscStatus_PENDIENTE_VALIDAR= 1;
	public static  Integer SigtIntTrxDscStatus_VALIDADO = 2;
	public static  Integer SigtOpTrxDscStatus_PENDIENTE_ENVIO = 1;
	public static  Integer SigtOpTrxDscStatus_ENVIADO = 2;
	public static  Integer SigtOpTrxDscStatus_COMPLETADO = 4;
	
	public static  Integer SigtIntTrxResponseCode_OK = 1;
	public static  String SigtIntTrxResponseCode_OK_Description = "Aceptada";
	
	public static  Integer SigtIntTrxPend_MAX_RESULT = 20;
	public static  String SigtIntTrxPend_COLUMN_ORDER = "TRX_DATE";
	public static  String SigtIntTrxPend_ORDER_TYPE = "ASC";
	public static  Integer SigtOpTrxPend_MAX_RESULT = 20;
	public static  String SigtOpTrxPend_COLUMN_ORDER = "TRX_DATE";
	public static  String SigtOpTrxPend_ORDER_TYPE = "ASC";

	/*
	 * Adjustment constants
	 */
	public static  Integer SigtIntTrxAdjDscStatus_PENDIENTE_VALIDAR = 1;
	public static  Integer SigtIntTrxAdjDscStatus_VALIDADO = 2;
	public static  Integer SigtOpTrxAdjDscStatus_PENDIENTE_ENVIO = 1;
	public static  Integer SigtOpTrxAdjDscStatus_ENVIADO = 2;
	public static  Integer SigtOpTrxAdjDscStatus_COMPLETADO = 4;
	public static  Integer SigtIntTrxAdjResponseCode_OK = 1;
	public static  String SigtIntTrxAdjResponseCode_OK_Description = "Aceptada";

	
	public static  Integer SigtIntTrxAdjPend_MAX_RESULT = 20;
	public static  String SigtIntTrxAdjPend_COLUMN_ORDER = "TRX_DATE";
	public static  String SigtIntTrxAdjPend_ORDER_TYPE = "ASC";
	public static  Integer SigtOpTrxAdjPend_MAX_RESULT = 20;
	public static  String SigtOpTrxAdjPend_COLUMN_ORDER = "TRX_DATE";
	public static  String SigtOpTrxAdjPend_ORDER_TYPE = "ASC";
	
	/*
	 * Negation Transit constants
	 */
	public static  Integer SigtIntTrxNegPassDscStatus_PENDIENTE_VALIDAR = 1;
	public static  Integer SigtOpTrxNegPassDscStatus_PENDIENTE_ENVIO = 1;
	public static  Integer SigtOpTrxNegPassDscStatus_ENVIADO = 2;
	
	
	public static  Integer SigtOpTrxNegPassPend_MAX_RESULT = 20;
	public static  String SigtOpTrxNegPassPend_COLUMN_ORDER = "TRX_DATE";
	public static  String SigtOpTrxNegPassPend_ORDER_TYPE = "ASC";
		
	/*
	 * Notification type
	 */
	public static  Integer SigtDscNotifType_NOTIFICACION_VIAJES = 1;
	public static  Integer SigtDscNotifType_CONFIRMACION_VIAJES = 2;
	public static  Integer SigtDscNotifType_NOTIFICACION_AJUSTES = 3;
	public static  Integer SigtDscNotifType_CONFIRMACION_AJUSTES = 4;
	public static  Integer SigtDscNotifType_NOTIFICACION_NEGACION_VIAJES = 5;
	
	/*
	 * Transit test constant
	 */	
	public static  Integer SigtOpDscTrxTestNotifStatus_PENDIENTE_ENVIAR = 1;
	public static  Integer SigtOpDscTrxTestNotifStatus_ENVIADO = 2;
	public static  Integer SigtOpDscTrxTestNotifStatus_OMITIDO = 3;
	//public static  String TRANSIT_IMAGES_PATH;
	//public static  String TRANSIT_IMAGES_EXT;
	public static  Integer SigtDscTrxTestResponseCode_PRUEBAS_DISPONIBLES = 1;	
	public static  Integer SigtDscTrxTestResponseCode_PRUEBAS_NO_DISPONIBLES = 2;
	public static  Integer SigtDscTrxTestResponseCode_TRANSITO_NO_ENCONTRADO = 3;
	public static  Integer SigtDscTrxTestResponseCode_TICKET_PENDIENTE_TRATAMIENTO = 4;

	
	public static  Integer SigtIntDscTrxTestNotifStatus_PENDIENTE_ENVIAR = 1;
	public static  Integer SigtIntDscTrxTestNotifStatus_ENVIADO = 2;


	
	/*
	 * Balance notification
	 */
	public static  Integer SigtIntDscBalanceSendStatus_PENDIENTE_ENVIAR = 1;
	public static  Integer SigtIntDscBalanceSendStatus_ENVIADO = 2;
	
	/*
	 * Generate values from application.yml
	 */
	@Value("${operator.application-id:1}")
	public void setApplicationId(Integer id) {
		APPLICATION_ID = id;
	}
	@Value("${operator.modification-user:SIGT_ADMIN}")
	public void setModificationUser(String modificationUser) {
		MODIFICATION_USER = modificationUser;
	}
	/*@Value("${operator.sleep-time:10000}")
	public void setSleepTime(Integer SleepTime) {
		SLEEP_TIME = SleepTime;
	}*/
	@Value("${operator.operator-code:10027}")
	public void setOperatorCode(Integer operatorCode) {
		OPERATOR_CODE = operatorCode;
	}
	/*@Value("${operator.intermediator-code:10000}")
	public void setIntermediatorCode(Integer intermediatorCode) {
		INTERMEDIATOR_CODE = intermediatorCode;
	}*/
	
	@Value("${operator.proxy.host}")
	public void setProxyHost(String host) {
		PROXY_HOST = host;
	}
	@Value("${operator.proxy.port}")
	public void getPROXY_PORT(String port) {
		PROXY_PORT = port;
	}
	@Value("${operator.proxy.user}")
	public void getPROXY_USER(String user) {
		PROXY_USER = user;
	}
	@Value("${operator.proxy.password}")
	public void getPROXY_PASSWORD(String password) {
		PROXY_PASSWORD = password;
	}
	
	@Value("${operator.login.tenant}")
	public void setTenant(String tenant) {
		TENANT = tenant;
	}
	@Value("${operator.login.authority}")
	public void setAuthority(String authority) {
		AUTHORITY = authority;
	}
	@Value("${operator.login.client-id}")
	public void setClientId(String clientId) {
		CLIENT_ID = clientId;
	}
	@Value("${operator.login.user-name}")
	public void setUserName(String userName) {
		USER_NAME = userName;
	}
	@Value("${operator.login.password}")
	public void setPassword(String password) {
		PASSWORD = password;
	}
	
	/*@Value("${operator.transit-images.path:C:\\SiGT\\TRANSIT_IMAGES}")
	public void setTransitImagesPath(String transitImagesPath) {
		TRANSIT_IMAGES_PATH = transitImagesPath;
	}
	@Value("${operator.transit-images.images-ext:.jpg}")
	public void setTransitImagesExt(String transitImagesExt) {
		TRANSIT_IMAGES_EXT = transitImagesExt;
	}*/
	
	/*
	 * SETTERS
	 */	
	/*public static void setAPPLICATION_ID(Integer aPPLICATION_ID) {
		APPLICATION_ID = aPPLICATION_ID;
	}
	public static void setMODIFICATION_USER(String mODIFICATION_USER) {
		MODIFICATION_USER = mODIFICATION_USER;
	}*/
	
	/*public static void setSLEEP_TIME(Integer sLEEP_TIME) {
		SLEEP_TIME = sLEEP_TIME;
	}*/
	/*public static void setOPERATOR_CODE(Integer oPERATOR_CODE) {
		OPERATOR_CODE = oPERATOR_CODE;
	}*/
	/*public static void setINTERMEDIATOR_CODE(Integer iNTERMEDIATOR_CODE) {
		INTERMEDIATOR_CODE = iNTERMEDIATOR_CODE;
	}*/
	
	public static void setTENANT(String tENANT) {
		TENANT = tENANT;
	}
	public static void setAUTHORITY(String aUTHORITY) {
		AUTHORITY = aUTHORITY;
	}
	public static void setCLIENT_ID(String cLIENT_ID) {
		CLIENT_ID = cLIENT_ID;
	}
	public static void setUSER_NAME(String uSER_NAME) {
		USER_NAME = uSER_NAME;
	}
	public static void setPASSWORD(String pASSWORD) {
		PASSWORD = pASSWORD;
	}
	
	public static void setSigtIntTrxResponseCode_OK(Integer sigtIntTrxResponseCode_OK) {
		SigtIntTrxResponseCode_OK = sigtIntTrxResponseCode_OK;
	}
	public static void setSigtIntTrxAdjResponseCode_OK(Integer sigtIntTrxAdjResponseCode_OK) {
		SigtIntTrxAdjResponseCode_OK = sigtIntTrxAdjResponseCode_OK;
	}
	
	public static void setSigtIntTrxResponseCode_OK_Description(String sigtIntTrxResponseCode_OK_Description) {
		SigtIntTrxResponseCode_OK_Description = sigtIntTrxResponseCode_OK_Description;
	}
	public static void setSigtIntTrxAdjResponseCode_OK_Description(String sigtIntTrxAdjResponseCode_OK_Description) {
		SigtIntTrxAdjResponseCode_OK_Description = sigtIntTrxAdjResponseCode_OK_Description;
	}
	public static void setSigtOpDscLstNotifStatus_PENDIENTE_SOLICITAR(Integer sigtOpDscLstNotifStatus_PENDIENTE_SOLICITAR) {
		SigtOpDscLstNotifStatus_PENDIENTE_SOLICITAR = sigtOpDscLstNotifStatus_PENDIENTE_SOLICITAR;
	}
	public static void setSigtOpDscLstNotifStatus_SOLICITADA(Integer sigtOpDscLstNotifStatus_SOLICITADA) {
		SigtOpDscLstNotifStatus_SOLICITADA = sigtOpDscLstNotifStatus_SOLICITADA;
	}
	public static void setSigtOpDscLstNotifStatus_PENDIENTE_INFORMAR_RECHAZOS(
			Integer sigtOpDscLstNotifStatus_PENDIENTE_INFORMAR_RECHAZOS) {
		SigtOpDscLstNotifStatus_PENDIENTE_INFORMAR_RECHAZOS = sigtOpDscLstNotifStatus_PENDIENTE_INFORMAR_RECHAZOS;
	}
	public static void setSigtOpDscLstNotifStatus_CERRADA(Integer sigtOpDscLstNotifStatus_CERRADA) {
		SigtOpDscLstNotifStatus_CERRADA = sigtOpDscLstNotifStatus_CERRADA;
	}
	public static void setSigtIntDscLstNotifStatus_PENDIENTE_ENVIAR(Integer sigtIntDscLstNotifStatus_PENDIENTE_ENVIAR) {
		SigtIntDscLstNotifStatus_PENDIENTE_ENVIAR = sigtIntDscLstNotifStatus_PENDIENTE_ENVIAR;
	}
	public static void setSigtIntDscLstNotifStatus_NOTIFICACION_ENVIADA(
			Integer sigtIntDscLstNotifStatus_NOTIFICACION_ENVIADA) {
		SigtIntDscLstNotifStatus_NOTIFICACION_ENVIADA = sigtIntDscLstNotifStatus_NOTIFICACION_ENVIADA;
	}
	public static void setSigtIntDscLstNotifStatus_DETALLES_ASOCIADOS_ENVIADOS(
			Integer sigtIntDscLstNotifStatus_DETALLES_ASOCIADOS_ENVIADOS) {
		SigtIntDscLstNotifStatus_DETALLES_ASOCIADOS_ENVIADOS = sigtIntDscLstNotifStatus_DETALLES_ASOCIADOS_ENVIADOS;
	}
	public static void setSigtIntDscLstNotifStatus_NOTIFICACION_CON_ERRORES(
			Integer sigtIntDscLstNotifStatus_NOTIFICACION_CON_ERRORES) {
		SigtIntDscLstNotifStatus_NOTIFICACION_CON_ERRORES = sigtIntDscLstNotifStatus_NOTIFICACION_CON_ERRORES;
	}
	public static void setSIGT_INT_LST_DTL_MAX_RESULT(Integer sIGT_INT_LST_DTL_MAX_RESULT) {
		SIGT_INT_LST_DTL_MAX_RESULT = sIGT_INT_LST_DTL_MAX_RESULT;
	}
	public static void setSigtIntTrxDscStatus_PENDIENTE_VALIDAR(Integer sigtIntTrxDscStatus_PENDIENTE_VALIDAR) {
		SigtIntTrxDscStatus_PENDIENTE_VALIDAR = sigtIntTrxDscStatus_PENDIENTE_VALIDAR;
	}
	public static void setSigtIntTrxDscStatus_VALIDADO(Integer sigtIntTrxDscStatus_VALIDADO) {
		SigtIntTrxDscStatus_VALIDADO = sigtIntTrxDscStatus_VALIDADO;
	}
	public static void setSigtOpTrxDscStatus_PENDIENTE_ENVIO(Integer sigtOpTrxDscStatus_PENDIENTE_ENVIO) {
		SigtOpTrxDscStatus_PENDIENTE_ENVIO = sigtOpTrxDscStatus_PENDIENTE_ENVIO;
	}
	public static void setSigtOpTrxDscStatus_ENVIADO(Integer sigtOpTrxDscStatus_ENVIADO) {
		SigtOpTrxDscStatus_ENVIADO = sigtOpTrxDscStatus_ENVIADO;
	}
	
	public static void setSigtIntTrxPend_MAX_RESULT(Integer sigtIntTrxPend_MAX_RESULT) {
		SigtIntTrxPend_MAX_RESULT = sigtIntTrxPend_MAX_RESULT;
	}
	public static void setSigtIntTrxPend_COLUMN_ORDER(String sigtIntTrxPend_COLUMN_ORDER) {
		SigtIntTrxPend_COLUMN_ORDER = sigtIntTrxPend_COLUMN_ORDER;
	}
	public static void setSigtIntTrxPend_ORDER_TYPE(String sigtIntTrxPend_ORDER_TYPE) {
		SigtIntTrxPend_ORDER_TYPE = sigtIntTrxPend_ORDER_TYPE;
	}
	public static void setSigtOpTrxPend_MAX_RESULT(Integer sigtOpTrxPend_MAX_RESULT) {
		SigtOpTrxPend_MAX_RESULT = sigtOpTrxPend_MAX_RESULT;
	}
	public static void setSigtOpTrxPend_COLUMN_ORDER(String sigtOpTrxPend_COLUMN_ORDER) {
		SigtOpTrxPend_COLUMN_ORDER = sigtOpTrxPend_COLUMN_ORDER;
	}
	public static void setSigtOpTrxPend_ORDER_TYPE(String sigtOpTrxPend_ORDER_TYPE) {
		SigtOpTrxPend_ORDER_TYPE = sigtOpTrxPend_ORDER_TYPE;
	}
	public static void setSigtIntTrxAdjDscStatus_PENDIENTE_VALIDAR(Integer sigtIntTrxAdjDscStatus_PENDIENTE_VALIDAR) {
		SigtIntTrxAdjDscStatus_PENDIENTE_VALIDAR = sigtIntTrxAdjDscStatus_PENDIENTE_VALIDAR;
	}
	public static void setSigtIntTrxAdjDscStatus_VALIDADO(Integer sigtIntTrxAdjDscStatus_VALIDADO) {
		SigtIntTrxAdjDscStatus_VALIDADO = sigtIntTrxAdjDscStatus_VALIDADO;
	}
	public static void setSigtOpTrxAdjDscStatus_PENDIENTE_ENVIO(Integer sigtOpTrxAdjDscStatus_PENDIENTE_ENVIO) {
		SigtOpTrxAdjDscStatus_PENDIENTE_ENVIO = sigtOpTrxAdjDscStatus_PENDIENTE_ENVIO;
	}
	public static void setSigtOpTrxAdjDscStatus_ENVIADO(Integer sigtOpTrxAdjDscStatus_ENVIADO) {
		SigtOpTrxAdjDscStatus_ENVIADO = sigtOpTrxAdjDscStatus_ENVIADO;
	}
	public static void setSigtIntTrxAdjPend_MAX_RESULT(Integer sigtIntTrxAdjPend_MAX_RESULT) {
		SigtIntTrxAdjPend_MAX_RESULT = sigtIntTrxAdjPend_MAX_RESULT;
	}
	public static void setSigtIntTrxAdjPend_COLUMN_ORDER(String sigtIntTrxAdjPend_COLUMN_ORDER) {
		SigtIntTrxAdjPend_COLUMN_ORDER = sigtIntTrxAdjPend_COLUMN_ORDER;
	}
	public static void setSigtIntTrxAdjPend_ORDER_TYPE(String sigtIntTrxAdjPend_ORDER_TYPE) {
		SigtIntTrxAdjPend_ORDER_TYPE = sigtIntTrxAdjPend_ORDER_TYPE;
	}
	public static void setSigtOpTrxAdjPend_MAX_RESULT(Integer sigtOpTrxAdjPend_MAX_RESULT) {
		SigtOpTrxAdjPend_MAX_RESULT = sigtOpTrxAdjPend_MAX_RESULT;
	}
	public static void setSigtOpTrxAdjPend_COLUMN_ORDER(String sigtOpTrxAdjPend_COLUMN_ORDER) {
		SigtOpTrxAdjPend_COLUMN_ORDER = sigtOpTrxAdjPend_COLUMN_ORDER;
	}
	public static void setSigtOpTrxAdjPend_ORDER_TYPE(String sigtOpTrxAdjPend_ORDER_TYPE) {
		SigtOpTrxAdjPend_ORDER_TYPE = sigtOpTrxAdjPend_ORDER_TYPE;
	}
	public static void setSigtIntTrxNegPassDscStatus_PENDIENTE_VALIDAR(
			Integer sigtIntTrxNegPassDscStatus_PENDIENTE_VALIDAR) {
		SigtIntTrxNegPassDscStatus_PENDIENTE_VALIDAR = sigtIntTrxNegPassDscStatus_PENDIENTE_VALIDAR;
	}
	public static void setSigtOpTrxNegPassDscStatus_PENDIENTE_ENVIO(Integer sigtOpTrxNegPassDscStatus_PENDIENTE_ENVIO) {
		SigtOpTrxNegPassDscStatus_PENDIENTE_ENVIO = sigtOpTrxNegPassDscStatus_PENDIENTE_ENVIO;
	}
	public static void setSigtOpTrxNegPassDscStatus_ENVIADO(Integer sigtOpTrxNegPassDscStatus_ENVIADO) {
		SigtOpTrxNegPassDscStatus_ENVIADO = sigtOpTrxNegPassDscStatus_ENVIADO;
	}
	public static void setSigtOpTrxNegPassPend_MAX_RESULT(Integer sigtOpTrxNegPassPend_MAX_RESULT) {
		SigtOpTrxNegPassPend_MAX_RESULT = sigtOpTrxNegPassPend_MAX_RESULT;
	}
	public static void setSigtOpTrxNegPassPend_COLUMN_ORDER(String sigtOpTrxNegPassPend_COLUMN_ORDER) {
		SigtOpTrxNegPassPend_COLUMN_ORDER = sigtOpTrxNegPassPend_COLUMN_ORDER;
	}
	public static void setSigtOpTrxNegPassPend_ORDER_TYPE(String sigtOpTrxNegPassPend_ORDER_TYPE) {
		SigtOpTrxNegPassPend_ORDER_TYPE = sigtOpTrxNegPassPend_ORDER_TYPE;
	}
	public static void setSigtDscNotifType_NOTIFICACION_VIAJES(Integer sigtDscNotifType_NOTIFICACION_VIAJES) {
		SigtDscNotifType_NOTIFICACION_VIAJES = sigtDscNotifType_NOTIFICACION_VIAJES;
	}
	public static void setSigtDscNotifType_CONFIRMACION_VIAJES(Integer sigtDscNotifType_CONFIRMACION_VIAJES) {
		SigtDscNotifType_CONFIRMACION_VIAJES = sigtDscNotifType_CONFIRMACION_VIAJES;
	}
	public static void setSigtDscNotifType_NOTIFICACION_AJUSTES(Integer sigtDscNotifType_NOTIFICACION_AJUSTES) {
		SigtDscNotifType_NOTIFICACION_AJUSTES = sigtDscNotifType_NOTIFICACION_AJUSTES;
	}
	public static void setSigtDscNotifType_CONFIRMACION_AJUSTES(Integer sigtDscNotifType_CONFIRMACION_AJUSTES) {
		SigtDscNotifType_CONFIRMACION_AJUSTES = sigtDscNotifType_CONFIRMACION_AJUSTES;
	}
	public static void setSigtDscNotifType_NOTIFICACION_NEGACION_VIAJES(
			Integer sigtDscNotifType_NOTIFICACION_NEGACION_VIAJES) {
		SigtDscNotifType_NOTIFICACION_NEGACION_VIAJES = sigtDscNotifType_NOTIFICACION_NEGACION_VIAJES;
	}
	public static void setSigtOpDscTrxTestNotifStatus_PENDIENTE_ENVIAR(
			Integer sigtOpDscTrxTestNotifStatus_PENDIENTE_ENVIAR) {
		SigtOpDscTrxTestNotifStatus_PENDIENTE_ENVIAR = sigtOpDscTrxTestNotifStatus_PENDIENTE_ENVIAR;
	}
	public static void setSigtOpDscTrxTestNotifStatus_ENVIADO(Integer sigtOpDscTrxTestNotifStatus_ENVIADO) {
		SigtOpDscTrxTestNotifStatus_ENVIADO = sigtOpDscTrxTestNotifStatus_ENVIADO;
	}
	public static void setSigtOpDscTrxTestNotifStatus_OMITIDO(Integer sigtOpDscTrxTestNotifStatus_OMITIDO) {
		SigtOpDscTrxTestNotifStatus_OMITIDO = sigtOpDscTrxTestNotifStatus_OMITIDO;
	}
	/*public static void setTRANSIT_IMAGES_PATH(String tRANSIT_IMAGES_PATH) {
		TRANSIT_IMAGES_PATH = tRANSIT_IMAGES_PATH;
	}
	public static void setTRANSIT_IMAGES_EXT(String tRANSIT_IMAGES_EXT) {
		TRANSIT_IMAGES_EXT = tRANSIT_IMAGES_EXT;
	}*/
	public static void setSigtDscTrxTestResponseCode_PRUEBAS_DISPONIBLES(
			Integer sigtDscTrxTestResponseCode_PRUEBAS_DISPONIBLES) {
		SigtDscTrxTestResponseCode_PRUEBAS_DISPONIBLES = sigtDscTrxTestResponseCode_PRUEBAS_DISPONIBLES;
	}
	public static void setSigtDscTrxTestResponseCode_PRUEBAS_NO_DISPONIBLES(
			Integer sigtDscTrxTestResponseCode_PRUEBAS_NO_DISPONIBLES) {
		SigtDscTrxTestResponseCode_PRUEBAS_NO_DISPONIBLES = sigtDscTrxTestResponseCode_PRUEBAS_NO_DISPONIBLES;
	}
	public static void setSigtDscTrxTestResponseCode_TRANSITO_NO_ENCONTRADO(
			Integer sigtDscTrxTestResponseCode_TRANSITO_NO_ENCONTRADO) {
		SigtDscTrxTestResponseCode_TRANSITO_NO_ENCONTRADO = sigtDscTrxTestResponseCode_TRANSITO_NO_ENCONTRADO;
	}
	public static void setSigtDscTrxTestResponseCode_TICKET_PENDIENTE_TRATAMIENTO(
			Integer sigtDscTrxTestResponseCode_TICKET_PENDIENTE_TRATAMIENTO) {
		SigtDscTrxTestResponseCode_TICKET_PENDIENTE_TRATAMIENTO = sigtDscTrxTestResponseCode_TICKET_PENDIENTE_TRATAMIENTO;
	}
	public static void setSigtIntDscTrxTestNotifStatus_PENDIENTE_ENVIAR(
			Integer sigtIntDscTrxTestNotifStatus_PENDIENTE_ENVIAR) {
		SigtIntDscTrxTestNotifStatus_PENDIENTE_ENVIAR = sigtIntDscTrxTestNotifStatus_PENDIENTE_ENVIAR;
	}
	public static void setSigtIntDscTrxTestNotifStatus_ENVIADO(Integer sigtIntDscTrxTestNotifStatus_ENVIADO) {
		SigtIntDscTrxTestNotifStatus_ENVIADO = sigtIntDscTrxTestNotifStatus_ENVIADO;
	}
	public static void setSigtIntDscBalanceSendStatus_PENDIENTE_ENVIAR(
			Integer sigtIntDscBalanceSendStatus_PENDIENTE_ENVIAR) {
		SigtIntDscBalanceSendStatus_PENDIENTE_ENVIAR = sigtIntDscBalanceSendStatus_PENDIENTE_ENVIAR;
	}
	public static void setSigtIntDscBalanceSendStatus_ENVIADO(Integer sigtIntDscBalanceSendStatus_ENVIADO) {
		SigtIntDscBalanceSendStatus_ENVIADO = sigtIntDscBalanceSendStatus_ENVIADO;
	}
	public static String getPROXY_HOST() {
		return PROXY_HOST;
	}
	public static String getPROXY_PORT() {
		return PROXY_PORT;
	}
	public static String getPROXY_USER() {
		return PROXY_USER;
	}
	public static String getPROXY_PASSWORD() {
		return PROXY_PASSWORD;
	}
	public static void setPROXY_HOST(String pROXY_HOST) {
		PROXY_HOST = pROXY_HOST;
	}
	public static void setPROXY_PORT(String pROXY_PORT) {
		PROXY_PORT = pROXY_PORT;
	}
	public static void setPROXY_USER(String pROXY_USER) {
		PROXY_USER = pROXY_USER;
	}
	public static void setPROXY_PASSWORD(String pROXY_PASSWORD) {
		PROXY_PASSWORD = pROXY_PASSWORD;
	}
	
}
