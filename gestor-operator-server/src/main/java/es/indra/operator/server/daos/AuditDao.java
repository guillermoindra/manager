package es.indra.operator.server.daos;

import es.indra.operator.server.utils.Audit;

public interface AuditDao {
	
	/**
	 * Register auditory operations.
	 * 
	 * @param audit - Audit object with auditory information
	 * 
	 */
	void registerAudit(Audit audit);
}
