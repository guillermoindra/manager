package es.indra.operator.server.daos;

/**
 * Interface for Descriptors DAO.
 * 
 * @author acoboj
 */
public interface DescriptorsDao{

	/**
	 * Get descriptor information by id.
	 * 
	 * @param id - Id filter
	 * @param c - Class of returned object
	 * return object - Returned object
	 */
	Object getDescriptorsById(Integer id, Class<?> c);
	
	/**
	 * Get descriptor information by external code.
	 * 
	 * @param externalCode - External code filter
	 * @param c - Class of returned object
	 * return object - Returned object
	 */
	Object getDescriptorsByExternalCode(String externalCode, Class<?> c);

}
