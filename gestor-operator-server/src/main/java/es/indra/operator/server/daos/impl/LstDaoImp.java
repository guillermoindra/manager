package es.indra.operator.server.daos.impl;

import org.springframework.transaction.annotation.Transactional;

import es.indra.operator.server.daos.LstDao;


/**
 * This class is the implementation of the LstDao interface.
 * 
 * @author acoboj
 */
@Transactional
public class LstDaoImp extends BaseDaoImp<Object> implements LstDao{

}
