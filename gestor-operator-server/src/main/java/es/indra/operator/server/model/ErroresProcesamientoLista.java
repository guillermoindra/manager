package es.indra.operator.server.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;

import es.indra.operator.server.model.ErrorUsuario;

import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Contiene los totales de los registros procesados y la lista de errores en el procesamiento de una lista de usuario. Cuando el detalle de la lista no se pudo deserializar (json inválido), se genera un ErrorUsuario indicando esta situación con los siguientes datos:  placa &#x3D; \&quot;AAA000\&quot; tid &#x3D; \&quot;AAAAAAAAAAAAAAAAAAAAAAAA\&quot;
 */
@ApiModel(description = "Contiene los totales de los registros procesados y la lista de errores en el procesamiento de una lista de usuario. Cuando el detalle de la lista no se pudo deserializar (json inválido), se genera un ErrorUsuario indicando esta situación con los siguientes datos:  placa = \"AAA000\" tid = \"AAAAAAAAAAAAAAAAAAAAAAAA\"")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-25T10:51:09.761+02:00")

public class ErroresProcesamientoLista   {
  @JsonProperty("codigoLista")
  private String codigoLista = null;

  @JsonProperty("cantidadRegistros")
  private Integer cantidadRegistros = null;

  @JsonProperty("cantidadAprobados")
  private Integer cantidadAprobados = null;

  @JsonProperty("cantidadRechazados")
  private Integer cantidadRechazados = null;

  @JsonProperty("errores")
  @Valid
  private List<ErrorUsuario> errores = new ArrayList<>();

  public ErroresProcesamientoLista codigoLista(String codigoLista) {
    this.codigoLista = codigoLista;
    return this;
  }

  /**
   * Get codigoLista
   * @return codigoLista
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getCodigoLista() {
    return codigoLista;
  }

  public void setCodigoLista(String codigoLista) {
    this.codigoLista = codigoLista;
  }

  public ErroresProcesamientoLista cantidadRegistros(Integer cantidadRegistros) {
    this.cantidadRegistros = cantidadRegistros;
    return this;
  }

  /**
   * Cantidad de registros de la lista.
   * minimum: 0
   * @return cantidadRegistros
  **/
  @ApiModelProperty(required = true, value = "Cantidad de registros de la lista.")
  @NotNull

@Min(0)
  public Integer getCantidadRegistros() {
    return cantidadRegistros;
  }

  public void setCantidadRegistros(Integer cantidadRegistros) {
    this.cantidadRegistros = cantidadRegistros;
  }

  public ErroresProcesamientoLista cantidadAprobados(Integer cantidadAprobados) {
    this.cantidadAprobados = cantidadAprobados;
    return this;
  }

  /**
   * Cantidad de registros aprobados.
   * minimum: 0
   * @return cantidadAprobados
  **/
  @ApiModelProperty(required = true, value = "Cantidad de registros aprobados.")
  @NotNull

@Min(0)
  public Integer getCantidadAprobados() {
    return cantidadAprobados;
  }

  public void setCantidadAprobados(Integer cantidadAprobados) {
    this.cantidadAprobados = cantidadAprobados;
  }

  public ErroresProcesamientoLista cantidadRechazados(Integer cantidadRechazados) {
    this.cantidadRechazados = cantidadRechazados;
    return this;
  }

  /**
   * Cantidad de registros rechazados.
   * minimum: 0
   * @return cantidadRechazados
  **/
  @ApiModelProperty(required = true, value = "Cantidad de registros rechazados.")
  @NotNull

@Min(0)
  public Integer getCantidadRechazados() {
    return cantidadRechazados;
  }

  public void setCantidadRechazados(Integer cantidadRechazados) {
    this.cantidadRechazados = cantidadRechazados;
  }

  public ErroresProcesamientoLista errores(List<ErrorUsuario> errores) {
    this.errores = errores;
    return this;
  }

  public ErroresProcesamientoLista addErroresItem(ErrorUsuario erroresItem) {
    this.errores.add(erroresItem);
    return this;
  }

  /**
   * Lista con detalle de usuarios rechazados.
   * @return errores
  **/
  @ApiModelProperty(required = true, value = "Lista con detalle de usuarios rechazados.")
  @NotNull

  @Valid

  public List<ErrorUsuario> getErrores() {
    return errores;
  }

  public void setErrores(List<ErrorUsuario> errores) {
    this.errores = errores;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ErroresProcesamientoLista erroresProcesamientoLista = (ErroresProcesamientoLista) o;
    return Objects.equals(this.codigoLista, erroresProcesamientoLista.codigoLista) &&
        Objects.equals(this.cantidadRegistros, erroresProcesamientoLista.cantidadRegistros) &&
        Objects.equals(this.cantidadAprobados, erroresProcesamientoLista.cantidadAprobados) &&
        Objects.equals(this.cantidadRechazados, erroresProcesamientoLista.cantidadRechazados) &&
        Objects.equals(this.errores, erroresProcesamientoLista.errores);
  }

  @Override
  public int hashCode() {
    return Objects.hash(codigoLista, cantidadRegistros, cantidadAprobados, cantidadRechazados, errores);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ErroresProcesamientoLista {\n");
    
    sb.append("    codigoLista: ").append(toIndentedString(codigoLista)).append("\n");
    sb.append("    cantidadRegistros: ").append(toIndentedString(cantidadRegistros)).append("\n");
    sb.append("    cantidadAprobados: ").append(toIndentedString(cantidadAprobados)).append("\n");
    sb.append("    cantidadRechazados: ").append(toIndentedString(cantidadRechazados)).append("\n");
    sb.append("    errores: ").append(toIndentedString(errores)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

