package es.indra.operator.server.daos;

import java.io.Serializable;
import java.util.List;

import org.hibernate.jdbc.Work;



/**
 * Interface for Base DAO.
 * 
 * @author acoboj
 */
public interface BaseDao<CLS>{

	List<?> find(String stringQuery);

	List<?> find(String stringQuery, int maxResult);

	List<?> find(String stringQuery, List<Object> params);
	
	List<?> find(String stringQuery, List<Object> params, int maxResult);

	void update(Object object);

	Serializable save(Object object);

	public Serializable saveOrUpdate(Object o);

	Serializable merge(Object object);

	Serializable findByPK(Serializable pk, Class<?> entityClass);

	void deleteByPK(Class<?> entityClass, Serializable pk);

	void doWork(Work work);

	void doFlush();
	
	
}
