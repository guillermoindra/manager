/**
 * NOTE: This class is auto generated by the swagger code generator program (2.3.1).
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */
package es.indra.operator.server.api;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.indra.operator.server.model.ConfirmacionProcesamientoAjuste;
import es.indra.operator.server.model.ConfirmacionProcesamientoPaso;
import es.indra.operator.server.model.ListaUsuario;
import es.indra.operator.server.model.Saldo;
import es.indra.operator.server.model.Ticket;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-25T10:51:09.761+02:00")

@Api(value = "v1", description = "the v1 API")
public interface V1Api {
		
    Logger log = LoggerFactory.getLogger(V1Api.class);

    default Optional<ObjectMapper> getObjectMapper() {
        return Optional.empty();
    }

    default Optional<HttpServletRequest> getRequest() {
        return Optional.empty();
    }

    default Optional<String> getAcceptHeader() {
        return getRequest().map(r -> r.getHeader("Accept"));
    }

    @ApiOperation(value = "Confirmación de procesamiento de ajuste(s).", nickname = "v1ConfirmacionesAjustesPut", notes = "Este servicio es utilizado por el INT IP/REV para confirmar el procesamiento de transacción de ajuste(s).", authorizations = {
        @Authorization(value = "OpenID Connect")
    }, tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Recepción de la confirmación."),
        @ApiResponse(code = 404, message = "El código de ajuste no fue encontrado.") })
    @RequestMapping(value = "/v1/confirmacionesAjustes",
        produces = { "application/json" }, 
        method = RequestMethod.PUT)
    default ResponseEntity<Void> v1ConfirmacionesAjustesPut(@ApiParam(value = "" ,required=true )  @Valid @RequestBody List<ConfirmacionProcesamientoAjuste> body) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default V1Api interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


    @ApiOperation(value = "Confirmar procesamiento de paso.", nickname = "v1ConfirmacionesPasosPut", notes = "Este servicio es consumido por el Intermediador para confirmar un procesamiento de un paso de vehículo.", authorizations = {
        @Authorization(value = "OpenID Connect")
    }, tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Status 200") })
    @RequestMapping(value = "/v1/confirmacionesPasos",
        produces = { "application/json" }, 
        method = RequestMethod.PUT)
    default ResponseEntity<Void> v1ConfirmacionesPasosPut(@ApiParam(value = "" ,required=true )  @Valid @RequestBody List<ConfirmacionProcesamientoPaso> body) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default V1Api interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


    @ApiOperation(value = "Notificar nueva lista de usuarios.", nickname = "v1ListasUsuariosPut", notes = "Permite al intermediador notificar de la existencia de una nueva lista.", authorizations = {
        @Authorization(value = "OpenID Connect")
    }, tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Status 200") })
    @RequestMapping(value = "/v1/listasUsuarios",
        produces = { "application/json" }, 
        method = RequestMethod.PUT)
    default ResponseEntity<Void> v1ListasUsuariosPut(@ApiParam(value = "" ,required=true )  @Valid @RequestBody ListaUsuario body) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default V1Api interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


    @ApiOperation(value = "Solicitar prueba del paso de vehículo.", nickname = "v1PasosCodigoPasoSolicitarPruebasPut", notes = "Esta servicio es consumido por el intermediador para solicitar al operador las pruebas de un paso de vehículo.", response = Ticket.class, authorizations = {
        @Authorization(value = "OpenID Connect")
    }, tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Status 200", response = Ticket.class) })
    @RequestMapping(value = "/v1/pasos/{codigoPaso}/solicitarPruebas",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.PUT)
    default ResponseEntity<Ticket> v1PasosCodigoPasoSolicitarPruebasPut(@ApiParam(value = "",required=true) @PathVariable("codigoPaso") String codigoPaso) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
            if (getAcceptHeader().get().contains("application/json")) {
                try {
                    return new ResponseEntity<>(getObjectMapper().get().readValue("{  \"fecha\" : \"2000-01-23T04:56:07.000+00:00\",  \"codigo\" : \"10012_552223\",  \"referencia\" : \"referencia\"}", Ticket.class), HttpStatus.NOT_IMPLEMENTED);
                } catch (IOException e) {
                    log.error("Couldn't serialize response for content type application/json", e);
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default V1Api interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


    @ApiOperation(value = "Notificar el saldo de un usuario", nickname = "v1SaldosPut", notes = "Este servicio permite al INT IP/REV actualizar los saldos en los OP IP/REV de los usuarios con modalidad prepago simple o prepago con cargo recurrente. ", authorizations = {
        @Authorization(value = "OpenID Connect")
    }, tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Recepción de la confirmación.") })
    @RequestMapping(value = "/v1/saldos",
        produces = { "application/json" }, 
        method = RequestMethod.PUT)
    default ResponseEntity<Void> v1SaldosPut(@ApiParam(value = "" ,required=true )  @Valid @RequestBody List<Saldo> body) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default V1Api interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

}
