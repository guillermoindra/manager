package es.indra.operator.server.daos.impl;

import org.springframework.transaction.annotation.Transactional;

import es.indra.operator.server.daos.BalanceDao;



/**
 * This class is the implementation of the BalanceDao interface.
 * 
 * @author acoboj
 */
@Transactional
public class BalanceDaoImp extends BaseDaoImp<Object> implements BalanceDao{


}
