package es.indra.operator.server.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Define un identificador que servirá de mecanismo de seguimiento para una solicitud.
 */
@ApiModel(description = "Define un identificador que servirá de mecanismo de seguimiento para una solicitud.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-25T10:51:09.761+02:00")

public class Ticket   {
  @JsonProperty("codigo")
  private String codigo = null;

  @JsonProperty("fecha")
  private OffsetDateTime fecha = null;

  @JsonProperty("referencia")
  private String referencia = null;

  public Ticket codigo(String codigo) {
    this.codigo = codigo;
    return this;
  }

  /**
   * El código único del Ticket. Está conformado por OPERADOR/INTERMEDIADOR_CODIGOTICKET. Aquí el operador representa el código del operador/intermediario asignado por el Ministerio de Transporte y el código único de ticket relacionado con la transacción.
   * @return codigo
  **/
  @ApiModelProperty(example = "10012_552223", required = true, value = "El código único del Ticket. Está conformado por OPERADOR/INTERMEDIADOR_CODIGOTICKET. Aquí el operador representa el código del operador/intermediario asignado por el Ministerio de Transporte y el código único de ticket relacionado con la transacción.")
  @NotNull

@Size(min=10,max=50) 
  public String getCodigo() {
    return codigo;
  }

  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }

  public Ticket fecha(OffsetDateTime fecha) {
    this.fecha = fecha;
    return this;
  }

  /**
   * Fecha y hora de generación del ticket.
   * @return fecha
  **/
  @ApiModelProperty(required = true, value = "Fecha y hora de generación del ticket.")
  @NotNull

  @Valid

  public OffsetDateTime getFecha() {
    return fecha;
  }

  public void setFecha(OffsetDateTime fecha) {
    this.fecha = fecha;
  }

  public Ticket referencia(String referencia) {
    this.referencia = referencia;
    return this;
  }

  /**
   * Define la referencia a la solicitud. Para el caso de solicitudes de pruebas de paso éste poseerá el codigoPaso.
   * @return referencia
  **/
  @ApiModelProperty(required = true, value = "Define la referencia a la solicitud. Para el caso de solicitudes de pruebas de paso éste poseerá el codigoPaso.")
  @NotNull

@Size(max=100) 
  public String getReferencia() {
    return referencia;
  }

  public void setReferencia(String referencia) {
    this.referencia = referencia;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Ticket ticket = (Ticket) o;
    return Objects.equals(this.codigo, ticket.codigo) &&
        Objects.equals(this.fecha, ticket.fecha) &&
        Objects.equals(this.referencia, ticket.referencia);
  }

  @Override
  public int hashCode() {
    return Objects.hash(codigo, fecha, referencia);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Ticket {\n");
    
    sb.append("    codigo: ").append(toIndentedString(codigo)).append("\n");
    sb.append("    fecha: ").append(toIndentedString(fecha)).append("\n");
    sb.append("    referencia: ").append(toIndentedString(referencia)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

