package es.indra.operator.server.daos.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import es.indra.operator.server.daos.DescriptorsDao;
import es.indra.operator.server.utils.GenericUtils;

/**
 * This class is the implementation of the DescriptorsDao interface.
 * 
 * @author acoboj
 */
@Transactional
public class DescriptorsDaoImp extends BaseDaoImp<Object> implements DescriptorsDao{

	@Override
	public Object getDescriptorsById(Integer id, Class<?> c) {
		ArrayList<Object> params = new ArrayList<Object>();
		String hql = "FROM "+ c.getSimpleName() + " WHERE id = ?";
		params.add(id);
		List<Object> finded = GenericUtils.castList(c, super.find(hql, params));
		if(finded != null && !finded.isEmpty()){
			return finded.get(0);
		}
		return null;	
	}

	@Override
	public Object getDescriptorsByExternalCode(String externalCode, Class<?> c) {
		ArrayList<Object> params = new ArrayList<Object>();
		String hql = "FROM "+ c.getSimpleName() + " WHERE externalCode = ?";
		params.add(externalCode);
		List<Object> finded = GenericUtils.castList(c, super.find(hql, params));
		if(finded != null && !finded.isEmpty()){
			return finded.get(0);
		}
		return null;		
	}

	
}
