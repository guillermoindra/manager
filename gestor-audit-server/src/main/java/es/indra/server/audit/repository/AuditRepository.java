package es.indra.server.audit.repository;

import org.springframework.data.repository.CrudRepository;

import es.indra.server.audit.model.Audit;

public interface AuditRepository extends CrudRepository<Audit, Long> {

}
