package es.indra.server.audit.service;

import es.indra.server.audit.model.Audit;

public interface AuditService {
	public void saveAudit(Audit audit);
}
