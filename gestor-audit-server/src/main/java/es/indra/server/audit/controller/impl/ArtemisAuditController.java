package es.indra.server.audit.controller.impl;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import es.indra.common.model.AuditModel;
import es.indra.server.audit.model.Audit;
import es.indra.server.audit.service.AuditService;

@Component
public class ArtemisAuditController {
	
	AuditService sqlAuditService;
	AuditService fileAuditService;

	public ArtemisAuditController(AuditService fileAuditService, AuditService sqlAuditService) {
		this.sqlAuditService = sqlAuditService;
		this.fileAuditService = fileAuditService;
	}

	@JmsListener(destination = "${artemis.queue}")
	public void receiveMessage(AuditModel model) {
		System.out.println("Received(Artemis) <" + model.toString() + ">");

		Audit audit = parseAudit(model);

		fileAuditService.saveAudit(audit);
		sqlAuditService.saveAudit(audit);

	}
	
	/**
	 * Parse the mode to the Entity.
	 * @param model
	 * @return
	 */
	private Audit parseAudit(AuditModel model) {
		Audit audit = new Audit();
		audit.setUuid(model.getUuid());
		audit.setDateReception(model.getDateReception());
		audit.setDateSend(model.getDateSend());
		audit.setDuration(model.getDuration());
		audit.setWsInput(model.getWsInput());
		audit.setWsOutput(model.getWsOutput());
		audit.setHost(model.getHost());
		audit.setUrl(model.getUrl());
		audit.setOperation(model.getOperation());
		audit.setStatus(model.getStatus());
		return audit;
	}
}
