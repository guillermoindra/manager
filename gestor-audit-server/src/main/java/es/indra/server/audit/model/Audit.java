package es.indra.server.audit.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="audit")
public class Audit {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	private String uuid;
	private String url;
	private String fileInput;
	private String fileOutput;
	@Transient
	private String wsInput;
	@Transient
	private String wsOutput;
	private String operation;
	private Date dateReception;
	private Date dateSend;
	private long duration;
	private int status;
	private String host;
	
	/**
	 * Get the id.
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * Get the uuid.
	 * @return the uuid
	 */
	public String getUuid() {
		return uuid;
	}
	/**
	 * Get the url.
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * Get the fileInput.
	 * @return the fileInput
	 */
	public String getFileInput() {
		return fileInput;
	}
	/**
	 * Get the fileOutput.
	 * @return the fileOutput
	 */
	public String getFileOutput() {
		return fileOutput;
	}
	/**
	 * Get the wsInput.
	 * @return the wsInput
	 */
	public String getWsInput() {
		return wsInput;
	}
	/**
	 * Get the wsOutput.
	 * @return the wsOutput
	 */
	public String getWsOutput() {
		return wsOutput;
	}
	/**
	 * Get the operation.
	 * @return the operation
	 */
	public String getOperation() {
		return operation;
	}
	/**
	 * Get the dateReception.
	 * @return the dateReception
	 */
	public Date getDateReception() {
		return dateReception;
	}
	/**
	 * Get the dateSend.
	 * @return the dateSend
	 */
	public Date getDateSend() {
		return dateSend;
	}
	/**
	 * Get the duration.
	 * @return the duration
	 */
	public long getDuration() {
		return duration;
	}
	/**
	 * Get the status.
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}
	/**
	 * Get the host.
	 * @return the host
	 */
	public String getHost() {
		return host;
	}
	/**
	 * Set the id.
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * Set the uuid.
	 * @param uuid the uuid to set
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	/**
	 * Set the url.
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * Set the fileInput.
	 * @param fileInput the fileInput to set
	 */
	public void setFileInput(String fileInput) {
		this.fileInput = fileInput;
	}
	/**
	 * Set the fileOutput.
	 * @param fileOutput the fileOutput to set
	 */
	public void setFileOutput(String fileOutput) {
		this.fileOutput = fileOutput;
	}
	/**
	 * Set the wsInput.
	 * @param wsInput the wsInput to set
	 */
	public void setWsInput(String wsInput) {
		this.wsInput = wsInput;
	}
	/**
	 * Set the wsOutput.
	 * @param wsOutput the wsOutput to set
	 */
	public void setWsOutput(String wsOutput) {
		this.wsOutput = wsOutput;
	}
	/**
	 * Set the operation.
	 * @param operation the operation to set
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}
	/**
	 * Set the dateReception.
	 * @param dateReception the dateReception to set
	 */
	public void setDateReception(Date dateReception) {
		this.dateReception = dateReception;
	}
	/**
	 * Set the dateSend.
	 * @param dateSend the dateSend to set
	 */
	public void setDateSend(Date dateSend) {
		this.dateSend = dateSend;
	}
	/**
	 * Set the duration.
	 * @param duration the duration to set
	 */
	public void setDuration(long duration) {
		this.duration = duration;
	}
	/**
	 * Set the status.
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	/**
	 * Set the host.
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}
	
	
	
}
