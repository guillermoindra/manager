package es.indra.server.audit.service.impl;

import org.springframework.stereotype.Service;

import es.indra.server.audit.model.Audit;
import es.indra.server.audit.repository.AuditRepository;
import es.indra.server.audit.service.AuditService;

@Service
public class SqlAuditService implements AuditService{
	
	AuditRepository auditRepository;
	
	public SqlAuditService(AuditRepository auditRepository) {
		super();
		this.auditRepository = auditRepository;
	}

	public void saveAudit(Audit audit){
    	auditRepository.save(audit);
	}
}
