/**
 * NOTE: This class is auto generated by the swagger code generator program (2.3.1).
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */
package es.indra.server.via.api;

import es.indra.server.via.model.TransitsJson;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.*;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-29T16:46:35.310+02:00")

@Api(value = "via", description = "the via API")
public interface ViaApi {

    Logger log = LoggerFactory.getLogger(ViaApi.class);

    default Optional<ObjectMapper> getObjectMapper() {
        return Optional.empty();
    }

    default Optional<HttpServletRequest> getRequest() {
        return Optional.empty();
    }

    default Optional<String> getAcceptHeader() {
        return getRequest().map(r -> r.getHeader("Accept"));
    }

    @ApiOperation(value = "Registrar paso(s) de vehículo.", nickname = "viaTransitsPost", notes = "Este servicio es consumido por vía para informar a centro de control de un paso(s) de vehículo.", tags={ "via", })
    @ApiResponses(value = { 
        @ApiResponse(code = 100, message = "Error de datos de entrada."),
        @ApiResponse(code = 200, message = "Viaje enviado."),
        @ApiResponse(code = 300, message = "Error general.") })
    @RequestMapping(value = "/via/transits",
        consumes = { "application/json" },
        method = RequestMethod.POST)
    default ResponseEntity<Void> viaTransitsPost(@ApiParam(value = "Transitos a enviar." ,required=true )  @Valid @RequestBody TransitsJson body) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default ViaApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

}
