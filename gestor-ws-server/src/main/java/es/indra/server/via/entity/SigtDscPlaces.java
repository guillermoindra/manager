package es.indra.server.via.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SIGT_DSC_PLACES")
public class SigtDscPlaces {
	
	@Id
	private Long idPlace = null ;
	
	private String externalCode = null ;	
	
	
	public SigtDscPlaces() {
		super();
	}
	
	
	
	
	public SigtDscPlaces(Long idPlace) {
		super();
		this.idPlace = idPlace;
	}




	public Long getIdPlace() {
		return idPlace;
	}

	public void setIdPlace(Long idPlace) {
		this.idPlace = idPlace;
	}

	
	public String getExternalCode() {
		return externalCode;
	}

	public void setExternalCode(String externalCode) {
		this.externalCode = externalCode;
	}

}