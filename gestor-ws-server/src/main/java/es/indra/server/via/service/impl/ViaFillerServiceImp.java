package es.indra.server.via.service.impl;

import java.math.BigDecimal;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import es.indra.server.via.entity.SigtDscCodAgency;
import es.indra.server.via.entity.SigtDscPlaces;
import es.indra.server.via.entity.SigtDscVehicleClass;
import es.indra.server.via.entity.SigtOpTrxDscStatus;
import es.indra.server.via.entity.SigtOpTrxPend;
import es.indra.server.via.model.TransitJson;
import es.indra.server.via.model.TransitsJson;
import es.indra.server.via.service.DescriptorService;
import es.indra.server.via.service.SqlService;
import es.indra.server.via.service.ViaFillerService;

@Service
public class ViaFillerServiceImp implements ViaFillerService {
	
	Logger LOGGER = LoggerFactory.getLogger(ViaFillerServiceImp.class);
	
	@Autowired
    private SqlService sqlTransitService;  
    
    @Autowired
    private DescriptorService descriptorService ;

    @Autowired
	private Environment env;

	@Override
	public void fillViaFromJSON(TransitsJson body, HttpServletRequest request) {
		
		for (int i = 0; i < body.size(); i++) {
			TransitJson transitDto = body.get(i);
			SigtOpTrxPend transit = new SigtOpTrxPend();
			
			SigtDscCodAgency codAgency = (SigtDscCodAgency) descriptorService.getDescriptorsByCodAgency(Long.valueOf(transitDto.getIdIntermediador()), SigtDscCodAgency.class) ;
			transit.setCodAgency(codAgency);
			
			transit.setUuid(request.getAttribute("requestUUID").toString());
			
			transit.setTrxId(transitDto.getIdTransaction());
			transit.setPlate(transitDto.getPlate());
			transit.setEpc(transitDto.getEpc());
			transit.setTid(transitDto.getTid());
			
			SigtDscPlaces lane = (SigtDscPlaces) descriptorService.getDescriptorsByIdPlace(Long.parseLong(transitDto.getLane()), SigtDscPlaces.class) ;
			transit.setLaneId(lane);
			if (lane != null) transit.setLaneExternalId(lane.getExternalCode()); 
			
			lane = (SigtDscPlaces) descriptorService.getDescriptorsByIdPlace(Long.parseLong(transitDto.getStation()), SigtDscPlaces.class) ;
			transit.setStationId(lane);	
			
			SigtDscVehicleClass vehicleClass = (SigtDscVehicleClass) descriptorService.getDescriptorsByIdVehicle(Long.valueOf(transitDto.getDetectedClass()), SigtDscVehicleClass.class) ;
			transit.setDetectedClass(vehicleClass);
			
			vehicleClass = (SigtDscVehicleClass) descriptorService.getDescriptorsByIdVehicle(Long.valueOf(transitDto.getChargedClass()), SigtDscVehicleClass.class) ;
			transit.setChargedClass(vehicleClass) ;
			
			transit.setIdNotif(null);
			transit.setIdNotifSigt(null);			
			transit.setNumSend(0);
			
			SigtOpTrxDscStatus status = (SigtOpTrxDscStatus) descriptorService.getDescriptorsByIdStatus((long) 1, SigtOpTrxDscStatus.class) ;
			transit.setStatus(status);
			
			transit.setReadType(transitDto.getReadType());
			transit.setTrxDate(new Date(transitDto.getTrxDate().toInstant().toEpochMilli()));		
			transit.setClientId(transitDto.getIdClient());		
			transit.setAmount(new BigDecimal(transitDto.getAmount()));		
			transit.setNumAxles(transitDto.getNumAxles());
			transit.setNumDoubleWheel(transitDto.getNumDoubleWheel());
			transit.setDirection(transitDto.getDirection());
			transit.setPlateOcr(transitDto.getPlateOCR());
			transit.setIsDiscrepancy(transitDto.isIsDiscrepancy());		
			transit.setListDate(new Date(transitDto.getDateList().toInstant().toEpochMilli()));			
			transit.setIdList(transitDto.getIdList());
			transit.setIsPlateDiscrepancy(transitDto.isIsDiscrepancyPlate());
			
			transit.setCreationDate(new Date());			
			transit.setModificationUser(env.getRequiredProperty("server-via.modification-user"));
			transit.setApplication(Integer.valueOf(env.getRequiredProperty("server-via.application-id")).intValue());
			transit.setModificationDate(null);
			transit.setModification((byte) 1);
			transit.setOptimistLock((byte) 1);
			
			
			//if (i==2) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

			LOGGER.info("Save (Transits): " + transit.getTrxId());
			sqlTransitService.saveTransit(transit);
		}
			
	}		

}
