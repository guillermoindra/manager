package es.indra.server.via.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SIGT_OP_TRX_DSC_STATUS")
public class SigtOpTrxDscStatus {
	
	@Id
	private Long idStatus = null ;
	
	private String description = null ;	
	
	
	public SigtOpTrxDscStatus() {
		super();
	}
	
	
	
	public SigtOpTrxDscStatus(Long idStatus) {
		super();
		this.idStatus = idStatus;
	}



	public Long getIdStatus() {
		return idStatus;
	}

	public void setIdStatus(Long idStatus) {
		this.idStatus = idStatus;
	}

	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}