package es.indra.server.via.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import es.indra.server.via.repository.DescriptorRepository;
import org.springframework.stereotype.Repository;

@Repository
public class DescriptorRepositoryImpl implements DescriptorRepository {
	
	@PersistenceContext
    EntityManager entityManager;

	@Override
	public Object getDescriptorsByIdStatus(Long id, Class<?> c) {
		String sql = "select * FROM sigt_op_trx_dsc_status WHERE id_status = ?";
		
		Query query = entityManager.createNativeQuery(sql, c);
		query.setParameter(1, id) ;
		
        List<?> result = new ArrayList<>() ;
        result = query.getResultList() ;
        
        if(result != null && !result.isEmpty()){
			return result.get(0);
		}
        
		return null;
		
	}
	
	@Override
	public Object getDescriptorsByIdPlace(Long id, Class<?> c) {
		String sql = "select * FROM sigt_dsc_places WHERE id_place = ?";
		
		Query query = entityManager.createNativeQuery(sql, c);
		query.setParameter(1, id) ;
		
        List<?> result = new ArrayList<>() ;
        result = query.getResultList() ;
        
        if(result != null && !result.isEmpty()){
			return result.get(0);
		}
        
		return null;
		
	}
	
	@Override
	public Object getDescriptorsByIdVehicle(Long id, Class<?> c) {
		String sql = "select * FROM sigt_dsc_vehicle_class WHERE id_category = ?";
		
		Query query = entityManager.createNativeQuery(sql, c);
		query.setParameter(1, id) ;
		
        List<?> result = new ArrayList<>() ;
        result = query.getResultList() ;
        
        if(result != null && !result.isEmpty()){
			return result.get(0);
		}
        
		return null;
		
	}
	
	@Override
	public Object getDescriptorsByCodAgency(Long id, Class<?> c) {
		String sql = "select * FROM sigt_dsc_cod_agency WHERE id = ?";
		
		Query query = entityManager.createNativeQuery(sql, c);
		query.setParameter(1, id) ;
		
        List<?> result = new ArrayList<>() ;
        result = query.getResultList() ;
        
        if(result != null && !result.isEmpty()){
			return result.get(0);
		}
        
		return null;
		
	}

	@Override
	public Object getDescriptorsByExternalCode(String externalCode, Class<?> c) {
		String sql = "select * FROM "+ c.getSimpleName() + " WHERE id = ?";
		
		Query query = entityManager.createNativeQuery(sql, c);
		query.setParameter(1, externalCode) ;
		
        List<?> result = new ArrayList<>() ;
        result = query.getResultList() ;
        
        if(result != null && !result.isEmpty()){
			return result.get(0);
		}
        
		return null;
	}

}
