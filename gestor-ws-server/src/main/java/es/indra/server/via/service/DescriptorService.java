package es.indra.server.via.service;

public interface DescriptorService {	
	public Object getDescriptorsByExternalCode(String externalCode, Class<?> c);
	public Object getDescriptorsByIdVehicle(Long id, Class<?> c);
	public Object getDescriptorsByIdStatus(Long id, Class<?> c);
	public Object getDescriptorsByIdPlace(Long id, Class<?> c);
	public Object getDescriptorsByCodAgency(Long id, Class<?> c);
}
