package es.indra.server.via.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SIGT_DSC_VEHICLE_CLASS")
public class SigtDscVehicleClass {

	@Id
	private Long idCategory = null ;
	
	private String externalCode = null ;	
	
	
	public SigtDscVehicleClass() {
		super();
	}
	
	
	
	public SigtDscVehicleClass(Long idCategory) {
		super();
		this.idCategory = idCategory;
	}



	public Long getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(Long idCategory) {
		this.idCategory = idCategory;
	}

	
	public String getExternalCode() {
		return externalCode;
	}

	public void setExternalCode(String externalCode) {
		this.externalCode = externalCode;
	}

}
