package es.indra.server.via.repository;

import org.springframework.data.repository.CrudRepository;

import es.indra.server.via.entity.SigtOpTrxPend;

public interface TransitRepository extends CrudRepository<SigtOpTrxPend, Long> {

}
