package es.indra.server.via.api;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.indra.server.via.model.TransitsJson;
import es.indra.server.via.service.ViaFillerService;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-23T09:38:53.430+02:00")
@Controller
public class ViaApiController implements ViaApi {
	
	Logger LOGGER = LoggerFactory.getLogger(ViaApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;
            
    @Autowired
    private ViaFillerService viaFillerService; 
    
    @Autowired
    public ViaApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @Override
    public Optional<ObjectMapper> getObjectMapper() {
        return Optional.ofNullable(objectMapper);
    }

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.ofNullable(request);
    }

	@Override
	public ResponseEntity<Void> viaTransitsPost(@Valid @RequestBody TransitsJson body) {	
		LOGGER.info("Controller transitsPost (Num Transits): " + body.size());	
		
		try {    
			viaFillerService.fillViaFromJSON(body, request);
		} catch (Exception e) {			
			LOGGER.error("viaTransitsPost: General error occurred during execution!. " + e.getMessage());
			LOGGER.error(GenericUtils.stackTraceStr(e));
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
