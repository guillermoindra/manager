package es.indra.server.via.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="SIGT_OP_TRX_PEND")
public class SigtOpTrxPend {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long identifier;

	private Long trxId = null;
	
	@ManyToOne
	@JoinColumn(name="codAgency", referencedColumnName="id")
	private SigtDscCodAgency codAgency = null;
	
	private String uuid = null;
	
	private String plate = null;

	private String tid = null;

	private String epc = null;
		
	@ManyToOne
	@JoinColumn(name="laneId", referencedColumnName="idPlace")
	private SigtDscPlaces laneId = null;
	
	private String laneExternalId = null ; 
	
	@ManyToOne
	@JoinColumn(name="stationId", referencedColumnName="idPlace") 
	private SigtDscPlaces stationId = null ;
	
	@ManyToOne
	@JoinColumn(name="detectedClass", referencedColumnName="idCategory")
	private SigtDscVehicleClass detectedClass = null ;
	
	@ManyToOne
	@JoinColumn(name="chargedClass", referencedColumnName="idCategory")
	private SigtDscVehicleClass chargedClass = null ; 
	
	@Column(nullable=true)
	private Integer idNotif = null ;
	
	@Column(nullable=true)
	private Integer idNotifSigt = null ;
	
	private Integer numSend = null ;

	@ManyToOne
	@JoinColumn(name="status", referencedColumnName="idStatus")
	private SigtOpTrxDscStatus status = null ;
		
	private Integer readType = null ;
	
	private Date trxDate = null ; 
		
	private String  clientId = null ;
	
	private BigDecimal amount = null ;
	
	private Integer numAxles = null ;
	
	private Integer numDoubleWheel = null ; 
	
	private String direction  = null ; 
	
	private String plateOcr = null ;
	
	private Boolean isDiscrepancy = null ;
	
	private Date listDate = null ;
	
	private String idList = null ;
	
	private Boolean isPlateDiscrepancy = null ;
	
	private Date creationDate = null ;
	
	private String modificationUser = null ;
	
	@Column(nullable=true)
	private Integer application = null ;
	
	@Column(nullable=true)
	private Date modificationDate = null ;
	
	private Byte modification = null ;
	
	private Byte optimistLock = null ; 
		
	
	public SigtOpTrxPend() {
		super();
	}


	public Long getIdentifier() {
		return identifier;
	}


	public void setIdentifier(Long identifier) {
		this.identifier = identifier;
	}


	public Long getTrxId() {
		return trxId;
	}


	public void setTrxId(Long trxId) {
		this.trxId = trxId;
	}


	public SigtDscCodAgency getCodAgency() {
		return codAgency;
	}


	public void setCodAgency(SigtDscCodAgency codAgency) {
		this.codAgency = codAgency;
	}


	public String getPlate() {
		return plate;
	}


	public void setPlate(String plate) {
		this.plate = plate;
	}


	public String getTid() {
		return tid;
	}


	public void setTid(String tid) {
		this.tid = tid;
	}


	public String getEpc() {
		return epc;
	}


	public void setEpc(String epc) {
		this.epc = epc;
	}


	public SigtDscPlaces getLaneId() {
		return laneId;
	}


	public void setLaneId(SigtDscPlaces laneId) {
		this.laneId = laneId;
	}


	public String getLaneExternalId() {
		return laneExternalId;
	}


	public void setLaneExternalId(String laneExternalId) {
		this.laneExternalId = laneExternalId;
	}


	public SigtDscPlaces getStationId() {
		return stationId;
	}


	public void setStationId(SigtDscPlaces stationId) {
		this.stationId = stationId;
	}


	public SigtDscVehicleClass getDetectedClass() {
		return detectedClass;
	}


	public void setDetectedClass(SigtDscVehicleClass detectedClass) {
		this.detectedClass = detectedClass;
	}


	public SigtDscVehicleClass getChargedClass() {
		return chargedClass;
	}


	public void setChargedClass(SigtDscVehicleClass chargedClass) {
		this.chargedClass = chargedClass;
	}


	public Integer getIdNotif() {
		return idNotif;
	}


	public void setIdNotif(Integer idNotif) {
		this.idNotif = idNotif;
	}


	public Integer getIdNotifSigt() {
		return idNotifSigt;
	}


	public void setIdNotifSigt(Integer idNotifSigt) {
		this.idNotifSigt = idNotifSigt;
	}


	public Integer getNumSend() {
		return numSend;
	}


	public void setNumSend(Integer numSend) {
		this.numSend = numSend;
	}


	public SigtOpTrxDscStatus getStatus() {
		return status;
	}


	public void setStatus(SigtOpTrxDscStatus status) {
		this.status = status;
	}


	public Integer getReadType() {
		return readType;
	}


	public void setReadType(Integer readType) {
		this.readType = readType;
	}


	public Date getTrxDate() {
		return trxDate;
	}


	public void setTrxDate(Date trxDate) {
		this.trxDate = trxDate;
	}


	public String getClientId() {
		return clientId;
	}


	public void setClientId(String clientId) {
		this.clientId = clientId;
	}


	public BigDecimal getAmount() {
		return amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public Integer getNumAxles() {
		return numAxles;
	}


	public void setNumAxles(Integer numAxles) {
		this.numAxles = numAxles;
	}


	public Integer getNumDoubleWheel() {
		return numDoubleWheel;
	}


	public void setNumDoubleWheel(Integer numDoubleWheel) {
		this.numDoubleWheel = numDoubleWheel;
	}


	public String getDirection() {
		return direction;
	}


	public void setDirection(String direction) {
		this.direction = direction;
	}


	public String getPlateOcr() {
		return plateOcr;
	}


	public void setPlateOcr(String plateOcr) {
		this.plateOcr = plateOcr;
	}


	public Boolean getIsDiscrepancy() {
		return isDiscrepancy;
	}


	public void setIsDiscrepancy(Boolean isDiscrepancy) {
		this.isDiscrepancy = isDiscrepancy;
	}


	public Date getListDate() {
		return listDate;
	}


	public void setListDate(Date listDate) {
		this.listDate = listDate;
	}


	public String getIdList() {
		return idList;
	}


	public void setIdList(String idList) {
		this.idList = idList;
	}


	public Boolean getIsPlateDiscrepancy() {
		return isPlateDiscrepancy;
	}


	public void setIsPlateDiscrepancy(Boolean isPlateDiscrepancy) {
		this.isPlateDiscrepancy = isPlateDiscrepancy;
	}


	public Date getCreationDate() {
		return creationDate;
	}


	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}


	public String getModificationUser() {
		return modificationUser;
	}


	public void setModificationUser(String modificationUser) {
		this.modificationUser = modificationUser;
	}


	public Integer getApplication() {
		return application;
	}


	public void setApplication(Integer application) {
		this.application = application;
	}


	public Date getModificationDate() {
		return modificationDate;
	}


	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}


	public Byte getModification() {
		return modification;
	}


	public void setModification(Byte modification) {
		this.modification = modification;
	}


	public Byte getOptimistLock() {
		return optimistLock;
	}


	public void setOptimistLock(Byte optimistLock) {
		this.optimistLock = optimistLock;
	}


	public String getUuid() {
		return uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	
}
