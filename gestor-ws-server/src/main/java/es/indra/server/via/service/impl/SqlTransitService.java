package es.indra.server.via.service.impl;

import org.springframework.stereotype.Service;

import es.indra.server.via.entity.SigtOpTrxPend;
import es.indra.server.via.repository.TransitRepository;
import es.indra.server.via.service.SqlService;


@Service
public class SqlTransitService implements SqlService {
	
	TransitRepository transitRepository;
	
	public SqlTransitService(TransitRepository transitRepository) {
		super();
		this.transitRepository = transitRepository;
	}
	
	@Override
	public void saveTransit(SigtOpTrxPend transit) {
		transitRepository.save(transit);
	}
}
