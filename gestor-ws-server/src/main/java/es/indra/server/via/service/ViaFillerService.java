package es.indra.server.via.service;

import javax.servlet.http.HttpServletRequest;

import es.indra.server.via.model.TransitsJson;

public interface ViaFillerService {
	public void fillViaFromJSON(TransitsJson body, HttpServletRequest request) ;
}
