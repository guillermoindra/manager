package es.indra.server.via.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import es.indra.server.via.repository.DescriptorRepository;
import es.indra.server.via.service.DescriptorService;
import org.springframework.stereotype.Service;

@Service
public class DescriptorServiceImp implements DescriptorService {

	@Autowired
	private DescriptorRepository descriptorRepository;
	
	@Override
	public Object getDescriptorsByCodAgency(Long id, Class<?> c) {
		return descriptorRepository.getDescriptorsByCodAgency(id, c);
	}
	
	@Override
	public Object getDescriptorsByIdPlace(Long id, Class<?> c) {
		return descriptorRepository.getDescriptorsByIdPlace(id, c);
	}
	
	@Override
	public Object getDescriptorsByIdStatus(Long id, Class<?> c) {
		return descriptorRepository.getDescriptorsByIdStatus(id, c) ;
	}
	
	@Override
	public Object getDescriptorsByIdVehicle(Long id, Class<?> c) {
		return descriptorRepository.getDescriptorsByIdVehicle(id, c) ;
	}

	@Override
	public Object getDescriptorsByExternalCode(String externalCode, Class<?> c) {
		return descriptorRepository.getDescriptorsByExternalCode(externalCode, c);
	}

}
