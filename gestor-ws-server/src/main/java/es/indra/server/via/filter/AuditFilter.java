package es.indra.server.via.filter;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import es.indra.common.controller.AuditApi;
import es.indra.common.model.AuditModel;

@Component
@Order(1)
public class AuditFilter implements Filter {

	@Value("#{'${audit-paths}'.split(',')}")
	private List<String> paths;

	AuditApi auditApi;

	public AuditFilter(AuditApi auditApi) {
		super();
		this.auditApi = auditApi;
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		AuditModel auditModel = new AuditModel();

		ContentCachingRequestWrapper requestWrapper = new ContentCachingRequestWrapper((HttpServletRequest) request);
		ContentCachingResponseWrapper responseWrapper = new ContentCachingResponseWrapper((HttpServletResponse) response);

		try {
			chain.doFilter(requestWrapper, responseWrapper);
		} finally {

			String requestBody = new String(requestWrapper.getContentAsByteArray());
			String responseBody = new String(responseWrapper.getContentAsByteArray());
			String[] firstPath = ((HttpServletRequest) request).getRequestURI().toString().split("/");
			if (firstPath.length > 0 && paths.contains(firstPath[1])) {
				saveAudit(auditModel, requestWrapper, responseWrapper, requestBody, responseBody);
			}
			responseWrapper.copyBodyToResponse();
		}

	}

	private void saveAudit(AuditModel auditModel, ServletRequest request, ServletResponse response, String requestBody,
			String responseBody) {

		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;

		auditModel.setUuid(request.getAttribute("requestUUID").toString()/*UUID.randomUUID().toString()*/);
		auditModel.setHost(request.getServerName());
		auditModel.setUrl(httpServletRequest.getRequestURL().toString());
		auditModel.setOperation(httpServletRequest.getMethod());

		auditModel.setWsInput(requestBody);
		auditModel.setWsOutput(responseBody);

		auditModel.setDateSend(new Date());
		auditModel.setDuration(auditModel.getDateSend().getTime()-auditModel.getDateReception().getTime());
		auditModel.setStatus(httpServletResponse.getStatus());

		auditApi.sendAudit(auditModel);
	}

}