package es.indra.server.via.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SIGT_DSC_COD_AGENCY")
public class SigtDscCodAgency {
	
	@Id
	private Integer id = null ;
	
	private String description = null ;
	
	private String wsUrl = null ;
	
	private String externalCode = null ;
	
	
	public SigtDscCodAgency() {
		super();
	}
	
	
	
	
	public SigtDscCodAgency(Integer id) {
		super();
		this.id = id;
	}




	public Integer getId() {
		return id;
	}

	public void setIdentifier(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/*public String getWsUrl() {
		return wsUrl;
	}

	public void setWsUrl(String wsUrl) {
		this.wsUrl = wsUrl;
	}*/

	public String getExternalCode() {
		return externalCode;
	}

	public void setExternalCode(String externalCode) {
		this.externalCode = externalCode;
	}




	public String getWsUrl() {
		return wsUrl;
	}




	public void setWsUrl(String wsUrl) {
		this.wsUrl = wsUrl;
	}

}
